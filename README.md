# SolsticePoints
Solstice Points is a forked and highly customized version of [LuaDKP](https://github.com/int3code/LuaDKP)

## Installing SolsticePoints
- Download `LuaDKP-Core`
- Download `LuaDKP-Data`
- Copy/paste these folders into your `interface/addons` folder

## Slash Commands
  - `/DKP` *(this will dispaly all current commands)*

## Running Loot
- **Verify Install**
  - 3 buttons should be present on bottom right corner of screen
    - Track - used to start/stop raid tracking
    - Master - displays all point values
    - Raid - displays points, loot, and boss kills for current raid

- **Start Tracking**
  - Ensure Group is aligned to **Solstice**
  - Name should be **date + raid description**

- **Assign raid points**
  - Manual point distribution:
    - Click `Raid` button -> Click `+10 Points` to provide all raid members with +10 points
    - `/DKP give all *number_of_points_to_give*`


  - Boss encounter pulls will automatically assign 1 raid point.  A console message confirms this.
  - Boss encounter kills will automatically assign 2 raid points.  A console message confirms this.

- **To start rolling loot with MACRO**
  - Bind this macro to set a 15 second timer <br>
  `/run DEFAULT_CHAT_FRAME.editBox:SetText("/dkp start15 " .. select(2, GameTooltip:GetItem()));ChatEdit_SendText(DEFAULT_CHAT_FRAME.editBox, 0)`
  - Bind this macro to set a 20 second timer <br>
  `/run DEFAULT_CHAT_FRAME.editBox:SetText("/dkp start20 " .. select(2, GameTooltip:GetItem()));ChatEdit_SendText(DEFAULT_CHAT_FRAME.editBox, 0)`

  - To initiate loot timer, hover over the loot and hit the keybind aligned to the above macro
  - Bidding will automatically stop when timer expires
  - The Window will automatically sort the appropriate order
  - Assign loot
    - Click the number aligned to the chosen winner (*almost always #1 unless a last minute withdrawl occurs*)
    - If points were spent, then points will be distributed automatically

- **To start rolling loot Manually**
  - `/DKP start *link item*` (new loot window should pop up)
  - When bidding is complete, hit `Stop`
  - The Window will automatically sort the appropriate order
  - Assign loot
    - Click the number aligned to the chosen winner (*almost always #1 unless a last minute withdrawl occurs*)
    - If points were spent, then points will be distributed automatically

- **Notes on non-standard loot scenarios**
  - If NO bids are made, then Stop button will cancel the loot roll automatically
  - If loot was linked in error and needs to be cancelled, then `/DKP cancel`
  - If the pop-up loot window is closed early, then `/DKP assign #`

- **If loot is exchanged after an assignment is made**
  - Click `Raid` button
  - Find loot entry that was exchanged
  - Click `edit`
  - Click # aligned to new recipient (note points will be updated if points were spent. However, if the original bidder was OVER the cap and spent more than the item's cost then those will not be included)
