local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Badreams",
    [ 4] = "Capecrusader",
    [ 5] = "Dagarle",
    [ 6] = "Elorn",
    [ 7] = "Estrellita",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Heidie",
    [11] = "Ironflurry",
    [12] = "Irontitanhc",
    [13] = "Kharlamagne",
    [14] = "Lachy",
    [15] = "Luethien",
    [16] = "Pencilvestor",
    [17] = "Skeeta",
    [18] = "Sneakydoodle",
    [19] = "Superboots",
    [20] = "Treeclaw",
    [21] = "Varv",
    [22] = "Velladonna",
    [23] = "Whitewidow",
    [24] = "Wootzz",
  },
  kills = {
    {boss = 731, timestamp = "2021-10-27 20:11", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24}},
  },
  description = "2021-10-27_TK",
  drops = {
    {player =  1, timestamp = "2021-10-27 20:15", item = 30248, cost = "80", costType = "Solstice Points"}, -- Ashgon : Pauldrons of the Vanquished Champion
    {player =  2, timestamp = "2021-10-27 20:15", item = 30248, cost = "80", costType = "Solstice Points"}, -- Aziza : Pauldrons of the Vanquished Champion
    {player = 11, timestamp = "2021-10-27 20:16", item = 30450, cost = "70", costType = "Solstice Points"}, -- Ironflurry : Warp-Spring Coil
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
