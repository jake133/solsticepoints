local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Annarinn",
    [ 2] = "Aythya",
    [ 3] = "Capecrusader",
    [ 4] = "Chamwow",
    [ 5] = "Classicx",
    [ 6] = "Dwinchester",
    [ 7] = "Eillowee",
    [ 8] = "Elesh",
    [ 9] = "Eviscerated",
    [10] = "Furiozah",
    [11] = "Gaylestrum",
    [12] = "Irontitann",
    [13] = "Jadbin",
    [14] = "Jaeran",
    [15] = "Jazzmene",
    [16] = "Khaeltiax",
    [17] = "Lachý",
    [18] = "Mayaell",
    [19] = "Pinn",
    [20] = "Sanif",
    [21] = "Slayingfreak",
    [22] = "Snoweyes",
    [23] = "Tinyluna",
    [24] = "Varvo",
    [25] = "Xandies",
  },
  kills = {
    {boss = 602, timestamp = "2022-08-31 18:03", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 603, timestamp = "2022-08-31 18:12", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 607, timestamp = "2022-08-31 18:31", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 608, timestamp = "2022-08-31 18:45", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 609, timestamp = "2022-08-31 19:01", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-08-31BT",
  drops = {
    {player = 23, timestamp = "2022-08-31 17:59", item = 32234, costType = "OS"}, -- Tinyluna : Fists of Mukoa
    {player = 19, timestamp = "2022-08-31 18:13", item = 32240, costType = "MS"}, -- Pinn : Guise of the Tidal Lurker
    {player = 16, timestamp = "2022-08-31 18:16", item = 32250, costType = "OS"}, -- Khaeltiax : Pauldrons of Abyssal Fury
    {player = 13, timestamp = "2022-08-31 18:18", item = 32261, costType = "MS"}, -- Jadbin : Band of the Abyssal Lord
    {player = 16, timestamp = "2022-08-31 18:23", item = 32263, costType = "OS"}, -- Khaeltiax : Praetorian's Legguards
    {player = 19, timestamp = "2022-08-31 18:25", item = 34012, costType = "MS"}, -- Pinn : Shroud of the Final Stand
    {player =  9, timestamp = "2022-08-31 18:33", item = 31102, costType = "MS"}, -- Eviscerated : Pauldrons of the Forgotten Vanquisher
    {player = 24, timestamp = "2022-08-31 18:34", item = 31101, costType = "MS"}, -- Varvo : Pauldrons of the Forgotten Conqueror
    {player =  8, timestamp = "2022-08-31 18:35", item = 31101, costType = "MS"}, -- Elesh : Pauldrons of the Forgotten Conqueror
    {player =  1, timestamp = "2022-08-31 18:46", item = 32505, cost = 70, costType = "Solstice Points"}, -- Annarinn : Madness of the Betrayer
    {player = 10, timestamp = "2022-08-31 18:49", item = 31100, costType = "MS"}, -- Furiozah : Leggings of the Forgotten Protector
    {player =  6, timestamp = "2022-08-31 18:50", item = 31100, costType = "MS"}, -- Dwinchester : Leggings of the Forgotten Protector
    {player = 16, timestamp = "2022-08-31 18:51", item = 31098, costType = "MS"}, -- Khaeltiax : Leggings of the Forgotten Conqueror
    {player = 18, timestamp = "2022-08-31 19:02", item = 32525, costType = "MS"}, -- Mayaell : Cowl of the Illidari High Lord
    {player =  9, timestamp = "2022-08-31 19:04", item = 31090, costType = "MS"}, -- Eviscerated : Chestguard of the Forgotten Vanquisher
    {player = 24, timestamp = "2022-08-31 19:06", item = 31089, costType = "MS"}, -- Varvo : Chestguard of the Forgotten Conqueror
    {player =  8, timestamp = "2022-08-31 19:08", item = 31089, costType = "MS"}, -- Elesh : Chestguard of the Forgotten Conqueror
    {player =  2, timestamp = "2022-08-31 19:10", item = 32235, cost = 60, costType = "Solstice Points"}, -- Aythya : Cursed Vision of Sargeras
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
