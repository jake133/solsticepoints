local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Capecrusader",
    [ 4] = "Cholula",
    [ 5] = "Eillowee",
    [ 6] = "Elorn",
    [ 7] = "Frodes",
    [ 8] = "Gaylestrum",
    [ 9] = "Ironankh",
    [10] = "Irontitanhc",
    [11] = "Jazzmean",
    [12] = "Lachy",
    [13] = "Luethien",
    [14] = "Pencilvestor",
    [15] = "Pulchra",
    [16] = "Roragorn",
    [17] = "Sadistia",
    [18] = "Sinniaa",
    [19] = "Slayingfreak",
    [20] = "Sneakydoodle",
    [21] = "Superboots",
    [22] = "Varv",
    [23] = "Velladonna",
    [24] = "Whitewidow",
    [25] = "Wootzz",
  },
  kills = {
    {boss = 601, timestamp = "2022-02-09 19:18", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 602, timestamp = "2022-02-09 19:42", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 603, timestamp = "2022-02-09 20:16", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 604, timestamp = "2022-02-09 21:08", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-02-09_BT",
  drops = {
    {player = 16, timestamp = "2022-02-09 19:20", item = 32238, cost = "", costType = "OS"}, -- Roragorn : Ring of Calming Waves
    {player =  1, timestamp = "2022-02-09 19:20", item = 32243, cost = "", costType = "OS"}, -- Ashgon : Pearl Inlaid Boots
    {player = 19, timestamp = "2022-02-09 19:44", item = 32262, cost = 70, costType = "Solstice Points"}, -- Slayingfreak : Syphon of the Nathrezim
    {player = 22, timestamp = "2022-02-09 19:44", item = 32254, cost = "", costType = "MS"}, -- Varv : The Brutalizer
    {player = 12, timestamp = "2022-02-09 20:17", item = 32270, cost = 60, costType = "Solstice Points"}, -- Lachy : Focused Mana Bindings
    {player = 11, timestamp = "2022-02-09 20:18", item = 32513, cost = "", costType = "MS"}, -- Jazzmean : Wristbands of Divine Influence
    {player = 13, timestamp = "2022-02-09 21:11", item = 32329, cost = 60, costType = "Solstice Points"}, -- Luethien : Cowl of Benevolence
    {player =  3, timestamp = "2022-02-09 21:11", item = 32323, cost = 60, costType = "Solstice Points"}, -- Capecrusader : Shadowmoon Destroyer's Drape
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
