local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
	[ 1] = "Ashgon",
	[ 2] = "Aziza",
	[ 3] = "Brickk",
	[ 4] = "Capecrusader",
	[ 5] = "Ceruwolfe",
	[ 6] = "Cholula",
	[ 7] = "Elorn",
	[ 8] = "Frodes",
	[ 9] = "Gaylestrum",
	[10] = "Halestormz",
	[11] = "Heidie",
	[12] = "Irontitanhc",
	[13] = "Jazzmean",
	[14] = "Kyrika",
	[15] = "Lachy",
	[16] = "Retrav",
	[17] = "Roragorn",
	[18] = "Sadistia",
	[19] = "Sinwave",
	[20] = "Slayingfreak",
	[21] = "Sneakydoodle",
	[22] = "Varv",
	[23] = "Velladonna",
	[24] = "Whitewidow",
	[25] = "Wootzz",

  },
  kills = {
    {boss = 623, timestamp = "2021-12-21", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 624, timestamp = "2021-12-21", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 625, timestamp = "2021-12-21", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 626, timestamp = "2021-12-21", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 627, timestamp = "2021-12-21", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2021-12-21_SSC",
  drops = {
    {player =  9, timestamp = "2021-12-21", item = 30049, cost = "", costType = "MS"}, -- Gayle : Fathomstone
    {player = 3, timestamp = "2021-12-21", item = 30053, cost = "", costType = "MS"}, -- Brickk : pauldrons of the Wardancer
    {player = 5, timestamp = "2021-12-21", item = 30063, cost = "", costType = "OS"}, -- Ceruwolfe : Libram of absolute truth
    {player = 20, timestamp = "2021-12-21", item = 30057, cost = "", costType = "MS"}, -- Slayingfreak : Bracers of eradication
    {player = 14, timestamp = "2021-12-21", item = 30241, cost = "", costType = "MS"}, -- Kyrika : Gloves of teh vanquished hero
    {player = 19, timestamp = "2021-12-21", item = 30239, cost = "", costType = "MS"}, -- sinwave : gloves of the vanquished champion
    {player = 10, timestamp = "2021-12-21", item = 30091, cost = "", costType = "MS"}, -- Halestormz : True-Aim Stalker Bands
    {player = 2, timestamp = "2021-12-21", item = 30245, cost = "80", costType = "Solstice Points"}, -- Aziza : Leggings of the vanquished Champion
    {player = 22, timestamp = "2021-12-21", item = 30099, cost = "", costType = "MS"}, -- Varv : Frayed tether of the drowned
    {player = 12, timestamp = "2021-12-21", item = 30085, cost = "", costType = "OS"}, -- Irontitanhc : mattle of the tireless tracker
    {player = 17, timestamp = "2021-12-21", item = 33058, cost = "", costType = "OS"}, -- Roragorn : band of the vigilant
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
