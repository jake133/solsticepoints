local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Arquinas",
    [ 3] = "Aythya",
    [ 4] = "Capecrusader",
    [ 5] = "Chalula",
    [ 6] = "Eillowee",
    [ 7] = "Elorn",
    [ 8] = "Furiozah",
    [ 9] = "Glancen",
    [10] = "Grayskyy",
    [11] = "Inxi",
    [12] = "Irontitann",
    [13] = "Lachý",
    [14] = "Mclitty",
    [15] = "Melisea",
    [16] = "Olando",
    [17] = "Pencilvestor",
    [18] = "Roragorn",
    [19] = "Skeeta",
    [20] = "Snoweyes",
    [21] = "Snozzberry",
    [22] = "Tamtamgirl",
    [23] = "Tinycurse",
    [24] = "Wootzz",
    [25] = "Xandies",
  },
  kills = {
    {boss = 618, timestamp = "2022-05-09 19:18", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 619, timestamp = "2022-05-09 19:32", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 620, timestamp = "2022-05-09 19:58", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 621, timestamp = "2022-05-09 20:25", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 622, timestamp = "2022-05-09 21:05", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-05-09_Hyjal",
  drops = {
    {player = 19, timestamp = "2022-05-09 19:34", item = 30864, costType = "MS"}, -- Skeeta : Bracers of the Pathfinder
    {player =  8, timestamp = "2022-05-09 19:35", item = 32946, costType = "MS"}, -- Furiozah : Claw of Molten Fury
    {player =  3, timestamp = "2022-05-09 19:35", item = 32591, costType = "MS"}, -- Aythya : Choker of Serrated Blades
    {player = 17, timestamp = "2022-05-09 19:36", item = 30881, costType = "MS"}, -- Pencilvestor : Blade of Infamy
    {player = 15, timestamp = "2022-05-09 19:36", item = 30885, costType = "MS"}, -- Melisea : Archbishop's Slippers
    {player =  2, timestamp = "2022-05-09 20:27", item = 30889, costType = "MS"}, -- Arquinas : Kaz'rogal's Hardened Heart
    {player = 23, timestamp = "2022-05-09 20:29", item = 30916, cost = 60, costType = "Solstice Points"}, -- Tinycurse : Leggings of Channeled Elements
    {player =  3, timestamp = "2022-05-09 20:54", item = 30901, costType = "MS"}, -- Aythya : Boundless Agony
    {player = 23, timestamp = "2022-05-09 20:55", item = 31092, costType = "MS"}, -- Tinycurse : Gloves of the Forgotten Conqueror
    {player = 16, timestamp = "2022-05-09 20:55", item = 31093, costType = "MS"}, -- Olando : Gloves of the Forgotten Vanquisher
    {player = 11, timestamp = "2022-05-09 21:06", item = 30905, costType = "MS"}, -- Inxi : Midnight Chestguard
    {player = 22, timestamp = "2022-05-09 21:07", item = 30912, costType = "MS"}, -- Tamtamgirl : Leggings of Eternity
    {player = 12, timestamp = "2022-05-09 21:08", item = 31097, cost = 80, costType = "Solstice Points"}, -- Irontitann : Helm of the Forgotten Conqueror
    {player = 19, timestamp = "2022-05-09 21:09", item = 31095, cost = 80, costType = "Solstice Points"}, -- Skeeta : Helm of the Forgotten Protector
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
