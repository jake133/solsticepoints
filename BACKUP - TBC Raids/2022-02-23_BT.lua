local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aythya",
    [ 3] = "Aziza",
    [ 4] = "Capecrusader",
    [ 5] = "Cholula",
    [ 6] = "Eillowee",
    [ 7] = "Elorn",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Ironankh",
    [11] = "Irontitanhc",
    [12] = "Jazzmean",
    [13] = "Kharlamagne",
    [14] = "Kyrika",
    [15] = "Lachy",
    [16] = "Pencilvestor",
    [17] = "Roragorn",
    [18] = "Sadistia",
    [19] = "Sinniaa",
    [20] = "Slayingfreak",
    [21] = "Sneakydoodle",
    [22] = "Superboots",
    [23] = "Varv",
    [24] = "Whitewidow",
    [25] = "Wootzz",
  },
  kills = {
    {boss = 601, timestamp = "2022-02-23 19:29", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 602, timestamp = "2022-02-23 19:51", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 603, timestamp = "2022-02-23 20:18", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 604, timestamp = "2022-02-23 21:00", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-02-23_BT",
  drops = {
    {player = 13, timestamp = "2022-02-23 19:31", item = 32241, cost = "", costType = "MS"}, -- Kharlamagne : Helm of Soothing Currents
    {player = 10, timestamp = "2022-02-23 19:31", item = 32236, cost = "", costType = "MS"}, -- Ironankh : Rising Tide
    {player =  1, timestamp = "2022-02-23 19:52", item = 32261, cost = "", costType = "MS"}, -- Ashgon : Band of the Abyssal Lord
    {player = 13, timestamp = "2022-02-23 19:52", item = 32255, cost = "", costType = "MS"}, -- Kharlamagne : Felstone Bulwark
    {player = 17, timestamp = "2022-02-23 20:19", item = 32513, cost = "", costType = "OS"}, -- Roragorn : Wristbands of Divine Influence
    {player = 20, timestamp = "2022-02-23 20:19", item = 32278, cost = "", costType = "MS"}, -- Slayingfreak : Grips of Silent Justice
    {player =  8, timestamp = "2022-02-23 20:35", item = 32528, cost = 70, costType = "Solstice Points"}, -- Frodes : Blessed Band of Karabor
    {player = 13, timestamp = "2022-02-23 21:00", item = 32512, cost = "", costType = "MS"}, -- Kharlamagne : Girdle of Lordaeron's Fallen
    {player = 20, timestamp = "2022-02-23 21:01", item = 32280, cost = "", costType = "OS"}, -- Slayingfreak : Gauntlets of Enforcement
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
