local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Annarinn",
    [ 3] = "Aythya",
    [ 4] = "Bellamira",
    [ 5] = "Birdlove",
    [ 6] = "Capecrusader",
    [ 7] = "Chalula",
    [ 8] = "Chamwow",
    [ 9] = "Eillowee",
    [10] = "Fellador",
    [11] = "Floordaddy",
    [12] = "Galerron",
    [13] = "Gaylestrum",
    [14] = "Hadgarthil",
    [15] = "Hydraxt",
    [16] = "Irontitann",
    [17] = "Jaeran",
    [18] = "Jazzmene",
    [19] = "Mollymorph",
    [20] = "Pallylove",
    [21] = "Quijuanjinn",
    [22] = "Rosemondon",
    [23] = "Sarcinis",
    [24] = "Shogen",
    [25] = "Shundin",
    [26] = "Slayingfreak",
    [27] = "Snoweyes",
    [28] = "Wcgstandard",
    [29] = "Xandies",
  },
  kills = {
    {boss = 724, timestamp = "2022-09-13 20:59", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29}},
    {boss = 725, timestamp = "2022-09-13 21:12", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29}},
    {boss = 726, timestamp = "2022-09-13 21:37", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29}},
  },
  description = "2022-09-13_RaidSWP",
  drops = {
    {player = 13, timestamp = "2022-09-13 21:39", item = 34852, cost = 80, costType = "Solstice Points"}, -- Gaylestrum : Bracers of the Forgotten Vanquisher
    {player =  1, timestamp = "2022-09-13 21:57", item = 34180, costType = "MS"}, -- Anatyr : Felfury Legplates
    {player = 18, timestamp = "2022-09-13 22:03", item = 34848, cost = 80, costType = "Solstice Points"}, -- Jazzmene : Bracers of the Forgotten Conqueror
    {player =  9, timestamp = "2022-09-13 22:04", item = 34848, cost = 80, costType = "Solstice Points"}, -- Eillowee : Bracers of the Forgotten Conqueror
    {player = 27, timestamp = "2022-09-13 22:06", item = 34854, cost = 80, costType = "Solstice Points"}, -- Snoweyes : Belt of the Forgotten Protector
    {player = 16, timestamp = "2022-09-13 22:07", item = 34853, cost = 80, costType = "Solstice Points"}, -- Irontitann : Belt of the Forgotten Conqueror
    {player =  5, timestamp = "2022-09-13 22:08", item = 34853, costType = "MS"}, -- Birdlove : Belt of the Forgotten Conqueror
    {player = 26, timestamp = "2022-09-13 22:09", item = 34352, costType = "MS"}, -- Slayingfreak : Borderland Fortress Grips
    {player = 28, timestamp = "2022-09-13 22:09", item = 34168, costType = "MS"}, -- Wcgstandard : Starstalker Legguards
    {player = 29, timestamp = "2022-09-13 22:10", item = 34857, cost = 80, costType = "Solstice Points"}, -- Xandies : Boots of the Forgotten Protector
    {player =  2, timestamp = "2022-09-13 22:10", item = 34857, costType = "MS"}, -- Annarinn : Boots of the Forgotten Protector
    {player = 18, timestamp = "2022-09-13 22:11", item = 34856, cost = 80, costType = "Solstice Points"}, -- Jazzmene : Boots of the Forgotten Conqueror
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
