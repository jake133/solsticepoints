local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aythya",
    [ 3] = "Aziza",
    [ 4] = "Blackclaws",
    [ 5] = "Capecrusader",
    [ 6] = "Cholula",
    [ 7] = "Eillowee",
    [ 8] = "Elorn",
    [ 9] = "Frodes",
    [10] = "Gaylestrum",
    [11] = "Ironankh",
    [12] = "Irontitanhc",
    [13] = "Jazzmean",
    [14] = "Kyrika",
    [15] = "Lachy",
    [16] = "Pencilvestor",
    [17] = "Roragorn",
    [18] = "Sadistia",
    [19] = "Sinniaa",
    [20] = "Sinwave",
    [21] = "Slayingfreak",
    [22] = "Sneakydoodle",
    [23] = "Superboots",
    [24] = "Varv",
    [25] = "Whitewidow",
  },
  kills = {
    {boss = 618, timestamp = "2022-02-22 19:09", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 619, timestamp = "2022-02-22 19:46", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 620, timestamp = "2022-02-22 21:00", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 621, timestamp = "2022-02-22 21:29", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-02-22_Hyjal",
  drops = {
    {player = 17, timestamp = "2022-02-22 20:19", item = 30885, cost = "", costType = "OS"}, -- Roragorn : Archbishop's Slippers
    {player = 19, timestamp = "2022-02-22 20:19", item = 32609, cost = "", costType = "MS"}, -- Sinniaa : Boots of the Divine Light
    {player =  9, timestamp = "2022-02-22 20:20", item = 30868, cost = "", costType = "MS"}, -- Frodes : Rejuvenating Bracers
    {player = 20, timestamp = "2022-02-22 20:20", item = 32592, cost = "", costType = "OS"}, -- Sinwave : Chestguard of Relentless Storms
    {player = 14, timestamp = "2022-02-22 20:21", item = 32590, cost = "", costType = "MS"}, -- Kyrika : Nethervoid Cloak
    {player = 12, timestamp = "2022-02-22 20:21", item = 30861, cost = "", costType = "OS"}, -- Irontitanhc : Furious Shackles
    {player = 21, timestamp = "2022-02-22 20:22", item = 30874, cost = "", costType = "OS"}, -- Slayingfreak : The Unbreakable Will
    {player =  3, timestamp = "2022-02-22 21:02", item = 32289, cost = "", costType = "MS"}, -- Aziza : Design: Stormy Empyrean Sapphire
    {player = 22, timestamp = "2022-02-22 21:29", item = 30898, cost = 60, costType = "Solstice Points"}, -- Sneakydoodle : Shady Dealer's Pantaloons
    {player =  3, timestamp = "2022-02-22 21:30", item = 31094, cost = 80, costType = "Solstice Points"}, -- Aziza : Gloves of the Forgotten Protector
    {player = 12, timestamp = "2022-02-22 21:31", item = 31092, cost = 80, costType = "Solstice Points"}, -- Irontitanhc : Gloves of the Forgotten Conqueror
    {player = 15, timestamp = "2022-02-22 21:32", item = 32589, cost = "", costType = "OS"}, -- Lachy : Hellfire-Encased Pendant
    {player = 15, timestamp = "2022-02-22 21:33", item = 30894, cost = "", costType = "MS"}, -- Lachy : Blue Suede Shoes
    {player = 20, timestamp = "2022-02-22 21:34", item = 30893, cost = "", costType = "MS"}, -- Sinwave : Sun-touched Chain Leggings
    {player = 22, timestamp = "2022-02-22 21:35", item = 32946, cost = 70, costType = "Solstice Points"}, -- Sneakydoodle : Claw of Molten Fury
    {player =  6, timestamp = "2022-02-22 21:35", item = 32609, cost = "", costType = "OS"}, -- Cholula : Boots of the Divine Light
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
