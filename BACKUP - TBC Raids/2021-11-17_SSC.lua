
local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Brickk",
    [ 4] = "Ceruwolfe",
    [ 5] = "Cholula",
    [ 6] = "Dagarle",
    [ 7] = "Elorn",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Grimdoom",
    [11] = "Heidie",
    [12] = "Ironankh",
    [13] = "Irontitanhc",
    [14] = "Kyrika",
    [15] = "Lachy",
    [16] = "Luethien",
    [17] = "Pencilvestor",
    [18] = "Retrav",
    [19] = "Sinniaa",
    [20] = "Skeeta",
    [21] = "Sneakydoodle",
    [22] = "Superboots",
    [23] = "Varv",
    [24] = "Velladonna",
    [25] = "Whitewidow",
    [26] = "Wootzz",
  },
  kills = {
    {boss = 623, timestamp = "2021-11-17 19:28", players = {1,2,3,4,5,6,7,8,9,11,12,13,14,15,16,17,18,19,20,21,22,23,25,26}},
    {boss = 624, timestamp = "2021-11-17 19:55", players = {1,2,3,4,5,6,7,8,9,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 625, timestamp = "2021-11-17 20:57", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 626, timestamp = "2021-11-17 21:30", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
  },
  description = "2021-11-17_SSC",
  drops = {
    {player = 23, timestamp = "2021-11-17 20:42", item = 30058, cost = "", costType = "MS"}, -- Varv : Mallet of the Tides
    {player =  2, timestamp = "2021-11-17 20:43", item = 30066, cost = "", costType = "OS"}, -- Aziza : Tempest-Strider Boots
    {player = 12, timestamp = "2021-11-17 20:44", item = 30022, cost = "", costType = "MS"}, -- Ironankh : Pendant of the Perilous
    {player = 13, timestamp = "2021-11-17 20:45", item = 30027, cost = "", costType = "OS"}, -- Irontitanhc : Boots of Courage Unending
    {player = 26, timestamp = "2021-11-17 20:46", item = 30049, cost = "", costType = "MS"}, -- Wootzz : Fathomstone
    {player =  1, timestamp = "2021-11-17 20:46", item = 30629, cost = "70", costType = "Solstice Points"}, -- Ashgon : Scarab of Displacement
    {player = 14, timestamp = "2021-11-17 21:32", item = 30247, cost = "", costType = "MS"}, -- Kyrika : Leggings of the Vanquished Hero
    {player = 25, timestamp = "2021-11-17 21:33", item = 30247, cost = "", costType = "MS"}, -- Whitewidow : Leggings of the Vanquished Hero
    {player =  5, timestamp = "2021-11-17 21:34", item = 30101, cost = "", costType = "MS"}, -- Cholula : Bloodsea Brigand's Vest
    {player = 24, timestamp = "2021-11-17 21:35", item = 30282, cost = "10", costType = "Solstice Points"}, -- Velladonna : Pattern: Boots of Blasting
    {player =  4, timestamp = "2021-11-17 21:42", item = 30239, cost = "", costType = "MS"}, -- Ceruwolfe : Gloves of the Vanquished Champion
    {player =  5, timestamp = "2021-11-17 21:43", item = 30627, cost = "70", costType = "Solstice Points"}, -- Cholula : Tsunami Talisman
    {player = 12, timestamp = "2021-11-17 21:43", item = 30239, cost = "", costType = "MS"}, -- Ironankh : Gloves of the Vanquished Champion
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
