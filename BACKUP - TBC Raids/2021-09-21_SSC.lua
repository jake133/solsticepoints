local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Airfoil",
    [ 2] = "Aziza",
    [ 3] = "Badreams",
    [ 4] = "Brickk",
    [ 5] = "Cailyse",
    [ 6] = "Capecrusader",
    [ 7] = "Dagarle",
    [ 8] = "Elorn",
    [ 9] = "Estrellita",
    [10] = "Felwins",
    [11] = "Frodes",
    [12] = "Inxi",
    [13] = "Ironflurry",
    [14] = "Irontitanhc",
    [15] = "Kalisae",
    [16] = "Kharlamagne",
    [17] = "Lachy",
    [18] = "Skeeta",
    [19] = "Soulbane",
    [20] = "Superboots",
    [21] = "Sylaramynd",
    [22] = "Velladonna",
    [23] = "Varv",
    [24] = "Whitewidow",
    [25] = "Xandie",
  },
  kills = {
    {boss = 624, timestamp = "2021-09-21 20:44", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  drops = {
    {player =  5, timestamp = "2021-09-21", item = 30025, cost = "", costType = "MS"}, -- Cailyse : Serpentshrine Shuriken
    },
  description = "2021-09-21_SSC",

}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
