local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Blackclaws",
    [ 4] = "Capecrusader",
    [ 5] = "Cholula",
    [ 6] = "Eillowee",
    [ 7] = "Elorn",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Ghostbeard",
    [11] = "Ironankh",
    [12] = "Irontitanhc",
    [13] = "Jazzmean",
    [14] = "Lachy",
    [15] = "Luethien",
    [16] = "Pencilvestor",
    [17] = "Roragorn",
    [18] = "Sadistia",
    [19] = "Sinniaa",
    [20] = "Sinwave",
    [21] = "Slayingfreak",
    [22] = "Sneakydoodle",
    [23] = "Superboots",
    [24] = "Varv",
    [25] = "Velladonna",
    [26] = "Whitewidow",
  },
  kills = {
    {boss = 623, timestamp = "2022-01-18 19:03", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 624, timestamp = "2022-01-18 19:22", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 627, timestamp = "2022-01-18 19:45", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 626, timestamp = "2022-01-18 20:06", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 625, timestamp = "2022-01-18 20:22", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 628, timestamp = "2022-01-18 22:08", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,18,19,20,21,22,23,24,25,26}},
  },
  description = "2022-01-18_SSC",
  drops = {
    {player =  6, timestamp = "2022-01-18 18:55", item = 30282, cost = "", costType = "MS"}, -- Eillowee : Pattern: Boots of Blasting
    {player = 11, timestamp = "2022-01-18 19:04", item = 30054, cost = "", costType = "MS"}, -- Ironankh : Ranger-General's Chestguard
    {player = 20, timestamp = "2022-01-18 19:04", item = 30047, cost = "", costType = "MS"}, -- Sinwave : Blackfathom Warbands
    {player = 17, timestamp = "2022-01-18 19:05", item = 30664, cost = "", costType = "MS"}, -- Roragorn : Living Root of the Wildheart
    {player =  1, timestamp = "2022-01-18 19:06", item = 30027, cost = "", costType = "OS"}, -- Ashgon : Boots of Courage Unending
    {player = 17, timestamp = "2022-01-18 19:23", item = 30062, cost = "", costType = "OS"}, -- Roragorn : Grove-Bands of Remulos
    {player =  5, timestamp = "2022-01-18 19:24", item = 33054, cost = "", costType = "OS"}, -- Cholula : The Seal of Danzalar
    {player =  3, timestamp = "2022-01-18 19:24", item = 30059, cost = "", costType = "MS"}, -- Blackclaws : Choker of Animalistic Fury
    {player =  9, timestamp = "2022-01-18 19:38", item = 30281, cost = "", costType = "MS"}, -- Gaylestrum : Pattern: Belt of the Long Road
    {player = 14, timestamp = "2022-01-18 19:45", item = 30720, cost = "70", costType = "Solstice Points"}, -- Lachy : Serpent-Coil Braid
    {player = 11, timestamp = "2022-01-18 19:46", item = 30098, cost = "", costType = "MS"}, -- Ironankh : Razor-Scale Battlecloak
    {player = 19, timestamp = "2022-01-18 19:47", item = 30075, cost = "", costType = "MS"}, -- Sinniaa : Gnarled Chestpiece of the Ancients
    {player =  6, timestamp = "2022-01-18 20:07", item = 30247, cost = "", costType = "MS"}, -- Eillowee : Leggings of the Vanquished Hero
    {player = 11, timestamp = "2022-01-18 20:08", item = 30245, cost = "80", costType = "Solstice Points"}, -- Ironankh : Leggings of the Vanquished Champion
    {player = 20, timestamp = "2022-01-18 20:08", item = 30663, cost = "", costType = "MS"}, -- Sinwave : Fathom-Brooch of the Tidewalker
    {player = 19, timestamp = "2022-01-18 20:24", item = 30240, cost = "", costType = "MS"}, -- Sinniaa : Gloves of the Vanquished Defender
    {player =  3, timestamp = "2022-01-18 20:24", item = 30240, cost = "", costType = "MS"}, -- Blackclaws : Gloves of the Vanquished Defender
    {player = 20, timestamp = "2022-01-18 20:24", item = 30239, cost = "", costType = "OS"}, -- Sinwave : Gloves of the Vanquished Champion
    {player = 11, timestamp = "2022-01-18 20:26", item = 30091, cost = "", costType = "MS"}, -- Ironankh : True-Aim Stalker Bands
    {player =  4, timestamp = "2022-01-18 22:09", item = 30242, cost = "80", costType = "Solstice Points"}, -- Capecrusader : Helm of the Vanquished Champion
    {player =  8, timestamp = "2022-01-18 22:10", item = 30243, cost = "80", costType = "Solstice Points"}, -- Frodes : Helm of the Vanquished Defender
    {player = 25, timestamp = "2022-01-18 22:10", item = 30244, cost = "80", costType = "Solstice Points"}, -- Velladonna : Helm of the Vanquished Hero
    {player = 16, timestamp = "2022-01-18 22:11", item = 30103, cost = "", costType = "MS"}, -- Pencilvestor : Fang of Vashj
    {player =  1, timestamp = "2022-01-18 22:12", item = 30112, cost = "", costType = "OS"}, -- Ashgon : Glorious Gauntlets of Crestfall
    {player = 10, timestamp = "2022-01-18 19:00", item = 30023, cost = "", costType = "OS"}, -- Ghostbeard : Totem of the Maelstrom
},
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
