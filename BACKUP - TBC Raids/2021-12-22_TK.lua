local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
	[ 1]="Ashgon",
	[ 2]="Aythya",
	[ 3]="Aziza",
	[ 4]="Brickk",
	[ 5]="Capecrusader",
	[ 6]="Ceruwolfe",
	[ 7]="Cholula",
	[ 8]="Eillowee",
	[ 9]="Elorn",
	[10]="Frodes",
	[11]="Gaylestrum",
	[12]="Halestormz",
	[13]="Heidie",
	[14]="Irontitanhc",
	[15]="Jazzmean",
	[16]="Kyrika",
	[17]="Lachy",
	[18]="Roragorn",
	[19]="Sadistia",
	[20]="Slayingfreak",
	[21]="Superboots",
	[22]="Treeclaw",
	[23]="Varv",
	[24]="Whitewidow",
	[25]="Wootzz",
  },
  kills = {
    {boss = 730, timestamp = "2021-12-22", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 731, timestamp = "2021-12-22", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 732, timestamp = "2021-12-22", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2021-12-22_TK",
  drops = {
    {player = 18, timestamp = "2021-12-22", item = 30029, cost = "", costType = "OS"}, -- Roragorn : Bark Gloves
    {player = 2, timestamp = "2021-12-22", item = 30026, cost = "", costType = "MS"}, -- Aythya : bands of the Celestial Archer
    {player = 14, timestamp = "2021-12-22", item = 30183, cost = "10", costType = "Solstice Points"}, -- Irontitanhc : Nether Vortex
    {player = 14, timestamp = "2021-12-22", item = 30183, cost = "10", costType = "Solstice Points"}, -- Irontitanhc : Nether Vortex
    {player = 14, timestamp = "2021-12-22", item = 30183, cost = "10", costType = "Solstice Points"}, -- Irontitanhc : Nether Vortex
    {player = 14, timestamp = "2021-12-22", item = 30183, cost = "10", costType = "Solstice Points"}, -- Irontitanhc : Nether Vortex
    {player = 4, timestamp = "2021-12-22", item = 29948, cost = "", costType = "OS"}, -- Brickk : Claw of the Phoenix
    {player = 14, timestamp = "2021-12-22", item = 30447, cost = "", costType = "MS"}, -- Irontitanhc : Tome of Fiery Redemption
    {player = 10, timestamp = "2021-12-22", item = 29984, cost = "", costType = "MS"}, -- Frodes : Girdle of Zaeter

    {player = 18, timestamp = "2021-12-22", item = 30249, cost = "80", costType = "Solstice Points"}, -- Roragorn : Pauldrons of the Defender
    {player = 21, timestamp = "2021-12-22", item = 30249, cost = "80", costType = "Solstice Points"}, -- Superboots : Pauldrons of the Defender

    {player = 1, timestamp = "2021-12-22", item = 32267, cost = "", costType = "MS"}, -- Ashgon : Boots of the Resilient
    {player = 4, timestamp = "2021-12-22", item = 29950, cost = "", costType = "MS"}, -- Brickk : LOOT
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
