local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aythya",
    [ 3] = "Aziza",
    [ 4] = "Brickk",
    [ 5] = "Capecrusader",
    [ 6] = "Cholula",
    [ 7] = "Eillowee",
    [ 8] = "Elorn",
    [ 9] = "Frodes",
    [10] = "Gaylestrum",
    [11] = "Ironankh",
    [12] = "Irontitanhc",
    [13] = "Kharlamagne",
    [14] = "Kyrika",
    [15] = "Lachy",
    [16] = "Luethien",
    [17] = "Pencilvestor",
    [18] = "Roragorn",
    [19] = "Sinwave",
    [20] = "Sneakydoodle",
    [21] = "Superboots",
    [22] = "Varv",
    [23] = "Velladonna",
    [24] = "Whitewidow",
    [25] = "Wootzz",
  },
  kills = {
    {boss = 730, timestamp = "2021-12-29 19:41", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 732, timestamp = "2021-12-29 19:59", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 731, timestamp = "2021-12-29 20:23", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2021-12-29_TK",
  drops = {
    {player = 18, timestamp = "2021-12-29 19:42", item = 29923, cost = "", costType = "OS"}, -- Roragorn : Talisman of the Sun King
    {player = 12, timestamp = "2021-12-29 19:48", item = 30183, cost = "10", costType = "Solstice Points"}, -- Irontitanhc : Nether Vortex
    {player = 19, timestamp = "2021-12-29 19:50", item = 30030, cost = "", costType = "MS"}, -- Sinwave : Girdle of Fallen Stars
    {player = 21, timestamp = "2021-12-29 19:54", item = 30281, cost = "", costType = "MS"}, -- Superboots : Pattern: Belt of the Long Road
    {player =  5, timestamp = "2021-12-29 20:01", item = 29962, cost = "", costType = "OS"}, -- Capecrusader : Heartrazor
    {player = 21, timestamp = "2021-12-29 20:01", item = 29981, cost = "", costType = "MS"}, -- Superboots : Ethereum Life-Staff
    {player =  4, timestamp = "2021-12-29 20:24", item = 30249, cost = "", costType = "MS"}, -- Brickk : Pauldrons of the Vanquished Defender
    {player = 12, timestamp = "2021-12-29 20:24", item = 30248, cost = "80", costType = "Solstice Points"}, -- Irontitanhc : Pauldrons of the Vanquished Champion
    {player = 20, timestamp = "2021-12-29 20:25", item = 30450, cost = "70", costType = "Solstice Points"}, -- Sneakydoodle : Warp-Spring Coil
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
