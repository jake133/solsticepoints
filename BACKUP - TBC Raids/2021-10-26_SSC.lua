local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Brickk",
    [ 4] = "Capecrusader",
    [ 5] = "Dagarle",
    [ 6] = "Elorn",
    [ 7] = "Estrellita",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Ironflurry",
    [11] = "Irontitanhc",
    [12] = "Jazzmean",
    [13] = "Kharlamagne",
    [14] = "Lachy",
    [15] = "Pencilvestor",
    [16] = "Sinwave",
    [17] = "Skeeta",
    [18] = "Slayingfreak",
    [19] = "Sneakydoodle",
    [20] = "Soulbane",
    [21] = "Superboots",
    [22] = "Varv",
    [23] = "Velladonna",
    [24] = "Whitewidow",
    [25] = "Wootzz",
  },
  kills = {
    {boss = 625, timestamp = "2021-10-26 20:14", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 626, timestamp = "2021-10-26 20:55", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 623, timestamp = "2021-10-26 21:50", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 624, timestamp = "2021-10-26 22:10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2021-10-26_SSC",
  drops = {
    {player =  5, timestamp = "2021-10-26 20:29", item = 30027, cost = "", costType = "OS"}, -- Dagarle : Boots of Courage Unending
    {player =  1, timestamp = "2021-10-26 20:30", item = 30027, cost = "", costType = "OS"}, -- Ashgon : Boots of Courage Unending
    {player = 18, timestamp = "2021-10-26 20:30", item = 30022, cost = "", costType = "OS"}, -- Slayingfrak : Pendant of the Perilous
    {player = 22, timestamp = "2021-10-26 20:30", item = 30022, cost = "", costType = "OS"}, -- Varv : Pendant of the Perilous
    {player = 13, timestamp = "2021-10-26 20:31", item = 30620, cost = "", costType = "OS"}, -- Kharl : Spyglass of the Hidden Fleet
    {player = 13, timestamp = "2021-10-26 20:33", item = 30239, cost = "70", costType = "Solstice Points"}, -- Kharlamagne : Gloves of the Vanquished Champion
    {player = 17, timestamp = "2021-10-26 20:33", item = 30241, cost = "", costType = "MS"}, -- Skeeta : Gloves of the Vanquished Hero
    {player = 15, timestamp = "2021-10-26 20:34", item = 30627, cost = "", costType = "MS"}, -- Pencilvestor : Tsunami Talisman
    {player = 11, timestamp = "2021-10-26 22:13", item = 30245, cost = "80", costType = "Solstice Points"}, -- Irontitanhc : Leggings of the Vanquished Champion
    {player =  7, timestamp = "2021-10-26 22:13", item = 30246, cost = "80", costType = "Solstice Points"}, -- Estrellita : Leggings of the Vanquished Defender
    {player = 18, timestamp = "2021-10-26 22:14", item = 30090, cost = "", costType = "MS"}, -- Slayingfreak : World Breaker
    {player = 18, timestamp = "2021-10-26 22:15", item = 30620, cost = "", costType = "OS"}, -- Slayingfreak : Spyglass of the Hidden Fleet
    {player =  4, timestamp = "2021-10-26 22:16", item = 30052, cost = "70", costType = "Solstice Points"}, -- Capecrusader : Ring of Lethality
    {player = 14, timestamp = "2021-10-26 22:17", item = 30049, cost = "70", costType = "Solstice Points"}, -- Lachy : Fathomstone
    {player = 13, timestamp = "2021-10-26 22:24", item = 33054, cost = "", costType = "MS"}, -- Kharlamagne : The Seal of Danzalar
    {player = 18, timestamp = "2021-10-26 22:25", item = 30060, cost = "", costType = "MS"}, -- Slayingfreak : Boots of Effortless Striking
    {player = 11, timestamp = "2021-10-26 22:27", item = 30323, cost = "", costType = "MS"}, -- Irontitanhc : Plans: Boots of the Protector
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
