local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Annarinn",
    [ 3] = "Aythya",
    [ 4] = "Blackcläws",
    [ 5] = "Capecrusader",
    [ 6] = "Chalula",
    [ 7] = "Elorn",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Irontitann",
    [11] = "Jazzmene",
    [12] = "Kattianna",
    [13] = "Keeganatotem",
    [14] = "Kekett",
    [15] = "Kharlamagne",
    [16] = "Lachý",
    [17] = "Melisea",
    [18] = "Pencilvestor",
    [19] = "Roragorn",
    [20] = "Sadistia",
    [21] = "Slayingfreak",
    [22] = "Snoweyes",
    [23] = "Snozzberry",
    [24] = "Superboots",
    [25] = "Wootzz",
    [26] = "Xandies",
  },
  kills = {
    {boss = 604, timestamp = "2022-05-31 20:15", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,26}},
    {boss = 606, timestamp = "2022-05-31 20:43", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 605, timestamp = "2022-05-31 21:06", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 607, timestamp = "2022-05-31 21:35", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 608, timestamp = "2022-05-31 22:06", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
  },
  description = "2022-05-31_BT1",
  drops = {
    {player = 10, timestamp = "2022-05-31 19:53", item = 32247, costType = "OS"}, -- Irontitann : Ring of Captured Storms
    {player = 10, timestamp = "2022-05-31 19:54", item = 32234, costType = "OS"}, -- Irontitann : Fists of Mukoa
    {player = 16, timestamp = "2022-05-31 19:55", item = 32527, cost = 70, costType = "Solstice Points"}, -- Lachý : Ring of Ancient Knowledge
    {player =  4, timestamp = "2022-05-31 19:56", item = 32608, costType = "OS"}, -- Blackcläws : Pillager's Gauntlets
    {player = 13, timestamp = "2022-05-31 20:00", item = 32252, costType = "MS"}, -- Keeganatotem : Nether Shadow Tunic
    {player = 14, timestamp = "2022-05-31 20:01", item = 32262, costType = "MS"}, -- Kekett : Syphon of the Nathrezim
    {player =  1, timestamp = "2022-05-31 20:44", item = 32526, costType = "MS"}, -- Anatyr : Band of Devastation
    {player =  2, timestamp = "2022-05-31 20:59", item = 32346, costType = "MS"}, -- Annarinn : Boneweave Girdle
    {player = 12, timestamp = "2022-05-31 20:59", item = 32327, costType = "MS"}, -- Kattianna : Robe of the Shadow Council
    {player = 23, timestamp = "2022-05-31 21:01", item = 32273, costType = "MS"}, -- Snozzberry : Amice of Brilliant Light
    {player = 10, timestamp = "2022-05-31 21:07", item = 32512, costType = "OS"}, -- Irontitann : Girdle of Lordaeron's Fallen
    {player = 10, timestamp = "2022-05-31 21:13", item = 32342, costType = "MS"}, -- Irontitann : Girdle of Mighty Resolve
    {player = 17, timestamp = "2022-05-31 21:27", item = 32337, costType = "MS"}, -- Melisea : Shroud of Forgiveness
    {player = 21, timestamp = "2022-05-31 21:36", item = 32943, costType = "OS"}, -- Slayingfreak : Swiftsteel Bludgeon
    {player = 26, timestamp = "2022-05-31 21:37", item = 32517, costType = "OS"}, -- Xandies : The Wavemender's Mantle
    {player = 15, timestamp = "2022-05-31 22:07", item = 32369, costType = "OS"}, -- Kharlamagne : Blade of Savagery
    {player =  7, timestamp = "2022-05-31 22:09", item = 31103, cost = 80, costType = "Solstice Points"}, -- Elorn : Pauldrons of the Forgotten Protector
    {player = 21, timestamp = "2022-05-31 22:09", item = 31103, costType = "MS"}, -- Slayingfreak : Pauldrons of the Forgotten Protector
    {player = 14, timestamp = "2022-05-31 22:09", item = 32376, cost = 60, costType = "Solstice Points"}, -- Kekett : Forest Prowler's Helm
    {player = 19, timestamp = "2022-05-31 22:10", item = 31102, cost = 80, costType = "Solstice Points"}, -- Roragorn : Pauldrons of the Forgotten Vanquisher
    {player = 21, timestamp = "2022-05-31 22:12", item = 31100, costType = "MS"}, -- Slayingfreak : Leggings of the Forgotten Protector
    {player = 17, timestamp = "2022-05-31 22:12", item = 31098, cost = 80, costType = "Solstice Points"}, -- Melisea : Leggings of the Forgotten Conqueror
    {player = 16, timestamp = "2022-05-31 22:13", item = 31099, costType = "MS"}, -- Lachý : Leggings of the Forgotten Vanquisher
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
