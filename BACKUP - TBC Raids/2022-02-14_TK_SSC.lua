local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aythya",
    [ 3] = "Aziza",
    [ 4] = "Capecrusader",
    [ 5] = "Cholula",
    [ 6] = "Eillowee",
    [ 7] = "Elorn",
    [ 8] = "Furiozah",
    [ 9] = "Glance",
    [10] = "Helletrix",
    [11] = "Ironankh",
    [12] = "Irontitanhc",
    [13] = "Kharlamagne",
    [14] = "Khorai",
    [15] = "Kyrika",
    [16] = "Lachy",
    [17] = "Lilybet",
    [18] = "Pencilvestor",
    [19] = "Pulchra",
    [20] = "Robotchickin",
    [21] = "Roragorn",
    [22] = "Sinniaa",
    [23] = "Sneakydoodle",
    [24] = "Velladonna",
    [25] = "Xandie",
  },
  kills = {
    {boss = 730, timestamp = "2022-02-14 19:38", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 733, timestamp = "2022-02-14 21:19", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-02-14_TK_SSC",
  drops = {
    {player = 13, timestamp = "2022-02-14 21:20", item = 30447, cost = "", costType = "MS"}, -- Kharlamagne : Tome of Fiery Redemption
    {player = 11, timestamp = "2022-02-14 21:22", item = 30029, cost = "", costType = "OS"}, -- Ironankh : Bark-Gloves of Ancient Wisdom
    {player =  5, timestamp = "2022-02-14 21:23", item = 30237, cost = 40, costType = "Solstice Points"}, -- Cholula : Chestguard of the Vanquished Defender
    {player = 24, timestamp = "2022-02-14 21:24", item = 30238, cost = 40, costType = "Solstice Points"}, -- Velladonna : Chestguard of the Vanquished Hero
    {player =  9, timestamp = "2022-02-14 21:24", item = 29990, cost = "", costType = "MS"}, -- Glance : Crown of the Sun
    {player = 11, timestamp = "2022-02-14 21:25", item = 30236, cost = 40, costType = "Solstice Points"}, -- Ironankh : Chestguard of the Vanquished Champion
    {player =  3, timestamp = "2022-02-14 21:25", item = 32405, cost = 35, costType = "Solstice Points"}, -- Aziza : Verdant Sphere
    {player = 13, timestamp = "2022-02-14 21:27", item = 29993, cost = "", costType = "OS"}, -- Kharlamagne : Twinblade of the Phoenix
    {player = 17, timestamp = "2022-02-14 21:27", item = 29920, cost = "", costType = "OS"}, -- Lilybet : Ring of rebirth
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
