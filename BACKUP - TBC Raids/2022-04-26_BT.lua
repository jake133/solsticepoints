local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Aythya",
    [ 2] = "Blackcläws",
    [ 3] = "Capecrusader",
    [ 4] = "Chalula",
    [ 5] = "Diisori",
    [ 6] = "Eillowee",
    [ 7] = "Elorn",
    [ 8] = "Gaylestrum",
    [ 9] = "Irontitann",
    [10] = "Jazzmene",
    [11] = "Kattianna",
    [12] = "Lachý",
    [13] = "Maërlyn",
    [14] = "Mclitty",
    [15] = "Noritotes",
    [16] = "Pencilvestor",
    [17] = "Roragorn",
    [18] = "Sadistia",
    [19] = "Skeeta",
    [20] = "Slayingfreak",
    [21] = "Snoweyes",
    [22] = "Snozzberry",
    [23] = "Superboots",
    [24] = "Wootzz",
    [25] = "Xandies",
  },
  kills = {
    {boss = 601, timestamp = "2022-04-26 19:42", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 602, timestamp = "2022-04-26 20:00", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 603, timestamp = "2022-04-26 20:20", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 604, timestamp = "2022-04-26 20:49", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 606, timestamp = "2022-04-26 21:08", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 605, timestamp = "2022-04-26 21:39", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-04-26_BT",
  drops = {
    {player = 14, timestamp = "2022-04-26 19:43", item = 32238, costType = "MS"}, -- Mclitty : Ring of Calming Waves
    {player = 25, timestamp = "2022-04-26 20:02", item = 32242, costType = "MS"}, -- Xandies : Boots of Oceanic Fury
    {player = 20, timestamp = "2022-04-26 20:11", item = 32254, costType = "OS"}, -- Slayingfreak : The Brutalizer
    {player =  4, timestamp = "2022-04-26 20:14", item = 32261, costType = "MS"}, -- Chalula : Band of the Abyssal Lord
    {player =  2, timestamp = "2022-04-26 20:20", item = 32608, costType = "OS"}, -- Blackcläws : Pillager's Gauntlets
    {player = 13, timestamp = "2022-04-26 20:21", item = 32270, costType = "MS"}, -- Maërlyn : Focused Mana Bindings
    {player =  9, timestamp = "2022-04-26 20:39", item = 32263, costType = "MS"}, -- Irontitann : Praetorian's Legguards
    {player =  9, timestamp = "2022-04-26 20:51", item = 32348, costType = "OS"}, -- Irontitann : Soul Cleaver
    {player = 21, timestamp = "2022-04-26 20:52", item = 32324, cost = 60, costType = "Solstice Points"}, -- Snoweyes : Insidious Bands
    {player =  5, timestamp = "2022-04-26 21:23", item = 32354, costType = "OS"}, -- Diisori : Crown of Empowered Fate
    {player =  1, timestamp = "2022-04-26 21:24", item = 32346, cost = 60, costType = "Solstice Points"}, -- Aythya : Boneweave Girdle
    {player = 14, timestamp = "2022-04-26 21:50", item = 32269, costType = "OS"}, -- Mclitty : Messenger of Fate
    {player =  5, timestamp = "2022-04-26 21:51", item = 32342, costType = "MS"}, -- Diisori : Girdle of Mighty Resolve
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
