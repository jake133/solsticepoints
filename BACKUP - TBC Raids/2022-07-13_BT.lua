local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Amoprobos",
    [ 2] = "Anatyr",
    [ 3] = "Annarinn",
    [ 4] = "Aythya",
    [ 5] = "Capecrusader",
    [ 6] = "Chalula",
    [ 7] = "Chamwow",
    [ 8] = "Dagarle",
    [ 9] = "Ditsy",
    [10] = "Eillowee",
    [11] = "Fârtblossom",
    [12] = "Gaylestrum",
    [13] = "Glancen",
    [14] = "Grayskyy",
    [15] = "Inxi",
    [16] = "Irontitann",
    [17] = "Jaeran",
    [18] = "Kattianna",
    [19] = "Lachý",
    [20] = "Mayaell",
    [21] = "Melisea",
    [22] = "Pencilvestor",
    [23] = "Snoweyes",
    [24] = "Superboots",
    [25] = "Wootzz",
    [26] = "Xandies",
  },
  kills = {
    {boss = 601, timestamp = "2022-07-13 19:34", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,18,19,20,21,22,23,24,25,26}},
    {boss = 602, timestamp = "2022-07-13 19:50", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,18,19,20,21,22,23,24,25,26}},
    {boss = 603, timestamp = "2022-07-13 20:17", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 607, timestamp = "2022-07-13 20:42", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 608, timestamp = "2022-07-13 21:43", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
  },
  description = "2022-07-13_BT",
  drops = {
    {player =  8, timestamp = "2022-07-13 20:17", item = 32248, costType = "OS"}, -- Dagarle : Halberd of Desolation
    {player = 21, timestamp = "2022-07-13 20:18", item = 34012, costType = "MS"}, -- Melisea : Shroud of the Final Stand
    {player =  1, timestamp = "2022-07-13 20:21", item = 32260, costType = "MS"}, -- Amoprobos : Choker of Endless Nightmares
    {player =  8, timestamp = "2022-07-13 20:25", item = 32266, costType = "OS"}, -- Dagarle : Ring of Deceitful Intent
    {player = 14, timestamp = "2022-07-13 20:25", item = 32266, costType = "MS"}, -- Grayskyy : Ring of Deceitful Intent
    {player = 16, timestamp = "2022-07-13 20:59", item = 32368, costType = "MS"}, -- Irontitann : Tome of the Lightbringer
    {player = 11, timestamp = "2022-07-13 21:15", item = 32526, costType = "MS"}, -- Fârtblossom : Band of Devastation
    {player = 24, timestamp = "2022-07-13 21:16", item = 32528, costType = "MS"}, -- Superboots : Blessed Band of Karabor
    {player = 15, timestamp = "2022-07-13 21:17", item = 31102, costType = "MS"}, -- Inxi : Pauldrons of the Forgotten Vanquisher
    {player = 14, timestamp = "2022-07-13 21:17", item = 31102, costType = "MS"}, -- Grayskyy : Pauldrons of the Forgotten Vanquisher
    {player = 25, timestamp = "2022-07-13 21:44", item = 31101, cost = 80, costType = "Solstice Points"}, -- Wootzz : Pauldrons of the Forgotten Conqueror
    {player = 15, timestamp = "2022-07-13 21:57", item = 31099, cost = 80, costType = "Solstice Points"}, -- Inxi : Leggings of the Forgotten Vanquisher
    {player =  8, timestamp = "2022-07-13 21:59", item = 31098, costType = "MS"}, -- Dagarle : Leggings of the Forgotten Conqueror
    {player = 10, timestamp = "2022-07-13 21:59", item = 31098, costType = "OS"}, -- Eillowee : Leggings of the Forgotten Conqueror
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
