local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Aythya",
    [ 2] = "Bbeamer",
    [ 3] = "Capecrusader",
    [ 4] = "Chalula",
    [ 5] = "Eillowee",
    [ 6] = "Elorn",
    [ 7] = "Frodes",
    [ 8] = "Gaylestrum",
    [ 9] = "Irontitann",
    [10] = "Jazzmene",
    [11] = "Kattianna",
    [12] = "Kekett",
    [13] = "Kharlamagne",
    [14] = "Lachý",
    [15] = "Maërlyn",
    [16] = "Noritotes",
    [17] = "Roachy",
    [18] = "Roragorn",
    [19] = "Sadistia",
    [20] = "Slayingfreak",
    [21] = "Snoweyes",
    [22] = "Snozzberry",
    [23] = "Superboots",
    [24] = "Wootzz",
    [25] = "Xandies",
  },
  kills = {
    {boss = 605, timestamp = "2022-03-30 20:09", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-03-30_BT2",
  drops = {
    {player =  5, timestamp = "2022-03-30 20:10", item = 32527, cost = 70, costType = "Solstice Points"}, -- Eillowee : Ring of Ancient Knowledge
    {player =  5, timestamp = "2022-03-30 20:11", item = 32338, cost = "", costType = "MS"}, -- Eillowee : Blood-cursed Shoulderpads
    {player = 20, timestamp = "2022-03-30 20:12", item = 32333, cost = "", costType = "MS"}, -- Slayingfreak : Girdle of Stability
    {player = 12, timestamp = "2022-03-30 21:29", item = 32526, cost = "", costType = "MS"}, -- Kekett : Band of Devastation
    {player =  7, timestamp = "2022-03-30 21:29", item = 34012, cost = "", costType = "MS"}, -- Frodes : Shroud of the Final Stand
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
