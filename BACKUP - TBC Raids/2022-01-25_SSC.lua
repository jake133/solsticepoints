local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Aythya",
    [ 2] = "Aziza",
    [ 3] = "Blackclaws",
    [ 4] = "Capecrusader",
    [ 5] = "Cholula",
    [ 6] = "Damelion",
    [ 7] = "Elorn",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Ironankh",
    [11] = "Irontitanhc",
    [12] = "Jazzmean",
    [13] = "Lachy",
    [14] = "Luethien",
    [15] = "Pencilvestor",
    [16] = "Pulchra",
    [17] = "Roragorn",
    [18] = "Sadistia",
    [19] = "Sinniaa",
    [20] = "Sinwave",
    [21] = "Slayingfreak",
    [22] = "Superboots",
    [23] = "Tenki",
    [24] = "Uniion",
    [25] = "Whitewidow",
    [26] = "Wootzz",
  },
  kills = {
    {boss = 624, timestamp = "2022-01-25 19:34", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,25,26}},
    {boss = 627, timestamp = "2022-01-25 20:04", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,25,26}},
    {boss = 626, timestamp = "2022-01-25 20:22", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 625, timestamp = "2022-01-25 20:41", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 628, timestamp = "2022-01-25 21:26", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,25,26}},
  },
  description = "2022-01-25_SSC",
  drops = {
    {player =  2, timestamp = "2022-01-25 19:35", item = 30064, cost = "", costType = "MS"}, -- Aziza : Cord of Screaming Terrors
    {player = 11, timestamp = "2022-01-25 19:36", item = 30060, cost = "", costType = "OS"}, -- Irontitanhc : Boots of Effortless Striking
    {player =  6, timestamp = "2022-01-25 19:36", item = 30065, cost = "", costType = "MS"}, -- Damelion : Glowing Breastplate of Truth
    {player =  6, timestamp = "2022-01-25 20:05", item = 30084, cost = "", costType = "MS"}, -- Damelion : Pauldrons of the Argent Sentinel
    {player = 14, timestamp = "2022-01-25 20:06", item = 30080, cost = "", costType = "MS"}, -- Luethien : Luminescent Rod of the Naaru
    {player = 11, timestamp = "2022-01-25 20:06", item = 30083, cost = "", costType = "MS"}, -- Irontitanhc : Ring of Sundered Souls
    {player =  5, timestamp = "2022-01-25 20:23", item = 30090, cost = "", costType = "OS"}, -- Cholula : World Breaker
    {player = 16, timestamp = "2022-01-25 20:24", item = 30245, cost = "", costType = "MS"}, -- Pulchra : Leggings of the Vanquished Champion
	{player = 15, timestamp = "2022-01-25 20:24", item = 30245, cost = "", costType = "MS"}, -- Pencilvestor : Leggings of the Vanquished Champion
	{player = 23, timestamp = "2022-01-25 20:24", item = 30245, cost = "", costType = "MS"}, -- Tenki : Leggings of the Vanquished Champion
    {player = 23, timestamp = "2022-01-25 20:41", item = 30096, cost = "", costType = "MS"}, -- Tenki : Girdle of the Invulnerable
    {player =  1, timestamp = "2022-01-25 20:42", item = 30241, cost = "", costType = "MS"}, -- Aythya : Gloves of the Vanquished Hero
    {player = 17, timestamp = "2022-01-25 20:42", item = 30240, cost = "", costType = "MS"}, -- Roragorn : Gloves of the Vanquished Defender
	{player =  5, timestamp = "2022-01-25 20:42", item = 30240, cost = "", costType = "OS"}, -- Cholula : Gloves of the Vanquished Defender
    {player = 21, timestamp = "2022-01-25 21:27", item = 30621, cost = "", costType = "MS"}, -- Slayingfreak : Prism of Inner Calm
    {player = 26, timestamp = "2022-01-25 21:28", item = 30107, cost = "60", costType = "Solstice Points"}, -- Wootzz : Vestments of the Sea-Witch
    {player = 15, timestamp = "2022-01-25 21:29", item = 30242, cost = "80", costType = "Solstice Points"}, -- Pencilvestor : Helm of the Vanquished Champion
    {player = 12, timestamp = "2022-01-25 21:30", item = 30243, cost = "80", costType = "Solstice Points"}, -- Jazzmean : Helm of the Vanquished Defender
    {player = 22, timestamp = "2022-01-25 21:30", item = 30243, cost = "80", costType = "Solstice Points"}, -- Superboots : Helm of the Vanquished Defender
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
