local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Adawin",
    [ 2] = "Anatyr",
    [ 3] = "Annarinn",
    [ 4] = "Aythya",
    [ 5] = "Brewsterr",
    [ 6] = "Capecrusader",
    [ 7] = "Chalula",
    [ 8] = "Gaylestrum",
    [ 9] = "Greico",
    [10] = "Hadgarthil",
    [11] = "Irontitann",
    [12] = "Jaeran",
    [13] = "Jazzmene",
    [14] = "Lachý",
    [15] = "Mayaell",
    [16] = "Pinn",
    [17] = "Sanif",
    [18] = "Scv",
    [19] = "Slayingfreak",
    [20] = "Snoweyes",
    [21] = "Staacysmom",
    [22] = "Totemflex",
    [23] = "Wisemasterpo",
    [24] = "Xandies",
    [25] = "Yourdadsdad",
  },
  kills = {
    {boss = 601, timestamp = "2022-09-07 17:17", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 602, timestamp = "2022-09-07 17:26", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 603, timestamp = "2022-09-07 17:33", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 607, timestamp = "2022-09-07 17:47", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 608, timestamp = "2022-09-07 18:01", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 609, timestamp = "2022-09-07 18:12", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-09-07_RaidBT2",
  drops = {
    {player =  9, timestamp = "2022-09-07 17:27", item = 32261, costType = "OS"}, -- Greico : Band of the Abyssal Lord
    {player = 24, timestamp = "2022-09-07 17:34", item = 32270, costType = "OS"}, -- Xandies : Focused Mana Bindings
    {player = 21, timestamp = "2022-09-07 17:35", item = 32264, costType = "MS"}, -- Staacysmom : Shoulders of the Hidden Predator
    {player = 22, timestamp = "2022-09-07 17:40", item = 32753, costType = "OS"}, -- Totemflex : Pattern: Swiftheal Mantle
    {player =  1, timestamp = "2022-09-07 17:43", item = 32747, costType = "OS"}, -- Adawin : Pattern: Swiftstrike Shoulders
    {player =  2, timestamp = "2022-09-07 17:52", item = 32526, costType = "MS"}, -- Anatyr : Band of Devastation
    {player = 15, timestamp = "2022-09-07 17:55", item = 32528, costType = "MS"}, -- Mayaell : Blessed Band of Karabor
    {player = 10, timestamp = "2022-09-07 18:03", item = 31101, costType = "MS"}, -- Hadgarthil : Pauldrons of the Forgotten Conqueror
    {player = 23, timestamp = "2022-09-07 18:04", item = 31101, costType = "MS"}, -- Wisemasterpo : Pauldrons of the Forgotten Conqueror
    {player = 22, timestamp = "2022-09-07 18:04", item = 31103, costType = "OS"}, -- Totemflex : Pauldrons of the Forgotten Protector
    {player =  1, timestamp = "2022-09-07 18:14", item = 31099, costType = "MS"}, -- Adawin : Leggings of the Forgotten Vanquisher
    {player = 16, timestamp = "2022-09-07 18:15", item = 31099, costType = "MS"}, -- Pinn : Leggings of the Forgotten Vanquisher
    {player = 22, timestamp = "2022-09-07 18:16", item = 31100, costType = "OS"}, -- Totemflex : Leggings of the Forgotten Protector
    {player = 25, timestamp = "2022-09-07 18:17", item = 32373, costType = "MS"}, -- Yourdadsdad : Helm of the Illidari Shatterer
    {player =  1, timestamp = "2022-09-07 18:18", item = 31090, costType = "MS"}, -- Adawin : Chestguard of the Forgotten Vanquisher
    {player = 16, timestamp = "2022-09-07 18:18", item = 31090, costType = "MS"}, -- Pinn : Chestguard of the Forgotten Vanquisher
    {player = 10, timestamp = "2022-09-07 18:19", item = 31089, costType = "MS"}, -- Hadgarthil : Chestguard of the Forgotten Conqueror
    {player = 19, timestamp = "2022-09-07 18:19", item = 32521, costType = "MS"}, -- Slayingfreak : Faceplate of the Impenetrable
    {player = 18, timestamp = "2022-09-07 18:20", item = 32524, costType = "MS"}, -- Scv : Shroud of the Highborne
    {player =  2, timestamp = "2022-09-07 18:22", item = 32366, costType = "MS"}, -- Anatyr : Shadowmaster's Boots
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
