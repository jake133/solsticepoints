local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Adondah",
    [ 2] = "Arquinas",
    [ 3] = "Capecrusader",
    [ 4] = "Chalula",
    [ 5] = "Eillowee",
    [ 6] = "Elorn",
    [ 7] = "Gaylestrum",
    [ 8] = "Glancen",
    [ 9] = "Grayskyy",
    [10] = "Healsforu",
    [11] = "Inxi",
    [12] = "Irontitann",
    [13] = "Kattianna",
    [14] = "Lachý",
    [15] = "Maërlyn",
    [16] = "Mclitty",
    [17] = "Meacha",
    [18] = "Noritotes",
    [19] = "Pencilvestor",
    [20] = "Roragorn",
    [21] = "Snoweyes",
    [22] = "Superboots",
    [23] = "Tinycurse",
    [24] = "Wootzz",
    [25] = "Xandies",
  },
  kills = {
    {boss = 618, timestamp = "2022-04-25 19:13", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 619, timestamp = "2022-04-25 19:27", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 620, timestamp = "2022-04-25 19:56", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 621, timestamp = "2022-04-25 20:19", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 622, timestamp = "2022-04-25 20:34", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-04-25_Hyjal",
  drops = {
    {player = 12, timestamp = "2022-04-25 19:31", item = 30869, costType = "OS"}, -- Irontitann : Howling Wind Bracers
    {player = 16, timestamp = "2022-04-25 19:32", item = 30864, costType = "OS"}, -- Mclitty : Bracers of the Pathfinder
    {player = 10, timestamp = "2022-04-25 19:33", item = 32609, costType = "MS"}, -- Healsforu : Boots of the Divine Light
    {player = 23, timestamp = "2022-04-25 19:34", item = 30884, cost = 60, costType = "Solstice Points"}, -- Tinycurse : Hatefury Mantle
    {player = 21, timestamp = "2022-04-25 19:34", item = 30879, costType = "MS"}, -- Snoweyes : Don Alejandro's Money Belt
    {player = 12, timestamp = "2022-04-25 20:35", item = 32592, costType = "OS"}, -- Irontitann : Chestguard of Relentless Storms
    {player =  1, timestamp = "2022-04-25 20:36", item = 30918, costType = "MS"}, -- Adondah : Hammer of Atonement
    {player = 17, timestamp = "2022-04-25 20:37", item = 34010, costType = "OS"}, -- Meacha : Pepe's Shroud of Pacification
    {player =  1, timestamp = "2022-04-25 20:37", item = 30899, cost = 60, costType = "Solstice Points"}, -- Adondah : Don Rodrigo's Poncho
    {player =  9, timestamp = "2022-04-25 20:38", item = 31093, cost = 80, costType = "Solstice Points"}, -- Grayskyy : Gloves of the Forgotten Vanquisher
    {player = 11, timestamp = "2022-04-25 20:39", item = 31093, cost = 80, costType = "Solstice Points"}, -- Inxi : Gloves of the Forgotten Vanquisher
    {player = 16, timestamp = "2022-04-25 20:40", item = 30907, costType = "OS"}, -- Mclitty : Mail of Fevered Pursuit
    {player = 22, timestamp = "2022-04-25 20:40", item = 30912, cost = 60, costType = "Solstice Points"}, -- Superboots : Leggings of Eternity
    {player = 24, timestamp = "2022-04-25 20:41", item = 31097, cost = 80, costType = "Solstice Points"}, -- Wootzz : Helm of the Forgotten Conqueror
    {player = 25, timestamp = "2022-04-25 20:42", item = 31095, cost = 80, costType = "Solstice Points"}, -- Xandies : Helm of the Forgotten Protector
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
