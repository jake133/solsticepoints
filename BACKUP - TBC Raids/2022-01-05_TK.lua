local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aythya",
    [ 3] = "Aziza",
    [ 4] = "Brickk",
    [ 5] = "Capecrusader",
    [ 6] = "Cholula",
    [ 7] = "Eillowee",
    [ 8] = "Elorn",
    [ 9] = "Frodes",
    [10] = "Gaylestrum",
    [11] = "Ironankh",
    [12] = "Irontitanhc",
    [13] = "Jazzmean",
    [14] = "Kharlamagne",
    [15] = "Kyrika",
    [16] = "Lachy",
    [17] = "Luethien",
    [18] = "Pencilvestor",
    [19] = "Roragorn",
    [20] = "Sinniaa",
    [21] = "Sneakydoodle",
    [22] = "Superboots",
    [23] = "Varv",
    [24] = "Whitewidow",
    [25] = "Wootzz",
  },
  kills = {
    {boss = 730, timestamp = "2022-01-05 19:31", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 731, timestamp = "2022-01-05 19:57", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 732, timestamp = "2022-01-05 20:16", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-01-05_TK",
  drops = {
    {player = 11, timestamp = "2022-01-05 19:15", item = 30026, cost = "", costType = "MS"}, -- Ironankh : Bands of the Celestial Archer
    {player = 21, timestamp = "2022-01-05 19:32", item = 32944, cost = "70", costType = "Solstice Points"}, -- Sneakydoodle : Talon of the Phoenix
    {player =  4, timestamp = "2022-01-05 19:35", item = 30323, cost = "", costType = "MS"}, -- Brickk : Plans: Boots of the Protector
    {player = 14, timestamp = "2022-01-05 19:38", item = 29921, cost = "", costType = "OS"}, -- Kharlamagne : Fire Crest Breastplate
    {player = 12, timestamp = "2022-01-05 19:41", item = 30324, cost = "", costType = "OS"}, -- Irontitanhc : Plans: Red Havoc Boots
    {player = 10, timestamp = "2022-01-05 19:57", item = 30282, cost = "10", costType = "Solstice Points"}, -- Gaylestrum : Pattern: Boots of Blasting
    {player = 12, timestamp = "2022-01-05 19:58", item = 29983, cost = "", costType = "OS"}, -- Irontitanhc : Fel-Steel Warhelm
    {player = 14, timestamp = "2022-01-05 19:59", item = 30248, cost = "", costType = "MS"}, -- Kharlamagne : Pauldrons of the Vanquished Champion
    {player = 11, timestamp = "2022-01-05 20:00", item = 30248, cost = "", costType = "MS"}, -- Ironankh : Pauldrons of the Vanquished Champion
    {player = 24, timestamp = "2022-01-05 20:02", item = 30301, cost = "", costType = "MS"}, -- Whitewidow : Pattern: Belt of Natural Power
    {player = 24, timestamp = "2022-01-05 20:04", item = 30306, cost = "", costType = "OS"}, -- Whitewidow : Pattern: Boots of Utter Darkness
    {player = 15, timestamp = "2022-01-05 20:06", item = 30020, cost = "", costType = "MS"}, -- Kyrika : Fire-Cord of the Magus
    {player =  7, timestamp = "2022-01-05 20:08", item = 30020, cost = "", costType = "OS"}, -- Eillowee : Fire-Cord of the Magus
    {player = 14, timestamp = "2022-01-05 20:10", item = 30028, cost = "", costType = "OS"}, -- Kharlamagne : Seventh Ring of the Tirisfalen
    {player = 21, timestamp = "2022-01-05 20:20", item = 29962, cost = "", costType = "MS"}, -- Sneakydoodle : Heartrazor
    {player = 14, timestamp = "2022-01-05 20:35", item = 29976, cost = "", costType = "OS"}, -- Kharlamagne : Worldstorm Gauntlets
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
