local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aythya",
    [ 3] = "Aziza",
    [ 4] = "Capecrusader",
    [ 5] = "Cholula",
    [ 6] = "Eillowee",
    [ 7] = "Elorn",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Ironankh",
    [11] = "Irontitanhc",
    [12] = "Jazzmean",
    [13] = "Lachy",
    [14] = "Pencilvestor",
    [15] = "Pulchra",
    [16] = "Roragorn",
    [17] = "Sadistia",
    [18] = "Sinniaa",
    [19] = "Sinwave",
    [20] = "Slayingfreak",
    [21] = "Sneakydoodle",
    [22] = "Superboots",
    [23] = "Varv",
    [24] = "Velladonna",
    [25] = "Whitewidow",
  },
  kills = {
    {boss = 601, timestamp = "2022-02-15 19:20", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 602, timestamp = "2022-02-15 20:00", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 603, timestamp = "2022-02-15 20:32", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 604, timestamp = "2022-02-15 21:18", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-02-15_BT",
  drops = {
    {player =  9, timestamp = "2022-02-15 20:01", item = 32247, cost = "", costType = "MS"}, -- Gaylestrum : Ring of Captured Storms
    {player = 10, timestamp = "2022-02-15 20:02", item = 32234, cost = "", costType = "MS"}, -- Ironankh : Fists of Mukoa
    {player = 15, timestamp = "2022-02-15 20:02", item = 32254, cost = "", costType = "OS"}, -- Pulchra : The Brutalizer
    {player =  5, timestamp = "2022-02-15 20:03", item = 32257, cost = "", costType = "MS"}, -- Cholula : Idol of the White Stag
    {player = 11, timestamp = "2022-02-15 20:32", item = 32264, cost = "", costType = "OS"}, -- Irontitanhc : Shoulders of the Hidden Predator
    {player = 15, timestamp = "2022-02-15 20:33", item = 32273, cost = "", costType = "OS"}, -- Pulchra : Amice of Brilliant Light
    {player = 22, timestamp = "2022-02-15 20:34", item = 32753, cost = "", costType = "MS"}, -- Superboots : Pattern: Swiftheal Mantle
    {player =  4, timestamp = "2022-02-15 22:05", item = 32326, cost = "", costType = "MS"}, -- Capecrusader : Twisted Blades of Zarak
    {player = 10, timestamp = "2022-02-15 22:05", item = 32510, cost = 60, costType = "Solstice Points"}, -- Ironankh : Softstep Boots of Tracking
    {player = 15, timestamp = "2022-02-15 22:06", item = 32526, cost = 70, costType = "Solstice Points"}, -- Pulchra : Band of Devastation
    {player = 17, timestamp = "2022-02-15 22:07", item = 32527, cost = 70, costType = "Solstice Points"}, -- Sadistia : Ring of Ancient Knowledge
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
