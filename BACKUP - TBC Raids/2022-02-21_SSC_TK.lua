local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aythya",
    [ 3] = "Blackclaws",
    [ 4] = "Capecrusader",
    [ 5] = "Cholula",
    [ 6] = "Damelion",
    [ 7] = "Eillowee",
    [ 8] = "Elorn",
    [ 9] = "Furiozah",
    [10] = "Gaylestrum",
    [11] = "Glance",
    [12] = "Grayskyy",
    [13] = "Helletrix",
    [14] = "Ironankh",
    [15] = "Irontitanhc",
    [16] = "Kharlamagne",
    [17] = "Kyrika",
    [18] = "Lachy",
    [19] = "Pencilvestor",
    [20] = "Roragorn",
    [21] = "Sadistia",
    [22] = "Sinniaa",
    [23] = "Sneakydoodle",
    [24] = "Varv",
    [25] = "Whitewidow",
    [26] = "Wootzz",
    [27] = "Xandie",
  },
  kills = {
    {boss = 628, timestamp = "2022-02-21 19:31", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,22,23,24,25,26,27}},
    {boss = 627, timestamp = "2022-02-21 19:51", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,22,23,24,25,26,27}},
    {boss = 626, timestamp = "2022-02-21 20:17", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,22,23,24,25,26,27}},
    {boss = 625, timestamp = "2022-02-21 20:31", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,22,23,24,25,26,27}},
    {boss = 730, timestamp = "2022-02-21 21:31", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,22,23,24,25,26,27}},
    {boss = 732, timestamp = "2022-02-21 21:47", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27}},
    {boss = 733, timestamp = "2022-02-21 22:14", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27}},
  },
  description = "2022-02-21_SSC_TK",
  drops = {
    {player =  2, timestamp = "2022-02-21 19:33", item = 30105, cost = "", costType = "MS"}, -- Aythya : Serpent Spine Longbow
    {player =  7, timestamp = "2022-02-21 19:34", item = 30107, cost = 30, costType = "Solstice Points"}, -- Eillowee : Vestments of the Sea-Witch
    {player = 14, timestamp = "2022-02-21 19:34", item = 30242, cost = 40, costType = "Solstice Points"}, -- Ironankh : Helm of the Vanquished Champion
    {player = 18, timestamp = "2022-02-21 19:35", item = 30244, cost = 40, costType = "Solstice Points"}, -- Lachy : Helm of the Vanquished Hero
    {player =  9, timestamp = "2022-02-21 19:36", item = 30243, cost = 40, costType = "Solstice Points"}, -- Furiozah : Helm of the Vanquished Defender
    {player = 10, timestamp = "2022-02-21 19:51", item = 30720, cost = "", costType = "MS"}, -- Gaylestrum : Serpent-Coil Braid
    {player = 11, timestamp = "2022-02-21 19:52", item = 30080, cost = "", costType = "MS"}, -- Glance : Luminescent Rod of the Naaru
    {player =  3, timestamp = "2022-02-21 19:58", item = 30022, cost = "", costType = "MS"}, -- Blackclaws : Pendant of the Perilous
    {player = 12, timestamp = "2022-02-21 20:00", item = 30025, cost = "", costType = "MS"}, -- Grayskyy : Serpentshrine Shuriken
    {player = 27, timestamp = "2022-02-21 20:32", item = 30239, cost = 40, costType = "Solstice Points"}, -- Xandie : Gloves of the Vanquished Champion
    {player =  6, timestamp = "2022-02-21 20:36", item = 30239, cost = 40, costType = "Solstice Points"}, -- Damelion : Gloves of the Vanquished Champion
    {player = 17, timestamp = "2022-02-21 20:37", item = 30241, cost = "", costType = "OS"}, -- Kyrika : Gloves of the Vanquished Hero
    {player =  2, timestamp = "2022-02-21 20:38", item = 30247, cost = 40, costType = "Solstice Points"}, -- Aythya : Leggings of the Vanquished Hero
    {player =  7, timestamp = "2022-02-21 20:39", item = 30247, cost = "", costType = "OS"}, -- Eillowee : Leggings of the Vanquished Hero
    {player =  9, timestamp = "2022-02-21 20:40", item = 30246, cost = "", costType = "MS"}, -- Furiozah : Leggings of the Vanquished Defender
    {player = 27, timestamp = "2022-02-21 20:41", item = 30097, cost = "", costType = "OS"}, -- Xandie : Coral-Barbed Shoulderpads
    {player = 11, timestamp = "2022-02-21 20:43", item = 30100, cost = "", costType = "MS"}, -- Glance : Soul-Strider Boots
    {player = 14, timestamp = "2022-02-21 21:33", item = 29947, cost = 30, costType = "Solstice Points"}, -- Ironankh : Gloves of the Searing Grip
    {player =  1, timestamp = "2022-02-21 21:34", item = 29925, cost = "", costType = "OS"}, -- Ashgon : Phoenix-Wing Cloak
    {player =  6, timestamp = "2022-02-21 21:35", item = 30447, cost = "", costType = "MS"}, -- Damelion : Tome of Fiery Redemption
    {player = 27, timestamp = "2022-02-21 21:48", item = 29972, cost = "", costType = "OS"}, -- Xandie : Trousers of the Astromancer
    {player = 16, timestamp = "2022-02-21 21:49", item = 29950, cost = "", costType = "OS"}, -- Kharlamagne : Greaves of the Bloodwarder
    {player = 11, timestamp = "2022-02-21 21:56", item = 29981, cost = "", costType = "MS"}, -- Glance : Ethereum Life-Staff
    {player = 18, timestamp = "2022-02-21 22:15", item = 32405, cost = 35, costType = "Solstice Points"}, -- Lachy : Verdant Sphere
    {player =  6, timestamp = "2022-02-21 22:16", item = 29990, cost = "", costType = "MS"}, -- Damelion : Crown of the Sun
    {player = 15, timestamp = "2022-02-21 22:16", item = 29998, cost = "", costType = "MS"}, -- Irontitanhc : Royal Gauntlets of Silvermoon
    {player = 22, timestamp = "2022-02-21 22:17", item = 30237, cost = 40, costType = "Solstice Points"}, -- Sinniaa : Chestguard of the Vanquished Defender
    {player =  9, timestamp = "2022-02-21 22:18", item = 30237, cost = "", costType = "MS"}, -- Furiozah : Chestguard of the Vanquished Defender
    {player = 11, timestamp = "2022-02-21 22:18", item = 30237, cost = "", costType = "MS"}, -- Glance : Chestguard of the Vanquished Defender
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
