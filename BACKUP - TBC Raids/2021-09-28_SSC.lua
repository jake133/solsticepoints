local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Brickk",
    [ 4] = "Capecrusader",
    [ 5] = "Dagarle",
    [ 6] = "Elorn",
    [ 7] = "Estrellita",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Inxi",
    [11] = "Ironflurry",
    [12] = "Irontitanhc",
    [13] = "Kharlamagne",
    [14] = "Kyrika",
    [15] = "Lachy",
    [16] = "Luethien",
    [17] = "Pencilvestor",
    [18] = "Sneakydoodle",
    [19] = "Soulbane",
    [20] = "Superboots",
    [21] = "Sylaramynd",
    [22] = "Velladonna",
    [23] = "Varv",
    [24] = "Whitewidow",
    [25] = "Wootzz",
  },
  kills = {
    {boss = 623, timestamp = "2021-09-28 19:51", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 624, timestamp = "2021-09-28 20:38", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  drops = {
    {player =  21, timestamp = "2021-09-28", item = 30051, cost = "", costType = "MS"}, -- Sylaramynd : Idol of the Crescent Goddess
    {player =  24, timestamp = "2021-09-28", item = 30054, cost = "", costType = "MS"}, -- Whitewidow : Ranger-General's Chestguard
    {player =  25, timestamp = "2021-09-28", item = 30067, cost = "", costType = "MS"}, -- Wootzz : Velvet boots of the guardian
    {player =  5, timestamp = "2021-09-28", item = 30057, cost = "", costType = "MS"}, -- Dagarle : Bracers of Eradication
    {player =  18, timestamp = "2021-09-28", item = 30025, cost = "", costType = "MS"}, -- Sneakydoodle : Serpentshrine Shuriken
    {player =  12, timestamp = "2021-09-28", item = 30022, cost = "", costType = "OS"}, -- Irontitanhc : Pendant of the Perilous
    },
  description = "2021-09-28_SSC",
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
