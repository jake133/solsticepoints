local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Annarinn",
    [ 3] = "Aythya",
    [ 4] = "Bellamira",
    [ 5] = "Brewsterr",
    [ 6] = "Capecrusader",
    [ 7] = "Ceresei",
    [ 8] = "Chalula",
    [ 9] = "Chamwow",
    [10] = "Daddygodsu",
    [11] = "Eillowee",
    [12] = "Fellador",
    [13] = "Gaylestrum",
    [14] = "Graeskyy",
    [15] = "Hadgarthil",
    [16] = "Irontitann",
    [17] = "Jazzmene",
    [18] = "Manzig",
    [19] = "Mayaell",
    [20] = "Melindara",
    [21] = "Momjeanz",
    [22] = "Pallylove",
    [23] = "Slayingfreak",
    [24] = "Snoweyes",
    [25] = "Xandies",
  },
  kills = {
    {boss = 724, timestamp = "2022-09-20 19:17", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 725, timestamp = "2022-09-20 19:27", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 726, timestamp = "2022-09-20 19:49", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 727, timestamp = "2022-09-20 20:09", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 728, timestamp = "2022-09-20 20:23", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 729, timestamp = "2022-09-20 21:32", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-09-20_RaidSWP",
  drops = {
    {player = 21, timestamp = "2022-09-20 20:05", item = 35206, costType = "MS"}, -- Momjeanz : Pattern: Sunfire Robe
    {player =  6, timestamp = "2022-09-20 20:53", item = 34427, cost = 70, costType = "Solstice Points"}, -- Capecrusader : Blackened Naaru Sliver
    {player = 23, timestamp = "2022-09-20 20:54", item = 34185, costType = "MS"}, -- Slayingfreak : Sword Breaker's Bulwark
    {player = 19, timestamp = "2022-09-20 20:55", item = 34852, costType = "MS"}, -- Mayaell : Bracers of the Forgotten Vanquisher
    {player = 16, timestamp = "2022-09-20 20:56", item = 34848, cost = 80, costType = "Solstice Points"}, -- Irontitann : Bracers of the Forgotten Conqueror
    {player = 25, timestamp = "2022-09-20 20:57", item = 34851, cost = 80, costType = "Solstice Points"}, -- Xandies : Bracers of the Forgotten Protector
    {player =  9, timestamp = "2022-09-20 20:58", item = 34851, cost = 80, costType = "Solstice Points"}, -- Chamwow : Bracers of the Forgotten Protector
    {player =  7, timestamp = "2022-09-20 20:59", item = 34165, costType = "OS"}, -- Ceresei : Fang of Kalecgos
    {player = 14, timestamp = "2022-09-20 20:59", item = 34177, costType = "MS"}, -- Graeskyy : Clutch of Demise
    {player =  8, timestamp = "2022-09-20 21:01", item = 34855, cost = 80, costType = "Solstice Points"}, -- Chalula : Belt of the Forgotten Vanquisher
    {player = 25, timestamp = "2022-09-20 21:01", item = 34854, cost = 80, costType = "Solstice Points"}, -- Xandies : Belt of the Forgotten Protector
    {player =  3, timestamp = "2022-09-20 21:02", item = 34854, costType = "MS"}, -- Aythya : Belt of the Forgotten Protector
    {player =  5, timestamp = "2022-09-20 21:02", item = 34857, costType = "MS"}, -- Brewsterr : Boots of the Forgotten Protector
    {player = 24, timestamp = "2022-09-20 21:03", item = 34857, costType = "MS"}, -- Snoweyes : Boots of the Forgotten Protector
    {player = 11, timestamp = "2022-09-20 21:04", item = 34856, cost = 80, costType = "Solstice Points"}, -- Eillowee : Boots of the Forgotten Conqueror
    {player =  5, timestamp = "2022-09-20 21:05", item = 34856, costType = "OS"}, -- Brewsterr : Boots of the Forgotten Conqueror
    {player =  9, timestamp = "2022-09-20 21:12", item = 34206, cost = 70, costType = "Solstice Points"}, -- Chamwow : Book of Highborne Hymns
    {player = 14, timestamp = "2022-09-20 21:13", item = 34214, cost = 70, costType = "Solstice Points"}, -- Graeskyy : Muramasa
    {player = 15, timestamp = "2022-09-20 21:14", item = 34199, costType = "MS"}, -- Hadgarthil : Archon's Gavel
    {player = 13, timestamp = "2022-09-20 21:14", item = 35283, costType = "MS"}, -- Gaylestrum : Sin'dorei Band of Salvation
    {player =  7, timestamp = "2022-09-20 21:22", item = 34195, costType = "MS"}, -- Ceresei : Shoulderpads of Vehemence
    {player = 18, timestamp = "2022-09-20 21:23", item = 34210, costType = "MS"}, -- Manzig : Amice of the Convoker
    {player = 22, timestamp = "2022-09-20 21:24", item = 34240, costType = "MS"}, -- Pallylove : Gauntlets of the Soothed Soul
    {player =  4, timestamp = "2022-09-20 21:24", item = 35290, costType = "MS"}, -- Bellamira : Sin'dorei Pendant of Conquest
    {player = 13, timestamp = "2022-09-20 21:33", item = 34336, cost = 70, costType = "Solstice Points"}, -- Gaylestrum : Sunflare
    {player =  3, timestamp = "2022-09-20 21:34", item = 34333, cost = 60, costType = "Solstice Points"}, -- Aythya : Coif of Alleria
    {player = 10, timestamp = "2022-09-20 21:35", item = 34244, costType = "MS"}, -- Daddygodsu : Duplicitous Guise
    {player = 23, timestamp = "2022-09-20 21:38", item = 34345, cost = 60, costType = "Solstice Points"}, -- Slayingfreak : Crown of Anasterian
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
