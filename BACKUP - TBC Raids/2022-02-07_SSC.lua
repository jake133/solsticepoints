local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aythya",
    [ 3] = "Blackclaws",
    [ 4] = "Cholula",
    [ 5] = "Eillowee",
    [ 6] = "Elorn",
    [ 7] = "Furiozah",
    [ 8] = "Gaylestrum",
    [ 9] = "Grayskyy",
    [10] = "Helletrix",
    [11] = "Ironankh",
    [12] = "Irontitanhc",
    [13] = "Kharlamagne",
    [14] = "Kyrika",
    [15] = "Lachy",
    [16] = "Lilybet",
    [17] = "Pencilvestor",
    [18] = "Pulchra",
    [19] = "Roragorn",
    [20] = "Sadistia",
    [22] = "Sinniaa",
    [23] = "Sneakydoodle",
    [24] = "Superboots",
    [25] = "Varv",
    [26] = "Velladonna",
    [27] = "Whitewidow",
    [28] = "Wootzz",
  },
  kills = {
    {boss = 628, timestamp = "2022-02-07 19:42", players = {1,2,3,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19,22,23,24,25,26,27,28}},
    {boss = 627, timestamp = "2022-02-07 20:01", players = {1,2,3,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19,22,23,24,25,26,27,28}},
    {boss = 626, timestamp = "2022-02-07 20:30", players = {1,2,3,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19,22,23,24,25,26,27,28}},
    {boss = 625, timestamp = "2022-02-07 20:52", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,22,23,24,25,26,27,28}},
    {boss = 732, timestamp = "2022-02-07 21:15", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,22,23,24,25,26,27,28}},
    {boss = 731, timestamp = "2022-02-07 21:37", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,22,23,24,25,26,27,28}},
  },
  description = "2022-02-07_SSC",
  drops = {
    {player = 18, timestamp = "2022-02-07 20:03", item = 30081, cost = "", costType = "MS"}, -- Pulchra : Warboots of Obliteration
    {player = 22, timestamp = "2022-02-07 20:04", item = 33058, cost = "", costType = "OS"}, -- Sinniaa : Band of the Vigilant
    {player = 18, timestamp = "2022-02-07 20:05", item = 30022, cost = 35, costType = "Solstice Points"}, -- Pulchra : Pendant of the Perilous
    {player =  4, timestamp = "2022-02-07 20:06", item = 30243, cost = 40, costType = "Solstice Points"}, -- Cholula : Helm of the Vanquished Defender
    {player = 22, timestamp = "2022-02-07 20:07", item = 30243, cost = 40, costType = "Solstice Points"}, -- Sinniaa : Helm of the Vanquished Defender
    {player =  3, timestamp = "2022-02-07 20:07", item = 30243, cost = 40, costType = "Solstice Points"}, -- Blackclaws : Helm of the Vanquished Defender
    {player = 14, timestamp = "2022-02-07 20:09", item = 30621, cost = "", costType = "OS"}, -- Kyrika : Prism of Inner Calm
    {player = 19, timestamp = "2022-02-07 20:10", item = 30107, cost = 30, costType = "Solstice Points"}, -- Roragorn : Vestments of the Sea-Witch
    {player = 10, timestamp = "2022-02-07 20:11", item = 30023, cost = "", costType = "OS"}, -- Helletrix : Totem of the Maelstrom
    {player = 19, timestamp = "2022-02-07 20:37", item = 30246, cost = 40, costType = "Solstice Points"}, -- Roragorn : Leggings of the Vanquished Defender
    {player =  3, timestamp = "2022-02-07 20:37", item = 30246, cost = "", costType = "MS"}, -- Blackclaws : Leggings of the Vanquished Defender
    {player =  1, timestamp = "2022-02-07 20:38", item = 30245, cost = 40, costType = "Solstice Points"}, -- Ashgon : Leggings of the Vanquished Champion
    {player = 10, timestamp = "2022-02-07 20:40", item = 30079, cost = "", costType = "OS"}, -- Helletrix : Illidari Shoulderpads
    {player = 13, timestamp = "2022-02-07 20:44", item = 30626, cost = 35, costType = "Solstice Points"}, -- Kharlamagne : Sextant of Unstable Currents
    {player = 28, timestamp = "2022-02-07 21:17", item = 30241, cost = "", costType = "OS"}, -- Wootzz : Gloves of the Vanquished Hero
    {player =  5, timestamp = "2022-02-07 21:17", item = 30241, cost = "", costType = "OS"}, -- Eillowee : Gloves of the Vanquished Hero
    {player =  7, timestamp = "2022-02-07 21:18", item = 30240, cost = "", costType = "MS"}, -- Furiozah : Gloves of the Vanquished Defender
    {player = 10, timestamp = "2022-02-07 21:19", item = 30097, cost = "", costType = "MS"}, -- Helletrix : Coral-Barbed Shoulderpads
    {player = 27, timestamp = "2022-02-07 21:20", item = 30026, cost = "", costType = "MS"}, -- Whitewidow : Bands of the Celestial Archer
    {player = 13, timestamp = "2022-02-07 21:21", item = 32267, cost = "", costType = "OS"}, -- Kharlamagne : Boots of the Resilient
    {player = 13, timestamp = "2022-02-07 21:21", item = 29965, cost = "", costType = "MS"}, -- Kharlamagne : Girdle of the Righteous Path
    {player =  7, timestamp = "2022-02-07 21:21", item = 30446, cost = "", costType = "MS"}, -- Furiozah : Solarian's Sapphire
    {player =  8, timestamp = "2022-02-07 21:54", item = 30250, cost = "", costType = "OS"}, -- Gaylestrum : Pauldrons of the Vanquished Hero
    {player = 28, timestamp = "2022-02-07 21:55", item = 30250, cost = "", costType = "OS"}, -- Wootzz : Pauldrons of the Vanquished Hero
    {player =  7, timestamp = "2022-02-07 21:55", item = 30249, cost = "", costType = "MS"}, -- Furiozah : Pauldrons of the Vanquished Defender
    {player = 11, timestamp = "2022-02-07 21:56", item = 29985, cost = "", costType = "MS"}, -- Ironankh : Void Reaver Greaves
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
