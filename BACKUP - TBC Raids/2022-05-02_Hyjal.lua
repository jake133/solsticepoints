local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Aythya",
    [ 2] = "Candrabella",
    [ 3] = "Capecrusader",
    [ 4] = "Chalula",
    [ 5] = "Dareye",
    [ 6] = "Eillowee",
    [ 7] = "Elorn",
    [ 8] = "Flameburst",
    [ 9] = "Furiozah",
    [10] = "Glancen",
    [11] = "Grayskyy",
    [12] = "Inxi",
    [13] = "Irontitann",
    [14] = "Kattianna",
    [15] = "Lachý",
    [16] = "Luethien",
    [17] = "Mclitty",
    [18] = "Meacha",
    [19] = "Pencilvestor",
    [20] = "Roragorn",
    [21] = "Snoweyes",
    [22] = "Tinycurse",
    [23] = "Velexi",
    [24] = "Wootzz",
    [25] = "Xandies",
  },
  kills = {
    {boss = 618, timestamp = "2022-05-02 19:25", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 619, timestamp = "2022-05-02 19:40", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 620, timestamp = "2022-05-02 20:05", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 621, timestamp = "2022-05-02 21:10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 622, timestamp = "2022-05-02 21:48", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-05-02_Hyjal",
  drops = {
    {player =  9, timestamp = "2022-05-02 19:42", item = 30866, costType = "MS"}, -- Furiozah : Blood-stained Pauldrons
    {player = 23, timestamp = "2022-05-02 19:43", item = 30869, costType = "OS"}, -- Velexi : Howling Wind Bracers
    {player = 14, timestamp = "2022-05-02 19:44", item = 30888, costType = "MS"}, -- Kattianna : Anetheron's Noose
    {player = 13, timestamp = "2022-05-02 19:45", item = 30878, costType = "OS"}, -- Irontitann : Glimmering Steel Mantle
    {player = 25, timestamp = "2022-05-02 20:44", item = 30893, costType = "OS"}, -- Xandies : Sun-touched Chain Leggings
    {player =  6, timestamp = "2022-05-02 20:45", item = 30894, cost = 60, costType = "Solstice Points"}, -- Eillowee : Blue Suede Shoes
    {player =  9, timestamp = "2022-05-02 20:46", item = 32591, cost = 70, costType = "Solstice Points"}, -- Furiozah : Choker of Serrated Blades
    {player = 17, timestamp = "2022-05-02 20:47", item = 34009, costType = "OS"}, -- Mclitty : Hammer of Judgement
    {player =  9, timestamp = "2022-05-02 21:48", item = 30901, costType = "MS"}, -- Furiozah : Boundless Agony
    {player = 15, timestamp = "2022-05-02 21:49", item = 31093, costType = "MS"}, -- Lachý : Gloves of the Forgotten Vanquisher
    {player =  8, timestamp = "2022-05-02 21:49", item = 31093, costType = "MS"}, -- Flameburst : Gloves of the Forgotten Vanquisher
    {player = 20, timestamp = "2022-05-02 21:51", item = 31096, cost = 80, costType = "Solstice Points"}, -- Roragorn : Helm of the Forgotten Vanquisher
    {player =  1, timestamp = "2022-05-02 21:52", item = 31095, cost = 80, costType = "Solstice Points"}, -- Aythya : Helm of the Forgotten Protector
    {player = 13, timestamp = "2022-05-02 21:52", item = 30909, costType = "MS"}, -- Irontitann : Antonidas's Aegis of Rapt Concentration
    {player = 13, timestamp = "2022-05-02 21:53", item = 30904, costType = "OS"}, -- Irontitann : Savior's Grasp
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
