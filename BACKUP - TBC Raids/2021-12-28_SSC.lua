local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
	[1]="Ashgon",
	[2]="Aythya",
	[3]="Aziza",
	[4]="Boneappletea",
	[5]="Brickk",
	[6]="Capecrusader",
	[7]="Cholula",
	[8]="Eillowee",
	[9]="Elorn",
	[10]="Frodes",
	[11]="Gaylestrum",
	[12]="Irontitanhc",
	[13]="Keeganatotem",
	[14]="Kharlamagne",
	[15]="Kyrika",
	[16]="Lachy",
	[17]="Luethien",
	[18]="Pencilvestor",
	[19]="Roragorn",
	[20]="Sinwave",
	[21]="Superboots",
	[22]="Varv",
	[23]="Velladonna",
	[24]="Whitewidow",
	[25]="Wootzz",

  },
  kills = {
    {boss = 623, timestamp = "2021-12-28", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 624, timestamp = "2021-12-28", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2021-12-28_SSC",
  drops = {
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
