local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Aythya",
    [ 3] = "Blackcläws",
    [ 4] = "Capecrusader",
    [ 5] = "Chalula",
    [ 6] = "Eillowee",
    [ 7] = "Elorn",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Irontitann",
    [11] = "Jazzmene",
    [12] = "Kattianna",
    [13] = "Kekett",
    [14] = "Lachý",
    [15] = "Maërlyn",
    [16] = "Mclitty",
    [17] = "Melisea",
    [18] = "Pencilvestor",
    [19] = "Roragorn",
    [20] = "Sadistia",
    [21] = "Slayingfreak",
    [22] = "Snoweyes",
    [23] = "Snozzberry",
    [24] = "Superboots",
    [25] = "Xandies",
  },
  kills = {
    {boss = 601, timestamp = "2022-05-03 19:25", players = {2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 602, timestamp = "2022-05-03 19:46", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 603, timestamp = "2022-05-03 20:05", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 604, timestamp = "2022-05-03 20:28", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 606, timestamp = "2022-05-03 20:55", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 605, timestamp = "2022-05-03 21:17", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 607, timestamp = "2022-05-03 21:50", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-05-03_BT1",
  drops = {
    {player = 14, timestamp = "2022-05-03 20:06", item = 32237, cost = 70, costType = "Solstice Points"}, -- Lachý : The Maelstrom's Fury
    {player =  7, timestamp = "2022-05-03 20:07", item = 32260, costType = "MS"}, -- Elorn : Choker of Endless Nightmares
    {player =  5, timestamp = "2022-05-03 20:07", item = 32252, costType = "OS"}, -- Chalula : Nether Shadow Tunic
    {player = 17, timestamp = "2022-05-03 20:08", item = 34011, costType = "OS"}, -- Melisea : Illidari Runeshield
    {player =  1, timestamp = "2022-05-03 20:09", item = 32278, costType = "MS"}, -- Anatyr : Grips of Silent Justice
    {player = 21, timestamp = "2022-05-03 20:09", item = 32268, costType = "MS"}, -- Slayingfreak : Myrmidon's Treads
    {player = 23, timestamp = "2022-05-03 21:18", item = 32328, cost = 60, costType = "Solstice Points"}, -- Snozzberry : Botanist's Gloves of Growth
    {player = 13, timestamp = "2022-05-03 21:19", item = 32324, costType = "MS"}, -- Kekett : Insidious Bands
    {player = 10, timestamp = "2022-05-03 21:22", item = 32362, costType = "MS"}, -- Irontitann : Pendant of Titans
    {player = 24, timestamp = "2022-05-03 21:24", item = 32363, costType = "MS"}, -- Superboots : Naaru-Blessed Life Rod
    {player =  2, timestamp = "2022-05-03 21:25", item = 32269, costType = "OS"}, -- Aythya : Messenger of Fate
    {player =  6, timestamp = "2022-05-03 21:26", item = 32343, cost = 70, costType = "Solstice Points"}, -- Eillowee : Wand of Prismatic Focus
    {player = 16, timestamp = "2022-05-03 21:51", item = 34011, costType = "OS"}, -- Mclitty : Illidari Runeshield
    {player = 21, timestamp = "2022-05-03 21:51", item = 32365, costType = "OS"}, -- Slayingfreak : Heartshatter Breastplate
    {player = 25, timestamp = "2022-05-03 21:52", item = 31103, cost = 80, costType = "Solstice Points"}, -- Xandies : Pauldrons of the Forgotten Protector
    {player =  3, timestamp = "2022-05-03 21:52", item = 31103, cost = 80, costType = "Solstice Points"}, -- Blackcläws : Pauldrons of the Forgotten Protector
    {player = 25, timestamp = "2022-05-03 22:11", item = 32247, cost = 70, costType = "Solstice Points"}, -- Xandies : Ring of Captured Storms
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
