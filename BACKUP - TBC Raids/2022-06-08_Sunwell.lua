local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Annarinn",
    [ 3] = "Aythya",
    [ 4] = "Capecrusader",
    [ 5] = "Chalula",
    [ 6] = "Chamwow",
    [ 7] = "Eillowee",
    [ 8] = "Elorn",
    [ 9] = "Gaylestrum",
    [10] = "Inxi",
    [11] = "Irontitann",
    [12] = "Jazzmene",
    [13] = "Kattianna",
    [14] = "Kharlamagne",
    [15] = "Luethien",
    [16] = "Melisea",
    [17] = "Noritotes",
    [18] = "Pencilvestor",
    [19] = "Roragorn",
    [20] = "Sadistia",
    [21] = "Slayingfreak",
    [22] = "Snoweyes",
    [23] = "Superboots",
    [24] = "Wootzz",
    [25] = "Xandies",
  },
  kills = {
    },
  description = "2022-06-08_Sunwell",
  drops = {
    {player = 25, timestamp = "2022-06-08 19:31", item = 34350, cost = 60, costType = "Solstice Points"}, -- Xandies : Gauntlets of the Ancient Shadowmoon
    {player = 12, timestamp = "2022-06-08 21:39", item = 35733, cost = "", costType = "MS"}, -- Jazzmene : Ring of Harmonic Beauty
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
