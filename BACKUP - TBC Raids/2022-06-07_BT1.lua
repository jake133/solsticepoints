local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Annarinn",
    [ 3] = "Aythya",
    [ 4] = "Blackcläws",
    [ 5] = "Capecrusader",
    [ 6] = "Chalula",
    [ 7] = "Eillowee",
    [ 8] = "Elorn",
    [ 9] = "Frodes",
    [10] = "Gaylestrum",
    [11] = "Inxi",
    [12] = "Irontitann",
    [13] = "Jazzmene",
    [14] = "Kattianna",
    [15] = "Kharlamagne",
    [16] = "Luethien",
    [17] = "Melisea",
    [18] = "Noritotes",
    [19] = "Pencilvestor",
    [20] = "Roragorn",
    [21] = "Sadistia",
    [22] = "Slayingfreak",
    [23] = "Snoweyes",
    [24] = "Superboots",
    [25] = "Xandies",
  },
  kills = {
    {boss = 601, timestamp = "2022-06-07 19:16", players = {1,2,3,4,5,6,7,8,9,10,11,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 602, timestamp = "2022-06-07 19:35", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 603, timestamp = "2022-06-07 19:51", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 604, timestamp = "2022-06-07 20:10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 606, timestamp = "2022-06-07 20:30", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 605, timestamp = "2022-06-07 20:49", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 607, timestamp = "2022-06-07 21:11", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 608, timestamp = "2022-06-07 21:33", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 609, timestamp = "2022-06-07 21:55", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-06-07_BT1",
  drops = {
    {player = 16, timestamp = "2022-06-07 19:52", item = 32238, costType = "MS"}, -- Luethien : Ring of Calming Waves
    {player = 14, timestamp = "2022-06-07 19:53", item = 32239, costType = "OS"}, -- Kattianna : Slippers of the Seacaller
    {player =  1, timestamp = "2022-06-07 19:55", item = 32260, cost = 70, costType = "Solstice Points"}, -- Anatyr : Choker of Endless Nightmares
    {player =  7, timestamp = "2022-06-07 20:31", item = 32361, costType = "MS"}, -- Eillowee : Blind-Seers Icon
    {player = 16, timestamp = "2022-06-07 20:31", item = 32363, costType = "MS"}, -- Luethien : Naaru-Blessed Life Rod
    {player =  5, timestamp = "2022-06-07 20:32", item = 32324, cost = 60, costType = "Solstice Points"}, -- Capecrusader : Insidious Bands
    {player =  1, timestamp = "2022-06-07 20:32", item = 32348, costType = "MS"}, -- Anatyr : Soul Cleaver
    {player =  8, timestamp = "2022-06-07 20:34", item = 32346, costType = "MS"}, -- Elorn : Boneweave Girdle
    {player =  3, timestamp = "2022-06-07 20:53", item = 32266, costType = "MS"}, -- Aythya : Ring of Deceitful Intent
    {player = 10, timestamp = "2022-06-07 21:56", item = 32343, costType = "OS"}, -- Gaylestrum : Wand of Prismatic Focus
    {player =  9, timestamp = "2022-06-07 21:56", item = 32337, costType = "MS"}, -- Frodes : Shroud of Forgiveness
    {player = 20, timestamp = "2022-06-07 21:57", item = 32367, costType = "OS"}, -- Roragorn : Leggings of Devastation
    {player =  7, timestamp = "2022-06-07 21:58", item = 31098, cost = 80, costType = "Solstice Points"}, -- Eillowee : Leggings of the Forgotten Conqueror
    {player =  4, timestamp = "2022-06-07 21:58", item = 31100, costType = "MS"}, -- Blackcläws : Leggings of the Forgotten Protector
    {player = 22, timestamp = "2022-06-07 21:58", item = 31100, costType = "MS"}, -- Slayingfreak : Leggings of the Forgotten Protector
    {player = 22, timestamp = "2022-06-07 21:59", item = 31103, costType = "OS"}, -- Slayingfreak : Pauldrons of the Forgotten Protector
    {player =  7, timestamp = "2022-06-07 21:59", item = 31101, cost = 80, costType = "Solstice Points"}, -- Eillowee : Pauldrons of the Forgotten Conqueror
    {player = 10, timestamp = "2022-06-07 22:00", item = 31102, costType = "MS"}, -- Gaylestrum : Pauldrons of the Forgotten Vanquisher
    {player =  9, timestamp = "2022-06-07 22:00", item = 32519, costType = "MS"}, -- Frodes : Belt of Divine Guidance
    {player = 19, timestamp = "2022-06-07 22:01", item = 32235, cost = 60, costType = "Solstice Points"}, -- Pencilvestor : Cursed Vision of Sargeras
    {player = 15, timestamp = "2022-06-07 22:02", item = 32496, cost = 70, costType = "Solstice Points"}, -- Kharlamagne : Memento of Tyrande
    {player = 13, timestamp = "2022-06-07 22:03", item = 31089, cost = 80, costType = "Solstice Points"}, -- Jazzmene : Chestguard of the Forgotten Conqueror
    {player =  3, timestamp = "2022-06-07 22:03", item = 31091, cost = 80, costType = "Solstice Points"}, -- Aythya : Chestguard of the Forgotten Protector
    {player = 14, timestamp = "2022-06-07 22:03", item = 31090, cost = 80, costType = "Solstice Points"}, -- Kattianna : Chestguard of the Forgotten Vanquisher
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
