local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Brickk",
    [ 4] = "Capecrusader",
    [ 5] = "Ceruwolfe",
    [ 6] = "Dagarle",
    [ 7] = "Elorn",
    [ 8] = "Estrellita",
    [ 9] = "Frodes",
    [10] = "Gaylestrum",
    [11] = "Ironflurry",
    [12] = "Irontitanhc",
    [13] = "Jazzmean",
    [14] = "Lachy",
    [15] = "Pencilvestor",
    [16] = "Retrav",
    [17] = "Sinniaa",
    [18] = "Sinwave",
    [19] = "Skeeta",
    [20] = "Slayingfreak",
    [21] = "Sneakydoodle",
    [22] = "Soulbane",
    [23] = "Superboots",
    [24] = "Varv",
    [25] = "Velladonna",
    [26] = "Whitewidow",
  },
  kills = {
    {boss = 623, timestamp = "2021-11-09 19:19", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 624, timestamp = "2021-11-09 19:55", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 625, timestamp = "2021-11-09 20:53", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 626, timestamp = "2021-11-09 21:54", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
  },
  description = "2021-11-09_SSC",
  drops = {
    {player = 20, timestamp = "2021-11-09 20:45", item = 30183, cost = "10", costType = "Solstice Points"}, -- Slayingfreak : Nether Vortex
    {player = 21, timestamp = "2021-11-09 20:55", item = 30239, cost = "80", costType = "Solstice Points"}, -- Sneakydoodle : Gloves of the Vanquished Champion
    {player = 24, timestamp = "2021-11-09 20:56", item = 30240, cost = "", costType = "MS"}, -- Varv : Gloves of the Vanquished Defender
    {player = 24, timestamp = "2021-11-09 20:57", item = 30096, cost = "60", costType = "Solstice Points"}, -- Varv : Girdle of the Invulnerable
    {player =  7, timestamp = "2021-11-09 20:57", item = 30054, cost = "60", costType = "Solstice Points"}, -- Elorn : Ranger-General's Chestguard
    {player = 13, timestamp = "2021-11-09 20:59", item = 30050, cost = "", costType = "OS"}, -- Jazzmean : Boots of the Shifting Nightmare
    {player = 16, timestamp = "2021-11-09 21:00", item = 30027, cost = "", costType = "OS"}, -- Retrav : Boots of Courage Unending
    {player =  3, timestamp = "2021-11-09 21:01", item = 30059, cost = "", costType = "OS"}, -- Brickk : Choker of Animalistic Fury
    {player = 12, timestamp = "2021-11-09 21:03", item = 33054, cost = "", costType = "OS"}, -- Irontitanhc : The Seal of Danzalar
    {player =  9, timestamp = "2021-11-09 21:56", item = 30246, cost = "", costType = "MS"}, -- Frodes : Leggings of the Vanquished Defender
    {player = 12, timestamp = "2021-11-09 21:57", item = 30099, cost = "70", costType = "Solstice Points"}, -- Irontitanhc : Frayed Tether of the Drowned
    {player = 24, timestamp = "2021-11-09 21:58", item = 30324, cost = "10", costType = "Solstice Points"}, -- Varv : Plans: Red Havoc Boots
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
