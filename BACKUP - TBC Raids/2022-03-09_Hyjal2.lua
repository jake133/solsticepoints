local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Aythya",
    [ 2] = "Capecrusader",
    [ 3] = "Cholula",
    [ 4] = "Eillowee",
    [ 5] = "Elorn",
    [ 6] = "Forestmaster",
    [ 7] = "Frodes",
    [ 8] = "Gaylestrum",
    [ 9] = "Ironankh",
    [10] = "Irontitanhc",
    [11] = "Jazzmean",
    [12] = "Kharlamagne",
    [13] = "Lachy",
    [14] = "Luethien",
    [15] = "Maërlyn",
    [16] = "Roragorn",
    [17] = "Sadistia",
    [18] = "Sinniaa",
    [19] = "Sinwave",
    [20] = "Skeeta",
    [21] = "Slayingfreak",
    [22] = "Superboots",
    [23] = "Whitewidow",
    [24] = "Wootzz",
    [25] = "Xandie",
  },
  kills = {
    {boss = 621, timestamp = "2022-03-09 20:36", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 622, timestamp = "2022-03-09 21:34", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-03-09_Hyjal2",
  drops = {
    {player = 12, timestamp = "2022-03-09 20:38", item = 32609, cost = "", costType = "MS"}, -- Kharlamagne : Boots of the Divine Light
    {player = 25, timestamp = "2022-03-09 20:39", item = 31094, cost = 80, costType = "Solstice Points"}, -- Xandie : Gloves of the Forgotten Protector
    {player = 3, timestamp = "2022-03-09 20:40", item = 31093, cost = 80, costType = "Solstice Points"}, -- Cholula : Gloves of the Forgotten Vanquisher
    {player = 3, timestamp = "2022-03-09 20:40", item = 30898, cost = "", costType = "OS"}, -- Cholula : Shady Dealer's Pantaloons
    {player = 25, timestamp = "2022-03-09 21:36", item = 30909, cost = 70, costType = "Solstice Points"}, -- Xandie : Antonidas's Aegis of Rapt Concentration
    {player = 21, timestamp = "2022-03-09 21:36", item = 30903, cost = "", costType = "OS"}, -- Slay : Legguards of Endless Rage
    {player =  3, timestamp = "2022-03-09 21:37", item = 31096, cost = 80, costType = "Solstice Points"}, -- Cholula : Helm of the Forgotten Vanquisher
    {player =  4, timestamp = "2022-03-09 21:38", item = 31097, cost = 80, costType = "Solstice Points"}, -- Eillowee : Helm of the Forgotten Conqueror
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
