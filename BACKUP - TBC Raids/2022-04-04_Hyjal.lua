local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Adondah",
    [ 2] = "Arquinas",
    [ 3] = "Aythya",
    [ 4] = "Capecrusader",
    [ 5] = "Chalula",
    [ 6] = "Elorn",
    [ 7] = "Gaylestrum",
    [ 8] = "Glancen",
    [ 9] = "Grayskyy",
    [10] = "Irontitann",
    [11] = "Jigowatt",
    [12] = "Kattianna",
    [13] = "Lachý",
    [14] = "Levithium",
    [15] = "Luethien",
    [16] = "Meacha",
    [17] = "Noritotes",
    [18] = "Pencilvestor",
    [19] = "Roragorn",
    [20] = "Sadistia",
    [21] = "Shampainee",
    [22] = "Skeeta",
    [23] = "Snoweyes",
    [24] = "Superboots",
    [25] = "Tocko",
    [26] = "Wootzz",
    [27] = "Xandies",
  },
  kills = {
    {boss = 618, timestamp = "2022-04-04 19:14", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,17,18,19,20,21,22,23,24,25,26,27}},
    {boss = 619, timestamp = "2022-04-04 20:06", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,15,16,17,18,19,20,22,23,24,25,26,27}},
    {boss = 620, timestamp = "2022-04-04 20:31", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,15,16,17,18,19,20,22,23,24,25,26,27}},
    {boss = 621, timestamp = "2022-04-04 21:04", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,15,16,17,18,19,20,22,23,24,25,26,27}},
  },
  description = "2022-04-04_Hyjal",
  drops = {
    {player =  8, timestamp = "2022-04-04 19:18", item = 30871, cost = "", costType = "MS"}, -- Glancen : Bracers of Martyrdom
    {player =  5, timestamp = "2022-04-04 19:19", item = 30863, cost = "", costType = "MS"}, -- Chalula : Deadly Cuffs
    {player =  8, timestamp = "2022-04-04 19:48", item = 32590, cost = "", costType = "OS"}, -- Glancen : Nethervoid Cloak
    {player = 16, timestamp = "2022-04-04 19:49", item = 32609, cost = "", costType = "MS"}, -- Meacha : Boots of the Divine Light
    {player =  8, timestamp = "2022-04-04 19:51", item = 32755, cost = "", costType = "OS"}, -- Glancen : Pattern: Mantle of Nimble Thought
    {player = 16, timestamp = "2022-04-04 20:09", item = 30882, cost = "", costType = "MS"}, -- Meacha : Bastion of Light
    {player = 19, timestamp = "2022-04-04 20:10", item = 30888, cost = 60, costType = "Solstice Points"}, -- Roragorn : Anetheron's Noose
    {player =  9, timestamp = "2022-04-04 20:32", item = 32945, cost = "", costType = "MS"}, -- Grayskyy : Fist of Molten Fury
    {player =  2, timestamp = "2022-04-04 20:32", item = 32946, cost = "", costType = "OS"}, -- Arquinas : Claw of Molten Fury
    {player = 27, timestamp = "2022-04-04 20:33", item = 34009, cost = "", costType = "MS"}, -- Xandies : Hammer of Judgement
    {player = 16, timestamp = "2022-04-04 20:34", item = 30893, cost = "", costType = "MS"}, -- Meacha : Sun-touched Chain Leggings
    {player =  9, timestamp = "2022-04-04 20:43", item = 30917, cost = "", costType = "OS"}, -- Grayskyy : Razorfury Mantle
    {player = 23, timestamp = "2022-04-04 22:14", item = 30900, cost = 60, costType = "Solstice Points"}, -- Snoweyes : Bow-stitched Leggings
    {player = 22, timestamp = "2022-04-04 22:15", item = 31094, cost = 80, costType = "Solstice Points"}, -- Skeeta : Gloves of the Forgotten Protector
    {player =  2, timestamp = "2022-04-04 22:16", item = 31094, cost = 80, costType = "Solstice Points"}, -- Arquinas : Gloves of the Forgotten Protector
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
