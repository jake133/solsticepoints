local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Annarinn",
    [ 3] = "Aythya",
    [ 4] = "Blackcläws",
    [ 5] = "Capecrusader",
    [ 6] = "Elorn",
    [ 7] = "Frodes",
    [ 8] = "Gaylestrum",
    [ 9] = "Inxi",
    [10] = "Irontitann",
    [11] = "Jazzmene",
    [12] = "Kattianna",
    [13] = "Keeganatotem",
    [14] = "Kekett",
    [15] = "Kharlamagne",
    [16] = "Lachý",
    [17] = "Mclitty",
    [18] = "Melisea",
    [19] = "Pencilvestor",
    [20] = "Roragorn",
    [21] = "Sadistia",
    [22] = "Slayingfreak",
    [23] = "Snoweyes",
    [24] = "Superboots",
    [25] = "Xandies",
  },
  kills = {
    {boss = 601, timestamp = "2022-05-17 19:20", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 602, timestamp = "2022-05-17 19:40", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 603, timestamp = "2022-05-17 20:00", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 607, timestamp = "2022-05-17 20:35", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 608, timestamp = "2022-05-17 21:47", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-05-17_BT1",
  drops = {
    {player = 13, timestamp = "2022-05-17 20:01", item = 32236, costType = "MS"}, -- Keeganatotem : Rising Tide
    {player = 20, timestamp = "2022-05-17 20:02", item = 32247, costType = "OS"}, -- Roragorn : Ring of Captured Storms
    {player = 17, timestamp = "2022-05-17 20:03", item = 32251, costType = "OS"}, -- Mclitty : Wraps of Precise Flight
    {player = 14, timestamp = "2022-05-17 20:04", item = 32262, costType = "MS"}, -- Kekett : Syphon of the Nathrezim
    {player = 18, timestamp = "2022-05-17 20:05", item = 32275, costType = "MS"}, -- Melisea : Spiritwalker Gauntlets
    {player = 18, timestamp = "2022-05-17 20:07", item = 32606, costType = "OS"}, -- Melisea : Girdle of the Lightbearer
    {player = 19, timestamp = "2022-05-17 20:19", item = 32266, costType = "MS"}, -- Pencilvestor : Ring of Deceitful Intent
    {player = 10, timestamp = "2022-05-17 20:36", item = 32365, costType = "OS"}, -- Irontitann : Heartshatter Breastplate
    {player = 18, timestamp = "2022-05-17 20:37", item = 31101, cost = 80, costType = "Solstice Points"}, -- Melisea : Pauldrons of the Forgotten Conqueror
    {player = 16, timestamp = "2022-05-17 20:38", item = 31102, costType = "MS"}, -- Lachý : Pauldrons of the Forgotten Vanquisher
    {player = 12, timestamp = "2022-05-17 20:39", item = 31102, costType = "MS"}, -- Kattianna : Pauldrons of the Forgotten Vanquisher
    {player =  6, timestamp = "2022-05-17 21:48", item = 32376, costType = "MS"}, -- Elorn : Forest Prowler's Helm
    {player = 17, timestamp = "2022-05-17 21:49", item = 31100, cost = 80, costType = "Solstice Points"}, -- Mclitty : Leggings of the Forgotten Protector
    {player =  5, timestamp = "2022-05-17 21:50", item = 31099, cost = 80, costType = "Solstice Points"}, -- Capecrusader : Leggings of the Forgotten Vanquisher
    {player = 20, timestamp = "2022-05-17 21:50", item = 31099, cost = 80, costType = "Solstice Points"}, -- Roragorn : Leggings of the Forgotten Vanquisher
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
