local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ambijous",
    [ 2] = "Anatyr",
    [ 3] = "Aythya",
    [ 4] = "Bedonkadonk",
    [ 5] = "Benediktus",
    [ 6] = "Bigtunuh",
    [ 7] = "Capecrusader",
    [ 8] = "Chalula",
    [ 9] = "Dagarle",
    [10] = "Eillowee",
    [11] = "Frodes",
    [12] = "Fârtblossom",
    [13] = "Gaylestrum",
    [14] = "Irontitann",
    [15] = "Jaeran",
    [16] = "Jazzmene",
    [17] = "Lachý",
    [18] = "Luethien",
    [19] = "Mayaell",
    [20] = "Pencilvestor",
    [21] = "Sanif",
    [22] = "Slayingfreak",
    [23] = "Snoweyes",
    [24] = "Superboots",
    [25] = "Wootzz",
    [26] = "Xandies",
  },
  kills = {
    {boss = 601, timestamp = "2022-08-24 19:36", players = {1,2,3,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 602, timestamp = "2022-08-24 19:55", players = {1,2,3,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 603, timestamp = "2022-08-24 20:09", players = {1,2,3,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 607, timestamp = "2022-08-24 20:59", players = {1,2,3,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 608, timestamp = "2022-08-24 21:36", players = {1,2,3,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 609, timestamp = "2022-08-24 22:07", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
  },
  description = "2022-08-24_BT",
  drops = {
    {player =  9, timestamp = "2022-08-24 20:10", item = 32238, costType = "OS"}, -- Dagarle : Ring of Calming Waves
    {player = 26, timestamp = "2022-08-24 20:10", item = 32241, costType = "OS"}, -- Xandies : Helm of Soothing Currents
    {player =  1, timestamp = "2022-08-24 20:11", item = 32251, costType = "MS"}, -- Ambijous : Wraps of Precise Flight
    {player =  1, timestamp = "2022-08-24 20:12", item = 32262, costType = "MS"}, -- Ambijous : Syphon of the Nathrezim
    {player =  9, timestamp = "2022-08-24 21:36", item = 32279, costType = "OS"}, -- Dagarle : The Seeker's Wristguards
    {player = 19, timestamp = "2022-08-24 21:42", item = 32366, costType = "OS"}, -- Mayaell : Shadowmaster's Boots
    {player = 21, timestamp = "2022-08-24 21:42", item = 31103, costType = "MS"}, -- Sanif : Pauldrons of the Forgotten Protector
    {player =  1, timestamp = "2022-08-24 21:43", item = 31103, costType = "MS"}, -- Ambijous : Pauldrons of the Forgotten Protector
    {player = 12, timestamp = "2022-08-24 21:43", item = 31103, costType = "OS"}, -- Fârtblossom : Pauldrons of the Forgotten Protector
    {player = 10, timestamp = "2022-08-24 21:44", item = 32331, costType = "MS"}, -- Eillowee : Cloak of the Illidari Council
    {player = 21, timestamp = "2022-08-24 21:45", item = 31100, costType = "MS"}, -- Sanif : Leggings of the Forgotten Protector
    {player = 11, timestamp = "2022-08-24 21:46", item = 31099, costType = "OS"}, -- Frodes : Leggings of the Forgotten Vanquisher
    {player = 14, timestamp = "2022-08-24 21:47", item = 31098, costType = "OS"}, -- Irontitann : Leggings of the Forgotten Conqueror
    {player = 11, timestamp = "2022-08-24 22:08", item = 32500, cost = 70, costType = "Solstice Points"}, -- Frodes : Crystal Spire of Karabor
    {player =  9, timestamp = "2022-08-24 22:09", item = 32375, costType = "MS"}, -- Dagarle : Bulwark of Azzinoth
    {player = 11, timestamp = "2022-08-24 22:10", item = 31090, cost = 80, costType = "Solstice Points"}, -- Frodes : Chestguard of the Forgotten Vanquisher
    {player = 21, timestamp = "2022-08-24 22:10", item = 31091, costType = "MS"}, -- Sanif : Chestguard of the Forgotten Protector
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
