local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Brickk",
    [ 4] = "Capecrusader",
    [ 5] = "Ceruwolfe",
    [ 6] = "Dagarle",
    [ 7] = "Elorn",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Heidie",
    [11] = "Ironankh",
    [12] = "Irontitanhc",
    [13] = "Kyrika",
    [14] = "Lachy",
    [15] = "Lbow",
    [16] = "Luethien",
    [17] = "Pencilvestor",
    [18] = "Retrav",
    [19] = "Sadistia",
    [20] = "Sneakydoodle",
    [21] = "Superboots",
    [22] = "Varv",
    [23] = "Velladonna",
    [24] = "Whitewidow",
    [25] = "Wootzz",
  },
  kills = {
    {boss = 730, timestamp = "2021-12-01 20:36", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 731, timestamp = "2021-12-01 21:18", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2021-12-01_TK",
  drops = {
    {player = 24, timestamp = "2021-12-01 20:37", item = 29949, cost = "100", costType = "Solstice Points"}, -- Whitewidow : Arcanite Steam-Pistol
    {player = 12, timestamp = "2021-12-01 20:39", item = 29922, cost = "", costType = "MS"}, -- Irontitanhc : Band of Al'ar
    {player = 12, timestamp = "2021-12-01 21:09", item = 30028, cost = "", costType = "MS"}, -- Irontitanhc : Seventh Ring of the Tirisfalen
    {player = 20, timestamp = "2021-12-01 21:19", item = 30248, cost = "80", costType = "Solstice Points"}, -- Sneakydoodle : Pauldrons of the Vanquished Champion
    {player = 17, timestamp = "2021-12-01 21:19", item = 30248, cost = "80", costType = "Solstice Points"}, -- Pencilvestor : Pauldrons of the Vanquished Champion
    {player = 22, timestamp = "2021-12-01 21:39", item = 30028, cost = "", costType = "MS"}, -- Varv : Seventh Ring of the Tirisfalen
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
