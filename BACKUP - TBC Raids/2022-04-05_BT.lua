local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Aythya",
    [ 2] = "Blackcläws",
    [ 3] = "Capecrusader",
    [ 4] = "Chalula",
    [ 5] = "Damelion",
    [ 6] = "Elorn",
    [ 7] = "Frodes",
    [ 8] = "Gaylestrum",
    [ 9] = "Irontitann",
    [10] = "Jazzmene",
    [11] = "Lachý",
    [12] = "Maërlyn",
    [13] = "Noritotes",
    [14] = "Omagosh",
    [15] = "Orodora",
    [16] = "Pencilvestor",
    [17] = "Roragorn",
    [18] = "Sadistia",
    [19] = "Skeeta",
    [20] = "Slayingfreak",
    [21] = "Snoweyes",
    [22] = "Snozzberry",
    [23] = "Superboots",
    [24] = "Velexi",
    [25] = "Xandies",
  },
  kills = {
    {boss = 601, timestamp = "2022-04-05 19:43", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 602, timestamp = "2022-04-05 20:04", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 603, timestamp = "2022-04-05 20:36", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 604, timestamp = "2022-04-05 21:09", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 606, timestamp = "2022-04-05 21:55", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-04-05_BT",
  drops = {
    {player = 17, timestamp = "2022-04-05 19:45", item = 32240, cost = "", costType = "OS"}, -- Roragorn : Guise of the Tidal Lurker
    {player = 25, timestamp = "2022-04-05 19:45", item = 32234, cost = "", costType = "OS"}, -- Xandies : Fists of Mukoa
    {player = 20, timestamp = "2022-04-05 20:06", item = 32261, cost = "", costType = "MS"}, -- Slayingfreak : Band of the Abyssal Lord
    {player = 19, timestamp = "2022-04-05 20:14", item = 32253, cost = "", costType = "OS"}, -- Skeeta : Legionkiller
    {player =  6, timestamp = "2022-04-05 20:37", item = 32265, cost = "", costType = "MS"}, -- Elorn : Shadow-walker's Cord
    {player =  5, timestamp = "2022-04-05 20:44", item = 32275, cost = "", costType = "MS"}, -- Damelion : Spiritwalker Gauntlets
    {player = 14, timestamp = "2022-04-05 21:12", item = 32327, cost = "", costType = "MS"}, -- Omagosh : Robe of the Shadow Council
    {player = 17, timestamp = "2022-04-05 21:56", item = 32352, cost = "", costType = "MS"}, -- Roragorn : Naturewarden's Treads
    {player =  4, timestamp = "2022-04-05 21:57", item = 32347, cost = "", costType = "OS"}, -- Chalula : Grips of Damnation
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
