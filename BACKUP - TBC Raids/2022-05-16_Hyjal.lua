local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Adondah",
    [ 2] = "Annarinn",
    [ 3] = "Arquinas",
    [ 4] = "Aythya",
    [ 5] = "Bennii",
    [ 6] = "Capecrusader",
    [ 7] = "Chalula",
    [ 8] = "Elorn",
    [ 9] = "Furiozah",
    [10] = "Glancen",
    [11] = "Grayskyy",
    [12] = "Inxi",
    [13] = "Irontitann",
    [14] = "Kattianna",
    [15] = "Keeganatotem",
    [16] = "Lachý",
    [17] = "Maxiel",
    [18] = "Mclitty",
    [19] = "Pencilvestor",
    [20] = "Roragorn",
    [21] = "Singleme",
    [22] = "Snoweyes",
    [23] = "Superboots",
    [24] = "Tinycurse",
    [25] = "Wootzz",
    [26] = "Xandies",
  },
  kills = {
    {boss = 619, timestamp = "2022-05-16 19:42", players = {1,2,3,4,5,6,7,8,9,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 618, timestamp = "2022-05-16 20:14", players = {1,2,3,4,5,6,7,8,9,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 620, timestamp = "2022-05-16 20:49", players = {1,2,3,4,5,6,7,8,9,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 621, timestamp = "2022-05-16 22:07", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
  },
  description = "2022-05-16_Hyjal",
  drops = {
    {player = 21, timestamp = "2022-05-16 19:58", item = 30878, costType = "MS"}, -- Singleme : Glimmering Steel Mantle
    {player =  5, timestamp = "2022-05-16 20:05", item = 30887, costType = "MS"}, -- Bennii : Golden Links of Restoration
    {player = 21, timestamp = "2022-05-16 20:06", item = 30862, costType = "MS"}, -- Singleme : Blessed Adamantite Bracers
    {player =  7, timestamp = "2022-05-16 20:07", item = 30871, costType = "OS"}, -- Chalula : Bracers of Martyrdom
    {player = 15, timestamp = "2022-05-16 20:22", item = 32285, costType = "MS"}, -- Keeganatotem : Design: Flashing Crimson Spinel
    {player = 18, timestamp = "2022-05-16 20:51", item = 30914, costType = "OS"}, -- Mclitty : Belt of the Crescent Moon
    {player = 13, timestamp = "2022-05-16 21:18", item = 30891, costType = "OS"}, -- Irontitann : Black Featherlight Boots
    {player =  5, timestamp = "2022-05-16 21:20", item = 32609, costType = "MS"}, -- Bennii : Boots of the Divine Light
    {player = 21, timestamp = "2022-05-16 21:21", item = 34009, costType = "MS"}, -- Singleme : Hammer of Judgement
    {player =  9, timestamp = "2022-05-16 22:07", item = 32945, costType = "MS"}, -- Furiozah : Fist of Molten Fury
    {player = 18, timestamp = "2022-05-16 22:09", item = 31094, cost = 80, costType = "Solstice Points"}, -- Mclitty : Gloves of the Forgotten Protector
    {player = 17, timestamp = "2022-05-16 22:09", item = 31093, costType = "MS"}, -- Maxiel : Gloves of the Forgotten Vanquisher
    {player =  1, timestamp = "2022-05-16 22:10", item = 31093, costType = "MS"}, -- Adondah : Gloves of the Forgotten Vanquisher
    {player = 17, timestamp = "2022-05-16 22:11", item = 32755, costType = "MS"}, -- Maxiel : Pattern: Mantle of Nimble Thought
    {player = 17, timestamp = "2022-05-16 22:12", item = 32752, costType = "MS"}, -- Maxiel : Pattern: Swiftheal Wraps
    {player = 24, timestamp = "2022-05-16 22:12", item = 32590, costType = "MS"}, -- Tinycurse : Nethervoid Cloak
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
