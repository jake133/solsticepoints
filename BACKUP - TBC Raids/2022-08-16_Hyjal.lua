local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Aythya",
    [ 2] = "Capecrusader",
    [ 3] = "Chalula",
    [ 4] = "Chamwow",
    [ 5] = "Dagarle",
    [ 6] = "Donkeyevsky",
    [ 7] = "Dwinchester",
    [ 8] = "Eillowee",
    [ 9] = "Embertempest",
    [10] = "Fârtblossom",
    [11] = "Gaylestrum",
    [12] = "Greenathorr",
    [13] = "Jaeran",
    [14] = "Johnjimbei",
    [15] = "Kashawvesh",
    [16] = "Lachý",
    [17] = "Mayaell",
    [18] = "Melisea",
    [19] = "Nätural",
    [20] = "Pencilvestor",
    [21] = "Shadowtitan",
    [22] = "Snoweyes",
    [23] = "Sodapopp",
    [24] = "Superboots",
    [25] = "Takeamaru",
    [26] = "Thiccsugi",
    [27] = "Xandies",
  },
  kills = {
    {boss = 619, timestamp = "2022-08-16 19:56", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 620, timestamp = "2022-08-16 20:27", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,26}},
    {boss = 621, timestamp = "2022-08-16 20:57", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,26}},
    {boss = 622, timestamp = "2022-08-16 21:31", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
  },
  description = "2022-08-16_Hyjal",
  drops = {
    {player = 23, timestamp = "2022-08-16 19:58", item = 30872, costType = "MS"}, -- Sodapopp : Chronicle of Dark Secrets
    {player = 18, timestamp = "2022-08-16 19:59", item = 30862, costType = "MS"}, -- Melisea : Blessed Adamantite Bracers
    {player = 10, timestamp = "2022-08-16 19:59", item = 30881, costType = "MS"}, -- Fârtblossom : Blade of Infamy
    {player =  9, timestamp = "2022-08-16 20:00", item = 30884, costType = "MS"}, -- Embertempest : Hatefury Mantle
    {player =  7, timestamp = "2022-08-16 20:02", item = 32736, costType = "MS"}, -- Dwinchester : Plans: Swiftsteel Bracers
    {player =  9, timestamp = "2022-08-16 20:03", item = 32752, costType = "MS"}, -- Embertempest : Pattern: Swiftheal Wraps
    {player =  4, timestamp = "2022-08-16 20:03", item = 32296, costType = "MS"}, -- Chamwow : Design: Great Lionseye
    {player =  9, timestamp = "2022-08-16 20:28", item = 30894, costType = "MS"}, -- Embertempest : Blue Suede Shoes
    {player =  5, timestamp = "2022-08-16 20:29", item = 30889, costType = "OS"}, -- Dagarle : Kaz'rogal's Hardened Heart
    {player = 19, timestamp = "2022-08-16 21:12", item = 31093, costType = "MS"}, -- Nätural : Gloves of the Forgotten Vanquisher
    {player = 15, timestamp = "2022-08-16 21:13", item = 31092, costType = "MS"}, -- Kashawvesh : Gloves of the Forgotten Conqueror
    {player =  4, timestamp = "2022-08-16 21:15", item = 31094, cost = 80, costType = "Solstice Points"}, -- Chamwow : Gloves of the Forgotten Protector
    {player =  6, timestamp = "2022-08-16 21:16", item = 30898, costType = "MS"}, -- Donkeyevsky : Shady Dealer's Pantaloons
    {player = 18, timestamp = "2022-08-16 21:32", item = 30904, costType = "OS"}, -- Melisea : Savior's Grasp
    {player =  5, timestamp = "2022-08-16 21:33", item = 30909, costType = "MS"}, -- Dagarle : Antonidas's Aegis of Rapt Concentration
    {player = 14, timestamp = "2022-08-16 21:34", item = 31095, costType = "MS"}, -- Johnjimbei : Helm of the Forgotten Protector
    {player = 16, timestamp = "2022-08-16 21:35", item = 31096, cost = 80, costType = "Solstice Points"}, -- Lachý : Helm of the Forgotten Vanquisher
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
