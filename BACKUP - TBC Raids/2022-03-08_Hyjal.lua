local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Aythya",
    [ 2] = "Blackclaws",
    [ 3] = "Capecrusader",
    [ 4] = "Cholula",
    [ 5] = "Eillowee",
    [ 6] = "Elorn",
    [ 7] = "Frodes",
    [ 8] = "Gaylestrum",
    [ 9] = "Ironankh",
    [10] = "Irontitanhc",
    [11] = "Jazzmean",
    [12] = "Kharlamagne",
    [13] = "Lachy",
    [14] = "Pencilvestor",
    [15] = "Rhodaree",
    [16] = "Roragorn",
    [17] = "Sadistia",
    [18] = "Sinniaa",
    [19] = "Sinwave",
    [20] = "Skeeta",
    [21] = "Slayingfreak",
    [22] = "Superboots",
    [23] = "Treeclaw",
    [24] = "Whitewidow",
    [25] = "Wymonso",
    [26] = "Xandie",
  },
  kills = {
    {boss = 618, timestamp = "2022-03-08 19:51", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,21,22,23,24,25,26}},
    {boss = 619, timestamp = "2022-03-08 20:27", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 620, timestamp = "2022-03-08 21:10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
  },
  description = "2022-03-08_Hyjal",
  drops = {
    {player =  9, timestamp = "2022-03-08 19:52", item = 30863, cost = "", costType = "MS"}, -- Ironankh : Deadly Cuffs
    {player = 19, timestamp = "2022-03-08 19:53", item = 30869, cost = "", costType = "MS"}, -- Sinwave : Howling Wind Bracers
    {player = 19, timestamp = "2022-03-08 20:28", item = 30880, cost = "", costType = "OS"}, -- Sinwave : Quickstrider Moccasins
    {player =  4, timestamp = "2022-03-08 20:28", item = 30886, cost = "", costType = "OS"}, -- Cholula : Enchanted Leather Sandals
    {player = 10, timestamp = "2022-03-08 21:12", item = 30893, cost = "", costType = "OS"}, -- Irontitanhc : Sun-touched Chain Leggings
    {player = 26, timestamp = "2022-03-08 21:13", item = 30916, cost = "", costType = "MS"}, -- Xandie : Leggings of Channeled Elements
    {player = 26, timestamp = "2022-03-08 21:58", item = 32592, cost = "", costType = "OS"}, -- Xandie : Chestguard of Relentless Storms
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
