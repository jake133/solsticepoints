local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Badreams",
    [ 4] = "Capecrusader",
    [ 5] = "Dagarle",
    [ 6] = "Elorn",
    [ 7] = "Estrellita",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Ironflurry",
    [11] = "Irontitanhc",
    [12] = "Kalisae",
    [13] = "Kharlamagne",
    [14] = "Lachy",
    [15] = "Luethien",
    [16] = "Pencilvestor",
    [17] = "Robotchickin",
    [18] = "Skeeta",
    [19] = "Sneakydoodle",
    [20] = "Superboots",
    [21] = "Uniion",
    [22] = "Varv",
    [23] = "Velladonna",
    [24] = "Whitewidow",
    [25] = "Xandie",
  },
  kills = {
    {boss = 626, timestamp = "2021-10-13 20:36", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2021-10-13_SSC",
  drops = {
    {player =  6, timestamp = "2021-10-13 20:38", item = 30247, cost = "80", costType = "Solstice Points"}, -- Elorn : Leggings of the Vanquished Hero
    {player =  1, timestamp = "2021-10-13 20:39", item = 30099, cost = "", costType = "MS"}, -- Ashgon : Frayed Tether of the Drowned
    {player = 22, timestamp = "2021-10-13 20:39", item = 30246, cost = "80", costType = "Solstice Points"}, -- Varv : Leggings of the Vanquished Defender
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
