local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Annarinn",
    [ 2] = "Aythya",
    [ 3] = "Capecrusader",
    [ 4] = "Chalula",
    [ 5] = "Eillowee",
    [ 6] = "Elorn",
    [ 7] = "Frodes",
    [ 8] = "Gaylestrum",
    [ 9] = "Irontitann",
    [10] = "Jazzmene",
    [11] = "Kattianna",
    [12] = "Kekett",
    [13] = "Lachý",
    [14] = "Mclitty",
    [15] = "Melisea",
    [16] = "Noritotes",
    [17] = "Pencilvestor",
    [18] = "Roragorn",
    [19] = "Sadistia",
    [20] = "Slayingfreak",
    [21] = "Snoweyes",
    [22] = "Snozzberry",
    [23] = "Superboots",
    [24] = "Wootzz",
    [25] = "Xandies",
  },
  kills = {
    {boss = 609, timestamp = "2022-05-25 19:34", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 618, timestamp = "2022-05-25 19:59", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 619, timestamp = "2022-05-25 20:31", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 620, timestamp = "2022-05-25 21:01", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 621, timestamp = "2022-05-25 21:21", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 622, timestamp = "2022-05-25 21:35", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-05-25_BT2",
  drops = {
    {player = 14, timestamp = "2022-05-25 19:36", item = 32524, cost = 60, costType = "Solstice Points"}, -- Mclitty : Shroud of the Highborne
    {player =  6, timestamp = "2022-05-25 19:37", item = 32497, cost = 70, costType = "Solstice Points"}, -- Elorn : Stormrage Signet Ring
    {player = 19, timestamp = "2022-05-25 19:38", item = 31089, cost = 80, costType = "Solstice Points"}, -- Sadistia : Chestguard of the Forgotten Conqueror
    {player = 16, timestamp = "2022-05-25 19:39", item = 31091, cost = 80, costType = "Solstice Points"}, -- Noritotes : Chestguard of the Forgotten Protector
    {player =  1, timestamp = "2022-05-25 19:40", item = 31091, cost = 80, costType = "Solstice Points"}, -- Annarinn : Chestguard of the Forgotten Protector
    {player = 18, timestamp = "2022-05-25 20:33", item = 30868, costType = "OS"}, -- Roragorn : Rejuvenating Bracers
    {player = 15, timestamp = "2022-05-25 20:34", item = 30882, cost = 70, costType = "Solstice Points"}, -- Melisea : Bastion of Light
    {player = 12, timestamp = "2022-05-25 21:36", item = 30917, costType = "MS"}, -- Kekett : Razorfury Mantle
    {player = 14, timestamp = "2022-05-25 21:37", item = 30919, costType = "OS"}, -- Mclitty : Valestalker Girdle
    {player = 20, timestamp = "2022-05-25 21:37", item = 32946, costType = "OS"}, -- Slayingfreak : Claw of Molten Fury
    {player = 15, timestamp = "2022-05-25 21:39", item = 31092, cost = 80, costType = "Solstice Points"}, -- Melisea : Gloves of the Forgotten Conqueror
    {player = 22, timestamp = "2022-05-25 21:39", item = 31093, costType = "MS"}, -- Snozzberry : Gloves of the Forgotten Vanquisher
    {player = 20, timestamp = "2022-05-25 21:40", item = 31094, costType = "MS"}, -- Slayingfreak : Gloves of the Forgotten Protector
    {player = 22, timestamp = "2022-05-25 21:40", item = 30911, costType = "MS"}, -- Snozzberry : Scepter of Purification
    {player = 12, timestamp = "2022-05-25 21:41", item = 30905, cost = 60, costType = "Solstice Points"}, -- Kekett : Midnight Chestguard
    {player = 16, timestamp = "2022-05-25 21:42", item = 31095, cost = 80, costType = "Solstice Points"}, -- Noritotes : Helm of the Forgotten Protector
    {player =  7, timestamp = "2022-05-25 21:42", item = 31096, cost = 80, costType = "Solstice Points"}, -- Frodes : Helm of the Forgotten Vanquisher
    {player = 19, timestamp = "2022-05-25 21:43", item = 31097, cost = 80, costType = "Solstice Points"}, -- Sadistia : Helm of the Forgotten Conqueror
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
