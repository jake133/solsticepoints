local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aythya",
    [ 3] = "Aziza",
    [ 4] = "Capecrusader",
    [ 5] = "Cholula",
    [ 6] = "Eillowee",
    [ 7] = "Elorn",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Ironankh",
    [11] = "Irontitanhc",
    [12] = "Jazzmean",
    [13] = "Jüsticé",
    [14] = "Kharlamagne",
    [15] = "Kyrika",
    [16] = "Lachy",
    [17] = "Pencilvestor",
    [18] = "Roragorn",
    [19] = "Sadistia",
    [20] = "Slayingfreak",
    [21] = "Sneakydoodle",
    [22] = "Superboots",
    [23] = "Varv",
    [24] = "Velladonna",
    [25] = "Whitewidow",
  },
  kills = {
    {boss = 618, timestamp = "2022-02-16 19:14", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 619, timestamp = "2022-02-16 19:52", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 620, timestamp = "2022-02-16 20:20", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 621, timestamp = "2022-02-16 21:29", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-02-16_Hyjal",
  drops = {
    {player = 13, timestamp = "2022-02-16 19:54", item = 30871, cost = "", costType = "MS"}, -- Jazzmean : Bracers of Martyrdom
    {player = 20, timestamp = "2022-02-16 19:54", item = 30861, cost = "", costType = "MS"}, -- Slayingfreak : Furious Shackles
    {player =  1, timestamp = "2022-02-16 19:55", item = 34010, cost = "", costType = "MS"}, -- Ashgon : Pepe's Shroud of Pacification
    {player = 10, timestamp = "2022-02-16 19:57", item = 30887, cost = "", costType = "OS"}, -- Ironankh : Golden Links of Restoration
    {player = 14, timestamp = "2022-02-16 19:57", item = 30878, cost = "", costType = "MS"}, -- Kharlamagne : Glimmering Steel Mantle
    {player =  6, timestamp = "2022-02-16 20:22", item = 32590, cost = 60, costType = "Solstice Points"}, -- Eillowee : Nethervoid Cloak
    {player =  1, timestamp = "2022-02-16 20:23", item = 34009, cost = 70, costType = "Solstice Points"}, -- Ashgon : Hammer of Judgement
    {player = 15, timestamp = "2022-02-16 20:24", item = 30894, cost = 60, costType = "Solstice Points"}, -- Kyrika : Blue Suede Shoes
    {player = 13, timestamp = "2022-02-16 21:32", item = 32609, cost = "", costType = "MS"}, -- Jüsticé : Boots of the Divine Light
    {player = 25, timestamp = "2022-02-16 21:33", item = 31094, cost = 80, costType = "Solstice Points"}, -- Whitewidow : Gloves of the Forgotten Protector
    {player = 12, timestamp = "2022-02-16 21:34", item = 31092, cost = 80, costType = "Solstice Points"}, -- Jazzmean : Gloves of the Forgotten Conqueror
    {player = 17, timestamp = "2022-02-16 21:35", item = 30901, cost = "", costType = "MS"}, -- Pencilvestor : Boundless Agony
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
