local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
	[ 1] = "Ashunti",
	[ 2] = "Aythya",
	[ 3] = "Capecrusader",
	[ 4] = "Cholula",
	[ 5] = "Eillowee",
	[ 6] = "Elorn",
	[ 7] = "Gaylestrum",
	[ 8] = "Irontitanhc",
	[ 9] = "Jazzmean",
	[10] = "Kharlamagne",
	[11] = "Lachy",
	[12] = "Pencilvestor",
	[13] = "Rhodaree",
	[14] = "Roragorn",
	[15] = "Sadistia",
	[16] = "Sinniaa",
	[17] = "Sinwave",
	[18] = "Slayingfreak",
	[19] = "Stormygal",
	[20] = "Superboots",
	[21] = "Treeclaw",
	[22] = "Whitewidow",
	[23] = "Xandie",
  },
  kills = {
    {boss = 618, timestamp = "2022-03-02 19:00", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}},
    {boss = 619, timestamp = "2022-03-02 19:00", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}},
    {boss = 620, timestamp = "2022-03-02 19:00", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}},
    {boss = 621, timestamp = "2022-03-02 19:00", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}},
  },
  description = "2022-03-02_Hyjal",
  drops = {
    {player = 4, timestamp = "2022-03-01 19:00", item = 30883, cost = "100", costType = "Solstice Points"}, -- Cholula : Pillar of Ferocity (5 pts each)
    {player = 10, timestamp = "2022-03-01 19:00", item = 30918, cost = "70", costType = "Solstice Points"}, -- Kharlamagne : Hammer of Atonement (4 pts each)
    {player = 12, timestamp = "2022-03-01 19:00", item = 31093, cost = "80", costType = "Solstice Points"}, -- Pencilvestor : Gloves of the Forgotten Vanquisher (4 pts each)
    {player = 15, timestamp = "2022-03-01 19:00", item = 31092, cost = "80", costType = "Solstice Points"}, -- Sadistia : Gloves of the Forgotten Conqueror (4 pts each)
    {player = 14, timestamp = "2022-03-01 19:00", item = 34009, cost = "70", costType = "Solstice Points"}, -- Roragorn : Hammer of Judgement (4 pts each)
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
