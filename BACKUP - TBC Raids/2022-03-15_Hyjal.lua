local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
	[1]="Aythya",
	[2]="Bbeamer",
	[3]="Behl",
	[4]="Bloodpoison",
	[5]="Capecrusader",
	[6]="Chalula",
	[7]="Eillowee",
	[8]="Elorn",
	[9]="Frodes",
	[10]="Gaylestrum",
	[11]="Glancen",
	[12]="Hateshift",
	[13]="Irontitann",
	[14]="Jazzmene",
	[15]="Kharlamagne",
	[16]="Lachy",
	[17]="Maërlyn",
	[18]="Pencilvestor",
	[19]="Roragorn",
	[20]="Sadistia",
	[21]="Skeeta",
	[22]="Slayingfreak",
	[23]="Snoweyes",
	[24]="Superboots",
	[25]="Xandies",
  },
  kills = {
    {boss = 618, timestamp = "2022-03-15 19:36", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 619, timestamp = "2022-03-15 20:11", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 620, timestamp = "2022-03-15 21:50", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-03-15_Hyjal",
  drops = {

    {player = 22, timestamp = "2022-03-15 20:00", item = 30889, cost = 70, costType = "Solstice Points"}, -- Slayingfreak : Kaz'rogal's Hardened Heart
    {player =  2, timestamp = "2022-03-15 20:00", item = 30879, cost = 60, costType = "Solstice Points"}, -- Bbeamer : Don Alejandro's Money Belt
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
