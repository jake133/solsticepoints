local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Arquinas",
    [ 2] = "Aythya",
    [ 3] = "Cholula",
    [ 4] = "Damelion",
    [ 5] = "Elorn",
    [ 6] = "Gaylestrum",
    [ 7] = "Glance",
    [ 8] = "Grayskyy",
    [ 9] = "Helletrix",
    [10] = "Ironankh",
    [11] = "Irontitanhc",
    [12] = "Iss",
    [13] = "Kharlamagne",
    [14] = "Lachy",
    [15] = "Macewinduwu",
    [16] = "Maërlyn",
    [17] = "Pencilvestor",
    [18] = "Roragorn",
    [19] = "Sinniaa",
    [20] = "Skeeta",
    [21] = "Superboots",
    [22] = "Togamiki",
    [23] = "Whitewidow",
    [24] = "Wootzz",
    [25] = "Xandie",
  },
  kills = {
    {boss = 627, timestamp = "2022-03-07 19:44", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 626, timestamp = "2022-03-07 20:04", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 625, timestamp = "2022-03-07 20:20", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 628, timestamp = "2022-03-07 20:50", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 733, timestamp = "2022-03-07 22:10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-03-07_SSC_TK",
  drops = {
    {player =  1, timestamp = "2022-03-07 19:50", item = 30025, cost = "", costType = "OS"}, -- Arquinas : Serpentshrine Shuriken
    {player = 25, timestamp = "2022-03-07 19:51", item = 30085, cost = "", costType = "OS"}, -- Xandie : Mantle of the Tireless Tracker
    {player =  1, timestamp = "2022-03-07 19:53", item = 30083, cost = "", costType = "MS"}, -- Arquinas : Ring of Sundered Souls
    {player = 22, timestamp = "2022-03-07 20:06", item = 30079, cost = "", costType = "MS"}, -- Togamiki : Illidari Shoulderpads
    {player = 13, timestamp = "2022-03-07 20:22", item = 30245, cost = 40, costType = "Solstice Points"}, -- Kharlamagne : Leggings of the Vanquished Champion
    {player = 15, timestamp = "2022-03-07 20:23", item = 30246, cost = "", costType = "MS"}, -- Macewinduwu : Leggings of the Vanquished Defender
    {player = 22, timestamp = "2022-03-07 20:26", item = 30247, cost = "", costType = "MS"}, -- Togamiki : Leggings of the Vanquished Hero
    {player = 25, timestamp = "2022-03-07 20:27", item = 30091, cost = "", costType = "OS"}, -- Xandie : True-Aim Stalker Bands
    {player =  8, timestamp = "2022-03-07 20:29", item = 30239, cost = "", costType = "MS"}, -- Grayskyy : Gloves of the Vanquished Champion
    {player = 13, timestamp = "2022-03-07 20:29", item = 30239, cost = "", costType = "OS"}, -- Kharlamagne : Gloves of the Vanquished Champion
    {player = 16, timestamp = "2022-03-07 21:16", item = 30241, cost = "", costType = "MS"}, -- Maërlyn : Gloves of the Vanquished Hero
    {player = 17, timestamp = "2022-03-07 21:30", item = 30106, cost = 30, costType = "Solstice Points"}, -- Pencilvestor : Belt of One-Hundred Deaths
    {player = 11, timestamp = "2022-03-07 21:34", item = 30111, cost = "", costType = "MS"}, -- Irontitanhc : Runetotem's Mantle
    {player = 20, timestamp = "2022-03-07 21:37", item = 30244, cost = 40, costType = "Solstice Points"}, -- Skeeta : Helm of the Vanquished Hero
    {player = 12, timestamp = "2022-03-07 21:39", item = 30243, cost = "", costType = "MS"}, -- Iss : Helm of the Vanquished Defender
    {player = 13, timestamp = "2022-03-07 21:40", item = 30242, cost = 40, costType = "Solstice Points"}, -- Kharlamagne : Helm of the Vanquished Champion
    {player =  4, timestamp = "2022-03-07 21:41", item = 30030, cost = "", costType = "MS"}, -- Damelion : Girdle of Fallen Stars
    {player =  9, timestamp = "2022-03-07 21:42", item = 30030, cost = "", costType = "OS"}, -- Helletrix : Girdle of Fallen Stars
    {player = 25, timestamp = "2022-03-07 21:43", item = 30024, cost = "", costType = "MS"}, -- Xandie : Mantle of the Elven Kings
    {player =  1, timestamp = "2022-03-07 21:44", item = 30028, cost = "", costType = "MS"}, -- Arquinas : Seventh Ring of the Tirisfalen
    {player = 20, timestamp = "2022-03-07 22:11", item = 30238, cost = 40, costType = "Solstice Points"}, -- Skeeta : Chestguard of the Vanquished Hero
    {player = 13, timestamp = "2022-03-07 22:12", item = 30236, cost = 40, costType = "Solstice Points"}, -- Kharlamagne : Chestguard of the Vanquished Champion
    {player =  6, timestamp = "2022-03-07 22:13", item = 29987, cost = "", costType = "MS"}, -- Gaylestrum : Gauntlets of the Sun King
    {player =  2, timestamp = "2022-03-07 22:13", item = 29994, cost = 30, costType = "Solstice Points"}, -- Aythya : Thalassian Wildercloak
    {player = 11, timestamp = "2022-03-07 22:14", item = 32405, cost = 35, costType = "Solstice Points"}, -- Irontitanhc : Verdant Sphere
    {player = 22, timestamp = "2022-03-07 22:14", item = 30238, cost = 40, costType = "Solstice Points"}, -- Togamiki : Chestguard of the Vanquished Hero
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
