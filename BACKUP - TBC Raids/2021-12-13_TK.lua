local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Boneappletea",
    [ 4] = "Cholula",
    [ 5] = "Dagarle",
    [ 6] = "Elorn",
    [ 7] = "Gaylestrum",
    [ 8] = "Greencloud",
    [ 9] = "Heidie",
    [10] = "Ironankh",
    [11] = "Irontitanhc",
    [12] = "Jazzmean",
    [13] = "Kyrika",
    [14] = "Lachy",
    [15] = "Luethien",
    [16] = "Pencilvestor",
    [17] = "Sadistia",
    [18] = "Sinwave",
    [19] = "Slayingfreak",
    [20] = "Sneakydoodle",
    [21] = "Varv",
    [22] = "Velladonna",
    [23] = "Whitewidow",
    [24] = "Wootzz",
    [25] = "Xremi",
  },
  kills = {
    {boss = 732, timestamp = "2021-12-13 21:53", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2021-12-13_TK",
  drops = {
    {player =  6, timestamp = "2021-12-13 21:55", item = 29951, cost = "", costType = "MS"}, -- Elorn : Star-Strider Boots
    {player =  9, timestamp = "2021-12-13 21:55", item = 29976, cost = "60", costType = "Solstice Points"}, -- Heidie : Worldstorm Gauntlets
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
