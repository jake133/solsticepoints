local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
	[ 1]="Aythya",
	[ 2]="Capecrusader",
	[ 3]="Chalula",
	[ 4]="Eillowee",
	[ 5]="Elorn",
	[ 6]="Furiozah",
	[ 7]="Gaylestrum",
	[ 8]="Grayskyy",
	[ 9]="Helletrix",
	[10]="Irontitann",
	[11]="Kattianna",
	[12]="Kharlamagne",
	[13]="Lachý",
	[14]="Ligmabalz",
	[15]="Luethien",
	[16]="Maërlyn",
	[17]="Rhodaree",
	[18]="Roachy",
	[19]="Roragorn",
	[20]="Skeeta",
	[21]="Snoweyes",
	[22]="Superboots",
	[23]="Sweatyape",
	[24]="Thagodfather",
	[25]="Wootzz",
	[26]="Xandies",
  },
  kills = {
    {boss = 618, timestamp = "2022-03-28 19:36", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 619, timestamp = "2022-03-28 20:11", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 620, timestamp = "2022-03-28 21:50", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 621, timestamp = "2022-03-28 21:50", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
  },
  description = "2022-03-28_Hyjal",
  drops = {
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
