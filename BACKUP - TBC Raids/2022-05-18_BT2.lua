local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Annarinn",
    [ 3] = "Aythya",
    [ 4] = "Capecrusader",
    [ 5] = "Chalula",
    [ 6] = "Elorn",
    [ 7] = "Frodes",
    [ 8] = "Gaylestrum",
    [ 9] = "Inxi",
    [10] = "Irontitann",
    [11] = "Jazzmene",
    [12] = "Kattianna",
    [13] = "Keeganatotem",
    [14] = "Kekett",
    [15] = "Kharlamagne",
    [16] = "Lachý",
    [17] = "Mclitty",
    [18] = "Melisea",
    [19] = "Pencilvestor",
    [20] = "Roragorn",
    [21] = "Sadistia",
    [22] = "Slayingfreak",
    [23] = "Snoweyes",
    [24] = "Superboots",
    [25] = "Wootzz",
    [26] = "Xandies",
  },
  kills = {
    {boss = 609, timestamp = "2022-05-18 20:15", players = {1,2,3,4,5,6,7,8,9,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 606, timestamp = "2022-05-18 20:47", players = {1,2,3,4,5,6,7,8,9,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 605, timestamp = "2022-05-18 21:28", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 604, timestamp = "2022-05-18 21:51", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
  },
  description = "2022-05-18_BT2",
  drops = {
    {player = 24, timestamp = "2022-05-18 20:18", item = 32524, cost = 60, costType = "Solstice Points"}, -- Superboots : Shroud of the Highborne
    {player = 23, timestamp = "2022-05-18 20:20", item = 31091, cost = 80, costType = "Solstice Points"}, -- Snoweyes : Chestguard of the Forgotten Protector
    {player = 22, timestamp = "2022-05-18 20:20", item = 31091, cost = 80, costType = "Solstice Points"}, -- Slayingfreak : Chestguard of the Forgotten Protector
    {player = 26, timestamp = "2022-05-18 20:20", item = 31091, cost = 80, costType = "Solstice Points"}, -- Xandies : Chestguard of the Forgotten Protector
    {player =  6, timestamp = "2022-05-18 20:21", item = 32235, cost = 60, costType = "Solstice Points"}, -- Elorn : Cursed Vision of Sargeras
    {player = 18, timestamp = "2022-05-18 20:48", item = 32354, cost = 60, costType = "Solstice Points"}, -- Melisea : Crown of Empowered Fate
    {player = 15, timestamp = "2022-05-18 20:57", item = 34011, costType = "OS"}, -- Kharlamagne : Illidari Runeshield
    {player = 14, timestamp = "2022-05-18 21:24", item = 32346, costType = "MS"}, -- Kekett : Boneweave Girdle
    {player = 24, timestamp = "2022-05-18 21:52", item = 32343, costType = "OS"}, -- Superboots : Wand of Prismatic Focus
    {player = 13, timestamp = "2022-05-18 21:53", item = 32334, costType = "MS"}, -- Keeganatotem : Vest of Mounting Assault
    {player = 17, timestamp = "2022-05-18 21:53", item = 32330, costType = "OS"}, -- Mclitty : Totem of Ancestral Guidance
    {player =  6, timestamp = "2022-05-18 21:54", item = 32323, cost = 60, costType = "Solstice Points"}, -- Elorn : Shadowmoon Destroyer's Drape
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
