local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
	[ 1] = "Aythya",
	[ 2] = "Aziza",
	[ 3] = "Blackclaws",
	[ 4] = "Capecrusader",
	[ 5] = "Cholula",
	[ 6] = "Eillowee",
	[ 7] = "Elorn",
	[ 8] = "Gaylestrum",
	[ 9] = "Irontitanhc",
	[10] = "Jazzmean",
	[11] = "Kharlamagne",
	[12] = "Kyrika",
	[13] = "Lachy",
	[14] = "Luethien",
	[15] = "Malferon",
	[16] = "Pencilvestor",
	[17] = "Roragorn",
	[18] = "Sadistia",
	[19] = "Sinniaa",
	[20] = "Sinwave",
	[21] = "Slayingfreak",
	[22] = "Superboots",
	[23] = "Whitewidow",
	[24] = "Wootzz",
  },
  kills = {
    {boss = 601, timestamp = "2022-03-01 19:00", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24}},
    {boss = 602, timestamp = "2022-03-01 19:00", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24}},
    {boss = 603, timestamp = "2022-03-01 19:00", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24}},
  },
  description = "2022-03-01_BT",
  drops = {
    {player = 14, timestamp = "2022-03-01 19:00", item = 32237, cost = "70", costType = "Solstice Points"}, -- Wootzz : Maelstrom Fury
    {player = 17, timestamp = "2022-03-01 19:00", item = 32240, cost = "", costType = "OS"}, -- Roragorn : Guise of the Tidal Lurker
    {player = 20, timestamp = "2022-03-01 19:00", item = 32252, cost = "", costType = "MS"}, -- Sinwave : Nether Shadow Tunic
    {player = 4, timestamp = "2022-03-01 19:00", item = 32252, cost = "", costType = "MS"}, -- Capecrusader : Nether Shadow Tunic
    {player = 3, timestamp = "2022-03-01 19:00", item = 32278, cost = "", costType = "MS"}, -- Blackclaws : Grips of Silent Justice
    {player = 20, timestamp = "2022-03-01 19:00", item = 32264, cost = "", costType = "OS"}, -- Sinwave : Shoulders of the Hidden Protector
    {player = 17, timestamp = "2022-03-01 19:00", item = 32527, cost = "70", costType = "Solstice Points"}, -- Roragorn : Ring of Ancient Knowledge
    {player = 11, timestamp = "2022-03-01 19:00", item = 32606, cost = "", costType = "OS"}, -- Kharlamagne : Girdle of the Lightbearer
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
