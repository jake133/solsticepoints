local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Annarinn",
    [ 2] = "Aythya",
    [ 3] = "Capecrusader",
    [ 4] = "Chalula",
    [ 5] = "Chamwow",
    [ 6] = "Dagarle",
    [ 7] = "Eillowee",
    [ 8] = "Frodes",
    [ 9] = "Fârtblossom",
    [10] = "Gaylestrum",
    [11] = "Glancen",
    [12] = "Inxi",
    [13] = "Jaeran",
    [14] = "Jazzmein",
    [15] = "Kaileon",
    [16] = "Kattianna",
    [17] = "Kekett",
    [18] = "Levithium",
    [19] = "Pencilvestor",
    [20] = "Skeeta",
    [21] = "Slayingfreak",
    [22] = "Snoweyes",
    [23] = "Superboots",
    [24] = "Varv",
    [25] = "Wootzz",
    [26] = "Xandies",
  },
  kills = {
    {boss = 609, timestamp = "2022-07-18 20:00", players = {1,2,3,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 605, timestamp = "2022-07-18 20:57", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
  },
  description = "2022-07-18_BT2",
  drops = {
    {player = 15, timestamp = "2022-07-18 20:09", item = 32235, costType = "MS"}, -- Kaileon : Cursed Vision of Sargeras
    {player = 10, timestamp = "2022-07-18 20:12", item = 32483, cost = 70, costType = "Solstice Points"}, -- Gaylestrum : The Skull of Gul'dan
    {player = 19, timestamp = "2022-07-18 20:15", item = 31090, cost = 80, costType = "Solstice Points"}, -- Pencilvestor : Chestguard of the Forgotten Vanquisher
    {player = 12, timestamp = "2022-07-18 20:16", item = 31090, cost = 80, costType = "Solstice Points"}, -- Inxi : Chestguard of the Forgotten Vanquisher
    {player = 18, timestamp = "2022-07-18 20:17", item = 31089, costType = "MS"}, -- Levithium : Chestguard of the Forgotten Conqueror
    {player =  4, timestamp = "2022-07-18 20:59", item = 32339, costType = "OS"}, -- Chalula : Belt of Primal Majesty
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
