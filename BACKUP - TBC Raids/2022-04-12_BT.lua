local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Aythya",
    [ 2] = "Blackcläws",
    [ 3] = "Capecrusader",
    [ 4] = "Eillowee",
    [ 5] = "Elorn",
    [ 6] = "Frodes",
    [ 7] = "Gaylestrum",
    [ 8] = "Irontitann",
    [ 9] = "Jazzmene",
    [10] = "Kattianna",
    [11] = "Kharlamagne",
    [12] = "Lachý",
    [13] = "Mclitty",
    [14] = "Noritotes",
    [15] = "Orodora",
    [16] = "Pencilvestor",
    [17] = "Roragorn",
    [18] = "Sadistia",
    [19] = "Skeeta",
    [20] = "Slayingfreak",
    [21] = "Snoonose",
    [22] = "Snoweyes",
    [23] = "Snozzberry",
    [24] = "Superboots",
    [25] = "Xandies",
  },
  kills = {
    {boss = 601, timestamp = "2022-04-12 19:31", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 602, timestamp = "2022-04-12 19:53", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 603, timestamp = "2022-04-12 20:16", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 604, timestamp = "2022-04-12 20:58", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 606, timestamp = "2022-04-12 21:46", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 605, timestamp = "2022-04-12 22:05", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-04-12_BT",
  drops = {
    {player = 11, timestamp = "2022-04-12 19:32", item = 32238, cost = "", costType = "MS"}, -- Kharlamagne : Ring of Calming Waves
    {player =  8, timestamp = "2022-04-12 19:32", item = 32243, cost = "", costType = "OS"}, -- Irontitann : Pearl Inlaid Boots
    {player = 17, timestamp = "2022-04-12 20:17", item = 32273, cost = "", costType = "OS"}, -- Roragorn : Amice of Brilliant Light
    {player = 13, timestamp = "2022-04-12 20:17", item = 32265, cost = "", costType = "OS"}, -- Mclitty : Shadow-walker's Cord
    {player = 15, timestamp = "2022-04-12 20:18", item = 32252, cost = "", costType = "MS"}, -- Orodora : Nether Shadow Tunic
    {player = 21, timestamp = "2022-04-12 20:19", item = 32261, cost = "", costType = "MS"}, -- Snoonose : Band of the Abyssal Lord
    {player = 25, timestamp = "2022-04-12 21:34", item = 32330, cost = "", costType = "MS"}, -- Xandies : Totem of Ancestral Guidance
    {player = 13, timestamp = "2022-04-12 22:06", item = 32353, cost = "", costType = "MS"}, -- Mclitty : Gloves of Unfailing Faith
    {player =  2, timestamp = "2022-04-12 22:07", item = 32345, cost = 60, costType = "Solstice Points"}, -- Blackcläws : Dreadboots of the Legion
    {player = 17, timestamp = "2022-04-12 22:07", item = 32340, cost = "", costType = "OS"}, -- Roragorn : Garments of Temperance
    {player = 20, timestamp = "2022-04-12 22:08", item = 32501, cost = 70, costType = "Solstice Points"}, -- Slayingfreak : Shadowmoon Insignia
    {player = 16, timestamp = "2022-04-12 22:09", item = 32323, cost = 60, costType = "Solstice Points"}, -- Pencilvestor : Shadowmoon Destroyer's Drape
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
