local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Aythya",
    [ 2] = "Capecrusader",
    [ 3] = "Eillowee",
    [ 4] = "Elorn",
    [ 5] = "Frodes",
    [ 6] = "Gaylestrum",
    [ 7] = "Irontitann",
    [ 8] = "Jazzmene",
    [ 9] = "Kattianna",
    [10] = "Kharlamagne",
    [11] = "Lachý",
    [12] = "Maërlyn",
    [13] = "Mclitty",
    [14] = "Noritotes",
    [15] = "Orodora",
    [16] = "Pencilvestor",
    [17] = "Roragorn",
    [18] = "Sadistia",
    [19] = "Slayingfreak",
    [20] = "Snoonose",
    [21] = "Snoweyes",
    [22] = "Snozzberry",
    [23] = "Superboots",
    [24] = "Wootzz",
    [25] = "Xandies",
  },
  kills = {
    {boss = 607, timestamp = "2022-04-13 20:55", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-04-13_BT2",
  drops = {
    {player = 16, timestamp = "2022-04-13 20:56", item = 32369, cost = 70, costType = "Solstice Points"}, -- Pencilvestor : Blade of Savagery
    {player =  1, timestamp = "2022-04-13 20:56", item = 31103, cost = 80, costType = "Solstice Points"}, -- Aythya : Pauldrons of the Forgotten Protector
    {player =  7, timestamp = "2022-04-13 20:57", item = 31101, cost = 80, costType = "Solstice Points"}, -- Irontitann : Pauldrons of the Forgotten Conqueror
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
