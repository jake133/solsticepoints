local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Brickk",
    [ 4] = "Capecrusader",
    [ 5] = "Cholula",
    [ 6] = "Eillowee",
    [ 7] = "Elorn",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Ironankh",
    [11] = "Irontitanhc",
    [12] = "Jazzmean",
    [13] = "Kyrika",
    [14] = "Lachy",
    [15] = "Luethien",
    [16] = "Pencilvestor",
    [17] = "Roragorn",
    [18] = "Sadistia",
    [19] = "Sinniaa",
    [20] = "Slayingfreak",
    [21] = "Sneakydoodle",
    [22] = "Superboots",
    [23] = "Varv",
    [24] = "Velladonna",
    [25] = "Whitewidow",
    [26] = "Wootzz",
  },
  kills = {
    {boss = 730, timestamp = "2022-01-12 19:19", players = {1,2,3,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 731, timestamp = "2022-01-12 19:42", players = {1,2,3,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 732, timestamp = "2022-01-12 20:10", players = {1,2,3,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 733, timestamp = "2022-01-12 21:50", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
  },
  description = "2022-01-12_TK",
  drops = {
    {player = 17, timestamp = "2022-01-12 18:57", item = 30024, cost = "", costType = "MS"}, -- Roragorn : Mantle of the Elven Kings
    {player = 22, timestamp = "2022-01-12 19:20", item = 29923, cost = "", costType = "MS"}, -- Superboots : Talisman of the Sun King
    {player =  6, timestamp = "2022-01-12 19:43", item = 30250, cost = "", costType = "MS"}, -- Eillowee : Pauldrons of the Vanquished Hero
    {player = 14, timestamp = "2022-01-12 19:43", item = 29986, cost = "", costType = "MS"}, -- Lachy : Cowl of the Grand Engineer
    {player = 14, timestamp = "2022-01-12 19:57", item = 30020, cost = "", costType = "OS"}, -- Lachy : Fire-Cord of the Magus
    {player = 17, timestamp = "2022-01-12 20:11", item = 29977, cost = "", costType = "OS"}, -- Roragorn : Star-Soul Breeches
    {player = 21, timestamp = "2022-01-12 20:12", item = 29966, cost = "", costType = "MS"}, -- Sneakydoodle : Vambraces of Ending
    {player =  9, timestamp = "2022-01-12 21:52", item = 30238, cost = "80", costType = "Solstice Points"}, -- Gaylestrum : Chestguard of the Vanquished Hero
    {player =  1, timestamp = "2022-01-12 21:53", item = 30236, cost = "80", costType = "Solstice Points"}, -- Ashgon : Chestguard of the Vanquished Champion
    {player = 13, timestamp = "2022-01-12 21:54", item = 29988, cost = "100", costType = "Solstice Points"}, -- Kyrika : The Nexus Key
    {player = 20, timestamp = "2022-01-12 21:55", item = 29995, cost = "", costType = "MS"}, -- Slayingfreak : Leggings of Murderous Intent
    {player = 26, timestamp = "2022-01-12 21:56", item = 32405, cost = "60", costType = "Solstice Points"}, -- Wootzz : Verdant Sphere
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
