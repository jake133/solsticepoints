local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Avriell",
    [ 2] = "Aythya",
    [ 3] = "Bedonkadonk",
    [ 4] = "Capecrusader",
    [ 5] = "Chamwow",
    [ 6] = "Dagarle",
    [ 7] = "Eillowee",
    [ 8] = "Frodes",
    [ 9] = "Fârtblossom",
    [10] = "Gaylestrum",
    [11] = "Giltey",
    [12] = "Jazzmene",
    [13] = "Kattianna",
    [14] = "Lachý",
    [15] = "Mayaell",
    [16] = "Melisea",
    [17] = "Pencilvestor",
    [18] = "Skeeta",
    [19] = "Slayingfreak",
    [20] = "Snoweyes",
    [21] = "Superboots",
    [22] = "Trs",
    [23] = "Tufenuf",
    [24] = "Wootzz",
    [25] = "Xandies",
  },
  kills = {
    {boss = 609, timestamp = "2022-07-20 20:33", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 605, timestamp = "2022-07-20 21:35", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-07-20_BT2",
  drops = {
    {player = 23, timestamp = "2022-07-20 20:41", item = 31091, costType = "MS"}, -- Tufenuf : Chestguard of the Forgotten Protector
    {player = 17, timestamp = "2022-07-20 20:42", item = 32471, costType = "MS"}, -- Pencilvestor : Shard of Azzinoth
    {player =  7, timestamp = "2022-07-20 20:43", item = 32525, cost = 60, costType = "Solstice Points"}, -- Eillowee : Cowl of the Illidari High Lord
    {player = 18, timestamp = "2022-07-20 20:44", item = 31091, cost = 80, costType = "Solstice Points"}, -- Skeeta : Chestguard of the Forgotten Protector
    {player = 15, timestamp = "2022-07-20 21:39", item = 32344, costType = "OS"}, -- Mayaell : Staff of Immaculate Recovery
    {player =  3, timestamp = "2022-07-20 22:02", item = 32943, costType = "MS"}, -- Bedonkadonk : Swiftsteel Bludgeon
    {player =  6, timestamp = "2022-07-20 22:03", item = 32501, cost = 70, costType = "Solstice Points"}, -- Dagarle : Shadowmoon Insignia
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
