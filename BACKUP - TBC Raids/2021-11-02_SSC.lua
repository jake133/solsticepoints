local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Brickk",
    [ 4] = "Cailyse",
    [ 5] = "Capecrusader",
    [ 6] = "Cholula",
    [ 7] = "Dagarle",
    [ 8] = "Elorn",
    [ 9] = "Estrellita",
    [10] = "Frodes",
    [11] = "Gaylestrum",
    [12] = "Heidie",
    [13] = "Ironflurry",
    [14] = "Irontitanhc",
    [15] = "Jazzmean",
    [16] = "Lachy",
    [17] = "Pencilvestor",
    [18] = "Sinwave",
    [19] = "Slayingfreak",
    [20] = "Sneakydoodle",
    [21] = "Soulbane",
    [22] = "Superboots",
    [23] = "Varv",
    [24] = "Velladonna",
    [25] = "Whitewidow",
  },
  kills = {
    {boss = 623, timestamp = "2021-11-02 19:22", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 624, timestamp = "2021-11-02 19:54", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 625, timestamp = "2021-11-02 20:59", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 626, timestamp = "2021-11-02 21:34", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2021-11-02_SSC",
  drops = {
    {player = 13, timestamp = "2021-11-02 19:57", item = 30055, cost = "", costType = "MS"}, -- Ironflurry : Shoulderpads of the Stranger
    {player = 21, timestamp = "2021-11-02 19:58", item = 30049, cost = "70", costType = "Solstice Points"}, -- Soulbane : Fathomstone
    {player = 22, timestamp = "2021-11-02 19:58", item = 30665, cost = "70", costType = "Solstice Points"}, -- Superboots : Earring of Soulful Meditation
    {player =  6, timestamp = "2021-11-02 19:59", item = 30061, cost = "", costType = "MS"}, -- Cholula : Ancestral Ring of Conquest
    {player =  6, timestamp = "2021-11-02 19:59", item = 30021, cost = "", costType = "MS"}, -- Cholula : Wildfury Greatstaff
    {player = 14, timestamp = "2021-11-02 22:08", item = 30239, cost = "80", costType = "Solstice Points"}, -- Irontitanhc : Gloves of the Vanquished Champion
    {player = 22, timestamp = "2021-11-02 22:08", item = 30240, cost = "80", costType = "Solstice Points"}, -- Superboots : Gloves of the Vanquished Defender
    {player = 11, timestamp = "2021-11-02 22:09", item = 30247, cost = "", costType = "MS"}, -- Gaylestrum : Leggings of the Vanquished Hero
    {player = 18, timestamp = "2021-11-02 22:11", item = 30097, cost = "", costType = "MS"}, -- Sinwave : Coral-Barbed Shoulderpads
    {player = 17, timestamp = "2021-11-02 22:11", item = 30101, cost = "60", costType = "Solstice Points"}, -- Pencilvestor : Bloodsea Brigand's Vest
    {player =  3, timestamp = "2021-11-02 22:12", item = 30246, cost = "80", costType = "Solstice Points"}, -- Brickk : Leggings of the Vanquished Defender
    {player = 19, timestamp = "2021-11-02 22:12", item = 30183, cost = "10", costType = "Solstice Points"}, -- Slayingfreak : Nether Vortex
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
