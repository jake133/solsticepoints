local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Annarinn",
    [ 2] = "Aythya",
    [ 3] = "Capecrusader",
    [ 4] = "Chalula",
    [ 5] = "Eillowee",
    [ 6] = "Elorn",
    [ 7] = "Gaylestrum",
    [ 8] = "Irontitann",
    [ 9] = "Jazzmene",
    [10] = "Kattianna",
    [11] = "Kekett",
    [12] = "Kharlamagne",
    [13] = "Lachý",
    [14] = "Mclitty",
    [15] = "Melisea",
    [16] = "Noritotes",
    [17] = "Pencilvestor",
    [18] = "Roragorn",
    [19] = "Sadistia",
    [20] = "Slayingfreak",
    [21] = "Snoweyes",
    [22] = "Snozzberry",
    [23] = "Superboots",
    [24] = "Wootzz",
    [25] = "Xandies",
  },
  kills = {
    {boss = 601, timestamp = "2022-05-24 19:08", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 602, timestamp = "2022-05-24 19:27", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 603, timestamp = "2022-05-24 19:44", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 607, timestamp = "2022-05-24 20:09", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 608, timestamp = "2022-05-24 20:48", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 605, timestamp = "2022-05-24 21:13", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 606, timestamp = "2022-05-24 21:37", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 604, timestamp = "2022-05-24 21:59", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-05-24_BT1",
  drops = {
    {player =  5, timestamp = "2022-05-24 19:44", item = 32247, costType = "MS"}, -- Eillowee : Ring of Captured Storms
    {player = 15, timestamp = "2022-05-24 19:45", item = 32232, costType = "OS"}, -- Melisea : Eternium Shell Bracers
    {player =  8, timestamp = "2022-05-24 19:46", item = 32261, costType = "MS"}, -- Irontitann : Band of the Abyssal Lord
    {player =  4, timestamp = "2022-05-24 19:47", item = 32273, costType = "OS"}, -- Chalula : Amice of Brilliant Light
    {player = 14, timestamp = "2022-05-24 19:47", item = 32264, costType = "OS"}, -- Mclitty : Shoulders of the Hidden Predator
    {player =  8, timestamp = "2022-05-24 20:15", item = 32526, costType = "OS"}, -- Irontitann : Band of Devastation
    {player = 14, timestamp = "2022-05-24 20:20", item = 32526, costType = "OS"}, -- Mclitty : Band of Devastation
    {player = 16, timestamp = "2022-05-24 20:52", item = 31103, cost = 80, costType = "Solstice Points"}, -- Noritotes : Pauldrons of the Forgotten Protector
    {player = 14, timestamp = "2022-05-24 20:52", item = 31103, cost = 80, costType = "Solstice Points"}, -- Mclitty : Pauldrons of the Forgotten Protector
    {player =  1, timestamp = "2022-05-24 20:52", item = 31103, cost = 80, costType = "Solstice Points"}, -- Annarinn : Pauldrons of the Forgotten Protector
    {player =  8, timestamp = "2022-05-24 20:54", item = 31098, cost = 80, costType = "Solstice Points"}, -- Irontitann : Leggings of the Forgotten Conqueror
    {player =  1, timestamp = "2022-05-24 20:55", item = 31100, costType = "MS"}, -- Annarinn : Leggings of the Forgotten Protector
    {player =  4, timestamp = "2022-05-24 20:56", item = 31099, cost = 80, costType = "Solstice Points"}, -- Chalula : Leggings of the Forgotten Vanquisher
    {player =  2, timestamp = "2022-05-24 21:14", item = 32505, cost = 70, costType = "Solstice Points"}, -- Aythya : Madness of the Betrayer
    {player = 14, timestamp = "2022-05-24 21:40", item = 32337, cost = 60, costType = "Solstice Points"}, -- Mclitty : Shroud of Forgiveness
    {player =  9, timestamp = "2022-05-24 21:42", item = 32363, costType = "MS"}, -- Jazzmene : Naaru-Blessed Life Rod
    {player = 20, timestamp = "2022-05-24 21:43", item = 32345, costType = "OS"}, -- Slayingfreak : Dreadboots of the Legion
    {player = 11, timestamp = "2022-05-24 22:01", item = 32510, costType = "MS"}, -- Kekett : Softstep Boots of Tracking
    {player = 14, timestamp = "2022-05-24 22:01", item = 32328, cost = 60, costType = "Solstice Points"}, -- Mclitty : Botanist's Gloves of Growth
    {player =  3, timestamp = "2022-05-24 22:08", item = 32366, costType = "MS"}, -- Capecrusader : Shadowmaster's Boots
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
