local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Aythya",
    [ 3] = "Aztur",
    [ 4] = "Bellamira",
    [ 5] = "Boozzé",
    [ 6] = "Brewsterr",
    [ 7] = "Capecrusader",
    [ 8] = "Chalula",
    [ 9] = "Courtorder",
    [10] = "Dagarle",
    [11] = "Dopaminé",
    [12] = "Eillowee",
    [13] = "Firefactor",
    [14] = "Frodes",
    [15] = "Gandrice",
    [16] = "Gaylestrum",
    [17] = "Gyelash",
    [18] = "Handofdaddy",
    [19] = "Heidiie",
    [20] = "Helladead",
    [21] = "Jaipee",
    [22] = "Jazzmein",
    [23] = "Jazzmene",
    [24] = "Kettlewick",
    [25] = "Lachý",
    [26] = "Legendshammy",
    [27] = "Mariangell",
    [28] = "Mayaell",
    [29] = "Maygical",
    [30] = "Melindara",
    [31] = "Mobeus",
    [32] = "Mournival",
    [33] = "Mözzarella",
    [34] = "Narino",
    [35] = "Ogogogogogog",
    [36] = "Pallylove",
    [37] = "Pencilvestor",
    [38] = "Radicalized",
    [39] = "Riftan",
    [40] = "Rrail",
    [41] = "Sacless",
    [42] = "Sadistia",
    [43] = "Sarcinis",
    [44] = "Slayingfreak",
    [45] = "Snoweyes",
    [46] = "Superboots",
    [47] = "Teras",
    [48] = "Wootzz",
    [49] = "Xandies",
    [50] = "ßander",
  },
  kills = {
    {boss = 601, timestamp = "2022-09-21 19:04", players = {1,2,4,6,7,8,12,13,15,16,18,21,23,25,26,27,28,30,35,36,43,44,45,49,50}},
    {boss = 602, timestamp = "2022-09-21 19:12", players = {1,2,4,6,7,8,12,13,15,16,18,21,23,25,26,27,28,30,35,36,43,44,45,49,50}},
    {boss = 603, timestamp = "2022-09-21 19:18", players = {1,2,4,6,7,8,12,13,15,16,18,21,23,25,26,27,28,30,35,36,43,44,45,49,50}},
    {boss = 607, timestamp = "2022-09-21 19:27", players = {1,2,4,6,7,8,12,13,15,16,18,21,23,25,26,27,28,30,35,36,43,44,45,49,50}},
    {boss = 608, timestamp = "2022-09-21 19:39", players = {1,2,4,6,7,8,12,13,15,16,18,21,23,25,26,27,28,30,35,36,43,44,45,49,50}},
    {boss = 609, timestamp = "2022-09-21 19:46", players = {1,2,4,6,7,8,12,13,15,16,18,21,23,25,26,27,28,30,35,36,43,44,45,49,50}},
    {boss = 724, timestamp = "2022-09-21 20:20", players = {1,2,4,6,7,8,12,13,15,16,18,21,23,25,26,27,28,30,35,36,43,44,45,49,50}},
    {boss = 725, timestamp = "2022-09-21 20:26", players = {1,2,4,6,7,8,12,13,15,16,18,21,23,25,26,27,28,30,35,36,43,44,45,49,50}},
    {boss = 726, timestamp = "2022-09-21 20:33", players = {1,2,4,6,7,8,12,13,15,16,18,21,23,25,26,27,28,30,35,36,43,44,45,49,50}},
    {boss = 727, timestamp = "2022-09-21 20:43", players = {1,2,4,6,7,8,12,13,15,16,18,21,23,25,26,27,28,30,35,36,43,44,45,49,50}},
    {boss = 728, timestamp = "2022-09-21 20:54", players = {1,2,4,6,7,8,12,13,15,16,18,21,23,25,26,27,28,30,35,36,43,44,45,49,50}},
    {boss = 729, timestamp = "2022-09-21 21:12", players = {1,2,4,6,7,8,12,13,15,16,18,21,23,25,26,27,28,30,35,36,43,44,45,49,50}},
  },
  description = "2022-09-21_RaidBT",
  drops = {
    {player = 49, timestamp = "2022-09-21 19:47", item = 32943, costType = "OS"}, -- Xandies : Swiftsteel Bludgeon
    {player = 18, timestamp = "2022-09-21 19:48", item = 32251, costType = "OS"}, -- Handofdaddy : Wraps of Precise Flight
    {player = 30, timestamp = "2022-09-21 19:49", item = 32261, costType = "MS"}, -- Melindara : Band of the Abyssal Lord
    {player = 35, timestamp = "2022-09-21 19:49", item = 32265, costType = "MS"}, -- Ogogogogogog : Shadow-walker's Cord
    {player = 13, timestamp = "2022-09-21 19:50", item = 32270, costType = "MS"}, -- Firefactor : Focused Mana Bindings
    {player = 36, timestamp = "2022-09-21 19:50", item = 32241, costType = "MS"}, -- Pallylove : Helm of Soothing Currents
    {player = 30, timestamp = "2022-09-21 19:51", item = 32232, cost = 60, costType = "Solstice Points"}, -- Melindara : Eternium Shell Bracers
    {player = 35, timestamp = "2022-09-21 19:51", item = 32366, costType = "MS"}, -- Ogogogogogog : Shadowmaster's Boots
    {player =  8, timestamp = "2022-09-21 19:52", item = 32505, cost = 70, costType = "Solstice Points"}, -- Chalula : Madness of the Betrayer
    {player = 13, timestamp = "2022-09-21 19:52", item = 31102, costType = "MS"}, -- Firefactor : Pauldrons of the Forgotten Vanquisher
    {player = 35, timestamp = "2022-09-21 19:53", item = 31102, costType = "MS"}, -- Ogogogogogog : Pauldrons of the Forgotten Vanquisher
    {player = 28, timestamp = "2022-09-21 19:53", item = 31102, costType = "MS"}, -- Mayaell : Pauldrons of the Forgotten Vanquisher
    {player = 49, timestamp = "2022-09-21 19:54", item = 32524, costType = "MS"}, -- Xandies : Shroud of the Highborne
    {player = 44, timestamp = "2022-09-21 19:55", item = 32336, costType = "OS"}, -- Slayingfreak : Black Bow of the Betrayer
    {player = 26, timestamp = "2022-09-21 19:55", item = 31100, costType = "MS"}, -- Legendshammy : Leggings of the Forgotten Protector
    {player = 13, timestamp = "2022-09-21 19:56", item = 31099, costType = "MS"}, -- Firefactor : Leggings of the Forgotten Vanquisher
    {player = 35, timestamp = "2022-09-21 19:56", item = 31099, costType = "MS"}, -- Ogogogogogog : Leggings of the Forgotten Vanquisher
    {player = 26, timestamp = "2022-09-21 19:56", item = 31091, costType = "MS"}, -- Legendshammy : Chestguard of the Forgotten Protector
    {player = 27, timestamp = "2022-09-21 19:57", item = 31090, costType = "MS"}, -- Mariangell : Chestguard of the Forgotten Vanquisher
    {player = 13, timestamp = "2022-09-21 19:57", item = 31090, costType = "MS"}, -- Firefactor : Chestguard of the Forgotten Vanquisher
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
