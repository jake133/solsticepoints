local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Brickk",
    [ 4] = "Capecrusader",
    [ 5] = "Dagarle",
    [ 6] = "Elorn",
    [ 7] = "Estrellita",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Jazzmean", -- added out of order
    [11] = "Slayingfreak", -- added out of order
    [12] = "Irontitanhc",
    [13] = "Kharlamagne",
    [14] = "Soulbane", -- added out of order
    [15] = "Lachy",
    [16] = "Sylaramynd", -- added out of order
    [17] = "Pencilvestor",
    [18] = "Skeeta",
    [19] = "Sneakydoodle",
    [20] = "Superboots",
    [21] = "Varv",
    [22] = "Velladonna",
    [23] = "Whitewidow",
    [24] = "Wootzz",
    [25] = "Xandie",
  },
  kills = {
    {boss = 623, timestamp = "2021-10-12 ", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 624, timestamp = "2021-10-12 ", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 625, timestamp = "2021-10-12 ", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2021-10-12_SSC",
  drops = {
    {player = 13, timestamp = "2021-10-12 ", item = 30047, cost = "", costType = "MS"},				-- Kharlamagne : Blackfathom Warbands
    {player = 5, timestamp = "2021-10-12 ", item = 30055, cost = "60", costType = "Solstice Points"},	-- Dagarle : Shoulderpads of the Stranger
    {player = 25, timestamp = "2021-10-12 ", item = 30066, cost = "", costType = "OS"},				-- Xandie : Tempest-Strider Boots
    {player = 8, timestamp = "2021-10-12 ", item = 30062, cost = "", costType = "MS"},					-- Frodes : Grove-Bands of Remulos
    {player = 15, timestamp = "2021-10-12 ", item = 30241, cost = "80", costType = "Solstice Points"},	-- Lachy : Gloves of the Vanquished Hero
    {player = 2, timestamp = "2021-10-12 ", item = 30239, cost = "80", costType = "Solstice Points"},	-- Aziza : Gloves of the Vanquished Champion
    {player = 8, timestamp = "2021-10-12 ", item = 30092, cost = "", costType = "MS"},					-- Frodes : Orca-Hide Boots
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
