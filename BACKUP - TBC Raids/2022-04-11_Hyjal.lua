local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Adondah",
    [ 2] = "Arquinas",
    [ 3] = "Aythya",
    [ 4] = "Caaras",
    [ 5] = "Capecrusader",
    [ 6] = "Coven",
    [ 7] = "Eillowee",
    [ 8] = "Elorn",
    [ 9] = "Flameburst",
    [10] = "Gaylestrum",
    [11] = "Inxi",
    [12] = "Irontitann",
    [13] = "Kattianna",
    [14] = "Lachý",
    [15] = "Luethien",
    [16] = "Mclitty",
    [17] = "Meacha",
    [18] = "Noritotes",
    [19] = "Orodora",
    [20] = "Pencilvestor",
    [21] = "Roragorn",
    [22] = "Skeeta",
    [23] = "Snoweyes",
    [24] = "Superboots",
    [25] = "Xandies",
  },
  kills = {
    {boss = 618, timestamp = "2022-04-11 19:21", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 619, timestamp = "2022-04-11 20:00", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 620, timestamp = "2022-04-11 20:26", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 621, timestamp = "2022-04-11 20:57", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-04-11_Hyjal",
  drops = {
    {player = 11, timestamp = "2022-04-11 19:23", item = 32591, cost = 70, costType = "Solstice Points"}, -- Inxi : Choker of Serrated Blades
    {player = 16, timestamp = "2022-04-11 19:25", item = 32592, cost = "", costType = "OS"}, -- Mclitty : Chestguard of Relentless Storms
    {player = 23, timestamp = "2022-04-11 19:25", item = 32751, cost = "", costType = "MS"}, -- Snoweyes : Pattern: Living Earth Shoulders
    {player = 16, timestamp = "2022-04-11 19:26", item = 30869, cost = "", costType = "MS"}, -- Mclitty : Howling Wind Bracers
    {player = 19, timestamp = "2022-04-11 19:27", item = 30861, cost = "", costType = "OS"}, -- Orodora : Furious Shackles
    {player = 17, timestamp = "2022-04-11 19:28", item = 34009, cost = "", costType = "OS"}, -- Meacha : Hammer of Judgement
    {player =  5, timestamp = "2022-04-11 20:01", item = 30881, cost = 70, costType = "Solstice Points"}, -- Capecrusader : Blade of Infamy
    {player = 16, timestamp = "2022-04-11 20:07", item = 32946, cost = "", costType = "OS"}, -- Mclitty : Claw of Molten Fury
    {player =  9, timestamp = "2022-04-11 20:07", item = 30884, cost = "", costType = "MS"}, -- Flameburst : Hatefury Mantle
    {player = 16, timestamp = "2022-04-11 20:27", item = 30893, cost = "", costType = "MS"}, -- Mclitty : Sun-touched Chain Leggings
    {player = 13, timestamp = "2022-04-11 20:27", item = 30894, cost = "", costType = "MS"}, -- Kattianna : Blue Suede Shoes
    {player = 16, timestamp = "2022-04-11 20:28", item = 32945, cost = "", costType = "OS"}, -- Mclitty : Fist of Molten Fury
    {player = 13, timestamp = "2022-04-11 20:58", item = 31093, cost = 80, costType = "Solstice Points"}, -- Kattianna : Gloves of the Forgotten Vanquisher
    {player = 19, timestamp = "2022-04-11 21:00", item = 31094, cost = 80, costType = "Solstice Points"}, -- Orodora : Gloves of the Forgotten Protector
    {player =  4, timestamp = "2022-04-11 21:01", item = 30900, cost = "", costType = "MS"}, -- Caaras : Bow-stitched Leggings
    {player =  2, timestamp = "2022-04-11 21:02", item = 34010, cost = "", costType = "MS"}, -- Arquinas : Pepe's Shroud of Pacification
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
