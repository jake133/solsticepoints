local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Annarinn",
    [ 3] = "Aythya",
    [ 4] = "Balsawood",
    [ 5] = "Blackcläws",
    [ 6] = "Capecrusader",
    [ 7] = "Chalula",
    [ 8] = "Chamwow",
    [ 9] = "Eillowee",
    [10] = "Fârtblossom",
    [11] = "Gaylestrum",
    [12] = "Irontitann",
    [13] = "Jaeran",
    [14] = "Kattianna",
    [15] = "Kekett",
    [16] = "Lachý",
    [17] = "Mayaell",
    [18] = "Pencilvestor",
    [19] = "Roragorn",
    [20] = "Siveren",
    [21] = "Snoweyes",
    [22] = "Superboots",
    [23] = "Varv",
    [24] = "Xandies",
    [25] = "ßander",
  },
  kills = {
    },
  description = "2022-07-12_SWP",
  drops = {
    {player = 13, timestamp = "2022-07-12 20:29", item = 35733, cost = 70, costType = "Solstice Points"}, -- Jaeran : Ring of Harmonic Beauty
    {player = 17, timestamp = "2022-07-12 20:35", item = 35216, costType = "MS"}, -- Mayaell : Pattern: Leather Chestguard of the Sun
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
