local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Adondah",
    [ 2] = "Arquinas",
    [ 3] = "Aythya",
    [ 4] = "Badboucher",
    [ 5] = "Capecrusader",
    [ 6] = "Chalula",
    [ 7] = "Eillowee",
    [ 8] = "Elorn",
    [ 9] = "Glancen",
    [10] = "Grayskyy",
    [11] = "Inxi",
    [12] = "Irontitann",
    [13] = "Kattianna",
    [14] = "Kharlamagne",
    [15] = "Lachý",
    [16] = "Mclitty",
    [17] = "Meacha",
    [18] = "Noritotes",
    [19] = "Orodora",
    [20] = "Roragorn",
    [21] = "Sadistia",
    [22] = "Skeeta",
    [23] = "Snoweyes",
    [24] = "Wootzz",
    [25] = "Xandies",
  },
  kills = {
    {boss = 618, timestamp = "2022-04-18 19:20", players = {1,2,3,4,5,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 619, timestamp = "2022-04-18 19:37", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 620, timestamp = "2022-04-18 20:06", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 621, timestamp = "2022-04-18 20:33", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-04-18_Hyjal",
  drops = {
    {player = 14, timestamp = "2022-04-18 19:20", item = 30862, cost = 60, costType = "Solstice Points"}, -- Kharlamagne : Blessed Adamantite Bracers
    {player = 13, timestamp = "2022-04-18 19:21", item = 30870, cost = "", costType = "MS"}, -- Kattianna : Cuffs of Devastation
    {player =  1, timestamp = "2022-04-18 19:39", item = 32609, cost = "", costType = "MS"}, -- Adondah : Boots of the Divine Light
    {player = 13, timestamp = "2022-04-18 19:40", item = 32589, cost = "", costType = "OS"}, -- Kattianna : Hellfire-Encased Pendant
    {player = 17, timestamp = "2022-04-18 19:40", item = 30878, cost = "", costType = "MS"}, -- Meacha : Glimmering Steel Mantle
    {player = 24, timestamp = "2022-04-18 19:41", item = 30888, cost = "", costType = "MS"}, -- Wootzz : Anetheron's Noose
    {player = 12, timestamp = "2022-04-18 20:07", item = 30915, cost = "", costType = "OS"}, -- Irontitann : Belt of Seething Fury
    {player = 11, timestamp = "2022-04-18 20:08", item = 34010, cost = "", costType = "OS"}, -- Inxi : Pepe's Shroud of Pacification
    {player =  9, timestamp = "2022-04-18 20:09", item = 34009, cost = "", costType = "OS"}, -- Glancen : Hammer of Judgement
    {player = 16, timestamp = "2022-04-18 20:34", item = 32592, cost = "", costType = "OS"}, -- Mclitty : Chestguard of Relentless Storms
    {player = 22, timestamp = "2022-04-18 20:36", item = 30900, cost = "", costType = "MS"}, -- Skeeta : Bow-stitched Leggings
    {player = 20, timestamp = "2022-04-18 20:37", item = 31093, cost = 80, costType = "Solstice Points"}, -- Roragorn : Gloves of the Forgotten Vanquisher
    {player =  7, timestamp = "2022-04-18 20:37", item = 31092, cost = 80, costType = "Solstice Points"}, -- Eillowee : Gloves of the Forgotten Conqueror
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
