local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Adondah",
    [ 2] = "Arquinas",
    [ 3] = "Aythya",
    [ 4] = "Bbeamer",
    [ 5] = "Blackcläws",
    [ 6] = "Capecrusader",
    [ 7] = "Chalula",
    [ 8] = "Eillowee",
    [ 9] = "Elorn",
    [10] = "Grayskyy",
    [11] = "Heidee",
    [12] = "Helletrix",
    [13] = "Irontitann",
    [14] = "Jesmond",
    [15] = "Kattianna",
    [16] = "Kharlamagne",
    [17] = "Lachý",
    [18] = "Luethien",
    [19] = "Noritotes",
    [20] = "Pencilvestor",
    [21] = "Roachy",
    [22] = "Roragorn",
    [23] = "Skeeta",
    [24] = "Snoweyes",
    [25] = "Superboots",
    [26] = "Wootzz",
    [27] = "Xandies",
  },
  kills = {
    {boss = 628, timestamp = "2022-03-21 19:34", players = {1,2,3,5,6,7,8,9,10,11,12,13,14,15,16,17,19,20,21,22,23,24,25,26,27}},
    {boss = 627, timestamp = "2022-03-21 19:56", players = {1,2,3,5,6,7,8,9,10,11,12,13,14,15,16,17,19,20,21,22,23,24,25,26,27}},
    {boss = 626, timestamp = "2022-03-21 20:21", players = {1,2,3,5,6,7,8,9,10,11,12,13,14,15,16,17,19,20,21,22,23,24,25,26,27}},
    {boss = 730, timestamp = "2022-03-21 21:09", players = {1,2,3,4,6,7,8,9,10,11,12,13,14,15,16,17,19,20,21,22,23,24,25,26,27}},
    {boss = 733, timestamp = "2022-03-21 21:56", players = {1,2,3,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27}},
  },
  description = "2022-03-21_SSC_TK",
  drops = {
    {player = 23, timestamp = "2022-03-21 20:22", item = 30104, cost = "", costType = "MS"}, -- Skeeta : Cobra-Lash Boots
    {player = 25, timestamp = "2022-03-21 20:23", item = 30108, cost = 35, costType = "Solstice Points"}, -- Superboots : Lightfathom Scepter
    {player =  1, timestamp = "2022-03-21 20:24", item = 30243, cost = "", costType = "MS"}, -- Adondah : Helm of the Vanquished Defender
    {player = 22, timestamp = "2022-03-21 20:24", item = 30243, cost = "", costType = "OS"}, -- Roragorn : Helm of the Vanquished Defender
    {player = 15, timestamp = "2022-03-21 20:25", item = 30244, cost = "", costType = "MS"}, -- Kattianna : Helm of the Vanquished Hero
    {player = 21, timestamp = "2022-03-21 20:27", item = 33058, cost = "", costType = "MS"}, -- Roachy : Band of the Vigilant
    {player = 12, timestamp = "2022-03-21 20:28", item = 30098, cost = "", costType = "OS"}, -- Helletrix : Razor-Scale Battlecloak
    {player = 16, timestamp = "2022-03-21 20:29", item = 30008, cost = 35, costType = "Solstice Points"}, -- Kharlamagne : Pendant of the Lost Ages
    {player = 15, timestamp = "2022-03-21 20:30", item = 30247, cost = "", costType = "MS"}, -- Kattianna : Leggings of the Vanquished Hero
    {player = 10, timestamp = "2022-03-21 20:31", item = 30245, cost = "", costType = "MS"}, -- Grayskyy : Leggings of the Vanquished Champion
    {player = 12, timestamp = "2022-03-21 20:32", item = 30100, cost = "", costType = "MS"}, -- Helletrix : Soul-Strider Boots
    {player = 27, timestamp = "2022-03-21 20:34", item = 30023, cost = "", costType = "OS"}, -- Xandies : Totem of the Maelstrom
    {player =  3, timestamp = "2022-03-21 21:59", item = 32405, cost = 35, costType = "Solstice Points"}, -- Aythya : Verdant Sphere
    {player = 15, timestamp = "2022-03-21 22:01", item = 29988, cost = "", costType = "MS"}, -- Kattianna : The Nexus Key
    {player = 18, timestamp = "2022-03-21 22:03", item = 29923, cost = "", costType = "MS"}, -- Luethien : Talisman of the Sun King
    {player = 21, timestamp = "2022-03-21 22:04", item = 29924, cost = "", costType = "MS"}, -- Roachy : Netherbane
    {player =  2, timestamp = "2022-03-21 22:05", item = 29924, cost = "", costType = "OS"}, -- Arquinas : Netherbane
    {player = 27, timestamp = "2022-03-21 22:13", item = 30236, cost = "", costType = "MS"}, -- Xandies : Chestguard of the Vanquished Champion
    {player = 14, timestamp = "2022-03-21 22:13", item = 30236, cost = "", costType = "MS"}, -- Jesmond : Chestguard of the Vanquished Champion
    {player =  7, timestamp = "2022-03-21 22:14", item = 29947, cost = "", costType = "OS"}, -- Chalula : Gloves of the Searing Grip
    {player = 21, timestamp = "2022-03-21 22:15", item = 30028, cost = "", costType = "OS"}, -- Roachy : Seventh Ring of the Tirisfalen
    {player =  2, timestamp = "2022-03-21 22:16", item = 30237, cost = 40, costType = "Solstice Points"}, -- Arq : Chestguard of the Vanquished Defender
    {player = 27, timestamp = "2022-03-21 22:17", item = 30024, cost = "", costType = "MS"}, -- Xandies : Mantle of the Elven Kings
    {player = 15, timestamp = "2022-03-21 22:17", item = 30024, cost = "", costType = "MS"}, -- Kattianna : Mantle of the Elven Kings
	{player = 23, timestamp = "2022-03-21 22:18", item = 29995, cost = "", costType = "OS"}, -- Skeeta : Leggings of Murderous Intent
    {player = 21, timestamp = "2022-03-21 22:18", item = 30030, cost = "", costType = "MS"}, -- Roachy : Girdle of Fallen Stars
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
