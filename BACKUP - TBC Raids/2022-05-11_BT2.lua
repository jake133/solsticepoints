local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Aythya",
    [ 3] = "Capecrusader",
    [ 4] = "Chalula",
    [ 5] = "Eillowee",
    [ 6] = "Elorn",
    [ 7] = "Frodes",
    [ 8] = "Gaylestrum",
    [ 9] = "Jazzmene",
    [10] = "Kharlamagne",
    [11] = "Lachý",
    [12] = "Luethien",
    [13] = "Melisea",
    [14] = "Ninejuanjuan",
    [15] = "Noritotes",
    [16] = "Pencilvestor",
    [17] = "Roragorn",
    [18] = "Sadistia",
    [19] = "Shamtitan",
    [20] = "Skeeta",
    [21] = "Slayingfreak",
    [22] = "Snoweyes",
    [23] = "Snozzberry",
    [24] = "Superboots",
    [25] = "Wootzz",
    [26] = "Xandies",
  },
  kills = {
  },
  description = "2022-05-11_BT2",
  drops = {
    },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
