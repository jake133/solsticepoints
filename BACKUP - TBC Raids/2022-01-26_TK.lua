local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Aythya",
    [ 2] = "Aziza",
    [ 3] = "Brickk",
    [ 4] = "Capecrusader",
    [ 5] = "Cholula",
    [ 6] = "Elorn",
    [ 7] = "Frodes",
    [ 8] = "Gaylestrum",
    [ 9] = "Ironankh",
    [10] = "Irontitanhc",
    [11] = "Jazzmean",
    [12] = "Lachy",
    [13] = "Lilybet",
    [14] = "Luethien",
    [15] = "Pencilvestor",
    [16] = "Pulchra",
    [17] = "Roragorn",
    [18] = "Sadistia",
    [19] = "Sinniaa",
    [20] = "Slayingfreak",
    [21] = "Superboots",
    [22] = "Varv",
    [23] = "Whitewidow",
    [24] = "Wootzz",
  },
  kills = {
    {boss = 730, timestamp = "2022-01-26 19:24", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24}},
    {boss = 731, timestamp = "2022-01-26 19:48", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24}},
    {boss = 732, timestamp = "2022-01-26 20:07", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24}},
    {boss = 733, timestamp = "2022-01-26 20:50", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24}},
  },
  description = "2022-01-26_TK",
  drops = {
    {player = 16, timestamp = "2022-01-26 19:24", item = 29947, cost = "", costType = "MS"}, -- Pulchra : Gloves of the Searing Grip
    {player =  9, timestamp = "2022-01-26 19:25", item = 29924, cost = "70", costType = "Solstice Points"}, -- Ironankh : Netherbane
    {player = 13, timestamp = "2022-01-26 19:26", item = 30447, cost = "", costType = "OS"}, -- Lilybet : Tome of Fiery Redemption
    {player =  9, timestamp = "2022-01-26 19:48", item = 30619, cost = "", costType = "OS"}, -- Ironankh : Fel Reaver's Piston
    {player = 11, timestamp = "2022-01-26 19:49", item = 30249, cost = "", costType = "MS"}, -- Jazzmean : Pauldrons of the Vanquished Defender
	{player = 19, timestamp = "2022-01-26 19:49", item = 30249, cost = "", costType = "MS"}, -- Sinniaa : Pauldrons of the Vanquished Defender
    {player = 13, timestamp = "2022-01-26 19:50", item = 30248, cost = "", costType = "MS"}, -- Lilybet : Pauldrons of the Vanquished Champion
    {player =  9, timestamp = "2022-01-26 19:55", item = 30302, cost = "", costType = "MS"}, -- Ironankh : Pattern: Belt of Deep Shadow
    {player =  9, timestamp = "2022-01-26 20:08", item = 29962, cost = "", costType = "OS"}, -- Ironankh : Heartrazor
    {player = 13, timestamp = "2022-01-26 20:09", item = 29965, cost = "", costType = "MS"}, -- Lilybet : Girdle of the Righteous Path
    {player = 23, timestamp = "2022-01-26 20:51", item = 30238, cost = "", costType = "MS"}, -- Whitewidow : Chestguard of the Vanquished Hero
    {player =  1, timestamp = "2022-01-26 20:52", item = 30238, cost = "", costType = "MS"}, -- Aythya : Chestguard of the Vanquished Hero
    {player = 12, timestamp = "2022-01-26 20:52", item = 30238, cost = "", costType = "MS"}, -- Lachy : Chestguard of the Vanquished Hero
    {player =  6, timestamp = "2022-01-26 20:53", item = 29997, cost = "", costType = "MS"}, -- Elorn : Band of the Ranger-General
    {player = 12, timestamp = "2022-01-26 20:53", item = 29987, cost = "60", costType = "Solstice Points"}, -- Lachy : Gauntlets of the Sun King
    {player = 23, timestamp = "2022-01-26 20:54", item = 32405, cost = "60", costType = "Solstice Points"}, -- Whitewidow : Verdant Sphere
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
