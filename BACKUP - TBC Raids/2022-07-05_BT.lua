local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Annarinn",
    [ 3] = "Aythya",
    [ 4] = "Blackcläws",
    [ 5] = "Bruxa",
    [ 6] = "Capecrusader",
    [ 7] = "Chalula",
    [ 8] = "Chamwow",
    [ 9] = "Christilla",
    [10] = "Dagarle",
    [11] = "Eillowee",
    [12] = "Frodes",
    [13] = "Gaylestrum",
    [14] = "Irontitann",
    [15] = "Lachý",
    [16] = "Mayaell",
    [17] = "Melisea",
    [18] = "Pencilvestor",
    [19] = "Sadistia",
    [20] = "Siveren",
    [21] = "Snoweyes",
    [22] = "Superboots",
    [23] = "Wootzz",
    [24] = "Xandies",
    [25] = "Yerubaval",
  },
  kills = {
    {boss = 601, timestamp = "2022-07-05 19:55", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,21,22,23,24,25}},
    {boss = 602, timestamp = "2022-07-05 20:09", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 603, timestamp = "2022-07-05 20:24", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 607, timestamp = "2022-07-05 20:56", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 608, timestamp = "2022-07-05 21:26", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 609, timestamp = "2022-07-05 21:44", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-07-05_BT",
  drops = {
    {player = 25, timestamp = "2022-07-05 20:26", item = 32251, costType = "MS"}, -- Yerubaval : Wraps of Precise Flight
    {player = 14, timestamp = "2022-07-05 20:38", item = 32278, costType = "MS"}, -- Irontitann : Grips of Silent Justice
    {player = 17, timestamp = "2022-07-05 20:43", item = 32263, costType = "OS"}, -- Melisea : Praetorian's Legguards
    {player =  8, timestamp = "2022-07-05 21:17", item = 32528, costType = "MS"}, -- Chamwow : Blessed Band of Karabor
    {player =  1, timestamp = "2022-07-05 21:46", item = 32366, cost = 60, costType = "Solstice Points"}, -- Anatyr : Shadowmaster's Boots
    {player = 16, timestamp = "2022-07-05 21:47", item = 31102, costType = "MS"}, -- Mayaell : Pauldrons of the Forgotten Vanquisher
    {player =  5, timestamp = "2022-07-05 21:48", item = 31102, costType = "OS"}, -- Bruxa : Pauldrons of the Forgotten Vanquisher
    {player =  4, timestamp = "2022-07-05 21:48", item = 31103, costType = "MS"}, -- Blackcläws : Pauldrons of the Forgotten Protector
    {player = 13, timestamp = "2022-07-05 21:49", item = 32519, costType = "OS"}, -- Gaylestrum : Belt of Divine Guidance
    {player = 15, timestamp = "2022-07-05 21:50", item = 31099, costType = "MS"}, -- Lachý : Leggings of the Forgotten Vanquisher
    {player = 25, timestamp = "2022-07-05 21:51", item = 31100, costType = "MS"}, -- Yerubaval : Leggings of the Forgotten Protector
    {player = 22, timestamp = "2022-07-05 21:52", item = 32496, cost = 70, costType = "Solstice Points"}, -- Superboots : Memento of Tyrande
    {player = 10, timestamp = "2022-07-05 21:52", item = 32521, costType = "MS"}, -- Dagarle : Faceplate of the Impenetrable
    {player =  4, timestamp = "2022-07-05 21:53", item = 31091, costType = "MS"}, -- Blackcläws : Chestguard of the Forgotten Protector
    {player =  5, timestamp = "2022-07-05 21:54", item = 31090, costType = "MS"}, -- Bruxa : Chestguard of the Forgotten Vanquisher
    {player = 10, timestamp = "2022-07-05 21:55", item = 31089, cost = 80, costType = "Solstice Points"}, -- Dagarle : Chestguard of the Forgotten Conqueror
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
