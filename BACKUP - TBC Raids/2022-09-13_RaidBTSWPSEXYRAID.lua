local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Aythya",
    [ 3] = "Bellamira",
    [ 4] = "Birdlove",
    [ 5] = "Capecrusader",
    [ 6] = "Chalula",
    [ 7] = "Chamwow",
    [ 8] = "Eillowee",
    [ 9] = "Fellador",
    [10] = "Floordaddy",
    [11] = "Galerron",
    [12] = "Gaylestrum",
    [13] = "Hadgarthil",
    [14] = "Hydraxt",
    [15] = "Irontitann",
    [16] = "Jaeran",
    [17] = "Jazzmene",
    [18] = "Mollymorph",
    [19] = "Quijuanjinn",
    [20] = "Rosemondon",
    [21] = "Sarcinis",
    [22] = "Shundin",
    [23] = "Slayingfreak",
    [24] = "Snoweyes",
    [25] = "Xandies",
  },
  kills = {
    {boss = 607, timestamp = "2022-09-13 19:38", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 608, timestamp = "2022-09-13 19:54", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 609, timestamp = "2022-09-13 20:15", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-09-13_RaidBTSWPSEXYRAID",
  drops = {
    {player = 18, timestamp = "2022-09-13 19:25", item = 32238, costType = "MS"}, -- Mollymorph : Ring of Calming Waves
    {player = 13, timestamp = "2022-09-13 19:27", item = 34012, costType = "MS"}, -- Hadgarthil : Shroud of the Final Stand
    {player =  4, timestamp = "2022-09-13 19:35", item = 32261, costType = "MS"}, -- Birdlove : Band of the Abyssal Lord
    {player = 14, timestamp = "2022-09-13 19:36", item = 32526, costType = "MS"}, -- Hydraxt : Band of Devastation
    {player = 24, timestamp = "2022-09-13 19:44", item = 32366, costType = "MS"}, -- Snoweyes : Shadowmaster's Boots
    {player =  9, timestamp = "2022-09-13 19:46", item = 32528, costType = "MS"}, -- Fellador : Blessed Band of Karabor
    {player = 21, timestamp = "2022-09-13 19:47", item = 32263, costType = "OS"}, -- Sarcinis : Praetorian's Legguards
    {player =  8, timestamp = "2022-09-13 19:48", item = 31101, costType = "OS"}, -- Eillowee : Pauldrons of the Forgotten Conqueror
    {player = 14, timestamp = "2022-09-13 19:49", item = 31103, costType = "MS"}, -- Hydraxt : Pauldrons of the Forgotten Protector
    {player = 22, timestamp = "2022-09-13 19:49", item = 31102, costType = "MS"}, -- Shundin : Pauldrons of the Forgotten Vanquisher
    {player =  4, timestamp = "2022-09-13 19:57", item = 32505, costType = "MS"}, -- Birdlove : Madness of the Betrayer
    {player = 25, timestamp = "2022-09-13 19:57", item = 31100, costType = "OS"}, -- Xandies : Leggings of the Forgotten Protector
    {player = 10, timestamp = "2022-09-13 19:58", item = 31099, costType = "MS"}, -- Floordaddy : Leggings of the Forgotten Vanquisher
    {player = 18, timestamp = "2022-09-13 19:59", item = 31099, costType = "MS"}, -- Mollymorph : Leggings of the Forgotten Vanquisher
    {player = 19, timestamp = "2022-09-13 20:18", item = 32235, costType = "MS"}, -- Quijuanjinn : Cursed Vision of Sargeras
    {player =  3, timestamp = "2022-09-13 20:19", item = 32374, costType = "MS"}, -- Bellamira : Zhar'doom, Greatstaff of the Devourer
    {player = 14, timestamp = "2022-09-13 20:20", item = 31091, costType = "MS"}, -- Hydraxt : Chestguard of the Forgotten Protector
    {player = 20, timestamp = "2022-09-13 20:21", item = 31090, costType = "MS"}, -- Rosemondon : Chestguard of the Forgotten Vanquisher
    {player = 22, timestamp = "2022-09-13 20:21", item = 31090, costType = "MS"}, -- Shundin : Chestguard of the Forgotten Vanquisher
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
