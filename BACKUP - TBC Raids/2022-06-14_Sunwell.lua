local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Annarinn",
    [ 3] = "Aythya",
    [ 4] = "Azraden",
    [ 5] = "Capecrusader",
    [ 6] = "Chalula",
    [ 7] = "Eillowee",
    [ 8] = "Elorn",
    [ 9] = "Frodes",
    [10] = "Gaylestrum",
    [11] = "Irontitann",
    [12] = "Jazzmene",
    [13] = "Kattianna",
    [14] = "Kekett",
    [15] = "Kharlamagne",
    [16] = "Lachý",
    [17] = "Luethien",
    [18] = "Melisea",
    [19] = "Noritotes",
    [20] = "Roragorn",
    [21] = "Sadistia",
    [22] = "Slayingfreak",
    [23] = "Snoweyes",
    [24] = "Superboots",
    [25] = "Xandies",
  },
  kills = {
    },
  description = "2022-06-14_Sunwell",
  drops = {
    {player = 23, timestamp = "2022-06-14 20:09", item = 34183, cost = 100, costType = "Solstice Points"}, -- Snoweyes : Shivering Felspine
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
