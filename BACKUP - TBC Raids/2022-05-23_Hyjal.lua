local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Adondah",
    [ 2] = "Annarinn",
    [ 3] = "Arquinas",
    [ 4] = "Aythya",
    [ 5] = "Azhri",
    [ 6] = "Capecrusader",
    [ 7] = "Elorn",
    [ 8] = "Gaylestrum",
    [ 9] = "Grayskyy",
    [10] = "Inxi",
    [11] = "Irontitann",
    [12] = "Lachý",
    [13] = "Maxiel",
    [14] = "Mclitty",
    [15] = "Melisea",
    [16] = "Olando",
    [17] = "Pencilvestor",
    [18] = "Roragorn",
    [19] = "Skeeta",
    [20] = "Snoweyes",
    [21] = "Superboots",
    [22] = "Tinycurse",
    [23] = "Varvo",
    [24] = "Wootzz",
    [25] = "Xandies",
  },
  kills = {
    {boss = 618, timestamp = "2022-05-23 19:28", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 619, timestamp = "2022-05-23 19:42", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 620, timestamp = "2022-05-23 20:06", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 621, timestamp = "2022-05-23 21:00", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 622, timestamp = "2022-05-23 21:45", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-05-23_Hyjal",
  drops = {
    {player = 15, timestamp = "2022-05-23 19:44", item = 34010, costType = "OS"}, -- Melisea : Pepe's Shroud of Pacification
    {player = 12, timestamp = "2022-05-23 19:45", item = 30872, cost = 70, costType = "Solstice Points"}, -- Lachý : Chronicle of Dark Secrets
    {player =  9, timestamp = "2022-05-23 19:46", item = 30863, costType = "MS"}, -- Grayskyy : Deadly Cuffs
    {player = 15, timestamp = "2022-05-23 19:47", item = 30878, costType = "OS"}, -- Melisea : Glimmering Steel Mantle
    {player =  5, timestamp = "2022-05-23 20:37", item = 34009, costType = "OS"}, -- Azhri : Hammer of Judgement
    {player = 11, timestamp = "2022-05-23 20:37", item = 32591, costType = "OS"}, -- Irontitann : Choker of Serrated Blades
    {player =  3, timestamp = "2022-05-23 20:38", item = 32591, costType = "OS"}, -- Arquinas : Choker of Serrated Blades
    {player = 24, timestamp = "2022-05-23 20:40", item = 30916, cost = 60, costType = "Solstice Points"}, -- Wootzz : Leggings of Channeled Elements
    {player =  1, timestamp = "2022-05-23 20:41", item = 30895, costType = "MS"}, -- Adondah : Angelista's Sash
    {player =  1, timestamp = "2022-05-23 21:46", item = 30899, costType = "MS"}, -- Adondah : Don Rodrigo's Poncho
    {player =  8, timestamp = "2022-05-23 21:47", item = 31093, costType = "MS"}, -- Gaylestrum : Gloves of the Forgotten Vanquisher
    {player = 16, timestamp = "2022-05-23 21:48", item = 31093, costType = "OS"}, -- Olando : Gloves of the Forgotten Vanquisher
    {player = 20, timestamp = "2022-05-23 21:49", item = 30906, cost = 100, costType = "Solstice Points"}, -- Snoweyes : Bristleblitz Striker
    {player = 14, timestamp = "2022-05-23 21:49", item = 30909, costType = "OS"}, -- Mclitty : Antonidas's Aegis of Rapt Concentration
    {player = 14, timestamp = "2022-05-23 21:50", item = 31095, cost = 80, costType = "Solstice Points"}, -- Mclitty : Helm of the Forgotten Protector
    {player =  6, timestamp = "2022-05-23 21:50", item = 31096, cost = 80, costType = "Solstice Points"}, -- Capecrusader : Helm of the Forgotten Vanquisher
    {player =  9, timestamp = "2022-05-23 21:51", item = 31096, cost = 80, costType = "Solstice Points"}, -- Grayskyy : Helm of the Forgotten Vanquisher
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
