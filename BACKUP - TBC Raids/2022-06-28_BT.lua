local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Annarinn",
    [ 3] = "Aythya",
    [ 4] = "Capecrusader",
    [ 5] = "Chalula",
    [ 6] = "Dagarle",
    [ 7] = "Eillowee",
    [ 8] = "Ferlinon",
    [ 9] = "Frodes",
    [10] = "Irontitann",
    [11] = "Jazzmein",
    [12] = "Kekett",
    [13] = "Lachý",
    [14] = "Mayaell",
    [15] = "Melisea",
    [16] = "Pencilvestor",
    [17] = "Sadistia",
    [18] = "Slayingfreak",
    [19] = "Snoweyes",
    [20] = "Superboots",
    [21] = "Tasmina",
    [22] = "Veltrox",
    [23] = "Wootzz",
    [24] = "Xandies",
    [25] = "Zquig",
    [26] = "ßander",
  },
  kills = {
    {boss = 601, timestamp = "2022-06-28 19:39", players = {1,2,3,4,5,6,7,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 602, timestamp = "2022-06-28 19:57", players = {1,2,3,4,5,6,7,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 603, timestamp = "2022-06-28 20:10", players = {1,2,3,4,5,6,7,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 607, timestamp = "2022-06-28 20:49", players = {1,2,3,4,5,6,7,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 608, timestamp = "2022-06-28 21:11", players = {1,2,3,4,5,6,7,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 609, timestamp = "2022-06-28 21:31", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 604, timestamp = "2022-06-28 22:03", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
  },
  description = "2022-06-28_BT",
  drops = {
    {player = 25, timestamp = "2022-06-28 20:51", item = 32238, costType = "MS"}, -- Zquig : Ring of Calming Waves
    {player =  7, timestamp = "2022-06-28 20:52", item = 32247, costType = "MS"}, -- Eillowee : Ring of Captured Storms
    {player = 13, timestamp = "2022-06-28 20:53", item = 32256, costType = "MS"}, -- Lachý : Waistwrap of Infinity
    {player =  6, timestamp = "2022-06-28 20:54", item = 32251, costType = "OS"}, -- Dagarle : Wraps of Precise Flight
    {player =  6, timestamp = "2022-06-28 20:55", item = 32264, costType = "OS"}, -- Dagarle : Shoulders of the Hidden Predator
    {player =  7, timestamp = "2022-06-28 21:14", item = 32361, costType = "MS"}, -- Eillowee : Blind-Seers Icon
    {player = 24, timestamp = "2022-06-28 21:36", item = 31103, costType = "OS"}, -- Xandies : Pauldrons of the Forgotten Protector
    {player = 14, timestamp = "2022-06-28 21:37", item = 31102, costType = "MS"}, -- Mayaell : Pauldrons of the Forgotten Vanquisher
    {player = 25, timestamp = "2022-06-28 21:37", item = 31102, costType = "MS"}, -- Zquig : Pauldrons of the Forgotten Vanquisher
    {player = 15, timestamp = "2022-06-28 21:38", item = 34012, costType = "MS"}, -- Melisea : Shroud of the Final Stand
    {player = 19, timestamp = "2022-06-28 21:41", item = 32505, cost = 70, costType = "Solstice Points"}, -- Snoweyes : Madness of the Betrayer
    {player = 25, timestamp = "2022-06-28 21:42", item = 31099, costType = "MS"}, -- Zquig : Leggings of the Forgotten Vanquisher
    {player = 12, timestamp = "2022-06-28 21:43", item = 31100, costType = "OS"}, -- Kekett : Leggings of the Forgotten Protector
    {player =  8, timestamp = "2022-06-28 21:44", item = 31091, costType = "MS"}, -- Ferlinon : Chestguard of the Forgotten Protector
    {player = 26, timestamp = "2022-06-28 21:46", item = 32496, costType = "MS"}, -- ßander : Memento of Tyrande
    {player =  1, timestamp = "2022-06-28 21:49", item = 31089, cost = 80, costType = "Solstice Points"}, -- Anatyr : Chestguard of the Forgotten Conqueror
    {player = 15, timestamp = "2022-06-28 21:49", item = 31089, cost = 80, costType = "Solstice Points"}, -- Melisea : Chestguard of the Forgotten Conqueror
    {player =  6, timestamp = "2022-06-28 22:04", item = 32512, costType = "OS"}, -- Dagarle : Girdle of Lordaeron's Fallen
    {player = 15, timestamp = "2022-06-28 22:04", item = 32280, costType = "OS"}, -- Melisea : Gauntlets of Enforcement
    {player = 10, timestamp = "2022-06-28 22:07", item = 32375, costType = "MS"}, -- Irontitann : Bulwark of Azzinoth
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
