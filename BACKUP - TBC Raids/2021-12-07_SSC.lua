local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Brickk",
    [ 4] = "Capecrusader",
    [ 5] = "Cholula",
    [ 6] = "Dagarle",
    [ 7] = "Elorn",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Heidie",
    [11] = "Ironankh",
    [12] = "Jazzmean",
    [13] = "Kyrika",
    [14] = "Lachy",
    [15] = "Pencilvestor",
    [16] = "Sadistia",
    [17] = "Sinniaa",
    [18] = "Sinwave",
    [19] = "Slayingfreak",
    [20] = "Sneakydoodle",
    [21] = "Soulbane",
    [22] = "Superboots",
    [23] = "Varv",
    [24] = "Velladonna",
    [25] = "Whitewidow",
  },
  kills = {
    {boss = 623, timestamp = "2021-12-07 19:27", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 624, timestamp = "2021-12-07 19:57", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 627, timestamp = "2021-12-07 21:20", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 626, timestamp = "2021-12-07 21:55", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2021-12-07_SSC",
  drops = {
    {player = 11, timestamp = "2021-12-07 19:05", item = 30307, cost = "10", costType = "Solstice Points"}, -- Ironankh : Pattern: Boots of the Crimson Hawk
    {player = 17, timestamp = "2021-12-07 19:28", item = 32516, cost = "60", costType = "Solstice Points"}, -- Sinniaa : Wraps of Purification
    {player =  7, timestamp = "2021-12-07 19:28", item = 30052, cost = "70", costType = "Solstice Points"}, -- Elorn : Ring of Lethality
    {player = 17, timestamp = "2021-12-07 19:59", item = 30062, cost = "60", costType = "Solstice Points"}, -- Sinniaa : Grove-Bands of Remulos
    {player = 18, timestamp = "2021-12-07 20:00", item = 30066, cost = "60", costType = "Solstice Points"}, -- Sinwave : Tempest-Strider Boots
    {player = 11, timestamp = "2021-12-07 20:04", item = 30304, cost = "", costType = "OS"}, -- Ironankh : Pattern: Monsoon Belt
    {player = 12, timestamp = "2021-12-07 21:21", item = 30080, cost = "70", costType = "Solstice Points"}, -- Jazzmean : Luminescent Rod of the Naaru
    {player = 23, timestamp = "2021-12-07 21:22", item = 30083, cost = "", costType = "MS"}, -- Varv : Ring of Sundered Souls
    {player = 12, timestamp = "2021-12-07 21:56", item = 30246, cost = "", costType = "MS"}, -- Jazzmean : Leggings of the Vanquished Defender
    {player = 12, timestamp = "2021-12-07 21:57", item = 30100, cost = "60", costType = "Solstice Points"}, -- Jazzmean : Soul-Strider Boots
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
