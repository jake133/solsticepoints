local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Aythya",
    [ 2] = "Capecrusader",
    [ 3] = "Chalula",
    [ 4] = "Eillowee",
    [ 5] = "Elorn",
    [ 6] = "Frodes",
    [ 7] = "Gaylestrum",
    [ 8] = "Irontitann",
    [ 9] = "Jazzmene",
    [10] = "Kattianna",
    [11] = "Kharlamagne",
    [12] = "Lachý",
    [13] = "Maërlyn",
    [14] = "Noritotes",
    [15] = "Pencilvestor",
    [16] = "Roachy",
    [17] = "Roragorn",
    [18] = "Sadistia",
    [19] = "Skeeta",
    [20] = "Slayingfreak",
    [21] = "Snoweyes",
    [22] = "Snozzberry",
    [23] = "Superboots",
    [24] = "Wootzz",
    [25] = "Xandies",
  },
  kills = {
    {boss = 606, timestamp = "2022-03-23 19:35", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 605, timestamp = "2022-03-23 21:03", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-03-23_BT2",
  drops = {
    {player = 12, timestamp = "2022-03-23 19:36", item = 32527, cost = 70, costType = "Solstice Points"}, -- Lachý : Ring of Ancient Knowledge
    {player =  3, timestamp = "2022-03-23 19:37", item = 32362, cost = 70, costType = "Solstice Points"}, -- Chalula : Pendant of Titans
    {player = 22, timestamp = "2022-03-23 19:44", item = 32350, cost = "", costType = "MS"}, -- Snozzberry : Touch of Inspiration
    {player = 15, timestamp = "2022-03-23 20:15", item = 32943, cost = "", costType = "MS"}, -- Pencilvestor : Swiftsteel Bludgeon
    {player = 22, timestamp = "2022-03-23 21:06", item = 32344, cost = 100, costType = "Solstice Points"}, -- Snozzberry : Staff of Immaculate Recovery
    {player =  9, timestamp = "2022-03-23 21:07", item = 32337, cost = 60, costType = "Solstice Points"}, -- Jazzmene : Shroud of Forgiveness
    {player = 11, timestamp = "2022-03-23 21:24", item = 32608, cost = "", costType = "OS"}, -- Kharlamagne : Pillager's Gauntlets
    {player = 16, timestamp = "2022-03-23 21:25", item = 34012, cost = "", costType = "MS"}, -- Roachy : Shroud of the Final Stand
    {player =  8, timestamp = "2022-03-23 21:25", item = 32606, cost = "", costType = "OS"}, -- Irontitann : Girdle of the Lightbearer
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
