local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Brickk",
    [ 4] = "Capecrusader",
    [ 5] = "Cholula",
    [ 6] = "Elorn",
    [ 7] = "Frodes",
    [ 8] = "Gaylestrum",
    [ 9] = "Heidie",
    [10] = "Ironankh",
    [11] = "Irontitanhc",
    [12] = "Jazzmean",
    [13] = "Lachy",
    [14] = "Pencilvestor",
    [15] = "Roragorn",
    [16] = "Sadistia",
    [17] = "Sinwave",
    [18] = "Slayingfreak",
    [19] = "Sneakydoodle",
    [20] = "Soulbane",
    [21] = "Superboots",
    [22] = "Varv",
    [23] = "Velladonna",
    [24] = "Whitewidow",
    [25] = "Wootzz",
    [26] = "Kyrika",
  },
  kills = {
    {boss = 624, timestamp = "2022-01-04 19:18", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 627, timestamp = "2022-01-04 19:49", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 626, timestamp = "2022-01-04 20:07", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 625, timestamp = "2022-01-04 20:24", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-01-04_SSC",
  drops = {
    {player = 14, timestamp = "2022-01-04 18:58", item = 30025, cost = "", costType = "OS"}, -- Pencilvestor : Serpentshrine Shuriken
    {player =  9, timestamp = "2022-01-04 19:02", item = 30023, cost = "", costType = "MS"}, -- Heidie : Totem of the Maelstrom
    {player = 23, timestamp = "2022-01-04 19:08", item = 30281, cost = "", costType = "MS"}, -- Velladonna : Pattern: Belt of the Long Road
    {player =  4, timestamp = "2022-01-04 19:09", item = 30301, cost = "", costType = "MS"}, -- Capecrusader : Pattern: Belt of Natural Power
    {player =  8, timestamp = "2022-01-04 19:19", item = 33054, cost = "", costType = "OS"}, -- Gaylestrum : The Seal of Danzalar
    {player = 18, timestamp = "2022-01-04 19:19", item = 30058, cost = "", costType = "OS"}, -- Slayingfreak : Mallet of the Tides
    {player = 17, timestamp = "2022-01-04 19:50", item = 33058, cost = "", costType = "MS"}, -- Sinwave : Band of the Vigilant
    {player = 11, timestamp = "2022-01-04 19:50", item = 30084, cost = "", costType = "OS"}, -- Irontitanhc : Pauldrons of the Argent Sentinel
    {player =  4, timestamp = "2022-01-04 20:09", item = 30245, cost = "", costType = "MS"}, -- Capecrusader : Leggings of the Vanquished Champion
    {player =  2, timestamp = "2022-01-04 20:11", item = 30663, cost = "", costType = "OS"}, -- Aziza : Fathom-Brooch of the Tidewalker
    {player = 21, timestamp = "2022-01-04 20:16", item = 30280, cost = "", costType = "MS"}, -- Superboots : Pattern: Belt of Blasting
    {player = 12, timestamp = "2022-01-04 20:26", item = 30240, cost = "", costType = "MS"}, -- Jazzmean : Gloves of the Vanquished Defender
    {player =  9, timestamp = "2022-01-04 20:27", item = 30092, cost = "60", costType = "Solstice Points"}, -- Heidie : Orca-Hide Boots
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
