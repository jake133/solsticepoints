local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Aythya",
    [ 2] = "Capecrusader",
    [ 3] = "Chalula",
    [ 4] = "Diisori",
    [ 5] = "Eillowee",
    [ 6] = "Elorn",
    [ 7] = "Frodes",
    [ 8] = "Gaylestrum",
    [ 9] = "Jazzmene",
    [10] = "Kattianna",
    [11] = "Lachý",
    [12] = "Maërlyn",
    [13] = "Mclitty",
    [14] = "Noritotes",
    [15] = "Pencilvestor",
    [16] = "Roragorn",
    [17] = "Sadistia",
    [18] = "Singleme",
    [19] = "Slayingfreak",
    [20] = "Snoweyes",
    [21] = "Snozzberry",
    [22] = "Superboots",
    [23] = "Tinywinks",
    [24] = "Wootzz",
    [25] = "Xandies",
  },
  kills = {
    {boss = 607, timestamp = "2022-04-27 19:36", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24}},
    {boss = 608, timestamp = "2022-04-27 20:25", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-04-27_BT",
  drops = {
    {player = 14, timestamp = "2022-04-27 19:37", item = 32526, costType = "MS"}, -- Noritotes : Band of Devastation
    {player =  3, timestamp = "2022-04-27 19:39", item = 31102, cost = 80, costType = "Solstice Points"}, -- Chalula : Pauldrons of the Forgotten Vanquisher
    {player =  4, timestamp = "2022-04-27 19:40", item = 32368, costType = "MS"}, -- Diisori : Tome of the Lightbringer
    {player = 18, timestamp = "2022-04-27 19:43", item = 31101, costType = "MS"}, -- Singleme : Pauldrons of the Forgotten Conqueror
    {player = 19, timestamp = "2022-04-27 20:26", item = 31100, cost = 80, costType = "Solstice Points"}, -- Slayingfreak : Leggings of the Forgotten Protector
    {player =  8, timestamp = "2022-04-27 20:28", item = 31099, cost = 80, costType = "Solstice Points"}, -- Gaylestrum : Leggings of the Forgotten Vanquisher
    {player = 14, timestamp = "2022-04-27 20:29", item = 32518, costType = "OS"}, -- Noritotes : Veil of Turning Leaves
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
