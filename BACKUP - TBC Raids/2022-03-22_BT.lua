local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Aythya",
    [ 2] = "Bbeamer",
    [ 3] = "Blackcläws",
    [ 4] = "Capecrusader",
    [ 5] = "Chalula",
    [ 6] = "Eillowee",
    [ 7] = "Elorn",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Hateshift",
    [11] = "Irontitann",
    [12] = "Jazzmene",
    [13] = "Kattianna",
    [14] = "Lachý",
    [15] = "Noritotes",
    [16] = "Pencilvestor",
    [17] = "Roachy",
    [18] = "Roragorn",
    [19] = "Sadistia",
    [20] = "Skeeta",
    [21] = "Slayingfreak",
    [22] = "Snoweyes",
    [23] = "Snozzberry",
    [24] = "Superboots",
    [25] = "Xandies",
  },
  kills = {
    {boss = 601, timestamp = "2022-03-22 19:22", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 602, timestamp = "2022-03-22 19:45", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 603, timestamp = "2022-03-22 20:39", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 604, timestamp = "2022-03-22 21:20", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-03-22_BT",
  drops = {
    {player = 11, timestamp = "2022-03-22 19:23", item = 32232, cost = "", costType = "MS"}, -- Irontitann : Eternium Shell Bracers
    {player = 17, timestamp = "2022-03-22 19:24", item = 32243, cost = "", costType = "MS"}, -- Roachy : Pearl Inlaid Boots
    {player =  7, timestamp = "2022-03-22 19:46", item = 32253, cost = "", costType = "MS"}, -- Elorn : Legionkiller
    {player = 19, timestamp = "2022-03-22 19:47", item = 32256, cost = "", costType = "MS"}, -- Sadistia : Waistwrap of Infinity
    {player =  2, timestamp = "2022-03-22 20:41", item = 32266, cost = "", costType = "MS"}, -- Bbeamer : Ring of Deceitful Intent
    {player = 16, timestamp = "2022-03-22 21:21", item = 32324, cost = 60, costType = "Solstice Points"}, -- Pencilvestor : Insidious Bands
    {player =  4, timestamp = "2022-03-22 21:23", item = 32747, cost = "", costType = "OS"}, -- Capecrusader : Pattern: Swiftstrike Shoulders
    {player = 17, timestamp = "2022-03-22 21:23", item = 32512, cost = "", costType = "MS"}, -- Roachy : Girdle of Lordaeron's Fallen
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
