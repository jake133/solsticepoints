local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aythya",
    [ 3] = "Aziza",
    [ 4] = "Brickk",
    [ 5] = "Capecrusader",
    [ 6] = "Ceruwolfe",
    [ 7] = "Cholula",
    [ 8] = "Dagarle",
    [ 9] = "Elorn",
    [10] = "Frodes",
    [11] = "Gaylestrum",
    [12] = "Heidie",
    [13] = "Ironankh",
    [14] = "Irontitanhc",
    [15] = "Jazzmean",
    [16] = "Kyrika",
    [17] = "Lachy",
    [18] = "Luethien",
    [19] = "Pencilvestor",
    [20] = "Retrav",
    [21] = "Sadistia",
    [22] = "Slayingfreak",
    [23] = "Sneakydoodle",
    [24] = "Varv",
    [25] = "Velladonna",
    [26] = "Whitewidow",
    [27] = "Wootzz",
    [28] = "Xremi",
  },
  kills = {
    {boss = 625, timestamp = "2021-12-08 20:06", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,18,19,20,21,23,24,25,26,27}},
    {boss = 730, timestamp = "2021-12-08 21:17", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28}},
    {boss = 731, timestamp = "2021-12-08 22:00", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28}},
  },
  description = "2021-12-08_SSC_TK",
  drops = {
    {player = 13, timestamp = "2021-12-08 19:33", item = 30023, cost = "", costType = "OS"}, -- Ironankh : Totem of the Maelstrom
    {player =  8, timestamp = "2021-12-08 20:09", item = 30239, cost = "", costType = "OS"}, -- Dagarle : Gloves of the Vanquished Champion
    {player =  4, timestamp = "2021-12-08 20:09", item = 30240, cost = "", costType = "MS"}, -- Brickk : Gloves of the Vanquished Defender
    {player =  1, timestamp = "2021-12-08 20:10", item = 30096, cost = "", costType = "MS"}, -- Ashgon : Girdle of the Invulnerable
    {player =  6, timestamp = "2021-12-08 20:47", item = 30024, cost = "", costType = "OS"}, -- Ceruwolfe : Mantle of the Elven Kings
    {player = 28, timestamp = "2021-12-08 21:20", item = 29921, cost = "", costType = "MS"}, -- Xremi : Fire Crest Breastplate
    {player = 9, timestamp = "2021-12-08 21:21", item = 29949, cost = "100", costType = "Solstice Points"}, -- Elorn : Arcanite Steam-Pistol
    {player =  5, timestamp = "2021-12-08 22:03", item = 30248, cost = "80", costType = "Solstice Points"}, -- Capecrusader : Pauldrons of the Vanquished Champion
    {player = 28, timestamp = "2021-12-08 22:04", item = 30619, cost = "", costType = "MS"}, -- Xremi : Fel Reaver's Piston
    {player = 25, timestamp = "2021-12-08 22:04", item = 30283, cost = "", costType = "MS"}, -- Velladonna : Pattern: Boots of the Long Road
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
