local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Brickk",
    [ 4] = "Cailyse",
    [ 5] = "Capecrusader",
    [ 6] = "Dagarle",
    [ 7] = "Elorn",
    [ 8] = "Estrellita",
    [ 9] = "Frodes",
    [10] = "Gaylestrum",
    [11] = "Inxi",
    [12] = "Ironflurry",
    [13] = "Jazzmean",
    [14] = "Kharlamagne",
    [15] = "Kyrika",
    [16] = "Lachy",
    [17] = "Luethien",
    [18] = "Skeeta",
    [19] = "Slayingfreak",
    [20] = "Soulbane",
    [21] = "Superboots",
    [22] = "Sylaramynd",
    [23] = "Varv",
    [24] = "Velladonna",
    [25] = "Whitewidow",
    [26] = "Wootzz",
  },
  kills = {
    {boss = 623, timestamp = "2021-10-05 19:53", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,18,19,20,21,22,23,24,25,26}},
    {boss = 624, timestamp = "2021-10-05 20:35", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,18,19,20,21,22,23,24,25,26}},
    {boss = 625, timestamp = "2021-10-05 21:59", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
  },
  drops = {
    {player =  3, timestamp = "2021-10-05 20:56", item = 30058, cost = "70", costType = "Solstice Points"}, -- Brickk : Mallet of the Tides
    {player = 12, timestamp = "2021-10-05 22:02", item = 30627, cost = "70", costType = "Solstice Points"}, -- Ironflurry : Tsunami Talisman
    {player =  1, timestamp = "2021-10-05 22:03", item = 30239, cost = "80", costType = "Solstice Points"}, -- Ashgon : Gloves of the Vanquished Champion
    {player =  8, timestamp = "2021-10-05 22:05", item = 30240, cost = "80", costType = "Solstice Points"}, -- Estrellita : Gloves of the Vanquished Defender
    {player =  23, timestamp = "2021-10-05      ", item = 33055, cost = "", costType = "OS"}, -- Varv : Band of Vile Aggression
    {player =  20, timestamp = "2021-10-05      ", item = 30050, cost = "", costType = "OS"}, -- Soulbane : Boots of the shifting nightmare
    {player =  14, timestamp = "2021-10-05      ", item = 30065, cost = "", costType = "OS"}, -- Kharl : Glowing Breastplate of Truth
    {player =  22, timestamp = "2021-10-05      ", item = 30021, cost = "", costType = "OS"}, -- Sylaramynd : Wilfury Greatstaff
    {player =  19, timestamp = "2021-10-05      ", item = 30025, cost = "", costType = "OS"}, -- Slayingfreak : Serpentshrine Shuriken
    {player =  3, timestamp = "2021-10-05      ", item = 30025, cost = "", costType = "OS"}, -- Brickk : Serpentshrine Shuriken
    {player =  9, timestamp = "2021-10-05      ", item = 30021, cost = "", costType = "OS"}, -- Frodes : Wilfury Greatstaff
    {player =  5, timestamp = "2021-10-05      ", item = 30302, cost = "", costType = "MS"}, -- Capecrusader : Pattern: Belt of Deep Shadow

  },
  description = "2021-10-05_SSC",
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
