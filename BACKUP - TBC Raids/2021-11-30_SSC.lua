local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Brickk",
    [ 4] = "Capecrusader",
    [ 5] = "Ceruwolfe",
    [ 6] = "Cholula",
    [ 7] = "Dagarle",
    [ 8] = "Elorn",
    [ 9] = "Frodes",
    [10] = "Gaylestrum",
    [11] = "Heidie",
    [12] = "Ironankh",
    [13] = "Irontitanhc",
    [14] = "Jazzmean",
    [15] = "Lachy",
    [16] = "Pencilvestor",
    [17] = "Sadistia",
    [18] = "Sinwave",
    [19] = "Slayingfreak",
    [20] = "Sneakydoodle",
    [21] = "Soulbane",
    [22] = "Superboots",
    [23] = "Varv",
    [24] = "Velladonna",
    [25] = "Whitewidow",
  },
  kills = {
    {boss = 623, timestamp = "2021-11-30 19:21", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 624, timestamp = "2021-11-30 19:49", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2021-11-30_SSC",
  drops = {
    {player =  6, timestamp = "2021-11-30 19:23", item = 30055, cost = "", costType = "MS"}, -- Cholula : Shoulderpads of the Stranger
    {player = 11, timestamp = "2021-11-30 19:24", item = 30047, cost = "", costType = "MS"}, -- Heidie : Blackfathom Warbands
    {player =  6, timestamp = "2021-11-30 19:51", item = 30060, cost = "", costType = "OS"}, -- Cholula : Boots of Effortless Striking
    {player =  1, timestamp = "2021-11-30 19:53", item = 33054, cost = "", costType = "OS"}, -- Ashgon : The Seal of Danzalar
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
