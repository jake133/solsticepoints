local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Adiyah",
    [ 2] = "Anatyr",
    [ 3] = "Annarinn",
    [ 4] = "Aythya",
    [ 5] = "Bedonkadonk",
    [ 6] = "Blackcläws",
    [ 7] = "Capecrusader",
    [ 8] = "Chalula",
    [ 9] = "Chamwow",
    [10] = "Dagarle",
    [11] = "Eillowee",
    [12] = "Gaylestrum",
    [13] = "Irontitann",
    [14] = "Jaeran",
    [15] = "Kattianna",
    [16] = "Lachý",
    [17] = "Luethien",
    [18] = "Luxmajora",
    [19] = "Mayaell",
    [20] = "Pencilvestor",
    [21] = "Sambearpig",
    [22] = "Sleepysak",
    [23] = "Snoweyes",
    [24] = "Superboots",
    [25] = "Xandies",
  },
  kills = {
    {boss = 601, timestamp = "2022-08-02 19:21", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 602, timestamp = "2022-08-02 19:37", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 603, timestamp = "2022-08-02 19:53", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 607, timestamp = "2022-08-02 20:20", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 608, timestamp = "2022-08-02 20:48", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 609, timestamp = "2022-08-02 21:14", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 604, timestamp = "2022-08-02 21:49", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-08-02_BT",
  drops = {
    {player =  5, timestamp = "2022-08-02 19:54", item = 32234, costType = "MS"}, -- Bedonkadonk : Fists of Mukoa
    {player = 25, timestamp = "2022-08-02 19:55", item = 32242, costType = "OS"}, -- Xandies : Boots of Oceanic Fury
    {player =  6, timestamp = "2022-08-02 19:57", item = 32260, cost = 70, costType = "Solstice Points"}, -- Blackcläws : Choker of Endless Nightmares
    {player = 13, timestamp = "2022-08-02 20:09", item = 32250, costType = "MS"}, -- Irontitann : Pauldrons of Abyssal Fury
    {player = 18, timestamp = "2022-08-02 20:22", item = 32278, costType = "OS"}, -- Luxmajora : Grips of Silent Justice
    {player = 17, timestamp = "2022-08-02 20:27", item = 32513, costType = "MS"}, -- Luethien : Wristbands of Divine Influence
    {player =  5, timestamp = "2022-08-02 21:22", item = 32943, costType = "MS"}, -- Bedonkadonk : Swiftsteel Bludgeon
    {player = 25, timestamp = "2022-08-02 21:24", item = 32367, costType = "OS"}, -- Xandies : Leggings of Devastation
    {player =  5, timestamp = "2022-08-02 21:26", item = 31103, costType = "MS"}, -- Bedonkadonk : Pauldrons of the Forgotten Protector
    {player = 25, timestamp = "2022-08-02 21:26", item = 31103, costType = "OS"}, -- Xandies : Pauldrons of the Forgotten Protector
    {player =  2, timestamp = "2022-08-02 21:29", item = 31101, cost = 80, costType = "Solstice Points"}, -- Anatyr : Pauldrons of the Forgotten Conqueror
    {player = 17, timestamp = "2022-08-02 21:31", item = 32519, costType = "MS"}, -- Luethien : Belt of Divine Guidance
    {player = 10, timestamp = "2022-08-02 21:33", item = 31098, costType = "OS"}, -- Dagarle : Leggings of the Forgotten Conqueror
    {player =  5, timestamp = "2022-08-02 21:34", item = 31100, costType = "MS"}, -- Bedonkadonk : Leggings of the Forgotten Protector
    {player = 19, timestamp = "2022-08-02 21:35", item = 31099, costType = "OS"}, -- Mayaell : Leggings of the Forgotten Vanquisher
    {player =  6, timestamp = "2022-08-02 21:44", item = 32521, costType = "OS"}, -- Blackcläws : Faceplate of the Impenetrable
    {player =  5, timestamp = "2022-08-02 21:45", item = 31091, costType = "MS"}, -- Bedonkadonk : Chestguard of the Forgotten Protector
    {player = 16, timestamp = "2022-08-02 21:50", item = 31090, costType = "MS"}, -- Lachý : Chestguard of the Forgotten Vanquisher
    {player = 12, timestamp = "2022-08-02 21:50", item = 31090, costType = "OS"}, -- Gaylestrum : Chestguard of the Forgotten Vanquisher
    {player =  1, timestamp = "2022-08-02 21:53", item = 32496, costType = "MS"}, -- Adiyah : Memento of Tyrande
    {player =  2, timestamp = "2022-08-02 21:54", item = 32323, cost = 60, costType = "Solstice Points"}, -- Anatyr : Shadowmoon Destroyer's Drape
    {player =  9, timestamp = "2022-08-02 21:55", item = 32330, costType = "MS"}, -- Chamwow : Totem of Ancestral Guidance
    {player = 10, timestamp = "2022-08-02 21:57", item = 32608, costType = "OS"}, -- Dagarle : Pillager's Gauntlets
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
