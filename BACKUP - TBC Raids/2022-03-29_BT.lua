local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Aythya",
    [ 2] = "Bbeamer",
    [ 3] = "Blackcläws",
    [ 4] = "Capecrusader",
    [ 5] = "Chalula",
    [ 6] = "Eillowee",
    [ 7] = "Elorn",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Irontitann",
    [11] = "Jazzmene",
    [12] = "Kattianna",
    [13] = "Kharlamagne",
    [14] = "Lachý",
    [15] = "Noritotes",
    [16] = "Pencilvestor",
    [17] = "Roachy",
    [18] = "Roragorn",
    [19] = "Sadistia",
    [20] = "Skeeta",
    [21] = "Slayingfreak",
    [22] = "Snoweyes",
    [23] = "Snozzberry",
    [24] = "Superboots",
    [25] = "Wootzz",
    [26] = "Xandies",
  },
  kills = {
    {boss = 601, timestamp = "2022-03-29 19:35", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,25,26}},
    {boss = 602, timestamp = "2022-03-29 19:56", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,25,26}},
    {boss = 603, timestamp = "2022-03-29 20:19", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,25,26}},
    {boss = 604, timestamp = "2022-03-29 20:53", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 606, timestamp = "2022-03-29 21:57", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
  },
  description = "2022-03-29_BT",
  drops = {
    {player = 21, timestamp = "2022-03-29 20:02", item = 32232, cost = "", costType = "OS"}, -- Slayingfreak : Eternium Shell Bracers
    {player = 17, timestamp = "2022-03-29 20:03", item = 32738, cost = "", costType = "MS"}, -- Roachy : Plans: Dawnsteel Bracers
    {player =  9, timestamp = "2022-03-29 20:19", item = 32239, cost = 60, costType = "Solstice Points"}, -- Gaylestrum : Slippers of the Seacaller
    {player = 21, timestamp = "2022-03-29 20:20", item = 32253, cost = "", costType = "MS"}, -- Slayingfreak : Legionkiller
    {player = 23, timestamp = "2022-03-29 20:20", item = 32257, cost = "", costType = "OS"}, -- Snozzberry : Idol of the White Stag
    {player = 11, timestamp = "2022-03-29 20:21", item = 32528, cost = 70, costType = "Solstice Points"}, -- Jazzmene : Blessed Band of Karabor
    {player = 17, timestamp = "2022-03-29 20:21", item = 32275, cost = "", costType = "MS"}, -- Roachy : Spiritwalker Gauntlets
    {player = 20, timestamp = "2022-03-29 20:22", item = 32264, cost = "", costType = "MS"}, -- Skeeta : Shoulders of the Hidden Predator
    {player = 22, timestamp = "2022-03-29 20:24", item = 32747, cost = "", costType = "MS"}, -- Snoweyes : Pattern: Swiftstrike Shoulders
    {player =  3, timestamp = "2022-03-29 20:56", item = 32348, cost = "", costType = "MS"}, -- Blackcläws : Soul Cleaver
    {player =  1, timestamp = "2022-03-29 21:00", item = 32324, cost = 60, costType = "Solstice Points"}, -- Aythya : Insidious Bands
    {player = 15, timestamp = "2022-03-29 21:58", item = 32346, cost = 60, costType = "Solstice Points"}, -- Noritotes : Boneweave Girdle
    {player = 26, timestamp = "2022-03-29 21:59", item = 32352, cost = "", costType = "MS"}, -- Xandies : Naturewarden's Treads
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)

