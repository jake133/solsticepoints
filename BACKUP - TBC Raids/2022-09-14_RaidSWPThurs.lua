local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Annarinn",
    [ 3] = "Aythya",
    [ 4] = "Birdlove",
    [ 5] = "Capecrusader",
    [ 6] = "Chalula",
    [ 7] = "Chamwow",
    [ 8] = "Devynna",
    [ 9] = "Donori",
    [10] = "Dévil",
    [11] = "Eillowee",
    [12] = "Fellador",
    [13] = "Floordaddy",
    [14] = "Gaylestrum",
    [15] = "Hadgarthil",
    [16] = "Headonaldo",
    [17] = "Hydraxt",
    [18] = "Irontitann",
    [19] = "Jazzmene",
    [20] = "Mayaell",
    [21] = "Pallylove",
    [22] = "Phackin",
    [23] = "Phillis",
    [24] = "Prostar",
    [25] = "Rosemondon",
    [26] = "Sethela",
    [27] = "Slayingfreak",
    [28] = "Snoweyes",
    [29] = "Xandies",
  },
  kills = {
    {boss = 727, timestamp = "2022-09-14 19:36", players = {1,2,3,4,5,6,7,10,11,12,13,14,15,17,18,19,20,21,23,24,25,26,27,28,29}},
    {boss = 728, timestamp = "2022-09-14 20:14", players = {1,2,3,4,5,6,7,9,10,11,12,13,14,15,17,18,19,20,21,23,24,25,26,27,28,29}},
    {boss = 729, timestamp = "2022-09-14 21:50", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29}},
  },
  description = "2022-09-14_RaidSWPThurs",
  drops = {
    {player = 12, timestamp = "2022-09-14 19:40", item = 34199, costType = "MS"}, -- Fellador : Archon's Gavel
    {player = 19, timestamp = "2022-09-14 19:42", item = 34856, cost = 80, costType = "Solstice Points"}, -- Jazzmene : Boots of the Forgotten Conqueror
    {player = 20, timestamp = "2022-09-14 19:42", item = 35291, costType = "MS"}, -- Mayaell : Sin'dorei Pendant of Salvation
    {player = 20, timestamp = "2022-09-14 19:46", item = 34209, costType = "MS"}, -- Mayaell : Spaulders of Reclamation
    {player = 21, timestamp = "2022-09-14 19:48", item = 35291, costType = "MS"}, -- Pallylove : Sin'dorei Pendant of Salvation
    {player = 13, timestamp = "2022-09-14 20:15", item = 34204, costType = "MS"}, -- Floordaddy : Amulet of Unfettered Magics
    {player =  6, timestamp = "2022-09-14 20:16", item = 34195, costType = "MS"}, -- Chalula : Shoulderpads of Vehemence
    {player = 11, timestamp = "2022-09-14 20:35", item = 34232, cost = 60, costType = "Solstice Points"}, -- Eillowee : Fel Conquerer Raiments
    {player =  7, timestamp = "2022-09-14 20:35", item = 34229, costType = "MS"}, -- Chamwow : Garments of Serene Shores
    {player = 27, timestamp = "2022-09-14 20:36", item = 35284, costType = "OS"}, -- Slayingfreak : Sin'dorei Band of Triumph
    {player =  7, timestamp = "2022-09-14 20:38", item = 34430, cost = 70, costType = "Solstice Points"}, -- Chamwow : Glimmering Naaru Sliver
    {player =  1, timestamp = "2022-09-14 21:52", item = 34345, costType = "MS"}, -- Anatyr : Crown of Anasterian
    {player = 18, timestamp = "2022-09-14 21:53", item = 34243, cost = 60, costType = "Solstice Points"}, -- Irontitann : Helm of Burning Righteousness
    {player = 28, timestamp = "2022-09-14 21:54", item = 34333, cost = 60, costType = "Solstice Points"}, -- Snoweyes : Coif of Alleria
    {player = 29, timestamp = "2022-09-14 21:56", item = 34340, cost = 60, costType = "Solstice Points"}, -- Xandies : Dark Conjuror's Collar
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
