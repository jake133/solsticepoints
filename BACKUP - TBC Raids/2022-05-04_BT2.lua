local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Aythya",
    [ 3] = "Capecrusader",
    [ 4] = "Eillowee",
    [ 5] = "Elorn",
    [ 6] = "Gaylestrum",
    [ 7] = "Irontitann",
    [ 8] = "Jazzmene",
    [ 9] = "Kattianna",
    [10] = "Kekett",
    [11] = "Lachý",
    [12] = "Luethien",
    [13] = "Maërlyn",
    [14] = "Melisea",
    [15] = "Obijuanknobi",
    [16] = "Pencilvestor",
    [17] = "Roragorn",
    [18] = "Sadistia",
    [19] = "Slayingfreak",
    [20] = "Snoweyes",
    [21] = "Snozzberry",
    [22] = "Superboots",
    [23] = "Tinywinks",
    [24] = "Varv",
    [25] = "Wootzz",
    [26] = "Xandies",
  },
  kills = {
    {boss = 608, timestamp = "2022-05-04 20:38", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
  },
  description = "2022-05-04_BT2",
  drops = {
    {player =  6, timestamp = "2022-05-04 20:40", item = 32331, cost = 60, costType = "Solstice Points"}, -- Gaylestrum : Cloak of the Illidari Council
    {player = 16, timestamp = "2022-05-04 20:41", item = 31099, cost = 80, costType = "Solstice Points"}, -- Pencilvestor : Leggings of the Forgotten Vanquisher
    {player =  2, timestamp = "2022-05-04 20:42", item = 31100, cost = 80, costType = "Solstice Points"}, -- Aythya : Leggings of the Forgotten Protector
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
