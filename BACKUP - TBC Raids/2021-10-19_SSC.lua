local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Brickk",
    [ 4] = "Capecrusader",
    [ 5] = "Dagarle",
    [ 6] = "Elorn",
    [ 7] = "Estrellita",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Ironflurry",
    [11] = "Irontitanhc",
    [12] = "Jazzmean",
    [13] = "Kharlamagne",
    [14] = "Lachy",
    [15] = "Luethien",
    [16] = "Pencilvestor",
    [17] = "Sinniaa",
    [18] = "Skeeta",
    [19] = "Slayingfreak",
    [20] = "Sneakydoodle",
    [21] = "Soulbane",
    [22] = "Superboots",
    [23] = "Varv",
    [24] = "Velladonna",
    [25] = "Whitewidow",
  },
  kills = {
    {boss = 625, timestamp = "2021-10-19 20:21", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 626, timestamp = "2021-10-19 21:03", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2021-10-19_SSC",
  drops = {
    {player = 13, timestamp = "2021-10-19 20:23", item = 30027, cost = "", costType = "MS"}, -- Kharlamagne : Boots of Courage Unending
    {player = 16, timestamp = "2021-10-19 20:24", item = 30239, cost = "80", costType = "Solstice Points"}, -- Pencilvestor : Gloves of the Vanquished Champion
    {player =  4, timestamp = "2021-10-19 20:25", item = 30239, cost = "", costType = "MS"}, -- Capecrusader : Gloves of the Vanquished Champion
    {player = 20, timestamp = "2021-10-19 20:27", item = 30627, cost = "70", costType = "Solstice Points"}, -- Sneakydoodle : Tsunami Talisman
    {player = 15, timestamp = "2021-10-19 22:14", item = 30246, cost = "80", costType = "Solstice Points"}, -- Luethien : Leggings of the Vanquished Defender
    {player = 21, timestamp = "2021-10-19 22:15", item = 30247, cost = "80", costType = "Solstice Points"}, -- Soulbane : Leggings of the Vanquished Hero
    {player =  4, timestamp = "2021-10-19 22:17", item = 30101, cost = "60", costType = "Solstice Points"}, -- Capecrusader : Bloodsea Brigand's Vest
    {player =  4, timestamp = "2021-10-19 22:17", item = 30306, cost = "", costType = "MS"}, -- Capecrusader : Pattern Boots of Utter Darkness
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
