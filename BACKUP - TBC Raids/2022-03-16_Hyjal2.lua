local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Aythya",
    [ 2] = "Blackcläws",
    [ 3] = "Capecrusader",
    [ 4] = "Chalula",
    [ 5] = "Eillowee",
    [ 6] = "Elorn",
    [ 7] = "Frodes",
    [ 8] = "Gaylestrum",
    [ 9] = "Hateshift",
    [10] = "Irontitann",
    [11] = "Jazzmene",
    [12] = "Kharlamagne",
    [13] = "Lachý",
    [14] = "Luethien",
    [15] = "Maërlyn",
    [16] = "Noritotes",
    [17] = "Pencilvestor",
    [18] = "Roragorn",
    [19] = "Sadistia",
    [20] = "Slayingfreak",
    [21] = "Snoweyes",
    [22] = "Superboots",
    [23] = "Wootzz",
    [24] = "Xandies",
  },
  kills = {
    {boss = 621, timestamp = "2022-03-16 19:48", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24}},
    {boss = 622, timestamp = "2022-03-16 21:00", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24}},
    {boss = 601, timestamp = "2022-03-16 21:33", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24}},
    {boss = 602, timestamp = "2022-03-16 21:56", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24}},
  },
  description = "2022-03-16_Hyjal2",
  drops = {
    {player = 23, timestamp = "2022-03-16 19:26", item = 32590, cost = 60, costType = "Solstice Points"}, -- Wootzz : Nethervoid Cloak
    {player =  9, timestamp = "2022-03-16 19:27", item = 32590, cost = "", costType = "MS"}, -- Hateshift : Nethervoid Cloak
    {player =  1, timestamp = "2022-03-16 20:45", item = 31094, cost = "", costType = "MS"}, -- Aythya : Gloves of the Forgotten Protector
    {player =  3, timestamp = "2022-03-16 21:01", item = 31093, cost = "", costType = "MS"}, -- Capecrusader : Gloves of the Forgotten Vanquisher
    {player =  4, timestamp = "2022-03-16 21:03", item = 30899, cost = "", costType = "OS"}, -- Chalula : Don Rodrigo's Poncho
    {player = 15, timestamp = "2022-03-16 21:04", item = 32589, cost = "", costType = "MS"}, -- Maërlyn : Hellfire-Encased Pendant
    {player = 16, timestamp = "2022-03-16 21:04", item = 30907, cost = "", costType = "MS"}, -- Noritotes : Mail of Fevered Pursuit
    {player = 22, timestamp = "2022-03-16 21:05", item = 30911, cost = 70, costType = "Solstice Points"}, -- Superboots : Scepter of Purification
    {player = 21, timestamp = "2022-03-16 21:06", item = 31095, cost = 80, costType = "Solstice Points"}, -- Snoweyes : Helm of the Forgotten Protector
    {player = 20, timestamp = "2022-03-16 21:06", item = 31095, cost = 80, costType = "Solstice Points"}, -- Slayingfreak : Helm of the Forgotten Protector
    {player = 10, timestamp = "2022-03-16 21:34", item = 32234, cost = "", costType = "OS"}, -- Irontitann : Fists of Mukoa
    {player = 12, timestamp = "2022-03-16 21:34", item = 32243, cost = "", costType = "MS"}, -- Kharlamagne : Pearl Inlaid Boots
    {player = 24, timestamp = "2022-03-16 21:57", item = 32255, cost = "", costType = "OS"}, -- Xandies : Felstone Bulwark
    {player = 24, timestamp = "2022-03-16 21:59", item = 32258, cost = "", costType = "MS"}, -- Xandies : Naturalist's Preserving Cinch
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
