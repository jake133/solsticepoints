local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Brickk",
    [ 4] = "Capecrusader",
    [ 5] = "Dagarle",
    [ 6] = "Elorn",
    [ 7] = "Estrellita",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Inxi",
    [11] = "Ironflurry",
    [12] = "Irontitanhc",
    [13] = "Kharlamagne",
    [14] = "Kyrika",
    [15] = "Lachy",
    [16] = "Luethien",
    [17] = "Pencilvestor",
    [18] = "Skeeta",
    [19] = "Sneakydoodle",
    [20] = "Superboots",
    [21] = "Varv",
    [22] = "Velladonna",
    [23] = "Whitewidow",
    [24] = "Wootzz",
    [25] = "Xandie",
  },
  kills = {
    {boss = 626, timestamp = "2021-10-06 20:20", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2021-10-06_SSC",
  drops = {
    {player = 18, timestamp = "2021-10-06 20:24", item = 30247, cost = "80", costType = "Solstice Points"}, -- Skeeta : Leggings of the Vanquished Hero
    {player = 20, timestamp = "2021-10-06 20:25", item = 30246, cost = "80", costType = "Solstice Points"}, -- Superboots : Leggings of the Vanquished Defender
    {player = 25, timestamp = "2021-10-06 20:26", item = 30663, cost = "70", costType = "Solstice Points"}, -- Xandie : Fathom-Brooch of the Tidewalker
    {player =  1, timestamp = "2021-10-06 20:46", item = 30022, cost = "", costType = "OS"}, -- Ashgon : Pendant of the Perilous
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
