local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Brickk",
    [ 4] = "Ceruwolfe",
    [ 5] = "Dagarle",
    [ 6] = "Elorn",
    [ 7] = "Frodes",
    [ 8] = "Gaylestrum",
    [ 9] = "Heidie",
    [10] = "Ironflurry",
    [11] = "Irontitanhc",
    [12] = "Jazzmean",
    [13] = "Lachy",
    [14] = "Pencilvestor",
    [15] = "Retrav",
    [16] = "Sadistia",
    [17] = "Sinwave",
    [18] = "Skeeta",
    [19] = "Slayingfreak",
    [20] = "Sneakydoodle",
    [21] = "Soulbane",
    [22] = "Superboots",
    [23] = "Varv",
    [24] = "Velladonna",
    [25] = "Whitewidow",
    [26] = "Wootzz",
  },
  kills = {
    {boss = 731, timestamp = "2021-11-16 21:43", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
  },
  description = "2021-11-16_TK",
  drops = {
    {player = 13, timestamp = "2021-11-16 21:45", item = 30250, cost = "80", costType = "Solstice Points"}, -- Lachy : Pauldrons of the Vanquished Hero
    {player = 14, timestamp = "2021-11-16 21:45", item = 30248, cost = "80", costType = "Solstice Points"}, -- Pencilvestor : Pauldrons of the Vanquished Champion
    {player =  9, timestamp = "2021-11-16 21:46", item = 30619, cost = "", costType = "MS"}, -- Heidie : Fel Reaver's Piston
    {player = 26, timestamp = "2021-11-16 21:47", item = 30020, cost = "", costType = "MS"}, -- Wootzz : Fire-Cord of the Magus
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
