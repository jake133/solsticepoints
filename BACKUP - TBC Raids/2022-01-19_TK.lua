local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aythya",
    [ 3] = "Aziza",
    [ 4] = "Capecrusader",
    [ 5] = "Cholula",
    [ 6] = "Eillowee",
    [ 7] = "Elorn",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Ironankh",
    [11] = "Irontitanhc",
    [12] = "Jazzmean",
    [13] = "Kharlamagne",
    [14] = "Kyrika",
    [15] = "Lachy",
    [16] = "Luethien",
    [17] = "Pencilvestor",
    [18] = "Pulchra",
    [19] = "Roragorn",
    [20] = "Sadistia",
    [21] = "Slayingfreak",
    [22] = "Sneakydoodle",
    [23] = "Superboots",
    [24] = "Varv",
    [25] = "Velladonna",
    [26] = "Wootzz",
  },
  kills = {
    {boss = 730, timestamp = "2022-01-19 19:36", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,19,20,21,22,23,24,25,26}},
    {boss = 731, timestamp = "2022-01-19 20:01", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 732, timestamp = "2022-01-19 20:26", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 733, timestamp = "2022-01-19 21:02", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
  },
  description = "2022-01-19_TK",
  drops = {
    {player = 24, timestamp = "2022-01-19 19:37", item = 29947, cost = "", costType = "MS"}, -- Varv : Gloves of the Searing Grip
    {player = 11, timestamp = "2022-01-19 19:38", item = 29925, cost = "", costType = "MS"}, -- Irontitanhc : Phoenix-Wing Cloak
    {player =  1, timestamp = "2022-01-19 19:38", item = 30447, cost = "", costType = "MS"}, -- Ashgon : Tome of Fiery Redemption
    {player = 18, timestamp = "2022-01-19 20:02", item = 29983, cost = "", costType = "MS"}, -- Pulchra : Fel-Steel Warhelm
    {player =  5, timestamp = "2022-01-19 20:03", item = 30249, cost = "", costType = "MS"}, -- Cholula : Pauldrons of the Vanquished Defender
    {player = 21, timestamp = "2022-01-19 20:03", item = 30249, cost = "", costType = "MS"}, -- Slayingfreak : Pauldrons of the Vanquished Defender
    {player = 18, timestamp = "2022-01-19 20:04", item = 30248, cost = "", costType = "MS"}, -- Pulchra : Pauldrons of the Vanquished Champion
    {player = 13, timestamp = "2022-01-19 20:07", item = 30030, cost = "", costType = "MS"}, -- Kharlamagne : Girdle of Fallen Stars
    {player = 12, timestamp = "2022-01-19 20:26", item = 29981, cost = "", costType = "MS"}, -- Jazz : Ethereum Life-Staff
    {player = 24, timestamp = "2022-01-19 20:27", item = 32267, cost = "", costType = "MS"}, -- Varv : Boots of the Resilient
    {player =  2, timestamp = "2022-01-19 20:28", item = 29951, cost = "", costType = "MS"}, -- Aythya : Star-Strider Boots
    {player = 18, timestamp = "2022-01-19 20:38", item = 30026, cost = "", costType = "MS"}, -- Pulchra : Bands of the Celestial Archer
    {player =  1, timestamp = "2022-01-19 20:42", item = 30030, cost = "", costType = "OS"}, -- Ashgon : Girdle of Fallen Stars
    {player =  6, timestamp = "2022-01-19 20:44", item = 30281, cost = "", costType = "MS"}, -- Eillowee : Pattern: Belt of the Long Road
    {player =  5, timestamp = "2022-01-19 20:46", item = 30029, cost = "", costType = "OS"}, -- Cholula : Bark-Gloves of Ancient Wisdom
    {player = 18, timestamp = "2022-01-19 21:03", item = 29993, cost = "", costType = "MS"}, -- Pulchra : Twinblade of the Phoenix
    {player =  3, timestamp = "2022-01-19 21:04", item = 30236, cost = "80", costType = "Solstice Points"}, -- Aziza : Chestguard of the Vanquished Champion
    {player = 11, timestamp = "2022-01-19 21:05", item = 30236, cost = "80", costType = "Solstice Points"}, -- Irontitanhc : Chestguard of the Vanquished Champion
    {player = 24, timestamp = "2022-01-19 21:06", item = 30237, cost = "80", costType = "Solstice Points"}, -- Varv : Chestguard of the Vanquished Defender
    {player = 13, timestamp = "2022-01-19 21:07", item = 29991, cost = "", costType = "MS"}, -- Kharlamagne : Sunhawk Leggings
    {player = 19, timestamp = "2022-01-19 21:08", item = 32405, cost = "60", costType = "Solstice Points"}, -- Roragorn : Verdant Sphere
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
