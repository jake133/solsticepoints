local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aythya",
    [ 3] = "Aziza",
    [ 4] = "Blackclaws",
    [ 5] = "Cholula",
    [ 6] = "Eillowee",
    [ 7] = "Elorn",
    [ 8] = "Gaylestrum",
    [ 9] = "Ironankh",
    [10] = "Irontitanhc",
    [11] = "Jazzmean",
    [12] = "Kharlamagne",
    [13] = "Kyrika",
    [14] = "Lachy",
    [15] = "Luethien",
    [16] = "Pencilvestor",
    [17] = "Roragorn",
    [18] = "Sadistia",
    [19] = "Sinniaa",
    [20] = "Slayingfreak",
    [21] = "Sneakydoodle",
    [22] = "Superboots",
    [23] = "Varv",
    [24] = "Velladonna",
    [25] = "Whitewidow",
  },
  kills = {
    {boss = 618, timestamp = "2022-02-08 19:13", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 619, timestamp = "2022-02-08 19:32", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 620, timestamp = "2022-02-08 20:43", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-02-08_Hyjal",
  drops = {
    {player = 12, timestamp = "2022-02-08 19:14", item = 30869, cost = "", costType = "MS"}, -- Kharlamagne : Howling Wind Bracers
    {player =  4, timestamp = "2022-02-08 19:15", item = 30861, cost = "", costType = "MS"}, -- Blackclaws : Furious Shackles
    {player = 23, timestamp = "2022-02-08 19:33", item = 30874, cost = "", costType = "MS"}, -- Varv : The Unbreakable Will
    {player = 19, timestamp = "2022-02-08 19:33", item = 30886, cost = "", costType = "MS"}, -- Sinniaa : Enchanted Leather Sandals
    {player = 21, timestamp = "2022-02-08 19:37", item = 32591, cost = 70, costType = "Solstice Points"}, -- Sneakydoodle : Choker of Serrated Blades
    {player = 10, timestamp = "2022-02-08 19:38", item = 34009, cost = 70, costType = "Solstice Points"}, -- Irontitanhc : Hammer of Judgement
    {player = 24, timestamp = "2022-02-08 19:40", item = 32755, cost = 10, costType = "Solstice Points"}, -- Velladonna : Pattern: Mantle of Nimble Thought
    {player = 18, timestamp = "2022-02-08 20:45", item = 30916, cost = 60, costType = "Solstice Points"}, -- Sadistia : Leggings of Channeled Elements
    {player =  7, timestamp = "2022-02-08 20:45", item = 30892, cost = "", costType = "MS"}, -- Elorn : Beast-tamer's Shoulders
    {player =  6, timestamp = "2022-02-08 20:46", item = 32755, cost = "", costType = "MS"}, -- Eillowee : Pattern: Mantle of Nimble Thought
    {player =  9, timestamp = "2022-02-08 20:47", item = 32751, cost = "", costType = "OS"}, -- Ironankh : Pattern: Living Earth Shoulders
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
