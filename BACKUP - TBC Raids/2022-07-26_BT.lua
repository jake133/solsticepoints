local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Adiyah",
    [ 2] = "Anatyr",
    [ 3] = "Annarinn",
    [ 4] = "Aythya",
    [ 5] = "Capecrusader",
    [ 6] = "Chalula",
    [ 7] = "Eillowee",
    [ 8] = "Frodes",
    [ 9] = "Fârtblossom",
    [10] = "Gaylestrum",
    [11] = "Irontitann",
    [12] = "Jaeran",
    [13] = "Jazmene",
    [14] = "Kattianna",
    [15] = "Lachý",
    [16] = "Luxmajora",
    [17] = "Mayaell",
    [18] = "Melisea",
    [19] = "Roragorn",
    [20] = "Slayingfreak",
    [21] = "Snoweyes",
    [22] = "Superboots",
    [23] = "Unholys",
    [24] = "Wootzz",
    [25] = "Xandies",
  },
  kills = {
    {boss = 601, timestamp = "2022-07-26 19:27", players = {1,2,3,4,5,6,7,8,9,10,11,12,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 602, timestamp = "2022-07-26 19:44", players = {1,2,3,4,5,6,7,8,9,10,11,12,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 603, timestamp = "2022-07-26 19:58", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 607, timestamp = "2022-07-26 20:30", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 608, timestamp = "2022-07-26 20:55", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 609, timestamp = "2022-07-26 21:10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 604, timestamp = "2022-07-26 21:37", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-07-26_BT",
  drops = {
    {player =  7, timestamp = "2022-07-26 19:59", item = 32237, cost = 70, costType = "Solstice Points"}, -- Eillowee : The Maelstrom's Fury
    {player = 15, timestamp = "2022-07-26 20:00", item = 32239, cost = 60, costType = "Solstice Points"}, -- Lachý : Slippers of the Seacaller
    {player =  3, timestamp = "2022-07-26 20:03", item = 32260, costType = "MS"}, -- Annarinn : Choker of Endless Nightmares
    {player = 10, timestamp = "2022-07-26 20:07", item = 32361, cost = 70, costType = "Solstice Points"}, -- Gaylestrum : Blind-Seers Icon
    {player = 20, timestamp = "2022-07-26 20:12", item = 32263, costType = "OS"}, -- Slayingfreak : Praetorian's Legguards
    {player = 12, timestamp = "2022-07-26 21:18", item = 32370, costType = "MS"}, -- Jaeran : Nadina's Pendant of Purity
    {player = 19, timestamp = "2022-07-26 21:20", item = 31102, costType = "OS"}, -- Roragorn : Pauldrons of the Forgotten Vanquisher
    {player =  8, timestamp = "2022-07-26 21:21", item = 31102, costType = "OS"}, -- Frodes : Pauldrons of the Forgotten Vanquisher
    {player = 25, timestamp = "2022-07-26 21:24", item = 32331, cost = 60, costType = "Solstice Points"}, -- Xandies : Cloak of the Illidari Council
    {player = 16, timestamp = "2022-07-26 21:25", item = 31098, costType = "MS"}, -- Luxmajora : Leggings of the Forgotten Conqueror
    {player = 13, timestamp = "2022-07-26 21:26", item = 31099, costType = "MS"}, -- Jazmene : Leggings of the Forgotten Vanquisher
    {player = 10, timestamp = "2022-07-26 21:26", item = 31099, costType = "OS"}, -- Gaylestrum : Leggings of the Forgotten Vanquisher
    {player =  3, timestamp = "2022-07-26 21:40", item = 32336, costType = "OS"}, -- Annarinn : Black Bow of the Betrayer
    {player = 12, timestamp = "2022-07-26 21:43", item = 31091, cost = 80, costType = "Solstice Points"}, -- Jaeran : Chestguard of the Forgotten Protector
    {player = 23, timestamp = "2022-07-26 21:43", item = 32525, costType = "MS"}, -- Unholys : Cowl of the Illidari High Lord
    {player =  9, timestamp = "2022-07-26 21:44", item = 31103, cost = 80, costType = "Solstice Points"}, -- Fârtblossom : Pauldrons of the Forgotten Protector
    {player = 18, timestamp = "2022-07-26 21:59", item = 32608, costType = "OS"}, -- Melisea : Pillager's Gauntlets
    {player = 11, timestamp = "2022-07-26 22:00", item = 32280, costType = "OS"}, -- Irontitann : Gauntlets of Enforcement
    {player = 16, timestamp = "2022-07-26 22:01", item = 32329, costType = "MS"}, -- Luxmajora : Cowl of Benevolence
    {player = 17, timestamp = "2022-07-26 22:01", item = 31090, cost = 80, costType = "Solstice Points"}, -- Mayaell : Chestguard of the Forgotten Vanquisher
    {player = 19, timestamp = "2022-07-26 22:02", item = 31090, costType = "MS"}, -- Roragorn : Chestguard of the Forgotten Vanquisher
    {player = 20, timestamp = "2022-07-26 22:10", item = 34011, costType = "OS"}, -- Slayingfreak : Illidari Runeshield
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
