local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Annarinn",
    [ 3] = "Aythya",
    [ 4] = "Capecrusader",
    [ 5] = "Chalula",
    [ 6] = "Chamwow",
    [ 7] = "Eillowee",
    [ 8] = "Elorn",
    [ 9] = "Felcurious",
    [10] = "Frodes",
    [11] = "Fârtblossom",
    [12] = "Gaylestrum",
    [13] = "Irontitann",
    [14] = "Jazzmene",
    [15] = "Kattianna",
    [16] = "Kekett",
    [17] = "Lachý",
    [18] = "Melisea",
    [19] = "Roragorn",
    [20] = "Sadistia",
    [21] = "Skeeta",
    [22] = "Slayingfreak",
    [23] = "Superboots",
    [24] = "Wootzz",
    [25] = "Xandies",
  },
  kills = {
    {boss = 601, timestamp = "2022-06-15 19:14", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 602, timestamp = "2022-06-15 19:32", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 603, timestamp = "2022-06-15 19:47", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 607, timestamp = "2022-06-15 20:21", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 608, timestamp = "2022-06-15 21:45", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 609, timestamp = "2022-06-15 22:02", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-06-15_BT",
  drops = {
    {player = 19, timestamp = "2022-06-15 19:48", item = 32270, costType = "OS"}, -- Roragorn : Focused Mana Bindings
    {player =  5, timestamp = "2022-06-15 19:49", item = 32240, costType = "OS"}, -- Chalula : Guise of the Tidal Lurker
    {player =  1, timestamp = "2022-06-15 19:51", item = 34011, costType = "OS"}, -- Anatyr : Illidari Runeshield
    {player = 18, timestamp = "2022-06-15 20:02", item = 32261, costType = "OS"}, -- Melisea : Band of the Abyssal Lord
    {player = 11, timestamp = "2022-06-15 20:03", item = 32738, costType = "MS"}, -- Fârtblossom : Plans: Dawnsteel Bracers
    {player = 11, timestamp = "2022-06-15 20:22", item = 32365, costType = "MS"}, -- Fârtblossom : Heartshatter Breastplate
    {player = 16, timestamp = "2022-06-15 21:46", item = 31103, costType = "MS"}, -- Kekett : Pauldrons of the Forgotten Protector
    {player =  9, timestamp = "2022-06-15 21:47", item = 31102, costType = "MS"}, -- Felcurious : Pauldrons of the Forgotten Vanquisher
    {player =  5, timestamp = "2022-06-15 21:47", item = 31102, costType = "OS"}, -- Chalula : Pauldrons of the Forgotten Vanquisher
    {player = 13, timestamp = "2022-06-15 21:47", item = 32376, costType = "OS"}, -- Irontitann : Forest Prowler's Helm
    {player = 15, timestamp = "2022-06-15 21:49", item = 31099, costType = "MS"}, -- Kattianna : Leggings of the Forgotten Vanquisher
    {player =  1, timestamp = "2022-06-15 21:50", item = 31098, costType = "MS"}, -- Anatyr : Leggings of the Forgotten Conqueror
    {player = 24, timestamp = "2022-06-15 21:50", item = 31098, costType = "MS"}, -- Wootzz : Leggings of the Forgotten Conqueror
    {player =  5, timestamp = "2022-06-15 22:03", item = 31090, cost = 80, costType = "Solstice Points"}, -- Chalula : Chestguard of the Forgotten Vanquisher
    {player =  4, timestamp = "2022-06-15 22:04", item = 31090, cost = 80, costType = "Solstice Points"}, -- Capecrusader : Chestguard of the Forgotten Vanquisher
    {player = 12, timestamp = "2022-06-15 22:04", item = 31090, costType = "MS"}, -- Gaylestrum : Chestguard of the Forgotten Vanquisher
    {player = 13, timestamp = "2022-06-15 22:05", item = 32521, costType = "MS"}, -- Irontitann : Faceplate of the Impenetrable
    {player = 14, timestamp = "2022-06-15 22:05", item = 32524, cost = 60, costType = "Solstice Points"}, -- Jazzmene : Shroud of the Highborne
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
