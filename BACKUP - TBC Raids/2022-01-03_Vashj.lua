local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
	[ 1]="Ashgon",
	[ 2]="Aythya",
	[ 3]="Aziza",
	[ 4]="Capecrusader",
	[ 5]="Cholula",
	[ 6]="Druzill",
	[ 7]="Eillowee",
	[ 8]="Elorn",
	[ 9]="Gaylestrum",
	[10]="Ironankh",
	[11]="Irontitanhc",
	[12]="Jazzmean",
	[13]="Kharlamagne",
	[14]="Kyrika",
	[15]="Lachy",
	[16]="Luethien",
	[17]="Pencilvestor",
	[18]="Roragorn",
	[19]="Slayingfreak",
	[20]="Sneakydoodle",
	[21]="Superboots",
	[22]="Varv",
	[23]="Velladonna",
	[24]="Whitewidow",
	[25]="Wootzz",

  },
  kills = {
  },
  description = "2022-01-03_Vashj",
  drops = {
    {player = 19, timestamp = "2021-12 Correction", item = 30183, cost = "10", costType = "Solstice Points"}, -- Slay : Nether Vortex
    {player = 19, timestamp = "2021-12 Correction", item = 30183, cost = "10", costType = "Solstice Points"}, -- Slay : Nether Vortex
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)