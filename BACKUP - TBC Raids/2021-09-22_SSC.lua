local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Airfoil",
    [ 2] = "Aziza",
    [ 3] = "Badreams",
    [ 4] = "Brickk",
    [ 5] = "Ashgon",
    [ 6] = "Capecrusader",
    [ 7] = "Dagarle",
    [ 8] = "Elorn",
    [ 9] = "Estrellita",
    [10] = "Luethien",
    [11] = "Wootzz",
    [12] = "Iohe",
    [13] = "Ironflurry",
    [14] = "Irontitanhc",
    [15] = "Kalisae",
    [16] = "Kharlamagne",
    [17] = "Lachy",
    [18] = "Skeeta",
    [19] = "Pencilvestor",
    [20] = "Superboots",
    [21] = "Sylaramynd",
    [22] = "Velladonna",
    [23] = "Varv",
    [24] = "Whitewidow",
    [25] = "Xandie",	
  },
  kills = {
  },
  drops = {
    {player =  7, timestamp = "2021-09-22", item = 30022, cost = "", costType = "MS"}, -- Dagarle : Pendant of the Perilous
    },
  description = "2021-09-22_SSC",

}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
