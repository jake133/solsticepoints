local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Annarinn",
    [ 2] = "Aythya",
    [ 3] = "Blackcläws",
    [ 4] = "Capecrusader",
    [ 5] = "Chalula",
    [ 6] = "Dagarle",
    [ 7] = "Eillowee",
    [ 8] = "Frodes",
    [ 9] = "Fârtblossom",
    [10] = "Gaylestrum",
    [11] = "Grayskyy",
    [12] = "Jaeran",
    [13] = "Jazzmene",
    [14] = "Kattianna",
    [15] = "Machiato",
    [16] = "Mayaell",
    [17] = "Melisea",
    [18] = "Pencilvestor",
    [19] = "Skeeta",
    [20] = "Slayingfreak",
    [21] = "Snoweyes",
    [22] = "Superboots",
    [23] = "Tufenuf",
    [24] = "Varv",
    [25] = "Wootzz",
    [26] = "Xandies",
  },
  kills = {
    {boss = 601, timestamp = "2022-07-19 19:22", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,21,22,23,24,26}},
    {boss = 602, timestamp = "2022-07-19 19:39", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,21,22,23,24,26}},
    {boss = 603, timestamp = "2022-07-19 19:58", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,21,22,23,24,26}},
    {boss = 607, timestamp = "2022-07-19 21:08", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,26}},
    {boss = 608, timestamp = "2022-07-19 21:33", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,26}},
  },
  description = "2022-07-19_BT",
  drops = {
    {player = 23, timestamp = "2022-07-19 19:59", item = 32236, costType = "MS"}, -- Tufenuf : Rising Tide
    {player = 17, timestamp = "2022-07-19 19:59", item = 32243, costType = "MS"}, -- Melisea : Pearl Inlaid Boots
    {player = 24, timestamp = "2022-07-19 20:09", item = 32263, costType = "MS"}, -- Varv : Praetorian's Legguards
    {player = 15, timestamp = "2022-07-19 20:10", item = 32270, costType = "MS"}, -- Machiato : Focused Mana Bindings
    {player =  5, timestamp = "2022-07-19 20:14", item = 34012, costType = "OS"}, -- Chalula : Shroud of the Final Stand
    {player =  6, timestamp = "2022-07-19 20:19", item = 32254, costType = "OS"}, -- Dagarle : The Brutalizer
    {player = 12, timestamp = "2022-07-19 20:56", item = 32528, costType = "MS"}, -- Jaeran : Blessed Band of Karabor
    {player = 17, timestamp = "2022-07-19 21:36", item = 32370, cost = 70, costType = "Solstice Points"}, -- Melisea : Nadina's Pendant of Purity
    {player = 11, timestamp = "2022-07-19 21:37", item = 31102, costType = "OS"}, -- Grayskyy : Pauldrons of the Forgotten Vanquisher
    {player =  9, timestamp = "2022-07-19 21:48", item = 31103, costType = "MS"}, -- Fârtblossom : Pauldrons of the Forgotten Protector
    {player =  6, timestamp = "2022-07-19 22:02", item = 31101, costType = "MS"}, -- Dagarle : Pauldrons of the Forgotten Conqueror
    {player = 24, timestamp = "2022-07-19 22:03", item = 31100, costType = "MS"}, -- Varv : Leggings of the Forgotten Protector
    {player = 17, timestamp = "2022-07-19 22:03", item = 31098, costType = "OS"}, -- Melisea : Leggings of the Forgotten Conqueror
    {player =  5, timestamp = "2022-07-19 22:04", item = 31099, costType = "MS"}, -- Chalula : Leggings of the Forgotten Vanquisher
    {player =  9, timestamp = "2022-07-19 22:05", item = 32373, costType = "MS"}, -- Fârtblossom : Helm of the Illidari Shatterer
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
