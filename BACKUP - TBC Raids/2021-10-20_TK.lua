local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Adderall",
    [ 2] = "Ashgon",
    [ 3] = "Aziza",
    [ 4] = "Badkujo",
    [ 5] = "Badreams",
    [ 6] = "Capecrusader",
    [ 7] = "Dagarle",
    [ 8] = "Elorn",
    [ 9] = "Estrellita",
    [10] = "Frodes",
    [11] = "Gaylestrum",
    [12] = "Ironflurry",
    [13] = "Irontitanhc",
    [14] = "Lachy",
    [15] = "Luethien",
    [16] = "Magnaroc",
    [17] = "Pencilvestor",
    [18] = "Skeeta",
    [19] = "Sneakydoodle",
    [20] = "Superboots",
    [21] = "Varv",
    [22] = "Velladonna",
    [23] = "Whitewidow",
    [24] = "Wootzz",
    [25] = "Xremi",
	},
  kills = {
    {boss = 731, timestamp = "2021-10-20 20:29", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24}},
  },
  description = "2021-10-20_TK",
  drops = {
    {player = 21, timestamp = "2021-10-20 20:35", item = 30249, cost = "80", costType = "Solstice Points"}, -- Varv : Pauldrons of the Vanquished Defender
    {player = 23, timestamp = "2021-10-20 20:36", item = 30250, cost = "80", costType = "Solstice Points"}, -- Whitewidow : Pauldrons of the Vanquished Hero
    {player = 21, timestamp = "2021-10-20 20:37", item = 29983, cost = "", costType = "OS"}, -- Varv : Fel-Steel Warhelm
    {player =  6, timestamp = "2021-10-20 20:38", item = 30304, cost = "", costType = "MS"}, -- Capecrusader : Pattern: Monsoon Belt
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
