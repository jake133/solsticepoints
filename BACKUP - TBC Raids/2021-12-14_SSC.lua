local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aythya",
    [ 3] = "Aziza",
    [ 4] = "Brickk",
    [ 5] = "Ceruwolfe",
    [ 6] = "Cholula",
    [ 7] = "Elorn",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Heidie",
    [11] = "Ironankh",
    [12] = "Irontitanhc",
    [13] = "Jazzmean",
    [14] = "Lachy",
    [15] = "Luethien",
    [16] = "Roragorn",
    [17] = "Sadistia",
    [18] = "Sinniaa",
    [19] = "Slayingfreak",
    [20] = "Sneakydoodle",
    [21] = "Soulbane",
    [22] = "Varv",
    [23] = "Velladonna",
    [24] = "Whitewidow",
    [25] = "Wootzz",
  },
  kills = {
    {boss = 623, timestamp = "2021-12-14 19:21", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 624, timestamp = "2021-12-14 19:44", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 627, timestamp = "2021-12-14 20:11", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 626, timestamp = "2021-12-14 20:30", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 625, timestamp = "2021-12-14 21:12", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2021-12-14_SSC",
  drops = {
    {player = 16, timestamp = "2021-12-14 19:14", item = 30021, cost = "", costType = "OS"}, -- Roragorn : Wildfury Greatstaff
    {player = 16, timestamp = "2021-12-14 19:21", item = 30056, cost = "60", costType = "Solstice Points"}, -- Roragorn : Robe of Hateful Echoes
    {player = 23, timestamp = "2021-12-14 19:22", item = 30049, cost = "", costType = "MS"}, -- Velladonna : Fathomstone
    {player = 16, timestamp = "2021-12-14 19:45", item = 30067, cost = "60", costType = "Solstice Points"}, -- Roragorn : Velvet Boots of the Guardian
    {player =  4, timestamp = "2021-12-14 19:46", item = 30057, cost = "60", costType = "Solstice Points"}, -- Brickk : Bracers of Eradication
    {player = 11, timestamp = "2021-12-14 19:57", item = 30306, cost = "", costType = "MS"}, -- Ironankh : Pattern: Boots of Utter Darkness
    {player =  1, timestamp = "2021-12-14 20:12", item = 30084, cost = "", costType = "OS"}, -- Ashgon : Pauldrons of the Argent Sentinel
    {player =  9, timestamp = "2021-12-14 20:13", item = 30008, cost = "70", costType = "Solstice Points"}, -- Gaylestrum : Pendant of the Lost Ages
    {player =  6, timestamp = "2021-12-14 20:16", item = 30620, cost = "", costType = "OS"}, -- Cholula : Spyglass of the Hidden Fleet
    {player = 18, timestamp = "2021-12-14 20:31", item = 30246, cost = "80", costType = "Solstice Points"}, -- Sinniaa : Leggings of the Vanquished Defender
    {player = 19, timestamp = "2021-12-14 20:32", item = 30246, cost = "", costType = "MS"}, -- Slayingfreak : Leggings of the Vanquished Defender
    {player = 11, timestamp = "2021-12-14 20:32", item = 30663, cost = "", costType = "OS"}, -- Ironankh : Fathom-Brooch of the Tidewalker
    {player = 18, timestamp = "2021-12-14 20:40", item = 30021, cost = "", costType = "OS"}, -- Sinniaa : Wildfury Greatstaff
    {player =  9, timestamp = "2021-12-14 21:14", item = 30241, cost = "", costType = "MS"}, -- Gaylestrum : Gloves of the Vanquished Hero
    {player =  6, timestamp = "2021-12-14 21:15", item = 30240, cost = "80", costType = "Solstice Points"}, -- Cholula : Gloves of the Vanquished Defender
    {player = 11, timestamp = "2021-12-14 21:16", item = 30627, cost = "70", costType = "Solstice Points"}, -- Ironankh : Tsunami Talisman
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
