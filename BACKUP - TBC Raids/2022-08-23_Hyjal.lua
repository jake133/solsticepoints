local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ambijous",
    [ 2] = "Annarinn",
    [ 3] = "Bloodpapu",
    [ 4] = "Capecrusader",
    [ 5] = "Chalula",
    [ 6] = "Chamwow",
    [ 7] = "Dagarle",
    [ 8] = "Dwinchester",
    [ 9] = "Eillowee",
    [10] = "Embertempest",
    [11] = "Fearlêss",
    [12] = "Frodes",
    [13] = "Fârtblossom",
    [14] = "Icybear",
    [15] = "Irontitann",
    [16] = "Jaeran",
    [17] = "Jazzmein",
    [18] = "Jazzmene",
    [19] = "Lachý",
    [20] = "Mayaell",
    [21] = "Pencilvestor",
    [22] = "Pinn",
    [23] = "Sanif",
    [24] = "Snoweyes",
    [25] = "Superboots",
    [26] = "Unholys",
    [27] = "Xandies",
  },
  kills = {
    {boss = 620, timestamp = "2022-08-23 20:35", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 621, timestamp = "2022-08-23 21:02", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 622, timestamp = "2022-08-23 21:21", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27}},
  },
  description = "2022-08-23_Hyjal",
  drops = {
    {player = 11, timestamp = "2022-08-23 20:04", item = 30863, costType = "MS"}, -- Fearlêss : Deadly Cuffs
    {player = 20, timestamp = "2022-08-23 20:05", item = 30870, costType = "MS"}, -- Mayaell : Cuffs of Devastation
    {player = 23, timestamp = "2022-08-23 20:05", item = 32591, costType = "MS"}, -- Sanif : Choker of Serrated Blades
    {player = 10, timestamp = "2022-08-23 20:08", item = 30874, costType = "OS"}, -- Embertempest : The Unbreakable Will
    {player = 23, timestamp = "2022-08-23 21:04", item = 32945, costType = "MS"}, -- Sanif : Fist of Molten Fury
    {player = 11, timestamp = "2022-08-23 21:04", item = 32945, costType = "MS"}, -- Fearlêss : Fist of Molten Fury
    {player =  5, timestamp = "2022-08-23 21:05", item = 30891, costType = "OS"}, -- Chalula : Black Featherlight Boots
    {player =  6, timestamp = "2022-08-23 21:06", item = 30918, costType = "MS"}, -- Chamwow : Hammer of Atonement
    {player = 22, timestamp = "2022-08-23 21:07", item = 31093, costType = "MS"}, -- Pinn : Gloves of the Forgotten Vanquisher
    {player =  8, timestamp = "2022-08-23 21:08", item = 31094, costType = "MS"}, -- Dwinchester : Gloves of the Forgotten Protector
    {player =  3, timestamp = "2022-08-23 21:21", item = 31092, costType = "MS"}, -- Bloodpapu : Gloves of the Forgotten Conqueror
    {player = 15, timestamp = "2022-08-23 21:22", item = 30903, costType = "OS"}, -- Irontitann : Legguards of Endless Rage
    {player = 12, timestamp = "2022-08-23 21:23", item = 30908, costType = "MS"}, -- Frodes : Apostle of Argus
    {player = 16, timestamp = "2022-08-23 21:23", item = 31095, costType = "MS"}, -- Jaeran : Helm of the Forgotten Protector
    {player =  7, timestamp = "2022-08-23 21:24", item = 31097, costType = "MS"}, -- Dagarle : Helm of the Forgotten Conqueror
    {player = 15, timestamp = "2022-08-23 21:24", item = 31097, costType = "OS"}, -- Irontitann : Helm of the Forgotten Conqueror
    {player =  1, timestamp = "2022-08-23 21:26", item = 30900, costType = "MS"}, -- Ambijous : Bow-stitched Leggings
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
