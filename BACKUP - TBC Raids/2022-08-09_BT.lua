local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Annarinn",
    [ 3] = "Blackcläws",
    [ 4] = "Bobbiflay",
    [ 5] = "Brewsterr",
    [ 6] = "Capecrusader",
    [ 7] = "Chalula",
    [ 8] = "Chamwow",
    [ 9] = "Dagarle",
    [10] = "Dëmonblade",
    [11] = "Eillowee",
    [12] = "Fârtblossom",
    [13] = "Irontitann",
    [14] = "Jaeran",
    [15] = "Lachý",
    [16] = "Maelea",
    [17] = "Mayaell",
    [18] = "Melisea",
    [19] = "Pencilvestor",
    [20] = "Scarrlett",
    [21] = "Snoweyes",
    [22] = "Superboots",
    [23] = "Unholys",
    [24] = "Xandies",
    [25] = "Yé",
  },
  kills = {
    {boss = 603, timestamp = "2022-08-09 20:26", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 607, timestamp = "2022-08-09 20:58", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 608, timestamp = "2022-08-09 21:33", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 609, timestamp = "2022-08-09 21:49", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-08-09_BT",
  drops = {
    {player = 10, timestamp = "2022-08-09 20:28", item = 32254, costType = "OS"}, -- Dëmonblade : The Brutalizer
    {player = 19, timestamp = "2022-08-09 20:29", item = 32260, cost = 70, costType = "Solstice Points"}, -- Pencilvestor : Choker of Endless Nightmares
    {player = 25, timestamp = "2022-08-09 20:29", item = 32270, costType = "MS"}, -- Yé : Focused Mana Bindings
    {player =  9, timestamp = "2022-08-09 20:30", item = 32266, costType = "OS"}, -- Dagarle : Ring of Deceitful Intent
    {player = 20, timestamp = "2022-08-09 21:49", item = 31102, costType = "MS"}, -- Scarrlett : Pauldrons of the Forgotten Vanquisher
    {player = 25, timestamp = "2022-08-09 21:51", item = 31099, costType = "MS"}, -- Yé : Leggings of the Forgotten Vanquisher
    {player = 10, timestamp = "2022-08-09 21:51", item = 31100, costType = "MS"}, -- Dëmonblade : Leggings of the Forgotten Protector
    {player = 15, timestamp = "2022-08-09 21:53", item = 32374, cost = 100, costType = "Solstice Points"}, -- Lachý : Zhar'doom, Greatstaff of the Devourer
    {player = 17, timestamp = "2022-08-09 21:54", item = 32331, cost = 60, costType = "Solstice Points"}, -- Mayaell : Cloak of the Illidari Council
    {player = 12, timestamp = "2022-08-09 21:55", item = 31091, costType = "OS"}, -- Fârtblossom : Chestguard of the Forgotten Protector
    {player = 25, timestamp = "2022-08-09 21:55", item = 31090, costType = "MS"}, -- Yé : Chestguard of the Forgotten Vanquisher
    {player =  6, timestamp = "2022-08-09 21:56", item = 32497, cost = 70, costType = "Solstice Points"}, -- Capecrusader : Stormrage Signet Ring
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
