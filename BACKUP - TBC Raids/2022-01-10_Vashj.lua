local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aythya",
    [ 3] = "Aziza",
    [ 4] = "Cholula",
    [ 5] = "Druzill",
    [ 6] = "Eillowee",
    [ 7] = "Elorn",
    [ 8] = "Gaylestrum",
    [ 9] = "Ironankh",
    [10] = "Irontitanhc",
    [11] = "Jazzmean",
    [12] = "Kharlamagne",
    [13] = "Kyrika",
    [14] = "Lachy",
    [15] = "Luethien",
    [16] = "Pencilvestor",
    [17] = "Roragorn",
    [18] = "Sadistia",
    [19] = "Slayingfreak",
    [20] = "Sneakydoodle",
    [21] = "Superboots",
    [22] = "Varv",
    [23] = "Velladonna",
    [24] = "Whitewidow",
    [25] = "Wootzz",
  },
  kills = {
    {boss = 628, timestamp = "2022-01-10 19:57", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-01-10_Vashj",
  drops = {
    {player = 17, timestamp = "2022-01-10 20:00", item = 30243, cost = "80", costType = "Solstice Points"}, -- Roragorn : Helm of the Vanquished Defender
    {player =  3, timestamp = "2022-01-10 20:01", item = 30242, cost = "80", costType = "Solstice Points"}, -- Aziza : Helm of the Vanquished Champion
    {player = 15, timestamp = "2022-01-10 20:02", item = 30110, cost = "70", costType = "Solstice Points"}, -- Luethien : Coral Band of the Revived
    {player = 24, timestamp = "2022-01-10 20:03", item = 30105, cost = "100", costType = "Solstice Points"}, -- Whitewidow : Serpent Spine Longbow
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
