local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Brickk",
    [ 4] = "Capecrusader",
    [ 5] = "Dagarle",
    [ 6] = "Elorn",
    [ 7] = "Estrellita",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Inxi",
    [11] = "Ironflurry",
    [12] = "Irontitanhc",
    [13] = "Kharlamagne",
    [14] = "Kyrika",
    [15] = "Lachy",
    [16] = "Luethien",
    [17] = "Pencilvestor",
    [18] = "Skeeta",
    [19] = "Sneakydoodle",
    [20] = "Superboots",
    [21] = "Sylaramynd",
    [22] = "Velladonna",
    [23] = "Varv",
    [24] = "Whitewidow",
    [25] = "Wootzz",
  },
  kills = {
    {boss = 625, timestamp = "2021-09-29 19:47", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 626, timestamp = "2021-09-29 21:42", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  drops = {
    {player =  6, timestamp = "2021-09-29 19:47", item = 30241, cost = "80", costType = "Solstice Points"}, -- Elorn : Gloves of the Vanquished Hero
    {player =  24, timestamp = "2021-09-29 19:47", item = 30241, cost = "80", costType = "Solstice Points"}, -- Whitewidow : Gloves of the Vanquished Hero
    {player =  15, timestamp = "2021-09-29 21:42", item = 30247, cost = "80", costType = "Solstice Points"}, -- Lachy : Leggings of the Vanquished Hero
    {player =  25, timestamp = "2021-09-29 21:42", item = 30247, cost = "80", costType = "Solstice Points"}, -- Wootzz : Leggings of the Vanquished Hero
    {player =  13, timestamp = "2021-09-29", item = 30097, cost = "", costType = "MS"}, -- Kharlamagne : Coral-barbed Shoulderpads
    {player =  5, timestamp = "2021-09-29", item = 30090, cost = "", costType = "MS"}, -- Dagarle : World Breaker
    {player =  3, timestamp = "2021-09-29", item = 30620, cost = "", costType = "MS"}, -- Brickk : Spyglass of teh Hidden Fleet
    {player =  2, timestamp = "2021-09-29", item = 30023, cost = "", costType = "OS"}, -- Aziza : Totem of the Maelstrom
    {player =  10, timestamp = "2021-09-29", item = 30021, cost = "", costType = "OS"}, -- Inxi : Wildfury Greatstaff
    },
  description = "2021-09-29_SSC",

}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
