local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Annarinn",
    [ 3] = "Aythya",
    [ 4] = "Capecrusader",
    [ 5] = "Chalula",
    [ 6] = "Chamwow",
    [ 7] = "Elorn",
    [ 8] = "Furiozah",
    [ 9] = "Gaylestrum",
    [10] = "Irontitann",
    [11] = "Jazzmene",
    [12] = "Kattianna",
    [13] = "Kekett",
    [14] = "Lachý",
    [15] = "Luethien",
    [16] = "Pencilvestor",
    [17] = "Roragorn",
    [18] = "Sadistia",
    [19] = "Slayingfreak",
    [20] = "Snoweyes",
    [21] = "Snozzberry",
    [22] = "Superboots",
    [23] = "Totemmoon",
    [24] = "Wootzz",
    [25] = "Xandies",
    [26] = "ßander",
  },
  kills = {
    {boss = 609, timestamp = "2022-06-01 20:59", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
  },
  description = "2022-06-01_BT2",
  drops = {
    {player = 19, timestamp = "2022-06-01 21:01", item = 31091, costType = "MS"}, -- Slayingfreak : Chestguard of the Forgotten Protector
    {player = 11, timestamp = "2022-06-01 21:03", item = 32496, cost = 70, costType = "Solstice Points"}, -- Jazzmene : Memento of Tyrande
    {player = 15, timestamp = "2022-06-01 21:04", item = 31089, cost = 80, costType = "Solstice Points"}, -- Luethien : Chestguard of the Forgotten Conqueror
    {player = 10, timestamp = "2022-06-01 21:04", item = 31089, cost = 80, costType = "Solstice Points"}, -- Irontitann : Chestguard of the Forgotten Conqueror
    {player = 24, timestamp = "2022-06-01 21:05", item = 32336, costType = "OS"}, -- Wootzz : Black Bow of the Betrayer
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
