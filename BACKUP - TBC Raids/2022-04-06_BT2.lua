local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Aythya",
    [ 2] = "Capecrusader",
    [ 3] = "Chalula",
    [ 4] = "Damelion",
    [ 5] = "Elorn",
    [ 6] = "Frodes",
    [ 7] = "Gaylestrum",
    [ 8] = "Irontitann",
    [ 9] = "Jazzmene",
    [10] = "Kattianna",
    [11] = "Lachý",
    [12] = "Luethien",
    [13] = "Maërlyn",
    [14] = "Noritotes",
    [15] = "Orodora",
    [16] = "Pencilvestor",
    [17] = "Roragorn",
    [18] = "Sadistia",
    [19] = "Skeeta",
    [20] = "Slayingfreak",
    [21] = "Snoweyes",
    [22] = "Snozzberry",
    [23] = "Superboots",
    [24] = "Velexi",
    [25] = "Wootzz",
    [26] = "Xandies",
  },
  kills = {
    {boss = 605, timestamp = "2022-04-06 20:05", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 607, timestamp = "2022-04-06 20:56", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
  },
  description = "2022-04-06_BT2",
  drops = {
    {player = 15, timestamp = "2022-04-06 19:55", item = 32943, cost = "", costType = "OS"}, -- Orodora : Swiftsteel Bludgeon
    {player = 25, timestamp = "2022-04-06 20:06", item = 32343, cost = 70, costType = "Solstice Points"}, -- Wootzz : Wand of Prismatic Focus
    {player =  3, timestamp = "2022-04-06 20:07", item = 32501, cost = "", costType = "MS"}, -- Chalula : Shadowmoon Insignia
    {player =  3, timestamp = "2022-04-06 20:21", item = 32593, cost = "", costType = "MS"}, -- Chalula : Treads of the Den Mother
    {player = 23, timestamp = "2022-04-06 20:58", item = 32370, cost = "", costType = "MS"}, -- Superboots : Nadina's Pendant of Purity
    {player = 20, timestamp = "2022-04-06 20:59", item = 31103, cost = 80, costType = "Solstice Points"}, -- Slayingfreak : Pauldrons of the Forgotten Protector
    {player =  2, timestamp = "2022-04-06 21:01", item = 31102, cost = 80, costType = "Solstice Points"}, -- Capecrusader : Pauldrons of the Forgotten Vanquisher
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
