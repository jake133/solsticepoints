local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Agmundr",
    [ 2] = "Annarinn",
    [ 3] = "Aythya",
    [ 4] = "Brewsterr",
    [ 5] = "Capecrusader",
    [ 6] = "Chalula",
    [ 7] = "Chamwow",
    [ 8] = "Daraver",
    [ 9] = "Dwinchester",
    [10] = "Fellador",
    [11] = "Flights",
    [12] = "Freshbreadd",
    [13] = "Garges",
    [14] = "Gaylestrum",
    [15] = "Hadgarthil",
    [16] = "Holysniz",
    [17] = "Irontitann",
    [18] = "Jaeran",
    [19] = "Jazzmene",
    [20] = "Lachý",
    [21] = "Mayaell",
    [22] = "Pinn",
    [23] = "Pooderton",
    [24] = "Rosemondon",
    [25] = "Sanif",
    [26] = "Simplejahk",
    [27] = "Slayingfreak",
    [28] = "Snoweyes",
    [29] = "Thordurinn",
    [30] = "Xandies",
    [31] = "Yourdadsdad",
  },
  kills = {
    {boss = 724, timestamp = "2022-09-06 17:44", players = {1,3,4,5,6,7,8,9,11,14,15,16,17,18,19,20,21,22,23,24,26,27,28,29,30}},
    {boss = 725, timestamp = "2022-09-06 19:17", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31}},
    {boss = 726, timestamp = "2022-09-06 19:52", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31}},
  },
  description = "2022-09-06_Raidswp",
  drops = {
    {player = 28, timestamp = "2022-09-06 17:38", item = 35213, costType = "MS"}, -- Snoweyes : Pattern: Fletcher's Gloves of the Phoenix
    {player = 30, timestamp = "2022-09-06 17:39", item = 34346, costType = "OS"}, -- Xandies : Mounting Vengeance
    {player = 24, timestamp = "2022-09-06 18:01", item = 34852, costType = "MS"}, -- Rosemondon : Bracers of the Forgotten Vanquisher
    {player =  6, timestamp = "2022-09-06 18:01", item = 34852, costType = "MS"}, -- Chalula : Bracers of the Forgotten Vanquisher
    {player = 26, timestamp = "2022-09-06 18:03", item = 34851, costType = "MS"}, -- Simplejahk : Bracers of the Forgotten Protector
    {player =  8, timestamp = "2022-09-06 18:04", item = 34165, costType = "MS"}, -- Daraver : Fang of Kalecgos
    {player = 14, timestamp = "2022-09-06 19:19", item = 34855, costType = "MS"}, -- Gaylestrum : Belt of the Forgotten Vanquisher
    {player = 30, timestamp = "2022-09-06 19:23", item = 34181, costType = "MS"}, -- Xandies : Leggings of Calamity
    {player =  4, timestamp = "2022-09-06 19:24", item = 34853, costType = "MS"}, -- Brewsterr : Belt of the Forgotten Conqueror
    {player = 31, timestamp = "2022-09-06 19:25", item = 34854, costType = "MS"}, -- Yourdadsdad : Belt of the Forgotten Protector
    {player =  6, timestamp = "2022-09-06 19:56", item = 34858, cost = 80, costType = "Solstice Points"}, -- Chalula : Boots of the Forgotten Vanquisher
    {player = 30, timestamp = "2022-09-06 19:58", item = 34857, cost = 80, costType = "Solstice Points"}, -- Xandies : Boots of the Forgotten Protector
    {player = 27, timestamp = "2022-09-06 19:58", item = 34857, costType = "MS"}, -- Slayingfreak : Boots of the Forgotten Protector
    {player = 28, timestamp = "2022-09-06 20:00", item = 34188, costType = "MS"}, -- Snoweyes : Leggings of the Immortal Night
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
