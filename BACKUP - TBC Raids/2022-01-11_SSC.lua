local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Brickk",
    [ 4] = "Cholula",
    [ 5] = "Eillowee",
    [ 6] = "Elorn",
    [ 7] = "Frodes",
    [ 8] = "Gaylestrum",
    [ 9] = "Ironankh",
    [10] = "Irontitanhc",
    [11] = "Jazzmean",
    [12] = "Kharlamagne",
    [13] = "Lachy",
    [14] = "Pencilvestor",
    [15] = "Roragorn",
    [16] = "Sadistia",
    [17] = "Sinniaa",
    [18] = "Sinwave",
    [19] = "Slayingfreak",
    [20] = "Sneakydoodle",
    [21] = "Soulbane",
    [22] = "Superboots",
    [23] = "Varv",
    [24] = "Velladonna",
    [25] = "Whitewidow",
  },
  kills = {
    {boss = 623, timestamp = "2022-01-11 19:18", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 624, timestamp = "2022-01-11 19:34", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 627, timestamp = "2022-01-11 19:59", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 626, timestamp = "2022-01-11 20:17", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 625, timestamp = "2022-01-11 20:36", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 628, timestamp = "2022-01-11 21:30", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-01-11_SSC",
  drops = {
    {player = 12, timestamp = "2022-01-11 19:18", item = 30048, cost = "", costType = "MS"}, -- Kharlamagne : Brighthelm of Justice
    {player = 23, timestamp = "2022-01-11 19:19", item = 30629, cost = "", costType = "MS"}, -- Varv : Scarab of Displacement
    {player =  1, timestamp = "2022-01-11 19:35", item = 30058, cost = "", costType = "OS"}, -- Ashgon : Mallet of the Tides
    {player = 13, timestamp = "2022-01-11 19:50", item = 30282, cost = "10", costType = "Solstice Points"}, -- Lachy : Pattern: Boots of Blasting
    {player = 22, timestamp = "2022-01-11 20:01", item = 30080, cost = "", costType = "MS"}, -- Superboots : Luminescent Rod of the Naaru
    {player = 12, timestamp = "2022-01-11 20:01", item = 30084, cost = "", costType = "OS"}, -- Kharlamagne : Pauldrons of the Argent Sentinel
    {player = 18, timestamp = "2022-01-11 20:18", item = 30245, cost = "", costType = "MS"}, -- Sinwave : Leggings of the Vanquished Champion
    {player =  4, timestamp = "2022-01-11 20:18", item = 30246, cost = "80", costType = "Solstice Points"}, -- Cholula : Leggings of the Vanquished Defender
    {player = 13, timestamp = "2022-01-11 20:19", item = 30626, cost = "70", costType = "Solstice Points"}, -- Lachy : Sextant of Unstable Currents
    {player = 15, timestamp = "2022-01-11 20:28", item = 30620, cost = "", costType = "OS"}, -- Roragorn : Spyglass of the Hidden Fleet
    {player = 18, timestamp = "2022-01-11 20:37", item = 30239, cost = "", costType = "MS"}, -- Sinwave : Gloves of the Vanquished Champion
    {player =  2, timestamp = "2022-01-11 20:38", item = 30097, cost = "", costType = "OS"}, -- Aziza : Coral-Barbed Shoulderpads
    {player = 15, timestamp = "2022-01-11 21:31", item = 30621, cost = "", costType = "MS"}, -- Roragorn : Prism of Inner Calm
    {player =  7, timestamp = "2022-01-11 21:31", item = 30108, cost = "70", costType = "Solstice Points"}, -- Frodes : Lightfathom Scepter
    {player =  3, timestamp = "2022-01-11 21:32", item = 30243, cost = "80", costType = "Solstice Points"}, -- Brickk : Helm of the Vanquished Defender
    {player =  8, timestamp = "2022-01-11 21:33", item = 30244, cost = "80", costType = "Solstice Points"}, -- Gaylestrum : Helm of the Vanquished Hero
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
