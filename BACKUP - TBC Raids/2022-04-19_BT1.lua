local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Aythya",
    [ 2] = "Blackcläws",
    [ 3] = "Capecrusader",
    [ 4] = "Chalula",
    [ 5] = "Eillowee",
    [ 6] = "Elorn",
    [ 7] = "Frodes",
    [ 8] = "Gaylestrum",
    [ 9] = "Irontitann",
    [10] = "Jazzmene",
    [11] = "Kattianna",
    [12] = "Kharlamagne",
    [13] = "Lachý",
    [14] = "Lunitados",
    [15] = "Maërlyn",
    [16] = "Mclitty",
    [17] = "Noritotes",
    [18] = "Orodora",
    [19] = "Roragorn",
    [20] = "Sadistia",
    [21] = "Skeeta",
    [22] = "Slayingfreak",
    [23] = "Snoweyes",
    [24] = "Superboots",
    [25] = "Xandies",
  },
  kills = {
    {boss = 601, timestamp = "2022-04-19 19:31", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 602, timestamp = "2022-04-19 19:54", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 603, timestamp = "2022-04-19 20:13", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 604, timestamp = "2022-04-19 20:48", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 606, timestamp = "2022-04-19 21:14", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 605, timestamp = "2022-04-19 21:31", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-04-19_BT1",
  drops = {
    {player = 16, timestamp = "2022-04-19 20:08", item = 32377, cost = "", costType = "OS"}, -- Mclitty : Mantle of Darkness
    {player =  8, timestamp = "2022-04-19 20:14", item = 32270, cost = "", costType = "MS"}, -- Gaylestrum : Focused Mana Bindings
    {player =  4, timestamp = "2022-04-19 20:14", item = 32265, cost = "", costType = "OS"}, -- Chalula : Shadow-walker's Cord
    {player = 22, timestamp = "2022-04-19 20:16", item = 32254, cost = "", costType = "MS"}, -- Slayingfreak : The Brutalizer
    {player =  3, timestamp = "2022-04-19 20:16", item = 32260, cost = 70, costType = "Solstice Points"}, -- Capecrusader : Choker of Endless Nightmares
    {player = 18, timestamp = "2022-04-19 20:51", item = 32526, cost = "", costType = "MS"}, -- Orodora : Band of Devastation
    {player = 16, timestamp = "2022-04-19 20:52", item = 32329, cost = "", costType = "MS"}, -- Mclitty : Cowl of Benevolence
    {player = 21, timestamp = "2022-04-19 20:57", item = 32510, cost = "", costType = "MS"}, -- Skeeta : Softstep Boots of Tracking
    {player = 18, timestamp = "2022-04-19 21:32", item = 32345, cost = "", costType = "MS"}, -- Orodora : Dreadboots of the Legion
    {player =  7, timestamp = "2022-04-19 21:34", item = 32353, cost = "", costType = "MS"}, -- Frodes : Gloves of Unfailing Faith
    {player = 14, timestamp = "2022-04-19 22:02", item = 32344, cost = "", costType = "MS"}, -- Lunitados : Staff of Immaculate Recovery
    {player = 24, timestamp = "2022-04-19 22:02", item = 32337, cost = 60, costType = "Solstice Points"}, -- Superboots : Shroud of Forgiveness
    {player =  2, timestamp = "2022-04-19 22:03", item = 32943, cost = "", costType = "OS"}, -- Blackcläws : Swiftsteel Bludgeon
    {player =  2, timestamp = "2022-04-19 22:03", item = 32737, cost = "", costType = "MS"}, -- Blackcläws : Plans: Swiftsteel Shoulders
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
