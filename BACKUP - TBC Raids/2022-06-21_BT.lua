local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Adiyah",
    [ 2] = "Annarinn",
    [ 3] = "Aythya",
    [ 4] = "Blackcläws",
    [ 5] = "Capecrusader",
    [ 6] = "Chalula",
    [ 7] = "Chamwow",
    [ 8] = "Eillowee",
    [ 9] = "Elorn",
    [10] = "Felcurious",
    [11] = "Fârtblossom",
    [12] = "Gaylestrum",
    [13] = "Irontitann",
    [14] = "Jazzmene",
    [15] = "Kattianna",
    [16] = "Kekett",
    [17] = "Kharlamagne",
    [18] = "Lachý",
    [19] = "Melisea",
    [20] = "Roragorn",
    [21] = "Sadistia",
    [22] = "Slayingfreak",
    [23] = "Snoweyes",
    [24] = "Superboots",
    [25] = "Xandies",
  },
  kills = {
    {boss = 601, timestamp = "2022-06-21 19:22", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 602, timestamp = "2022-06-21 19:38", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 603, timestamp = "2022-06-21 19:52", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 607, timestamp = "2022-06-21 20:15", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 608, timestamp = "2022-06-21 20:35", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 609, timestamp = "2022-06-21 21:34", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 605, timestamp = "2022-06-21 21:54", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 606, timestamp = "2022-06-21 22:10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-06-21_BT",
  drops = {
    {player = 25, timestamp = "2022-06-21 19:53", item = 32237, costType = "MS"}, -- Xandies : The Maelstrom's Fury
    {player = 17, timestamp = "2022-06-21 19:54", item = 32261, costType = "OS"}, -- Kharlamagne : Band of the Abyssal Lord
    {player = 13, timestamp = "2022-06-21 19:54", item = 32254, costType = "OS"}, -- Irontitann : The Brutalizer
    {player =  4, timestamp = "2022-06-21 19:55", item = 32943, costType = "OS"}, -- Blackcläws : Swiftsteel Bludgeon
    {player =  6, timestamp = "2022-06-21 20:17", item = 32266, costType = "OS"}, -- Chalula : Ring of Deceitful Intent
    {player = 20, timestamp = "2022-06-21 20:17", item = 32271, costType = "OS"}, -- Roragorn : Kilt of Immortal Nature
    {player = 22, timestamp = "2022-06-21 21:07", item = 32369, costType = "OS"}, -- Slayingfreak : Blade of Savagery
    {player = 12, timestamp = "2022-06-21 21:07", item = 31102, costType = "OS"}, -- Gaylestrum : Pauldrons of the Forgotten Vanquisher
    {player = 17, timestamp = "2022-06-21 21:08", item = 31101, costType = "MS"}, -- Kharlamagne : Pauldrons of the Forgotten Conqueror
    {player = 14, timestamp = "2022-06-21 21:08", item = 31101, costType = "MS"}, -- Jazzmene : Pauldrons of the Forgotten Conqueror
    {player = 10, timestamp = "2022-06-21 21:22", item = 31099, costType = "MS"}, -- Felcurious : Leggings of the Forgotten Vanquisher
    {player = 11, timestamp = "2022-06-21 21:23", item = 31100, costType = "MS"}, -- Fârtblossom : Leggings of the Forgotten Protector
    {player = 16, timestamp = "2022-06-21 21:23", item = 31100, costType = "MS"}, -- Kekett : Leggings of the Forgotten Protector
    {player = 22, timestamp = "2022-06-21 22:10", item = 32375, cost = 70, costType = "Solstice Points"}, -- Slayingfreak : Bulwark of Azzinoth
    {player = 11, timestamp = "2022-06-21 22:11", item = 31091, cost = 80, costType = "Solstice Points"}, -- Fârtblossom : Chestguard of the Forgotten Protector
    {player =  7, timestamp = "2022-06-21 22:11", item = 31091, costType = "MS"}, -- Chamwow : Chestguard of the Forgotten Protector
    {player = 17, timestamp = "2022-06-21 22:12", item = 31089, cost = 80, costType = "Solstice Points"}, -- Kharlamagne : Chestguard of the Forgotten Conqueror
    {player = 15, timestamp = "2022-06-21 22:12", item = 32525, costType = "MS"}, -- Kattianna : Cowl of the Illidari High Lord
    {player = 17, timestamp = "2022-06-21 22:13", item = 32501, cost = 70, costType = "Solstice Points"}, -- Kharlamagne : Shadowmoon Insignia
    {player = 20, timestamp = "2022-06-21 22:14", item = 32339, costType = "OS"}, -- Roragorn : Belt of Primal Majesty
    {player = 13, timestamp = "2022-06-21 22:14", item = 32347, costType = "OS"}, -- Irontitann : Grips of Damnation
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
