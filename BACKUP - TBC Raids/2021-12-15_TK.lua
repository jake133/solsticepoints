local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aythya",
    [ 3] = "Aziza",
    [ 4] = "Brickk",
    [ 5] = "Ceruwolfe",
    [ 6] = "Cholula",
    [ 7] = "Elorn",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Grimdoom",
    [11] = "Heidie",
    [12] = "Ironankh",
    [13] = "Irontitanhc",
    [14] = "Lachy",
    [15] = "Luethien",
    [16] = "Pencilvestor",
    [17] = "Roragorn",
    [18] = "Sadistia",
    [19] = "Sinniaa",
    [20] = "Sneakydoodle",
    [21] = "Varv",
    [22] = "Velladonna",
    [23] = "Whitewidow",
    [24] = "Wootzz",
    [25] = "Xremi",
  },
  kills = {
    {boss = 730, timestamp = "2021-12-15 19:29", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 731, timestamp = "2021-12-15 19:57", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 732, timestamp = "2021-12-15 20:25", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2021-12-15_TK",
  drops = {
    {player = 21, timestamp = "2021-12-15 19:29", item = 29925, cost = "60", costType = "Solstice Points"}, -- Varv : Phoenix-Wing Cloak
    {player =  8, timestamp = "2021-12-15 19:30", item = 29923, cost = "70", costType = "Solstice Points"}, -- Frodes : Talisman of the Sun King
    {player =  4, timestamp = "2021-12-15 19:46", item = 30028, cost = "", costType = "MS"}, -- Brickk : Seventh Ring of the Tirisfalen
    {player = 11, timestamp = "2021-12-15 19:58", item = 30248, cost = "80", costType = "Solstice Points"}, -- Heidie : Pauldrons of the Vanquished Champion
    {player =  2, timestamp = "2021-12-15 19:58", item = 30250, cost = "", costType = "MS"}, -- Aythya : Pauldrons of the Vanquished HERO
	{player =  1, timestamp = "2021-12-15 19:59", item = 30619, cost = "", costType = "OS"}, -- Ashgon : Fel Reaver's Piston
    {player = 25, timestamp = "2021-12-15 20:07", item = 30028, cost = "", costType = "OS"}, -- Xremi : Seventh Ring of the Tirisfalen
    {player =  5, timestamp = "2021-12-15 20:10", item = 30030, cost = "60", costType = "Solstice Points"}, -- Ceruwolfe : Girdle of Fallen Stars
    {player = 14, timestamp = "2021-12-15 20:11", item = 30280, cost = "", costType = "MS"}, -- Lachy : Pattern: Belt of Blasting
    {player = 23, timestamp = "2021-12-15 20:12", item = 30302, cost = "", costType = "MS"}, -- Whitewidow : Pattern: Belt of Deep Shadow
    {player = 21, timestamp = "2021-12-15 20:17", item = 30321, cost = "", costType = "MS"}, -- Varv : Plans: Belt of the Guardian
    {player =  5, timestamp = "2021-12-15 20:26", item = 29976, cost = "", costType = "MS"}, -- Ceruwolfe : Worldstorm Gauntlets
    {player = 16, timestamp = "2021-12-15 20:26", item = 29962, cost = "", costType = "MS"}, -- Pencilvestor : Heartrazor
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
