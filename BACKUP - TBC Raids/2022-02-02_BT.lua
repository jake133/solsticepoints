local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Capecrusader",
    [ 4] = "Cholula",
    [ 5] = "Eillowee",
    [ 6] = "Elorn",
    [ 7] = "Frodes",
    [ 8] = "Gaylestrum",
    [ 9] = "Ironankh",
    [10] = "Irontitanhc",
    [11] = "Jazzmean",
    [12] = "Lachy",
    [13] = "Luethien",
    [14] = "Pencilvestor",
    [15] = "Pulchra",
    [16] = "Roragorn",
    [17] = "Sadistia",
    [18] = "Sinniaa",
    [19] = "Slayingfreak",
    [20] = "Sneakydoodle",
    [21] = "Superboots",
    [22] = "Varv",
    [23] = "Velladonna",
    [24] = "Whitewidow",
    [25] = "Wootzz",
  },
  kills = {
    {boss = 601, timestamp = "2022-02-02 19:46", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 602, timestamp = "2022-02-02 20:35", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 603, timestamp = "2022-02-02 21:16", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-02-02_BT",
  drops = {
    {player =  2, timestamp = "2022-02-02 19:48", item = 32242, cost = "", costType = "MS"}, -- Aziza : Boots of Oceanic Fury
    {player =  4, timestamp = "2022-02-02 19:48", item = 32377, cost = "", costType = "MS"}, -- Cholula : Mantle of Darkness
    {player = 14, timestamp = "2022-02-02 20:37", item = 32252, cost = "", costType = "MS"}, -- Pencilvestor : Nether Shadow Tunic
    {player = 22, timestamp = "2022-02-02 20:38", item = 32261, cost = 70, costType = "Solstice Points"}, -- Varv : Band of the Abyssal Lord
    {player = 14, timestamp = "2022-02-02 20:56", item = 32526, cost = 70, costType = "Solstice Points"}, -- Pencilvestor : Band of Devastation
    {player =  7, timestamp = "2022-02-02 21:17", item = 32271, cost = "", costType = "MS"}, -- Frodes : Kilt of Immortal Nature
    {player = 20, timestamp = "2022-02-02 21:17", item = 32266, cost = 70, costType = "Solstice Points"}, -- Sneakydoodle : Ring of Deceitful Intent
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
