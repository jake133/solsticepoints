local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Brickk",
    [ 4] = "Capecrusader",
    [ 5] = "Ceruwolfe",
    [ 6] = "Dagarle",
    [ 7] = "Elorn",
    [ 8] = "Estrellita",
    [ 9] = "Frodes",
    [10] = "Gaylestrum",
    [11] = "Heidie",
    [12] = "Ironflurry",
    [13] = "Irontitanhc",
    [14] = "Kyrika",
    [15] = "Lachy",
    [16] = "Luethien",
    [17] = "Pencilvestor",
    [18] = "Retrav",
    [19] = "Sneakydoodle",
    [20] = "Superboots",
    [21] = "Varv",
    [22] = "Velladonna",
    [23] = "Whitewidow",
    [24] = "Wootzz",
    [25] = "Xandie",
  },
  kills = {
    {boss = 731, timestamp = "2021-11-03 21:45", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2021-11-03_TK",
  drops = {
    {player = 23, timestamp = "2021-11-03 21:40", item = 30304, cost = "", costType = "MS"}, -- Whitewidow : Recipe monsoon belt
    {player =  7, timestamp = "2021-11-03 21:47", item = 30250, cost = "80", costType = "Solstice Points"}, -- Elorn : Pauldrons of the Vanquished Hero
    {player = 14, timestamp = "2021-11-03 21:47", item = 30250, cost = "80", costType = "Solstice Points"}, -- Kyrika : Pauldrons of the Vanquished Hero
    {player =  3, timestamp = "2021-11-03 21:48", item = 29983, cost = "", costType = "OS"}, -- Brickk : Fel-Steel Warhelm
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
