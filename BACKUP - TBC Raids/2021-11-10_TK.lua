local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aziza",
    [ 3] = "Brickk",
    [ 4] = "Brigidfitch",
    [ 5] = "Capecrusader",
    [ 6] = "Dagarle",
    [ 7] = "Elorn",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Grimdoom",
    [11] = "Heidie",
    [12] = "Ironflurry",
    [13] = "Irontitanhc",
    [14] = "Kyrika",
    [15] = "Lachy",
    [16] = "Liteheart",
    [17] = "Pencilvestor",
    [18] = "Retrav",
    [19] = "Skeeta",
    [20] = "Sneakydoodle",
    [21] = "Superboots",
    [22] = "Varv",
    [23] = "Velladonna",
    [24] = "Whitewidow",
    [25] = "Wootzz",
  },
  kills = {
    {boss = 731, timestamp = "2021-11-10 22:03", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2021-11-10_TK",
  drops = {
    {player =  9, timestamp = "2021-11-10 22:05", item = 30250, cost = "80", costType = "Solstice Points"}, -- Gaylestrum : Pauldrons of the Vanquished Hero
    {player = 14, timestamp = "2021-11-10 22:05", item = 29986, cost = "60", costType = "Solstice Points"}, -- Kyrika : Cowl of the Grand Engineer
    {player =  1, timestamp = "2021-11-10 22:06", item = 30028, cost = "70", costType = "Solstice Points"}, -- Ashgon : Seventh Ring of the Tirisfalen
    {player = 19, timestamp = "2021-11-10 22:07", item = 30250, cost = "80", costType = "Solstice Points"}, -- Skeeta : Pauldrons of the Vanquished Hero
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
