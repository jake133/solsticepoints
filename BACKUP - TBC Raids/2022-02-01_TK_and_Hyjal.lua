local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ashgon",
    [ 2] = "Aythya",
    [ 3] = "Aziza",
    [ 4] = "Blackclaws",
    [ 5] = "Capecrusader",
    [ 6] = "Cholula",
    [ 7] = "Eillowee",
    [ 8] = "Elorn",
    [ 9] = "Frodes",
    [10] = "Gaylestrum",
    [11] = "Ironankh",
    [12] = "Irontitanhc",
    [13] = "Jazzmean",
    [14] = "Lachy",
    [15] = "Pencilvestor",
    [16] = "Roragorn",
    [17] = "Sadistia",
    [18] = "Sinniaa",
    [19] = "Sinwave",
    [20] = "Slayingfreak",
    [21] = "Sneakydoodle",
    [22] = "Superboots",
    [23] = "Velladonna",
    [24] = "Whitewidow",
    [25] = "Wootzz",
  },
  kills = {
    {boss = 730, timestamp = "2022-02-01 19:25", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 733, timestamp = "2022-02-01 20:16", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 618, timestamp = "2022-02-01 21:30", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 619, timestamp = "2022-02-01 22:11", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-02-01_TK_and_Hyjal",
  drops = {
    {player = 20, timestamp = "2022-02-01 18:58", item = 30028, cost = "", costType = "OS"}, -- Slayingfreak : Seventh Ring of the Tirisfalen
    {player = 11, timestamp = "2022-02-01 19:26", item = 29924, cost = 35, costType = "Solstice Points"}, -- Ironankh : Netherbane
    {player = 19, timestamp = "2022-02-01 19:27", item = 29921, cost = "", costType = "MS"}, -- Sinwave : Fire Crest Breastplate
    {player = 21, timestamp = "2022-02-01 19:27", item = 29948, cost = 35, costType = "Solstice Points"}, -- Sneakydoodle : Claw of the Phoenix
    {player = 20, timestamp = "2022-02-01 19:32", item = 30324, cost = "", costType = "OS"}, -- Slayingfreak : Plans: Red Havoc Boots
    {player = 14, timestamp = "2022-02-01 20:17", item = 29992, cost = "", costType = "MS"}, -- Lachy : Royal Cloak of the Sunstriders
    {player =  8, timestamp = "2022-02-01 20:18", item = 30238, cost = 40, costType = "Solstice Points"}, -- Elorn : Chestguard of the Vanquished Hero
    {player = 16, timestamp = "2022-02-01 20:18", item = 30237, cost = 40, costType = "Solstice Points"}, -- Roragorn : Chestguard of the Vanquished Defender
    {player = 21, timestamp = "2022-02-01 20:19", item = 30236, cost = 40, costType = "Solstice Points"}, -- Sneakydoodle : Chestguard of the Vanquished Champion
    {player = 24, timestamp = "2022-02-01 20:19", item = 29994, cost = 30, costType = "Solstice Points"}, -- Whitewidow : Thalassian Wildercloak
    {player =  1, timestamp = "2022-02-01 20:21", item = 32405, cost = 35, costType = "Solstice Points"}, -- Ashgon : Verdant Sphere
    {player = 22, timestamp = "2022-02-01 21:16", item = 32609, cost = 60, costType = "Solstice Points"}, -- Superboots : Boots of the Divine Light
    {player =  4, timestamp = "2022-02-01 21:17", item = 30866, cost = "", costType = "MS"}, -- Blackclaws : Blood-stained Pauldrons
    {player =  3, timestamp = "2022-02-01 21:18", item = 30870, cost = 60, costType = "Solstice Points"}, -- Aziza : Cuffs of Devastation
    {player = 13, timestamp = "2022-02-01 22:13", item = 32609, cost = "", costType = "MS"}, -- Jazzmean : Boots of the Divine Light
    {player =  9, timestamp = "2022-02-01 22:13", item = 32609, cost = "", costType = "MS"}, -- Frodes : Boots of the Divine Light
    {player = 19, timestamp = "2022-02-01 22:14", item = 30887, cost = "", costType = "MS"}, -- Sinwave : Golden Links of Restoration
    {player =  1, timestamp = "2022-02-01 22:14", item = 30878, cost = "", costType = "OS"}, -- Ashgon : Glimmering Steel Mantle
    {player = 10, timestamp = "2022-02-01 22:16", item = 32755, cost = 10, costType = "Solstice Points"}, -- Gaylestrum : Pattern: Mantle of Nimble Thought
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
