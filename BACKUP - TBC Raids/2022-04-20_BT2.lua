local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Aythya",
    [ 2] = "Capecrusader",
    [ 3] = "Chalula",
    [ 4] = "Damelion",
    [ 5] = "Eillowee",
    [ 6] = "Elorn",
    [ 7] = "Frodes",
    [ 8] = "Gaylestrum",
    [ 9] = "Grayskyy",
    [10] = "Irontitann",
    [11] = "Jazzmene",
    [12] = "Kattianna",
    [13] = "Kharlamagne",
    [14] = "Lachý",
    [15] = "Luethien",
    [16] = "Maërlyn",
    [17] = "Mclitty",
    [18] = "Noritotes",
    [19] = "Pencilvestor",
    [20] = "Roragorn",
    [21] = "Sadistia",
    [22] = "Skeeta",
    [23] = "Slayingfreak",
    [24] = "Snoweyes",
    [25] = "Superboots",
    [26] = "Wootzz",
    [27] = "Xandies",
  },
  kills = {
    {boss = 607, timestamp = "2022-04-20 20:42", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27}},
  },
  description = "2022-04-20_BT2",
  drops = {
    {player =  7, timestamp = "2022-04-20 20:56", item = 32370, cost = "", costType = "MS"}, -- Frodes : Nadina's Pendant of Purity
    {player =  7, timestamp = "2022-04-20 20:56", item = 31102, cost = 80, costType = "Solstice Points"}, -- Frodes : Pauldrons of the Forgotten Vanquisher
    {player = 24, timestamp = "2022-04-20 20:57", item = 31103, cost = 80, costType = "Solstice Points"}, -- Snoweyes : Pauldrons of the Forgotten Protector
    {player =  9, timestamp = "2022-04-20 21:05", item = 32747, cost = "", costType = "MS"}, -- Grayskyy : Pattern: Swiftstrike Shoulders
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
