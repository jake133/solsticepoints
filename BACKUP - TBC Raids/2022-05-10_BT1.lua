local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Aythya",
    [ 3] = "Blackcläws",
    [ 4] = "Capecrusader",
    [ 5] = "Chalula",
    [ 6] = "Eillowee",
    [ 7] = "Elorn",
    [ 8] = "Frodes",
    [ 9] = "Gaylestrum",
    [10] = "Jazzmene",
    [11] = "Kharlamagne",
    [12] = "Lachý",
    [13] = "Mclitty",
    [14] = "Melisea",
    [15] = "Ninejuanjuan",
    [16] = "Noritotes",
    [17] = "Pencilvestor",
    [18] = "Roragorn",
    [19] = "Sadistia",
    [20] = "Skeeta",
    [21] = "Slayingfreak",
    [22] = "Snoweyes",
    [23] = "Snozzberry",
    [24] = "Superboots",
    [25] = "Xandies",
  },
  kills = {
    {boss = 601, timestamp = "2022-05-10 19:21", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,18,19,20,21,22,23,24,25}},
    {boss = 602, timestamp = "2022-05-10 20:05", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 603, timestamp = "2022-05-10 20:25", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 607, timestamp = "2022-05-10 21:13", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 608, timestamp = "2022-05-10 21:39", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 605, timestamp = "2022-05-10 22:03", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-05-10_BT1",
  drops = {
    {player = 14, timestamp = "2022-05-10 19:22", item = 32238, costType = "MS"}, -- Melisea : Ring of Calming Waves
    {player = 25, timestamp = "2022-05-10 20:26", item = 32259, costType = "MS"}, -- Xandies : Bands of the Coming Storm
    {player = 11, timestamp = "2022-05-10 20:26", item = 32254, costType = "OS"}, -- Kharlamagne : The Brutalizer
    {player = 23, timestamp = "2022-05-10 20:27", item = 32271, cost = 60, costType = "Solstice Points"}, -- Snozzberry : Kilt of Immortal Nature
    {player =  6, timestamp = "2022-05-10 20:28", item = 32239, cost = 60, costType = "Solstice Points"}, -- Eillowee : Slippers of the Seacaller
    {player =  1, timestamp = "2022-05-10 20:39", item = 32266, costType = "MS"}, -- Anatyr : Ring of Deceitful Intent
    {player = 19, timestamp = "2022-05-10 21:42", item = 31101, cost = 80, costType = "Solstice Points"}, -- Sadistia : Pauldrons of the Forgotten Conqueror
    {player = 20, timestamp = "2022-05-10 21:44", item = 31103, cost = 80, costType = "Solstice Points"}, -- Skeeta : Pauldrons of the Forgotten Protector
    {player = 17, timestamp = "2022-05-10 21:44", item = 31102, cost = 80, costType = "Solstice Points"}, -- Pencilvestor : Pauldrons of the Forgotten Vanquisher
    {player = 25, timestamp = "2022-05-10 21:48", item = 31100, cost = 80, costType = "Solstice Points"}, -- Xandies : Leggings of the Forgotten Protector
    {player = 20, timestamp = "2022-05-10 21:48", item = 31100, cost = 80, costType = "Solstice Points"}, -- Skeeta : Leggings of the Forgotten Protector
    {player = 16, timestamp = "2022-05-10 21:48", item = 31100, costType = "MS"}, -- Noritotes : Leggings of the Forgotten Protector
    {player =  4, timestamp = "2022-05-10 21:51", item = 32369, costType = "MS"}, -- Capecrusader : Blade of Savagery
    {player =  3, timestamp = "2022-05-10 21:52", item = 32373, cost = 60, costType = "Solstice Points"}, -- Blackcläws : Helm of the Illidari Shatterer
    {player = 14, timestamp = "2022-05-10 22:04", item = 32340, costType = "MS"}, -- Melisea : Garments of Temperance
    {player = 15, timestamp = "2022-05-10 22:05", item = 32501, costType = "MS"}, -- Ninejuanjuan : Shadowmoon Insignia
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
