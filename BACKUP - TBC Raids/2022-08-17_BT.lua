local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Annarinn",
    [ 3] = "Aythya",
    [ 4] = "Capecrusader",
    [ 5] = "Chalula",
    [ 6] = "Chamwow",
    [ 7] = "Dagarle",
    [ 8] = "Eillowee",
    [ 9] = "Fârtblossom",
    [10] = "Gaylestrum",
    [11] = "Gramashamz",
    [12] = "Grayskyy",
    [13] = "Irontitann",
    [14] = "Jaeran",
    [15] = "Jazzmene",
    [16] = "Lachý",
    [17] = "Luethien",
    [18] = "Mayaell",
    [19] = "Painsoul",
    [20] = "Pencilvestor",
    [21] = "Snoweyes",
    [22] = "Superboots",
    [23] = "Wootzz",
    [24] = "Xandies",
    [25] = "Ôverheals",
  },
  kills = {
    {boss = 601, timestamp = "2022-08-17 19:32", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 602, timestamp = "2022-08-17 19:48", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 603, timestamp = "2022-08-17 20:02", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 607, timestamp = "2022-08-17 20:42", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 608, timestamp = "2022-08-17 21:02", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24}},
    {boss = 609, timestamp = "2022-08-17 21:20", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24}},
    {boss = 606, timestamp = "2022-08-17 21:44", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24}},
    {boss = 605, timestamp = "2022-08-17 22:08", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24}},
  },
  description = "2022-08-17_BT",
  drops = {
    {player = 25, timestamp = "2022-08-17 20:03", item = 32236, costType = "OS"}, -- Ôverheals : Rising Tide
    {player = 13, timestamp = "2022-08-17 20:03", item = 32245, costType = "MS"}, -- Irontitann : Tide-stomper's Greaves
    {player = 13, timestamp = "2022-08-17 20:05", item = 32266, costType = "OS"}, -- Irontitann : Ring of Deceitful Intent
    {player = 13, timestamp = "2022-08-17 20:06", item = 32260, cost = 70, costType = "Solstice Points"}, -- Irontitann : Choker of Endless Nightmares
    {player =  9, timestamp = "2022-08-17 20:17", item = 32278, costType = "MS"}, -- Fârtblossom : Grips of Silent Justice
    {player =  7, timestamp = "2022-08-17 21:03", item = 32526, costType = "OS"}, -- Dagarle : Band of Devastation
    {player = 25, timestamp = "2022-08-17 21:04", item = 32527, costType = "MS"}, -- Ôverheals : Ring of Ancient Knowledge
    {player =  6, timestamp = "2022-08-17 21:04", item = 32370, costType = "MS"}, -- Chamwow : Nadina's Pendant of Purity
    {player = 16, timestamp = "2022-08-17 21:21", item = 32331, cost = 60, costType = "Solstice Points"}, -- Lachý : Cloak of the Illidari Council
    {player =  5, timestamp = "2022-08-17 21:22", item = 31102, costType = "OS"}, -- Chalula : Pauldrons of the Forgotten Vanquisher
    {player = 18, timestamp = "2022-08-17 21:22", item = 31102, costType = "OS"}, -- Mayaell : Pauldrons of the Forgotten Vanquisher
    {player =  8, timestamp = "2022-08-17 21:23", item = 31101, costType = "OS"}, -- Eillowee : Pauldrons of the Forgotten Conqueror
    {player =  5, timestamp = "2022-08-17 21:28", item = 31099, costType = "MS"}, -- Chalula : Leggings of the Forgotten Vanquisher
    {player = 11, timestamp = "2022-08-17 21:30", item = 31100, costType = "MS"}, -- Gramashamz : Leggings of the Forgotten Protector
    {player = 19, timestamp = "2022-08-17 21:31", item = 31098, costType = "OS"}, -- Painsoul : Leggings of the Forgotten Conqueror
    {player =  1, timestamp = "2022-08-17 21:48", item = 32521, costType = "OS"}, -- Anatyr : Faceplate of the Impenetrable
    {player = 23, timestamp = "2022-08-17 21:50", item = 32374, cost = 100, costType = "Solstice Points"}, -- Wootzz : Zhar'doom, Greatstaff of the Devourer
    {player = 11, timestamp = "2022-08-17 21:51", item = 31091, costType = "MS"}, -- Gramashamz : Chestguard of the Forgotten Protector
    {player = 24, timestamp = "2022-08-17 21:51", item = 31091, costType = "OS"}, -- Xandies : Chestguard of the Forgotten Protector
    {player = 18, timestamp = "2022-08-17 21:53", item = 31090, costType = "OS"}, -- Mayaell : Chestguard of the Forgotten Vanquisher
    {player = 18, timestamp = "2022-08-17 22:08", item = 32352, costType = "MS"}, -- Mayaell : Naturewarden's Treads
    {player =  9, timestamp = "2022-08-17 22:09", item = 32345, costType = "MS"}, -- Fârtblossom : Dreadboots of the Legion
    {player = 19, timestamp = "2022-08-17 22:09", item = 32343, costType = "OS"}, -- Painsoul : Wand of Prismatic Focus
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
