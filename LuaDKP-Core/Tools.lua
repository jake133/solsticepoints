local _, ADDON = ...

------------------------------------------------------------

local function Split(s, sep)
  local t = {}
  local i = 1
  repeat
    local j = string.find(s, sep, 1, true)
    if not j then
      if s ~= "" then
        t[i] = s
      end
      break
    end
    t[i] = s:sub(1, j-1)
    s = s:sub(j+1)
    i = i + 1
  until false
  return t
end

------------------------------------------------------------

local function Trim(s)
   return s:gsub("^%s+", ""):gsub("%s+$", "")
end

------------------------------------------------------------

local function Contains(t, e)
  for i,v in ipairs(t) do
    if v == e then
      return true
    end
  end
  return false
end

------------------------------------------------------------

local function GetShortName(name)
  local i = string.find(name, "-", 1, true)
  if i then
    return name:sub(1, i-1)
  end
  return name
end

------------------------------------------------------------

local function GetLinkInfo(link)
  local a = Split(link, "|")
  if a[3] then
    local b = Split(a[3], ":")
    if b[2] then
      return tonumber(b[2]), string.sub(a[4], 3, -2)
    end
  end
end

------------------------------------------------------------


-- SOLSTICE functions
local function TableLength(t)
  local count = 0

  for i,v in ipairs(t) do
    count = count + 1
  end

  return count
end

local function GetLootCount(playerIn)
	local msCount, osCount, pointsCount = 0, 0, 0
	raid = ADDON.GetRaid(ADDON.Group, ADDON.File)

	local rowCount = 1
	if type(raid) == "table" then
		for i,line in ipairs(raid.drops) do
			if raid.players[line.player] == playerIn then
				if line.costType == "MS" then
					msCount = msCount + 1
				elseif line.costType == "OS" then
					osCount = osCount + 1
				else
					pointsCount = pointsCount + 1
				end
			end
		end
	end

	return msCount, osCount, pointsCount
end
-- HARDCODING ITEM COSTS BASED ON INVENTORY SLOT !!!
	local itemSlot = {}
	itemSlot["INVTYPE_NON_EQUIP"] = 25
	itemSlot["INVTYPE_HEAD"] = 90
	itemSlot["INVTYPE_NECK"] = 100
	itemSlot["INVTYPE_SHOULDER"] = 90
	itemSlot["INVTYPE_BODY"] = 90
	itemSlot["INVTYPE_CHEST"] = 90
	itemSlot["INVTYPE_WAIST"] = 90
	itemSlot["INVTYPE_LEGS"] = 90
	itemSlot["INVTYPE_FEET"] = 90
	itemSlot["INVTYPE_WRIST"] = 90
	itemSlot["INVTYPE_HAND"] = 90
	itemSlot["INVTYPE_FINGER"] = 100
	itemSlot["INVTYPE_TRINKET"] = 100
	itemSlot["INVTYPE_WEAPON"] = 100
	itemSlot["INVTYPE_SHIELD"] = 100
	itemSlot["INVTYPE_RANGED"] = 150
	itemSlot["INVTYPE_CLOAK"] = 90
	itemSlot["INVTYPE_2HWEAPON"] = 150
	itemSlot["INVTYPE_BAG"] = 25
	itemSlot["INVTYPE_TABARD"] = 25
	itemSlot["INVTYPE_ROBE"] = 90
	itemSlot["INVTYPE_WEAPONMAINHAND"] = 100
	itemSlot["INVTYPE_WEAPONOFFHAND"] = 100
	itemSlot["INVTYPE_HOLDABLE"] = 100
	itemSlot["INVTYPE_AMMO"] = 25
	itemSlot["INVTYPE_THROWN"] = 100
	itemSlot["INVTYPE_RANGEDRIGHT"] = 100
	itemSlot["INVTYPE_QUIVER"] = 25
	itemSlot["INVTYPE_RELIC"] = 100


local function GetItemCost(itemID)
	local itemQuality = select(3, GetItemInfo(itemID))
	local invSlot = select(9, GetItemInfo(itemID))
	local BoE = select(14, GetItemInfo(itemID))
	local setID = select(16, GetItemInfo(itemID))

	if BoE == 0 then
		ADDON.console("BoE item")
		return 25
	elseif itemQuality == 4 and invSlot == "" then
		return 120 --This is an epic item with no inventory slot.  Assume this is a tier token
	elseif setID == nil then
		if invSlot == "" then
			return 25 -- non-standard item, and not a token.  So probably weird trash items
		else
			return itemSlot[invSlot]
		end
	else
		return 120 --item is part of a set, and set items = 120
	end
end


-- export
ADDON.Split = Split
ADDON.Trim = Trim
ADDON.Contains = Contains
ADDON.GetShortName = GetShortName
ADDON.GetLinkInfo = GetLinkInfo
ADDON.TableLength = TableLength
ADDON.GetLootCount = GetLootCount
ADDON.GetItemCost = GetItemCost