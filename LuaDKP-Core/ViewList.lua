local _, ADDON = ...

------------------------------------------------------------

local self = nil

local label = nil

local groups = {}
local delay = 0
local row1 = 25
local row2 = 290
local row3 = 555
local row4 = 820
local captionName = {}
local captionPts = {}
local buttonUp = {}
local buttonDown = {}
local buttonOneTwo = {}
local pageToDisplay = 1

------------------------------------------------------------

local function Refresh()
	dkptable = masterDKP
	tableX = row1
	tableY = -54
	rowSpace = -15
	buttonoffset = 10
	local rowCount = 1

	-- sort the master dkp list	------------------
	local sortedNames = {}
	local sortedPoints = {}
	tempVal = {}
	for i,v in pairs(dkptable) do
	  table.insert(sortedNames, i)
	end
	
	table.sort(sortedNames)

    for i,v in pairs(sortedNames) do
	  sortedPoints[i] = dkptable[v]
	end

	------------------------------------------------

		-- nullify everything
  	for i = 1, 141, 1 do
      captionName[i]:SetText("")
      captionPts[i]:SetText("")
	  buttonDown[i]:SetScript("OnClick",nil)
	  buttonUp[i]:SetScript("OnClick",nil)
  	  buttonDown[i]:SetEnabled(false)
  	  buttonUp[i]:SetEnabled(false)
	end

		-- populate DKP values
	--local i = 0
	--while (rowCount < 141) do
	--  i = i + 1
	-- THIS DOESNT WORK.  THE LAST SetScript is BEING USED FOR ALL buttons

	for i in pairs(sortedNames) do
		if (rowCount < 141) then
			if (pageToDisplay == 2 and i < 141) then
				--print("skipping sortedNames[i] " .. sortedNames[i])
			else
				brokenName = ADDON.BreakName(sortedNames[i])

				if brokenName == nil then
					captionName[rowCount]:SetTextColor(0.5, 0.5, 0.5)
				else
					local c = RAID_CLASS_COLORS[select(2,UnitClass(brokenName))]

					if c == nil then
					captionName[rowCount]:SetTextColor(0.5, 0.5, 0.5)
					else
					captionName[rowCount]:SetTextColor(c.r,c.g,c.b)
					end
				end  

				if rowCount == 36 then
					tableX = row2
					tableY = -54
				elseif rowCount == 71 then
					tableX = row3
					tableY = -54
				elseif rowCount == 106 then
					tableX = row4
					tableY = -54
				end

				tableY = tableY + rowSpace

				captionName[rowCount]:SetText(sortedNames[i])

				captionPts[rowCount]:SetText(sortedPoints[i])

				----------------------------------------
				buttonDown[rowCount]:SetPoint("TOPLEFT", tableX + 155, tableY + buttonoffset)
   				buttonDown[rowCount]:SetEnabled(true)
				buttonDown[rowCount]:SetSize(15, 15)
				buttonDown[rowCount]:SetText(" - ")
				buttonDown[rowCount]:SetScript("OnClick", function() ButtonDownClick(sortedNames[i]) end)

				----------------------------------------
				buttonUp[rowCount]:SetPoint("TOPLEFT", tableX + 170, tableY + buttonoffset)
   				buttonUp[rowCount]:SetEnabled(true)
				buttonUp[rowCount]:SetSize(15, 15)
				buttonUp[rowCount]:SetText(" + ")
				buttonUp[rowCount]:SetScript("OnClick", function() ButtonUpClick(sortedNames[i]) end)
				----------------------------------------
				rowCount = rowCount + 1
			end
		end
	end
end


------------------------------------------------------------

function ButtonUpClick(name)
  ADDON.StoreDKP(name, 1, "UI Correction", ADDON.Group, ADDON.File)
  Refresh()
end

------------------------------------------------------------

function ButtonDownClick(name)
  ADDON.StoreDKP(name, -1, "UI Correction", ADDON.Group, ADDON.File)
  Refresh()
end

------------------------------------------------------------

function ButtonOneTwoClick()
	if pageToDisplay == 1 then
		pageToDisplay = 2
		buttonOneTwo:SetText("< 2 ")
	else
		pageToDisplay = 1
		buttonOneTwo:SetText(" 1 >")
	end
	
	Refresh()
end
------------------------------------------------------------

local function CreateListFrame()
  listFrame = CreateFrame("Frame", "LuaDKP_ListFrame", UIParent, "TooltipBorderedFrameTemplate")
  listFrame:SetMovable(true)
  listFrame:EnableMouse(true)
  listFrame:RegisterForDrag("LeftButton")
  listFrame:SetScript("OnDragStart", listFrame.StartMoving)
  listFrame:SetScript("OnDragStop", listFrame.StopMovingOrSizing)

  _G["LuaDKP_ListFrame"] = listFrame
  tinsert(UISpecialFrames, listFrame:GetName())



  --listFrame:SetSize(800, 550)
  listFrame:SetSize(1065, 620)
  listFrame:SetPoint("TOP", 0, -25)
  listFrame:SetFrameStrata("DIALOG")
  --listFrame:Hide()

  local caption1 = listFrame:CreateFontString(nil, "BORDER", "GameFontNormal")
  caption1:SetPoint("TOP", 0, -12)
  caption1:SetText("Solstice Points")

  -- left column
  local caption2 = listFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
  caption2:SetPoint("TOPLEFT", row1, -44)
  caption2:SetText("Player")

  local caption3 = listFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
  caption3:SetPoint("TOPLEFT", row1 + 130, -44)
  caption3:SetText("Points")

  -- middle column
  local caption4 = listFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
  caption4:SetPoint("TOPLEFT", row2, -44)
  caption4:SetText("Player")

  local caption5 = listFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
  caption5:SetPoint("TOPLEFT", row2 + 130, -44)
  caption5:SetText("Points")

  -- right column
  local caption6 = listFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
  caption6:SetPoint("TOPLEFT", row3, -44)
  caption6:SetText("Player")

  local caption7 = listFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
  caption7:SetPoint("TOPLEFT", row3 + 130, -44)
  caption7:SetText("Points")

    -- FAAAR right column
  local caption6 = listFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
  caption6:SetPoint("TOPLEFT", row4, -44)
  caption6:SetText("Player")

  local caption7 = listFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
  caption7:SetPoint("TOPLEFT", row4 + 130, -44)
  caption7:SetText("Points")

	----------------------------------------
  	buttonOneTwo = CreateFrame("Button", nil, listFrame, "UIPanelButtonTemplate")
	buttonOneTwo:SetPoint("BOTTOMRIGHT", -10, 10)
	buttonOneTwo:SetEnabled(true)
	buttonOneTwo:SetSize(45, 20)
	buttonOneTwo:SetText(" 1 >")
	buttonOneTwo:SetScript("OnClick", function() ButtonOneTwoClick() end)
	----------------------------------------


  -- create the empty rows for data later
  	tableX = row1
	tableY = -44
	rowSpace = -15
	buttonoffset = 10
	local rowCount = 1

	-- VERIFY THIS COUNTER
  	for i = 1, 141, 1 do
	  --print(i .. " " .. v)
	  
	  if rowCount == 36 then
	    tableX = row2
		tableY = -44
	  elseif rowCount == 71 then
	    tableX = row3
		tableY = -44
	  elseif rowCount == 106 then
	    tableX = row4
		tableY = -44
	  end

	  tableY = tableY + rowSpace

	  captionName[rowCount] = listFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
	  captionName[rowCount]:SetPoint("TOPLEFT", tableX, tableY)
      captionName[rowCount]:SetTextColor(0, 1, 0)

	  captionPts[rowCount] = listFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
	  captionPts[rowCount]:SetPoint("TOPLEFT", tableX + 130, tableY)

	-- Buttons --------------------------------------
	  buttonDown[rowCount] = CreateFrame("Button", nil, listFrame, "UIPanelButtonTemplate")
	  buttonUp[rowCount] = CreateFrame("Button", nil, listFrame, "UIPanelButtonTemplate")


	  rowCount = rowCount + 1
	end
	listFrame:Hide()
end

------------------------------------------------------------

local function List()
  Refresh()
  listFrame:Show()
end

local function HideList()
  Refresh()
  listFrame:Hide()
end

-- export
ADDON.List = List
ADDON.HideList = HideList
ADDON.CreateListFrame = CreateListFrame