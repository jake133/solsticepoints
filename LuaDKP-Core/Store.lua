local _, ADDON = ...

------------------------------------------------------------
local function InitPoints()
  -- clear all data
	if masterDKP["Noritotes"] == nil then
	else
		StaticPopupDialogs["LUADKP_CLEAR_POINTS"] = {
		  text = "Solstice Points Outdated. Clear ALL points?",
		  button1 = "Reset Points",
		  button2 = "Cancel",
		  OnAccept = function() masterDKP = {} ADDON.console("All points cleared.") end,
		  whileDead = true,
		  hideOnEscape = true,
		  preferredIndex = 3,
		}
		StaticPopup_Show("LUADKP_CLEAR_POINTS")
	  end
end
------------------------------------------------------------

local function GetStatString(kills, drops)
  local k = " Kills / "
  local d = " Drops"

  -- deal with singular
  if kills == 1 then
    k = " Kill / "
  end
  if drops == 1 then
    d = " Drop"
  end

  return kills .. k .. drops .. d
end

------------------------------------------------------------

local function GetRaidStats(group, file)

  -- check store
  local none = GetStatString(0, 0)
  if type(LuaDKP_Export) ~= "table" then
    return none
  end
  if type(LuaDKP_Export[group]) ~= "table" then
    return none
  end
  if type(LuaDKP_Export[group][file]) ~= "table" then
    return none
  end

  local raid = LuaDKP_Export[group][file]
  return GetStatString(#raid.kills, #raid.drops)
end

------------------------------------------------------------

local function GetRaid(group, file)

  -- check store
  local none = GetStatString(0, 0)
  if type(LuaDKP_Export) ~= "table" then
    return none
  end
  if type(LuaDKP_Export[group]) ~= "table" then
    return none
  end
  if type(LuaDKP_Export[group][file]) ~= "table" then
    return none
  end

  return LuaDKP_Export[group][file]
end

------------------------------------------------------------

local function GetStoredRaids(group)

  -- check store
  if type(LuaDKP_Export) ~= "table" then
    return {}
  end
  if type(LuaDKP_Export[group]) ~= "table" then
    return {}
  end

  return LuaDKP_Export[group]
end

------------------------------------------------------------

local function GetStoredStats(group)

  -- check store
  if type(LuaDKP_Export) ~= "table" then
    return {}
  end
  if type(LuaDKP_Export[group]) ~= "table" then
    return {}
  end

  -- compile list
  local list = {}
  for file,raid in pairs(LuaDKP_Export[group]) do
    table.insert(list, {file, GetStatString(#raid.kills, #raid.drops)})
  end
  table.sort(list, function(a, b) return a[1] < b[1] end)

  return list
end

------------------------------------------------------------

local function CreateRaid(group, file)

  -- create raid
  if type(LuaDKP_Export) ~= "table" then
    LuaDKP_Export = {}
  end
  if type(LuaDKP_Export[group]) ~= "table" then
    LuaDKP_Export[group] = {}
  end
  if type(LuaDKP_Export[group][file]) ~= "table" then
    LuaDKP_Export[group][file] = {}
  end

  -- create fields
  local raid = LuaDKP_Export[group][file]
  if type(raid.players) ~= "table" then
    raid.players = {}
  end
  if type(raid.kills) ~= "table" then
    raid.kills = {}
  end
  if type(raid.pulls) ~= "table" then
    raid.pulls = {}
  end  if type(raid.drops) ~= "table" then
    raid.drops = {}
  end

  raid.description = file
  return raid
end

------------------------------------------------------------

local function ClearRaid(group, file)

  -- check raid
  if type(LuaDKP_Export) == "table" then
    if type(LuaDKP_Export[group]) == "table" then
      if type(LuaDKP_Export[group][file]) == "table" then

        -- clear raid
        LuaDKP_Export[group][file] = nil
      end
    end
  end
end

------------------------------------------------------------

local function ClearAll()
  LuaDKP_Export = {}
end

------------------------------------------------------------

local function StorePlayer(group, file, name)
  local raid = CreateRaid(group, file)

  -- find name in players list
  for k,v in pairs(raid.players) do
    if v == name then
      return k
    end
  end

  -- find free index
  local i = 1
  while true do
    if raid.players[i] == nil then
      raid.players[i] = name
      return i
    end
    i = i + 1
  end
end

------------------------------------------------------------

local function GetSavedRosterList(group, file)
  if group == nil or file == nil then
    return {}
  else
	local raid = CreateRaid(group, file)
    return raid.players
  end
  
end
------------------------------------------------------------

local function StoreKill(group, file, id, players)
  local raid = CreateRaid(group, file)
  table.insert(raid.kills, {boss = id, timestamp = date("%Y-%m-%d %H:%M"), players = players})
end


local function StoreKillAndTimestamp(group, file, id, players, time)
  local raid = CreateRaid(group, file)
  table.insert(raid.kills, {boss = id, timestamp = time, players = players})
end
------------------------------------------------------------

local function StorePull(group, file, id, players)
  local raid = CreateRaid(group, file)
  table.insert(raid.pulls, {boss = id, timestamp = date("%Y-%m-%d %H:%M"), players = players})
end


local function StorePullAndTimestamp(group, file, id, players, time)
  local raid = CreateRaid(group, file)
  table.insert(raid.pulls, {boss = id, timestamp = time, players = players})
end
------------------------------------------------------------

local function StoreDrop(group, file, name, item, costType, itemName, cost)
  local raid = CreateRaid(group, file)
  local player = StorePlayer(group, file, name)
  local drop = {player = player, timestamp = date("%Y-%m-%d %H:%M"), item = item, costType = costType, name = itemName, cost = cost}
  table.insert(raid.drops, drop)
end

local function StoreDropAndTimestamp(group, file, name, item, costType, itemName, cost, time)
  local raid = CreateRaid(group, file)
  local player = StorePlayer(group, file, name)
  local drop = {player = player, timestamp = time, item = item, costType = costType, name = itemName, cost = cost}
  table.insert(raid.drops, drop)
end

local function DeleteDrop(group, file, playerIndex, item, costType, timestamp)
	raid = ADDON.GetRaid(group, file)

	for i,line in ipairs(raid.drops) do
		if (line.item == item and line.player == playerIndex and line.costType == costType and line.timestamp == timestamp) then
			print("DeleteDrop success from index " .. i)
			table.remove(raid.drops, i)
			return
		end
	end
end

------------------------------------------------------------
-- NEW FUNCTIONS ADDED FOR SOLSTICE
------------------------------------------------------------
local function StoreDKP(name, changeAmount, logText, group, file)
  if type(masterDKP) ~= "table" then
    masterDKP = {}
  end

  if type(dkpLog) ~= "table" then
    dkpLog = {}
  end

  if masterDKP[name] == nil then
    masterDKP[name] = 0
  end    
  
  masterDKP[name] = masterDKP[name] + changeAmount
  
  local raidDesc = GetRaid(group, file).description
  if raidDesc == nil then
	raidDesc = "";
  end
  table.insert(dkpLog, date("%Y-%m-%d %H:%M:%S") .. " | StoreDKP " .. raidDesc .. " | " .. name .. " | " .. changeAmount .. " | " .. logText .. " | Balance " .. masterDKP[name])
end

local function SetDKP(name, amount, logText, group, file)
  if type(masterDKP) ~= "table" then
    masterDKP = {}
  end

  if type(dkpLog) ~= "table" then
    dkpLog = {}
  end
  
  masterDKP[name] = amount

  local raidDesc = GetRaid(group, file).description
  if raidDesc == nil then
	raidDesc = "";
  end
  table.insert(dkpLog, date("%Y-%m-%d %H:%M:%S") .. " | SetDKP " .. raidDesc .. "| " .. name .. " | " .. amount .. " | " .. logText .. " | Balance " .. masterDKP[name])
end

local function RetrieveDKP(name)
  --local name = ADDON.FixName(playerIN)
  if type(masterDKP) ~= "table" then
    masterDKP = {}
	return 0
  elseif masterDKP[name] == nil then
    return 0
  else
	return masterDKP[name]
  end
end

local function RetrieveDKPMaster()
  if type(masterDKP) ~= "table" then
	return {}
  else
	return masterDKP
  end
end

-- initialize WTF
LuaDKP_Export = {}
masterDKP = {}
dkpLog = {}

-- exports
ADDON.InitPoints = InitPoints
ADDON.GetRaidStats = GetRaidStats
ADDON.GetStoredRaids = GetStoredRaids
ADDON.GetStoredStats = GetStoredStats
ADDON.ClearRaid = ClearRaid
ADDON.ClearAll = ClearAll
ADDON.StorePlayer = StorePlayer
ADDON.StoreKill = StoreKill
ADDON.StoreKillAndTimestamp = StoreKillAndTimestamp
ADDON.StorePull = StorePull
ADDON.StorePullAndTimestamp = StorePullAndTimestamp
ADDON.StoreDrop = StoreDrop
ADDON.StoreDropAndTimestamp = StoreDropAndTimestamp
ADDON.DeleteDrop = DeleteDrop
ADDON.StoreDKP = StoreDKP
ADDON.SetDKP = SetDKP
ADDON.RetrieveDKP = RetrieveDKP
ADDON.GetRaid = GetRaid
ADDON.RetrieveDKPMaster = RetrieveDKPMaster
ADDON.GetSavedRosterList = GetSavedRosterList