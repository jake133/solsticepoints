local _, ADDON = ...

------------------------------------------------------------

local self = nil

local label = nil

local groups = {}
local delay = 0
local row1 = 25
local row2 = 225
local row3 = 525

local captionNameRoster = {}
local captionPts = {}
local buttonUp = {}
local buttonDown = {}
local buttonPlayerNum = {}
local button10 = {}

local captionNameLoot = {}
local buttonEditPlayerLoot = {}
local buttonDeletePlayerLoot = {}
local captionPlayerLoot = {}

local rowSpace = -15

local g_group
local g_raid
local g_drop
local g_player
local g_playerIndex


------------------------------------------------------------
------------------------------------------------------------
local function RefreshRoster()
	dkptable = masterDKP
	tableX = row1
	tableY = -44
	buttonoffset = 2
	local rowCount = 1

	-- nullify everything
  	for i = 1, 30, 1 do
      captionNameRoster[i]:SetText("")
      captionPts[i]:SetText("")

	  buttonDown[i]:SetScript("OnClick",nil)
	  buttonUp[i]:SetScript("OnClick",nil)
	  buttonPlayerNum[i]:SetScript("OnClick",nil)

  	  buttonDown[i]:SetEnabled(false)
  	  buttonUp[i]:SetEnabled(false)
	  buttonPlayerNum[i]:SetEnabled(false)
	end

	-- Update accumulated point totals
	captionPoints:SetText("Accumulated Loot Points: " .. ADDON.GetAccumPoints())

	-- populate DKP values
	roster  = ADDON.GetSavedRosterList(ADDON.Group, ADDON.File)

	-- LOADING ROSTER PLAYERS AND DKP POINT VALUES
	for i,v in ipairs(roster) do
	  if rowCount == 31 then
	    return
	  end

	  tableY = tableY + rowSpace
      
	  local c = RAID_CLASS_COLORS[select(2,UnitClass(ADDON.BreakName(v)))]

	  if c == nil then
        captionNameRoster[rowCount]:SetTextColor(0.5, 0.5, 0.5)
	  else
        captionNameRoster[rowCount]:SetTextColor(c.r,c.g,c.b)
	  end

	  captionNameRoster[rowCount]:SetText(i .. " " .. v)

	  points = ADDON.RetrieveDKP(v)
	  captionPts[rowCount]:SetText(points)

		----------------------------------------
	  buttonDown[rowCount]:SetPoint("TOPLEFT", tableX + 155, tableY + buttonoffset)
   	  buttonDown[rowCount]:SetEnabled(true)
	  buttonDown[rowCount]:SetSize(15, 15)
	  buttonDown[rowCount]:SetText(" - ")
	  buttonDown[rowCount]:SetScript("OnClick", function() RosterButtonDownClick(v) end)

		----------------------------------------
	  buttonUp[rowCount]:SetPoint("TOPLEFT", tableX + 170, tableY + buttonoffset)
   	  buttonUp[rowCount]:SetEnabled(true)
	  buttonUp[rowCount]:SetSize(15, 15)
	  buttonUp[rowCount]:SetText(" + ")
	  buttonUp[rowCount]:SetScript("OnClick", function() RosterButtonUpClick(v) end)

	  ----------------------------------------



	  rowCount = rowCount + 1
	end

	raid = ADDON.GetRaid(ADDON.Group, ADDON.File)
	-- LOADING LOOT DROPS
  		for i = 1, 60, 1 do
		  captionNameLoot[i]:SetText("")
		  captionPlayerLoot[i]:SetText("")

		  buttonEditPlayerLoot[i]:SetText("")
		  buttonEditPlayerLoot[i]:SetSize(33, 15)
		  buttonEditPlayerLoot[i]:SetScript("OnClick",nil)
		  buttonEditPlayerLoot[i]:SetEnabled(false)

		  buttonDeletePlayerLoot[i]:SetText("")
		  buttonDeletePlayerLoot[i]:SetSize(15, 15)
		  buttonDeletePlayerLoot[i]:SetScript("OnClick",nil)
		  buttonDeletePlayerLoot[i]:SetEnabled(false)
		end

		local lootRowCount = 1
		local index = 0
		local playerName = ""

		if type(raid) == "table" then
			for i,line in ipairs(raid.drops) do
				  localCostType = line.costType
				  if (localCostType == "Solstice Points") then
					localCostType = "+"
				  end
				  
				  buttonEditPlayerLoot[lootRowCount]:SetEnabled(true)
				  buttonEditPlayerLoot[lootRowCount]:SetText("edit")
				  buttonEditPlayerLoot[lootRowCount]:SetScript("OnClick", function() LootButtonClick(line) end)

  				  buttonDeletePlayerLoot[lootRowCount]:SetEnabled(true)
				  buttonDeletePlayerLoot[lootRowCount]:SetText("X")
				  buttonDeletePlayerLoot[lootRowCount]:SetScript("OnClick", function() LootButtonDELETEClick(line) end)

		  		  captionNameLoot[lootRowCount]:SetText(line.name)
				  captionPlayerLoot[lootRowCount]:SetText(line.player .. " " .. raid.players[line.player] .. " (" .. localCostType ..")")

				  lootRowCount = lootRowCount + 1

				  if lootRowCount > 60 then
					return
				  end
			end
		end


	--[[ LOADING BOSS KILLS
		local bossRowCount = 1
		if type(raid) == "table" then
			local bossname = ""
			for i,line in ipairs(raid.kills) do
				bossName = ADDON.GetBossName(ADDON.Group, line.boss) or "invalid"
				captionBoss[bossRowCount]:SetText(bossName .. " / " .. line.timestamp)

				bossRowCount = bossRowCount + 1

				if bossRowCount > 10 then
					return
				end
			end
		end
	--]]
end
------------------------------------------------------------
function LootButtonDELETEClick (drop)
	savedFileRoster = ADDON.GetSavedRosterList(ADDON.Group, ADDON.File)

	g_group = ADDON.Group
	g_raid = ADDON.File
	g_drop = drop
	g_player = savedFileRoster[drop.player]
	g_playerIndex = drop.player

	DeleteLootConfirmation(drop.name)

end

function DeleteLootConfirmation(loot)
	StaticPopupDialogs["LUADKP_DeleteLoot"] = {
      text = "Delete \n|cffA020F0" .. loot .. "|cffFFFFFF\n from [" .. g_drop.player .. "] ?",
      button1 = "Delete",
      button2 = "Cancel",
      OnAccept = function(self)
			DeleteConfirmed()
		end,
      OnCancel = function(self)
			ResetGlobals()
		end,
	  hideOnEscape = true,
    }
    StaticPopup_Show("LUADKP_DeleteLoot")
end

function DeleteConfirmed()
	priorRecipientMSG = "Solstice Points - " .. g_drop.costType .. " - " .. g_drop.name .. " was deleted."
	SendChatMessage(priorRecipientMSG, "WHISPER", nil, ADDON.BreakName(g_player))

	if g_drop.cost == "" then
	elseif g_drop.cost == nil then
	else
		ADDON.StoreDKP(ADDON.BreakName(g_player), g_drop.cost * 1, "Loot deleted " ..  g_drop.name, ADDON.Group, ADDON.File)

		outputString = "Solstice Points - Correction of " .. g_drop.cost .. " points.  Updated total: " .. ADDON.RetrieveDKP(g_player)
		SendChatMessage(outputString, "WHISPER", nil, ADDON.BreakName(g_player))	
	end

	ADDON.DeleteDrop(g_group, g_raid, g_playerIndex, g_drop.item, g_drop.costType, g_drop.timestamp)

	ResetGlobals()
	RefreshRoster()
end

function LootButtonClick (drop)
	savedFileRoster = ADDON.GetSavedRosterList(ADDON.Group, ADDON.File)

	g_group = ADDON.Group
	g_raid = ADDON.File
	g_drop = drop
	g_player = savedFileRoster[drop.player]
	g_playerIndex = drop.player

	tableY = -44
	for i,v in ipairs(ADDON.GetSavedRosterList(ADDON.Group, ADDON.File)) do
  	  tableY = tableY + rowSpace
	  buttonPlayerNum[i]:SetPoint("TOPLEFT", tableX -2 , tableY + buttonoffset)
   	  buttonPlayerNum[i]:SetEnabled(true)
	  buttonPlayerNum[i]:SetSize(15, 15)
	  buttonPlayerNum[i]:SetText(i)
	  buttonPlayerNum[i]:SetScript("OnClick", function() ReassignLootConfirmation(i, v, drop.name) end)

	  end
end

function ResetGlobals()
	g_group = ""
	g_raid = ""
	g_drop = {}
	g_player = ""
	g_playerIndex = ""

	for i,v in ipairs(ADDON.GetSavedRosterList(ADDON.Group, ADDON.File)) do
		buttonPlayerNum[i]:SetEnabled(false)
	end
end

function ReassignLootConfirmation(index, name, loot)
	StaticPopupDialogs["LUADKP_REASSIGN"] = {
      text = "Reassign \n|cffA020F0" .. loot .. "|cffFFFFFF\nto [" .. index .. "] " .. name,
      button1 = "Reasign",
      button2 = "Cancel",
      OnAccept = function(self)
			ReassignConfirmed(name)
		end,
      OnCancel = function(self)
			ResetGlobals()
		end,
	  hideOnEscape = true,
    }
    StaticPopup_Show("LUADKP_REASSIGN")
end

------------------------------------------------------------
function ReassignConfirmed(newLootRecipient)
	priorRecipientMSG = "Solstice Points - " .. g_drop.costType .. " - " .. g_drop.name .. " is now assigned to " .. newLootRecipient
	newRecipientMSG = "Solstice Points - " .. g_drop.costType .. " - " .. g_drop.name .. " reassigned to you."
	SendChatMessage(priorRecipientMSG, "WHISPER", nil, ADDON.BreakName(g_player))
	SendChatMessage(newRecipientMSG, "WHISPER", nil, ADDON.BreakName(newLootRecipient))

	if g_drop.cost == "" then
	elseif g_drop.cost == nil then
	else
		ADDON.StoreDKP(ADDON.BreakName(g_player), g_drop.cost * 1, "Loot reassigned " .. g_drop.name, ADDON.Group, ADDON.File)
		ADDON.StoreDKP(ADDON.BreakName(newLootRecipient), g_drop.cost * -1, "Loot purchased " .. g_drop.name, ADDON.Group, ADDON.File)

		outputString = "Solstice Points - Correction of " .. g_drop.cost .. " points.  Updated total: " .. ADDON.RetrieveDKP(g_player)
		SendChatMessage(outputString, "WHISPER", nil, ADDON.BreakName(g_player))

		outputString = "Solstice Points - Correction of " .. g_drop.cost * -1 .. " points.  Updated total: " .. ADDON.RetrieveDKP(newLootRecipient)
		SendChatMessage(outputString, "WHISPER", nil, ADDON.BreakName(newLootRecipient))
	
	end

	ADDON.StoreDrop(g_group, g_raid, newLootRecipient, g_drop.item, g_drop.costType, g_drop.name, g_drop.cost)
	ADDON.DeleteDrop(g_group, g_raid, g_playerIndex, g_drop.item, g_drop.costType, g_drop.timestamp)

	ResetGlobals()
	RefreshRoster()
end

function RosterButtonUpClick(name)
  ADDON.StoreDKP(name, 1, "UI Correction", ADDON.Group, ADDON.File)
  RefreshRoster()
end

------------------------------------------------------------

function RosterButtonDownClick(name)
  ADDON.StoreDKP(name, -1, "UI Correction", ADDON.Group, ADDON.File)
  RefreshRoster()
end

------------------------------------------------------------

function LSPStopButton()
  ADDON.LSPStop()
  RefreshRoster()
end

------------------------------------------------------------

function LSPAssignButton(number)
  ADDON.LSPAssign(number)
  RefreshRoster()
end
------------------------------------------------------------

local function CreateRosterFrame()
  rosterFrame = CreateFrame("Frame", "LuaDKP_RosterFrame", UIParent, "TooltipBorderedFrameTemplate")
  rosterFrame:SetMovable(true)
  rosterFrame:EnableMouse(true)
  rosterFrame:RegisterForDrag("LeftButton")
  rosterFrame:SetScript("OnDragStart", rosterFrame.StartMoving)
  rosterFrame:SetScript("OnDragStop", rosterFrame.StopMovingOrSizing)

  _G["LuaDKP_RosterFrame"] = rosterFrame
  tinsert(UISpecialFrames, rosterFrame:GetName())

  rosterFrame:SetSize(850, 675)
  rosterFrame:SetPoint("TOPRIGHT", -50, -25)
  rosterFrame:SetFrameStrata("DIALOG")
  --rosterFrame:Hide()

  local caption1 = rosterFrame:CreateFontString(nil, "BORDER", "GameFontNormal")
  caption1:SetPoint("TOP", 0, -12)
  caption1:SetText("Solstice Points")

  -- left column PLAYER ROSTER ---------------------------------------------------
  local caption2 = rosterFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
  caption2:SetPoint("TOPLEFT", row1, -44)
  caption2:SetText("Player")

  local caption3 = rosterFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
  caption3:SetPoint("TOPLEFT", row1 + 120, -44)
  caption3:SetText("Points")


    -- middle column LOOT RECEIVED 1 thru 30 -------------------------------------
  local caption5 = rosterFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
  caption5:SetPoint("TOPLEFT", row2 + 160, -44)
  caption5:SetText("Loot")

  local caption6 = rosterFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
  caption6:SetPoint("TOPLEFT", row2 + 35, -44)
  caption6:SetText("Player")


      -- Right column LOOT RECEIVED 31 thru 60 -------------------------------------
  local caption7 = rosterFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
  caption7:SetPoint("TOPLEFT", row3 + 160, -44)
  caption7:SetText("Loot")

  local caption8 = rosterFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
  caption8:SetPoint("TOPLEFT", row3 + 35, -44)
  caption8:SetText("Player")


  -- create the empty PLAYER ROSTER DKP rows for data later
  	tableX = row1
	tableY = -44
	buttonoffset = 2
	local rowCount = 1

  	for i = 1, 30, 1 do
		tableY = tableY + rowSpace

		captionNameRoster[rowCount] = rosterFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
		captionNameRoster[rowCount]:SetPoint("TOPLEFT", tableX, tableY)
		captionNameRoster[rowCount]:SetTextColor(0, 1, 0)

		captionPts[rowCount] = rosterFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
		captionPts[rowCount]:SetPoint("TOPLEFT", tableX + 130, tableY)

	-- Buttons --------------------------------------
		buttonDown[rowCount] = CreateFrame("Button", nil, rosterFrame, "UIPanelButtonTemplate")
		buttonUp[rowCount] = CreateFrame("Button", nil, rosterFrame, "UIPanelButtonTemplate")
		buttonPlayerNum[rowCount] = CreateFrame("Button", nil, rosterFrame, "UIPanelButtonTemplate")
		rowCount = rowCount + 1
	end


	-- Create buttons to use during raid for +10 POINTS to all attendees
		button10 = CreateFrame("Button", nil, rosterFrame, "UIPanelButtonTemplate")
		button10:SetPoint("BOTTOMRIGHT", -40, 45)
		button10:SetSize(150, 25)
		button10:SetText("+ 10 Points")
		button10:SetScript("OnClick", function() GiveAttendancePoints() end)

		buttonStop = CreateFrame("Button", nil, rosterFrame, "UIPanelButtonTemplate")
		buttonStop:SetPoint("BOTTOMRIGHT", -40, 15)
		buttonStop:SetSize(150, 25)
		buttonStop:SetText("Whisper Points")
		buttonStop:SetScript("OnClick", function() ADDON.WhisperRaidPoints() end)


		rosterFrame:Hide()

	-- Create ACCUMULATED LOOT TOTAL DISPLAY
		captionPoints = rosterFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
		captionPoints:SetPoint("BOTTOMLEFT", 25, 15)
		captionPoints:SetText("Accumulated Loot Points: " .. ADDON.GetAccumPoints())
		captionPoints:SetTextColor(.75, .25, .75)



  -- create the empty BOSS KILL rows for data later
	tableY = -44
	local rowCount = 1

	--[[
  	for i = 1, 10, 1 do
	  --print(i .. " " .. v)

	  tableY = tableY + rowSpace

  	  captionBoss[rowCount] = rosterFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
	  captionBoss[rowCount]:SetPoint("TOPLEFT", row2, tableY)
      captionBoss[rowCount]:SetTextColor(1, 0, 0)

	  rowCount = rowCount + 1
	end
	--]]

	-- create the empty LOOT RECEIVED rows for data later
	--tableY = -244
	local rowCount = 1

  	for i = 1, 60, 1 do
		if i < 31 then
		  --print(i .. " " .. v)
		  tableY = tableY + rowSpace

  		  captionNameLoot[rowCount] = rosterFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
		  captionNameLoot[rowCount]:SetPoint("TOPLEFT", row2 + 160, tableY)
		  captionNameLoot[rowCount]:SetTextColor(.75, .25, .75)

		  buttonEditPlayerLoot[rowCount] = CreateFrame("Button", nil, rosterFrame, "UIPanelButtonTemplate")
  		  buttonEditPlayerLoot[rowCount]:SetPoint("TOPLEFT", row2, tableY)

		  buttonDeletePlayerLoot[rowCount] = CreateFrame("Button", nil, rosterFrame, "UIPanelButtonTemplate")
  		  buttonDeletePlayerLoot[rowCount]:SetPoint("TOPLEFT", row2 + 35, tableY)
  
		  captionPlayerLoot[rowCount] = rosterFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
		  captionPlayerLoot[rowCount]:SetPoint("TOPLEFT", row2 + 55, tableY)
		  captionPlayerLoot[rowCount]:SetTextColor(0, 1, 0)

		  rowCount = rowCount + 1
	  else
		  tableY = tableY + rowSpace

  		  captionNameLoot[rowCount] = rosterFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
		  captionNameLoot[rowCount]:SetPoint("TOPLEFT", row3 + 160, tableY)
		  captionNameLoot[rowCount]:SetTextColor(.75, .25, .75)

		  buttonEditPlayerLoot[rowCount] = CreateFrame("Button", nil, rosterFrame, "UIPanelButtonTemplate")
  		  buttonEditPlayerLoot[rowCount]:SetPoint("TOPLEFT", row3, tableY)
  
		  buttonDeletePlayerLoot[rowCount] = CreateFrame("Button", nil, rosterFrame, "UIPanelButtonTemplate")
  		  buttonDeletePlayerLoot[rowCount]:SetPoint("TOPLEFT", row3 + 35, tableY)

		  captionPlayerLoot[rowCount] = rosterFrame:CreateFontString(nil, "BORDER", "GameFontHighlight")
		  captionPlayerLoot[rowCount]:SetPoint("TOPLEFT", row3 + 55, tableY)
		  captionPlayerLoot[rowCount]:SetTextColor(0, 1, 0)

		  rowCount = rowCount + 1
	  end

	  if i == 30 then
		tableY = -44
	  end
	end
end

------------------------------------------------------------
function GiveAttendancePoints()
	ADDON.GiveEveryonePoints(10, "Attendance")
	
	button10:SetEnabled(false)
	RefreshRoster()
end

------------------------------------------------------------
local function ListRoster()
  ADDON.UpdateRoster()
  RefreshRoster()
  rosterFrame:Show()
end

local function HideListRoster()
  rosterFrame:Hide()
end

-- export
ADDON.ListRoster = ListRoster
ADDON.HideListRoster = HideListRoster
ADDON.CreateRosterFrame = CreateRosterFrame
ADDON.RefreshRoster = 	RefreshRoster