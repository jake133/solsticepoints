local _, ADDON = ...

------------------------------------------------------------

local self = nil
local isCounting = false

local itemFrame
local buttonStart
local Ticker
local timerDuration = 5;

------------------------------------------------------------
local function HideItemFrame()
  itemFrame:Hide()
end
------------------------------------------------------------
local function ShowItemFrame()
  itemFrame:Show()
end
------------------------------------------------------------

local function RefreshItemFrame()
	if isCounting then
	end
	ShowItemFrame()

	local scale,x,y=frame:GetEffectiveScale(),GetCursorPosition();
	itemFrame:SetPoint("BOTTOMLEFT",x/scale,y/scale);

	local timer = 1;
	Ticker = C_Timer.NewTicker(1, function()
		isCounting = true
		timer = timer + 1

		if timer == timerDuration then
			HideItemFrame()
		end

	end, timerDuration)

	isCounting = false
end


------------------------------------------------------------

local function CreateItemFrame()
	ADDON.console("CreateItemFrame")

  itemFrame = CreateFrame("Button", "LuaDKP_itemFrame", UIParent, "TooltipBorderedFrameTemplate")
  itemFrame:SetMovable(false)
  itemFrame:EnableMouse(true)

  itemFrame:SetSize(100, 75)
  itemFrame:SetPoint("BOTTOMLEFT", 0, 0)
  --itemFrame:SetFrameStrata("DIALOG")

	
	-- STOP BIDS BUTTON
	buttonStart = CreateFrame("Button", nil, itemFrame, "UIPanelButtonTemplate")
	buttonStart:SetPoint("TOP", 0, -10)
	buttonStart:SetSize(25, 10)
	buttonStart:SetText("start")
	buttonStart:SetScript("OnClick", LSPStopButton)

	--itemFrame:Hide()

	
	itemFrame:SetScript("mouseOver", function (self, button)
		print ('onClick')
		if button=='RightButton' then 
			print ('OMG left button?!')
			RefreshItemFrame()
		end
	end)
end

function LSPStopButton()
  ADDON.console("test from ViewItemPopup.lua")
end

-- export
ADDON.ViewItemFrame = ViewItemFrame
ADDON.HideItemFrame = HideItemFrame
ADDON.CreateItemFrame = CreateItemFrame
ADDON.RefreshItemFrame = RefreshItemFrame