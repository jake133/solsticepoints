local _, ADDON = ...

------------------------------------------------------------

local roster = {}
local bidder = {}
local bidderRoll = {}
local score = {}
local currentid = 0
local getlinkid = 0
local dkp = {}
local tag = ""
local itemID, itemName
local accumPointsPerPlayer = 0
------------------------------------------------------------


local function console(text)
	print("|cffFFFFFF< |cffFF00FFSolstice Points|cffFFFFFF >|cffFF00FF " .. text)
end

local function Output(text)
-- raid chat
	if IsInRaid() then
		SendChatMessage(text, "RAID_WARNING")
	else
		console(text)
	end
end

function GetRoster()
  return roster
end

------------------------------------------------------------

local function UpdateRoster()
  for i = 1, 40 do
    local name, _, _, _, class, _, _, online, isDead, _, isML = GetRaidRosterInfo(i)

    -- ignore players who are offline
    if name ~= nil and online then
	  if isML then 
		mlName = name 
		console(name .. " set as master looter")
	  end

      fixedName = ADDON.FixName(name)
	  ADDON.SetBreakName(name, fixedName)
      if not ADDON.Contains(roster, fixedName) then
		table.insert(roster, fixedName)
      end
    end
  end
  
  -- add player if not in raid
  if #roster == 0 then
    local name, realm = UnitName("player")
	name = ADDON.FixName(name)

    table.insert(roster, name)
  end

  table.sort(roster)
end

------------------------------------------------------------
local function EventHandler(self, event, ...)

  if ADDON.Group then
	tag = ADDON.GetTag(ADDON.Group)

	  -- answer whispers
	  if event == "CHAT_MSG_WHISPER" then
		local text, name = ...
		local param = ADDON.Split(text, " ")
		-- ADDED FOR SOLSTICE
		-- Answer query for other players
		if param[1]:lower() == "points?" and param[2] and not param[3] then
		  requesterName = ADDON.GetShortName(name)
		  local requestedName = ADDON.FixName(param[2])
		  local dkpAmount = ADDON.RetrieveDKP(requestedName)
		  local outputString = "Solstice Points Query - " .. requestedName .. ": " .. dkpAmount
		  SendChatMessage(outputString, "WHISPER", nil, requesterName)

		elseif param[1]:lower() == "points" and param[2] and not param[3] then
		  requesterName = ADDON.GetShortName(name)
		  local requestedName = ADDON.FixName(param[2])
		  local dkpAmount = ADDON.RetrieveDKP(requestedName)
		  local outputString = "Solstice Points Query - " .. requestedName .. ": " .. dkpAmount
		  SendChatMessage(outputString, "WHISPER", nil, requesterName)

		elseif param[1]:lower() == "points" or param[1]:lower() == "points?" and not parama[2] then
		  name = ADDON.GetShortName(name)
		  queryName = ADDON.FixName(name)
		  local dkpAmount = ADDON.RetrieveDKP(queryName)
		  local outputString = "Solstice Points Query - " .. queryName .. ": " .. dkpAmount
		  SendChatMessage(outputString, "WHISPER", nil, name)
		end

		-- Allow master looter to start bids by whispering me
		if param[1] == "start" then
			if mlName == ADDON.GetShortName(name) then
				biddingLink = ADDON.Trim(text:sub(7))
				StartBid(biddingLink)
			end
		end

		-- 
	  -- track players on pull
	  elseif event == "ENCOUNTER_START" then
		local id = ...
		console("Boss Pulled.  ID " .. id )
		if id < 601 then
			console("BOSS_PULL suppressed: id " .. id)
		else
			local boss = ADDON.GetBossName(ADDON.Group, id)
			if boss then
			  Output("Boss Pulled (" .. id .. ") - " .. boss)
			  UpdateRoster()

			  -- create player id list
			  local players = {}
			  for i,name in ipairs(roster) do
				table.insert(players, ADDON.StorePlayer(ADDON.Group, ADDON.File, name))
			  end

			  ADDON.StorePull(ADDON.Group, ADDON.File, id, players)
			  GiveEveryonePoints(1, "Pull " .. id .. "/" .. boss)
			end
		end

	  -- get link
	  elseif event == "GET_ITEM_INFO_RECEIVED" then
		local id, success = ...
		if id == getlinkid and success then
		  local name, link = GetItemInfo(id)
		  if name then
			console(link)
		  end
		  getlinkid = 0
		end

	  -- track players after kill
	  elseif event == "BOSS_KILL" then
		local id = ...
		if id < 601 then
			console("BOSS_KILL suppressed: id " .. id)
		else
			local boss = ADDON.GetBossName(ADDON.Group, id)
			if boss then
			  Output(tag .. ": Bosskill ID " .. id .. " - " .. boss)
			  UpdateRoster()

 			  -- create player id list
			  local players = {}
			  for i,name in ipairs(roster) do
				table.insert(players, ADDON.StorePlayer(ADDON.Group, ADDON.File, name))
			  end

			  ADDON.StoreKill(ADDON.Group, ADDON.File, id, players)
			  GiveEveryonePoints(2, "Kill " .. id .. "/" .. boss)
			end
		end
	  end

	  -- watch raid messages
	  if event == "CHAT_MSG_RAID" or event == "CHAT_MSG_RAID_LEADER" or event == "CHAT_MSG_RAID_WARNING" then
		local text, name = ...
		text = ADDON.Trim(text)
		name = ADDON.GetShortName(name)
		name = ADDON.FixName(name)

		-- add player to bidder list
		if text == "+" then
		  bidder[name] = true
		end


		-- remove player from bidder list
		if text == "-" then
		  bidder[name] = nil
		end

		-- add MS bidders to list
		if text:upper() == "MS"  then
		  bidder[name] = "MS"
		  if bidderRoll[name] == nil then
			bidderRoll[name] = 0
		  end
		end

		-- add OS bidders to list
		if text:upper() == "OS" then
		  bidder[name] = "OS"
  		  if bidderRoll[name] == nil then
			bidderRoll[name] = 0
		  end

		end
	  end

	  if event == "CHAT_MSG_SYSTEM" then
		valid, player, roll = FormatDiceRoll(...)

		if valid then
			UpdateBid(player, roll)
		end
	  end
  end
  -- watch addon messages
  if event == "CHAT_MSG_ADDON" then
    local prefix, message, channel, sender = ...
    sender = ADDON.GetShortName(sender)
    if channel == "GUILD" and not UnitIsUnit("player", sender) then
      -- LuaDKP:
		if prefix == "luadkp_v1" then
			local param = ADDON.Split(message, "%")
			-- used for testing only.  these msgs are sent and received from teh same machine
			if param[1] == "request" then
				PushNotificationRequest(sender)
			elseif param[1] == "accept" then
				PushDKPPrivate()
			elseif param[1] == "points"  and param[2] and param[3] then
				origPoints = ADDON.RetrieveDKP(param[2])
				newPoints = tonumber(param[3])
				if origPoints == newPoints then
					ADDON.SyncUpdate(param[2] .. " has " .. origPoints .. " points.  No Change")
				else
					ADDON.SetDKP(param[2], newPoints, "Synced from " .. sender, ADDON.Group, ADDON.File)
					ADDON.SyncUpdate("Updated " .. param[2] .. " from " .. origPoints .. " to " .. newPoints)
				end
			elseif param[1] == "player" and param[2] and param[3] and param[4] then
				-- player%group%file%name
				ADDON.SyncUpdate("Raid " .. param[2] .. " " .. param[3] .. " attendance: " .. param[4])
				ADDON.StorePlayer(param[2], param[3], param[4])

			elseif param[1] == "kill" and param[2] and param[3] and param[4] and param[5] and param[6] then
				-- kill%group%file%1,2,3,4%bossID%timestamp

				localPlayerStringTable = ADDON.Split(param[4], ",")
				localPlayers = {}
				for i in pairs(localPlayerStringTable) do
					localPlayers[i] = tonumber(localPlayerStringTable[i])
				end

				ADDON.SyncUpdate("Raid " .. param[2] .. " " .. param[3] .. " kill: " .. param[5] .. " at " .. param[6])
				ADDON.StoreKillAndTimestamp(param[2], param[3], tonumber(param[5]), localPlayers, param[6]) --group, file, ID, players, timestamp

			elseif param[1] == "drop" and param[2] and param[3] and param[4] and param[5] and param[6] and param[7] and param[8] and param[9] then
				-- drop%group%file%playerName%itemID%costType%itemName%costValue%timestamp
				ADDON.SyncUpdate("Raid " .. param[2] .. " " .. param[3] .. " loot: " .. param[5] .. " to " .. param[4])
				ADDON.StoreDropAndTimestamp(param[2], param[3], param[4], tonumber(param[5]), param[6], param[7], tonumber(param[8]), param[9]) 
				--StoreDropAndTimestamp(group, file, name, item, costType, itemName, cost, time)
			elseif param[1] == "end" then
				-- DONE
			else
				console("Error receiving data addon MSG")
			end
		end
    else
	--[[
	]]
	end
  end
end

------------------------------------------------------------
-- Parses System MSG to find the rolled value
------------------------------------------------------------

function FormatDiceRoll(systemMSG)
	local param = ADDON.Split(systemMSG, " ")

	if param[4] then
		if param[2] == "rolls" and param[4] == "(1-100)" then
			return true, param[1], param[3] *1
		end
	end

	return false
end

------------------------------------------------------------
-- Parses System MSG to find the rolled value
------------------------------------------------------------

function UpdateBid(name, roll)
    name = ADDON.GetShortName(name)
	name = ADDON.FixName(name)

	if bidder[name] == nil then
		bidder[name] = "MS"
	end

	bidderRoll[name] = roll

end

function Countdown15()
	-- RUN A TIMER TO AUTOMATICALLY STOP BIDS - 15 seconds
	local counter = 0
	local Ticker = C_Timer.NewTicker(1, function()
		counter = counter + 1

		if counter == 2 then
			Output("Rolls end in:  15")
		elseif counter == 7 then
			Output("Rolls end in:  10")
		elseif counter == 14 then
			Output("Rolls end in:  3")
		elseif counter == 15 then
			Output("Rolls end in:  2")
		elseif counter == 16 then
			Output("Rolls end in:  1")
		elseif counter == 17 then
			LSPStop()
		end
	end, 18)
end

function Countdown20()
	-- RUN A TIMER TO AUTOMATICALLY STOP BIDS - 20 seconds
	local counter = 0
	local Ticker = C_Timer.NewTicker(1, function()
		counter = counter + 1

		if counter == 2 then
			Output("Rolls end in:  20")
		elseif counter == 12 then
			Output("Rolls end in:  10")
		elseif counter == 19 then
			Output("Rolls end in:  3")
		elseif counter == 20 then
			Output("Rolls end in:  2")
		elseif counter == 21 then
			Output("Rolls end in:  1")
		elseif counter == 22 then
			LSPStop()
		end
	end, 23)
end
------------------------------------------------------------
-- HANDLES INBOUND SLASH commands
------------------------------------------------------------
local function SlashHandler(line)

  -- console help
  if line == "" then
    ADDON.LSP()
    console("/dkp track")
    console("/dkp query <name>")
    console("/dkp give <player> <points>")
    console("/dkp give all <points>")
	if ADDON.Group then
      console("/dkp start <itemlink>")
      console("/dkp toggle <name>")
      console("/dkp stop")
      console("/dkp assign <number>")
      console("/dkp cancel")
      console("/dkp kill <bossid>")
      console("/dkp clear [all]")
    else
      console("/dkp clear all")
      console("Enable tracking to get more commands...")
    end
    return
  end

  -- split line into parameters
  local param = ADDON.Split(line, " ")

  -- show controller frame
  if param[1] == "show" then
    ADDON.LSP()

  -- show tracking frame
  elseif param[1] == "track" then
    ADDON.Track()

  -- testing list frame functions
  elseif param[1] == "list" then
    ADDON.List()

  -- test output options
  elseif param[1] == "test" then
    console("TEST 1")
  	for i = 1, 16, 1 do
		local tempVar = select(i, GetItemInfo(31089))

		if tempVar == nil then
		else
			console(i .." ".. tempVar)
		end
	end	
    console("TEST 2")
	--ADDON.RefreshItemFrame()

  -- test output options
  elseif param[1] == "push" then
	C_ChatInfo.SendAddonMessage("luadkp_v1", "request", "GUILD")

  -- test output options
  elseif param[1] == "pull" then
	C_ChatInfo.SendAddonMessage("luadkp_v1", "accept", "GUILD")
	ADDON.SyncShow()

  -- query player score
  elseif param[1] == "query" and param[2] then
    local name = ADDON.FixName(param[2])
    local dkpAmount = ADDON.RetrieveDKP(name)
	console("Points Query - " .. name .. ": " .. dkpAmount)
  
  -- SOLSTICE use for making sure to keep accurate roster even if no bosses dead
  -- update roster manually
  elseif param[1] == "roster" then
    UpdateRoster()
	local rosterCount = 0
	for i,v in ipairs(roster) do
	  rosterCount = rosterCount + 1
    end
	console("Roster count: " .. rosterCount)

	ADDON.ListRoster()
  elseif param[1] == "whisper" then
	WhisperRaidPoints()

  ---------------------------------------------------
  --   POINTS FOR specific ATTENDEES
  ---------------------------------------------------
  elseif param[1] == "give" and param[2] and param[3] then

	if param[2]:lower() == "all" then
		GiveEveryonePoints(param[3], "Group Correction")
	else
	  local name = param[2]
	  local points = param[3]
	  name = ADDON.FixName(name)

	  ADDON.StoreDKP(name, points, "Correction", ADDON.Group, ADDON.File)
	  outputString = "Solstice Points - Correction of " .. points .. " points.  Updated total: " .. ADDON.RetrieveDKP(name)
	  --console(outputString)
      SendChatMessage(outputString, "WHISPER", nil, ADDON.BreakName(name))
	end
  -- get itemlink
  elseif param[1] == "getlink" then
    getlinkid = tonumber(param[2])
    if GetItemInfo(getlinkid) then
      EventHandler(nil, "GET_ITEM_INFO_RECEIVED", getlinkid, true)
    end

  -- clear all data
  elseif param[1] == "clear" and param[2] == "all" then
    StaticPopupDialogs["LUADKP_CLEARALL"] = {
      text = "Clear ALL data in ALL groups/files?",
      button1 = "Clear All",
      button2 = "Cancel",
      OnAccept = function() ADDON.ClearAll() console("All data cleared.") end,
      whileDead = true,
      hideOnEscape = true,
      preferredIndex = 3,
    }
    StaticPopup_Show("LUADKP_CLEARALL")
  end

  if not ADDON.Group then
    return
  end

  tag = ADDON.GetTag(ADDON.Group)

  -- start bidding
  if param[1] == "start" then
      biddingLink = ADDON.Trim(line:sub(7))
      StartBid(biddingLink)

  elseif param[1] == "start15" then
      biddingLink = ADDON.Trim(line:sub(9))
      StartBid(biddingLink)
	  Countdown15()

  elseif param[1] == "start20" then
      biddingLink = ADDON.Trim(line:sub(9))
      StartBid(biddingLink)
	  Countdown20()

  -- toggle bidder
  elseif param[1] == "toggle" then
    if currentid > 0 then
      local name = ADDON.FixName(param[2])
      if bidder[name] then
        Output("Toggle: " .. name .. " -")
        bidder[name] = nil
      else
        Output("Toggle: " .. name .. " +")
        bidder[name] = true
      end
    else
      console("Error: Bidding inactive! Use start first!")
    end

  -- stop bidding and display score
  elseif param[1] == "stop" then
	LSPStop()
  
  -- assign item to bidder
  elseif param[1] == "assign" and param[2] then
	LSPAssign()

  -- cancel bidding/assignment
  elseif param[1] == "cancel" then
    Output("** " .. tag .. " - " .. biddingLink .. " cancelled")
    bidder = {}
	bidderRoll = {}
    score = {}
    currentid = 0
	ADDON.HideBid()

  -- direct loot assign: name points loot
  elseif param[1] == "direct" and param[2] and param[3] and param[4] then
	  fullName = ""

	  for i = 4, ADDON.TableLength(param), 1 do
		fullName = fullName .. param[i] .. " "
	  end
      biddingLink = ADDON.Trim(fullName)
      StartBid(biddingLink)
       itemID, itemName = ADDON.GetLinkInfo(biddingLink)

	  -- toggle param[2]
	  playerName = ADDON.FixName(param[2])
		if param[3]:upper() == "MS"  then
		  bidder[playerName] = "MS"
		end
		if param[3]:upper() == "OS"  then
		  bidder[playerName] = "OS"
		end
		if param[3] == "+"  then
		  bidder[playerName] = true
		end

	  LSPStop()
	  LSPAssign(1)
  	  ADDON.HideBid()

  -- kill boss
  elseif param[1] == "kill" and param[2] then
    local id = tonumber(param[2])
    EventHandler(nil, "BOSS_KILL", id)

  -- clear data
  elseif param[1] == "clear" and param[2] ~= "all" then
    local group = ADDON.Group
    local file = ADDON.File
    StaticPopupDialogs["LUADKP_CLEAR"] = {
      text = "Clear data in currently tracked file?|n" .. file .. "|n" .. ADDON.GetRaidStats(group, file),
      button1 = "Clear Data",
      button2 = "Cancel",
      OnAccept = function() ADDON.ClearRaid(group, file) console("Data cleared.") end,
      whileDead = true,
      hideOnEscape = true,
      preferredIndex = 3,
    }
    StaticPopup_Show("LUADKP_CLEAR")
  end
end

function StartBid(biddingLink)

    if currentid > 0 then
      console("Error: Bidding still in progress! Use stop/assign or cancel!")
    else
      --biddingLink = ADDON.Trim(line:sub(7))
      itemID, itemName = ADDON.GetLinkInfo(biddingLink)
      if itemID then
        -- display item infos
        local b, cost, note, accid, accname = ADDON.GetItemInfo(ADDON.Group, itemID)
        if b then
   		  ADDON.ViewBid(cost, biddingLink)

          Output("** " .. tag .. " - " .. biddingLink .. " " .. cost .. " points")
		  Output("   \"+\" to use points")
		  Output("   /roll for MS")
		  Output("   /roll and \"os\" for OS")
		  --Output("   Whisper me \"points\" or \"points <player name>\" at any time")
          bidder = {}
          score = {}
          currentid = itemID
        else
          console("Error: Unknown item!")
        end
      end
    end
end


function LSPStop()
    if currentid > 0 then
	  score = {}
      scorePoints = {}
      scoreMS = {}
      scoreOS = {}

	  ---------------------------------------------
      -- build score for point spends using Points
      local b, cost, note, accid, accname = ADDON.GetItemInfo(ADDON.Group, currentid)
      for name,bid in pairs(bidder) do
	    name = ADDON.FixName(name)
		if bid == true then
		  local msCount, _, _ = ADDON.GetLootCount(name)
          table.insert(scorePoints, {name, ADDON.RetrieveDKP(name), "Points", msCount})
		end
      end
      -- sort scorePoints
      table.sort(scorePoints, function(a, b)
        if a[2] == b[2] then
          return a[1] < b[1]
        end
        return a[2] > b[2] end
      )

	  ---------------------------------------------
	  -- build score for MS only rolls
		for name,bid in pairs(bidder) do
			name = ADDON.FixName(name)
			if bid == "MS" then
				local msCount, _, _ = ADDON.GetLootCount(name)
				table.insert(scoreMS, {name, bidderRoll[name], "MS", msCount})
			end
		end
		-- sort the MS only table, first by prior MS count (lower better) then by roll value (higher better)
		table.sort(scoreMS, function(a, b)
			if a[4] == b[4] then
			  return a[2] > b[2]
			end
			return a[4] < b[4] end
		  )

	  ---------------------------------------------
	  -- build score for OS only rolls
		for name,bid in pairs(bidder) do
			name = ADDON.FixName(name)
			if bid == "OS" then
				local _, osCount, _ = ADDON.GetLootCount(name)
				table.insert(scoreOS, {name, bidderRoll[name], "OS", osCount})
			end
		end
		-- sort the OS only table, first by prior OS count (lower better) then by roll value (higher better)
		table.sort(scoreOS, function(a, b)
			if a[4] == b[4] then
			  return a[2] > b[2]
			end
			return a[4] < b[4] end
		  )

	  --------------------------------------------
	  -- Merge all scores together.  Points, then MS, then OS
      for i,t in ipairs(scorePoints) do
          table.insert(score, {t[1], t[2], t[3], t[4]})
      end
      for i,t in ipairs(scoreMS) do
		  t[2] = t[2] or 0
          table.insert(score, {t[1], t[2], t[3], t[4]})
      end
      for i,t in ipairs(scoreOS) do
  		  t[2] = t[2] or 0
          table.insert(score, {t[1], t[2], t[3], t[4]})
      end

      -- display Bids
      if #score > 0 then
        for i,t in ipairs(score) do
		  if t[3] == "Points" then
		  -- 1 : Points Ironflurry : 333 points.  1 prior MS
            Output(i .. " : Points " .. t[1] .. " with " .. t[2] .. " points. " .. t[4] .. " prior MS")
			ADDON.UpdateBid(i, "Points " .. t[1] .. " with " .. t[2] .. " points. " .. t[4] .. " prior MS")
		  else
		  -- 1 : MS Ironflurry Rolled 50. 1 prior MS
            Output(i .. " : " .. t[3] .. " ".. t[1] .. " Rolled " .. t[2] .. ".  Prior " .. t[3] .. " count " .. t[4])
			ADDON.UpdateBid(i, t[3] .. " ".. t[1] .. " Rolled " .. t[2] .. ".  Prior " .. t[3] .. " count " .. t[4])

		  -- 1 : Ironflurry : MS : Rolled 50 : 1 prior MS
          --Output(i .. " : " .. t[1] .. " : " .. t[3] .. " : Rolled " .. t[2] .. " : " .. t[4] .. " prior " .. t[3])
		  --ADDON.UpdateBid(i, t[1] .. " : " .. t[3] .. " : Rolled " .. t[2] .. " : " .. t[4] .. " prior "  .. t[3])
		  end
        end
      else
		Output("** " .. tag .. " - " .. biddingLink .. " cancelled")
		bidder = {}
		bidderRoll = {}
		score = {}
		scorePoints = {}
		scoreMS = {}
		scoreOS = {}
		currentid = 0
  	    ADDON.HideBid()
      end
    else
      console("Error: Use start first!")
    end
end

function LSPAssign(numAlignment)
    -- get item info
    local b, cost, note, accid, accname = ADDON.GetItemInfo(ADDON.Group, currentid)

    local i = tonumber(numAlignment)
    if score[i] then
      local name = score[i][1]
	  local points = score[i][2] -- this holds dkp value OR the roll amount
	  local bidType = score[i][3] -- this should be points, MS, or OS

	  --------
	  -- ADDING NEW VALUE FOR DKP, MS, OS
	  if bidType == "OS" or bidType == "MS" then
      -- store item
        ADDON.StoreDrop(ADDON.Group, ADDON.File, name, currentid, bidType, itemName, "")

        Output("** " .. tag .. " - " .. bidType .. ": " .. name .. " " .. biddingLink)

	  else
	    bidType = "Solstice Points"
        -- store item
        ADDON.StoreDrop(ADDON.Group, ADDON.File, name, currentid, bidType, itemName, cost)

		  -- SOLSTICE DECREMENT DKP
		  winnerCurrentDKP = ADDON.RetrieveDKP(name)
		  console("winnerCurrentDKP " .. winnerCurrentDKP)
		  if winnerCurrentDKP > 350 then
			local realDKPcost = winnerCurrentDKP - 350 + cost
			ADDON.StoreDKP(name, -realDKPcost, "Loot purchased " .. itemName, ADDON.Group, ADDON.File)
		    Output("** " .. tag .. " - " .. name .. " is ABOVE SOFTCAP at " ..winnerCurrentDKP .. " points. Total points deducted: " .. realDKPcost)
		  else
			ADDON.StoreDKP(name, -cost, "Loot purchased " .. itemName, ADDON.Group, ADDON.File)
		  end

		  Output("** " .. tag .. " - " .. name .. " purchased " .. biddingLink)

		  -- SOLSTICE DISBURSE DKP POINTS TO RAID
		  local rosterCount = ADDON.TableLength(roster)

		  local pointsPerPlayer = cost / rosterCount
		  pointsPerPlayer = math.ceil(pointsPerPlayer)

  		  GiveEveryonePoints(pointsPerPlayer, "Distributed points from Loot")
	  end

	  ADDON.HideBid()
      bidder = {}
      score = {}
      scorePoints = {}
	  scoreMS = {}
	  scoreOS = {}
      currentid = 0
    else
      console("Error: Unknown player")
    end
end

function GiveEveryonePoints(pointsToAdd, reason)
	accumPointsPerPlayer = accumPointsPerPlayer + pointsToAdd
    UpdateRoster()
    local rosterCount = ADDON.TableLength(roster)

	for i,name in ipairs(roster) do	
		ADDON.StoreDKP(name, pointsToAdd, reason, ADDON.Group, ADDON.File)
	end
    --console(pointsToAdd .. " points added to " .. rosterCount .. " raiders")
end

function WhisperRaidPoints()
    UpdateRoster()
    local rosterCount = ADDON.TableLength(roster)
  	local counter = 0
	local Ticker = C_Timer.NewTicker(0.75, function()
		counter = counter + 1

		v= roster[counter]
		outputString = tag .. " - You have " .. ADDON.RetrieveDKP(v) .. " points.  Whisper \"points\" or \"points <player>\" at anytime"
		SendChatMessage(outputString, "WHISPER", nil, ADDON.BreakName(v))
	end, rosterCount)
end

function GetAccumPoints()
	return accumPointsPerPlayer
end

function PushNotificationRequest(sender)
    StaticPopupDialogs["LUADKP_PUSH_NOTIFICATION"] = {
      text = sender .. " would like to send Solstice Points data",
      button1 = "Accept",
      button2 = "Cancel",
      OnAccept = function() C_ChatInfo.SendAddonMessage("luadkp_v1", "accept", "GUILD") console("Request accepted") ADDON.SyncShow() end,
      whileDead = true,
      hideOnEscape = true,
      preferredIndex = 3,
    }
    StaticPopup_Show("LUADKP_PUSH_NOTIFICATION")
end

function PushEverything()
    StaticPopupDialogs["LUADKP_PUSH"] = {
      text = "Accept DKP Push Request",
      button1 = "Accept",
      button2 = "Cancel",
      OnAccept = function() PushDKPPrivate() console("Push accepted") end,
      whileDead = true,
      hideOnEscape = true,
      preferredIndex = 3,
    }
    StaticPopup_Show("LUADKP_PUSH")
end

function PushDKPPrivate()
	local masterDKP = ADDON.RetrieveDKPMaster()
	local masterName = {}
	local masterPoints = {}
  	local playerCounter = 1

	ADDON.SyncShow()

	-- Load array into numerical array
	for i,v in pairs(masterDKP) do
		masterName[playerCounter] = i
		masterPoints[playerCounter] = v
		playerCounter = playerCounter + 1
	end

	raidMSGTable = PushRaidPrivate()

  	local masterSize = playerCounter - 1 + ADDON.TableLength(raidMSGTable)
	raidCounter = 1
	playerCounter = 1

	local Ticker = C_Timer.NewTicker(1, function()
		if masterName[playerCounter] ~= nil and masterPoints[playerCounter] ~= nil then
		    C_ChatInfo.SendAddonMessage("luadkp_v1", "points%" .. masterName[playerCounter] .. "%" .. masterPoints[playerCounter], "GUILD")
			ADDON.SyncUpdate(playerCounter .. "/" .. masterSize .. " " .. masterName[playerCounter] .. ": " .. masterPoints[playerCounter])

			text = playerCounter .. "/" .. masterSize .. " " .. masterName[playerCounter] .. ": " .. masterPoints[playerCounter]

			playerCounter = playerCounter + 1
		elseif raidMSGTable[raidCounter] ~= nil and raidMSGTable[raidCounter] ~= nil then
		    C_ChatInfo.SendAddonMessage("luadkp_v1", raidMSGTable[raidCounter], "GUILD")
			ADDON.SyncUpdate(playerCounter + raidCounter .. "/" .. masterSize .. " " .. raidMSGTable[raidCounter])
			raidCounter = raidCounter + 1
		end
	end, masterSize)
end

function PushRaidPrivate()
	raidMSG = {}
	for raid in pairs(ADDON.GetStoredRaids(ADDON.Group)) do
		local playerArray = {}
		for num, name in ipairs(ADDON.GetSavedRosterList(ADDON.Group, raid)) do
			-- player%group%file%name
			table.insert(raidMSG, "player%" .. ADDON.Group .. "%" .. raid .. "%" .. name)
			playerArray[num] = name
		end

		raidObj = ADDON.GetRaid(ADDON.Group, raid)
		for indx,lineItem in ipairs(raidObj.kills) do
			localTable = ""
			for x in pairs(lineItem.players) do
				localTable = localTable .. x .. ","
			end

			-- kill%group%file%1,2,3,4%bossID%timestamp
			killStr = "kill%" .. ADDON.Group .. "%" .. raid .. "%" .. localTable .. "%" .. lineItem.boss .. "%" .. lineItem.timestamp
			table.insert(raidMSG, killStr)
		end		
		for indx,drop in ipairs(raidObj.drops) do
			-- drop%group%file%playerName%itemID%costType%itemName%costValue%timestamp
			localCostValu = drop.cost or ""
			dropStr = "drop%" .. ADDON.Group .. "%" .. raid .. "%" .. playerArray[drop.player] .. "%" .. drop.item .. "%" .. drop.costType .. "%" .. drop.name .. "%" .. localCostValu .. "%" .. drop.timestamp
			table.insert(raidMSG, dropStr)
		end
	end

	return raidMSG
end

------------------------------------------------------------
local function InitMain()

  -- register prefix
  C_ChatInfo.RegisterAddonMessagePrefix("luadkp_v1")

  -- register event handler
  local f = CreateFrame("Frame")
  f:SetScript("OnEvent", EventHandler)
  f:RegisterEvent("ENCOUNTER_START")
  f:RegisterEvent("BOSS_KILL")
  f:RegisterEvent("CHAT_MSG_WHISPER")
  f:RegisterEvent("CHAT_MSG_RAID")
  f:RegisterEvent("CHAT_MSG_RAID_LEADER")
  f:RegisterEvent("CHAT_MSG_RAID_WARNING")
  f:RegisterEvent("CHAT_MSG_ADDON")
  f:RegisterEvent("GET_ITEM_INFO_RECEIVED")
  f:RegisterEvent("CHAT_MSG_SYSTEM")

  -- register slash handler
  SLASH_LUADKP1 = "/luadkp"
  SLASH_LUADKP2 = "/dkp"
  SlashCmdList["LUADKP"] = SlashHandler
end

------------------------------------------------------------

-- export
ADDON.InitMain = InitMain
ADDON.Output = Output
ADDON.GetRoster = GetRoster
ADDON.UpdateRoster = UpdateRoster
ADDON.LSPStop = LSPStop
ADDON.LSPAssign = LSPAssign
ADDON.GetAccumPoints = GetAccumPoints
ADDON.console = console
ADDON.GiveEveryonePoints = GiveEveryonePoints
ADDON.WhisperRaidPoints = WhisperRaidPoints