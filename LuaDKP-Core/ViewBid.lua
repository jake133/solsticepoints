local _, ADDON = ...

------------------------------------------------------------

local self = nil

local label = nil

local groups = {}
local delay = 0
local row1 = 25
local row2 = 225
local row3 = 555
local row3 = 650

local caption1
local caption2

local playerBid1
local playerBid2
local playerBid3
local playerBid4
local playerBid5
local playerBid6

------------------------------------------------------------
local function RefreshBid()
	raid = ADDON.GetRaid(ADDON.Group, ADDON.File)
	-- LOADING LOOT DROPS

	caption2:SetText("Loot Link Here")

	playerBid1:SetText("")
	playerBid2:SetText("")
	playerBid3:SetText("")
	playerBid4:SetText("")
	playerBid5:SetText("")
	playerBid6:SetText("")

end


------------------------------------------------------------

function LSPStopButton()
  ADDON.LSPStop()
end

------------------------------------------------------------

function LSPAssignButton(number)
	ADDON.LSPAssign(number)
	RefreshBid()
	bidFrame:Hide()
end
------------------------------------------------------------

local function CreatebidFrame()
  bidFrame = CreateFrame("Frame", "LuaDKP_bidFrame", UIParent, "TooltipBorderedFrameTemplate")
  bidFrame:SetMovable(true)
  bidFrame:EnableMouse(true)
  bidFrame:RegisterForDrag("LeftButton")
  bidFrame:SetScript("OnDragStart", bidFrame.StartMoving)
  bidFrame:SetScript("OnDragStop", bidFrame.StopMovingOrSizing)

  bidFrame:SetSize(300, 225)
  bidFrame:SetPoint("CENTER", 0, 0)
  bidFrame:SetFrameStrata("DIALOG")
  --bidFrame:Hide()

  caption1 = bidFrame:CreateFontString(nil, "BORDER", "GameFontNormal")
  caption1:SetPoint("TOP", 0, -12)
  caption1:SetText("Solstice Points")

  caption2 = bidFrame:CreateFontString(nil, "BORDER", "GameFontNormal")
  caption2:SetPoint("TOPLEFT", 20, -30)
  caption2:SetText("Loot Link Here")
  caption2:SetTextColor(1, 1, 1)

	
		-- STOP BIDS BUTTON
		buttonStop = CreateFrame("Button", nil, bidFrame, "UIPanelButtonTemplate")
		buttonStop:SetPoint("TOP", 0, -50)
		buttonStop:SetSize(150, 25)
		buttonStop:SetText("Stop Bids")
		buttonStop:SetScript("OnClick", LSPStopButton)

		-- Create buttons to ASSIGN LOOT
		buttonStop1 = CreateFrame("Button", nil, bidFrame, "UIPanelButtonTemplate")
		buttonStop1:SetPoint("TOPLEFT", 20, -100)
		buttonStop1:SetSize(20, 20)
		buttonStop1:SetText("1")
		buttonStop1:SetScript("OnClick", function() LSPAssignButton(1) end)

		playerBid1 = bidFrame:CreateFontString(nil, "BORDER", "GameFontNormal")
		playerBid1:SetPoint("TOPLEFT", 40, -102)
		playerBid1:SetText("")
		playerBid1:SetTextColor(1, 1, 1)

		buttonStop2 = CreateFrame("Button", nil, bidFrame, "UIPanelButtonTemplate")
		buttonStop2:SetPoint("TOPLEFT", 20, -115)
		buttonStop2:SetSize(20, 20)
		buttonStop2:SetText("2")
		buttonStop2:SetScript("OnClick", function() LSPAssignButton(2) end)
		
		playerBid2 = bidFrame:CreateFontString(nil, "BORDER", "GameFontNormal")
		playerBid2:SetPoint("TOPLEFT", 40, -117)
		playerBid2:SetText("")
		playerBid2:SetTextColor(1, 1, 1)
		
		buttonStop3 = CreateFrame("Button", nil, bidFrame, "UIPanelButtonTemplate")
		buttonStop3:SetPoint("TOPLEFT", 20, -130)
		buttonStop3:SetSize(20, 20)
		buttonStop3:SetText("3")
		buttonStop3:SetScript("OnClick", function() LSPAssignButton(3) end)

		playerBid3 = bidFrame:CreateFontString(nil, "BORDER", "GameFontNormal")
		playerBid3:SetPoint("TOPLEFT", 40, -132)
		playerBid3:SetText("")
		playerBid3:SetTextColor(1, 1, 1)

		buttonStop4 = CreateFrame("Button", nil, bidFrame, "UIPanelButtonTemplate")
		buttonStop4:SetPoint("TOPLEFT", 20, -145)
		buttonStop4:SetSize(20, 20)
		buttonStop4:SetText("4")
		buttonStop4:SetScript("OnClick", function() LSPAssignButton(4) end)

		playerBid4 = bidFrame:CreateFontString(nil, "BORDER", "GameFontNormal")
		playerBid4:SetPoint("TOPLEFT", 40, -147)
		playerBid4:SetText("")
		playerBid4:SetTextColor(1, 1, 1)

		buttonStop5 = CreateFrame("Button", nil, bidFrame, "UIPanelButtonTemplate")
		buttonStop5:SetPoint("TOPLEFT", 20, -160)
		buttonStop5:SetSize(20, 20)
		buttonStop5:SetText("5")
		buttonStop5:SetScript("OnClick", function() LSPAssignButton(5) end)

		playerBid5 = bidFrame:CreateFontString(nil, "BORDER", "GameFontNormal")
		playerBid5:SetPoint("TOPLEFT", 40, -162)
		playerBid5:SetText("")
		playerBid5:SetTextColor(1, 1, 1)

		buttonStop6 = CreateFrame("Button", nil, bidFrame, "UIPanelButtonTemplate")
		buttonStop6:SetPoint("TOPLEFT", 20, -175)
		buttonStop6:SetSize(20, 20)
		buttonStop6:SetText("6")
		buttonStop6:SetScript("OnClick", function() LSPAssignButton(6) end)

		playerBid6 = bidFrame:CreateFontString(nil, "BORDER", "GameFontNormal")
		playerBid6:SetPoint("TOPLEFT", 40, -177)
		playerBid6:SetText("")
		playerBid6:SetTextColor(1, 1, 1)

		bidFrame:Hide()
end

------------------------------------------------------------
local function ViewBid(itemCost, itemText)
  RefreshBid()
  caption2:SetText(itemText .. " - " .. itemCost .. " points")
  bidFrame:Show()
end

local function UpdateBid(bidNum, bidText)

	if bidNum == 1 then
		playerBid1:SetText(bidText)
	elseif bidNum == 2 then
		playerBid2:SetText(bidText)
	elseif bidNum == 3 then
		playerBid3:SetText(bidText)
	elseif bidNum == 4 then
		playerBid4:SetText(bidText)
	elseif bidNum == 5 then
		playerBid5:SetText(bidText)
	elseif bidNum == 6 then
		playerBid6:SetText(bidText)
	end
end

local function HideBid()
  bidFrame:Hide()
end

-- export
ADDON.ViewBid = ViewBid
ADDON.HideBid = HideBid
ADDON.UpdateBid = UpdateBid
ADDON.CreatebidFrame = CreatebidFrame