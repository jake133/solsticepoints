local _, ADDON = ...

------------------------------------------------------------

function ShowTrack()
  ADDON.Track()
  ADDON.HideList()
  ADDON.HideListRoster()
end

function ShowList()
  ADDON.HideTrack()
  ADDON.List()
  ADDON.HideListRoster()
end

function ShowRoster()
  ADDON.HideTrack()
  ADDON.HideList()
  ADDON.ListRoster()
end

------------------------------------------------------------

local function CreateLSPFrame()
	buttonWidth = 50
	buttonHeight = 25

  frame = CreateFrame("Frame", "LSP_Frame", UIParent, "TooltipBorderedFrameTemplate")
  frame:SetMovable(true)
  frame:EnableMouse(true)
  frame:RegisterForDrag("LeftButton")
  frame:SetScript("OnDragStart", frame.StartMoving)
  frame:SetScript("OnDragStop", frame.StopMovingOrSizing)

  frame:SetSize(165, 60)
  frame:SetPoint("BOTTOMRIGHT", 0, 0)
  frame:SetFrameStrata("DIALOG")
  --frame:Hide()

  local caption1 = frame:CreateFontString(nil, "BORDER", "GameFontNormal")
  caption1:SetPoint("TOP", 0, -12)
  caption1:SetText("Solstice Points")

  	buttonTrack = CreateFrame("Button", nil, frame, "UIPanelButtonTemplate")
    buttonTrack:SetPoint("CENTER", -50, -10)
    buttonTrack:SetSize(buttonWidth, buttonHeight)
    buttonTrack:SetText("Track")
    buttonTrack:SetScript("OnClick", ShowTrack)

  	buttonList = CreateFrame("Button", nil, frame, "UIPanelButtonTemplate")
    buttonList:SetPoint("CENTER", 0, -10)
    buttonList:SetSize(buttonWidth, buttonHeight)
    buttonList:SetText("Master")
    buttonList:SetScript("OnClick", ShowList)

 	buttonRoster = CreateFrame("Button", nil, frame, "UIPanelButtonTemplate")
    buttonRoster:SetPoint("CENTER", 50, -10)
    buttonRoster:SetSize(buttonWidth, buttonHeight)
    buttonRoster:SetText("Raid")
    buttonRoster:SetScript("OnClick", ShowRoster)

	frame:Show()
end

------------------------------------------------------------

local function LSP()
  frame:Show()
end

-- export
ADDON.LSP = LSP
ADDON.CreateLSPFrame = CreateLSPFrame