local _, ADDON = ...

local caption1 = {}
local caption2 = {}
local caption3 = {}

local text1 = ""
local text2 = ""
local text3 = ""
------------------------------------------------------------
------------------------------------------------------------

local function CreateSyncFrame()
	buttonWidth = 50
	buttonHeight = 25

  frame = CreateFrame("Frame", "Sync_Frame", UIParent, "TooltipBorderedFrameTemplate")
  frame:SetMovable(true)
  frame:EnableMouse(true)
  frame:RegisterForDrag("LeftButton")
  frame:SetScript("OnDragStart", frame.StartMoving)
  frame:SetScript("OnDragStop", frame.StopMovingOrSizing)

  frame:SetSize(400, 55)
  frame:SetPoint("CENTER", 0, 0)
  frame:SetFrameStrata("DIALOG")
  --frame:Hide()

	caption1 = frame:CreateFontString(nil, "BORDER", "GameFontNormal")
	caption1:SetPoint("TOPLEFT", 10, -5)
	caption1:SetText("")
	caption1:SetTextColor(0, 1, 0)

	caption2 = frame:CreateFontString(nil, "BORDER", "GameFontNormal")
	caption2:SetPoint("TOPLEFT", 10, -20)
	caption2:SetText("")
	caption2:SetTextColor(0, 1, 0)

	caption3 = frame:CreateFontString(nil, "BORDER", "GameFontNormal")
	caption3:SetPoint("TOPLEFT", 10, -35)
	caption3:SetText("")
	caption3:SetTextColor(0, 1, 0)

	local btn = CreateFrame("Button", nil, frame, "UIPanelButtonTemplate")
	btn:SetPoint("TOPRIGHT", -5, -5)
	btn:SetSize(17, 17)
	btn:SetText("X")
	btn:SetScript("OnClick", function(self) frame:Hide() end)

	frame:Hide()
end

------------------------------------------------------------

local function SyncShow()
  frame:Show()
end

local function SyncUpdate(lineIn)
	text1 = text2
	text2 = text3
	text3 = lineIn

	caption1:SetText(text1)
	caption2:SetText(text2)
	caption3:SetText(text3)
end

-- export
ADDON.SyncShow = SyncShow
ADDON.SyncUpdate = SyncUpdate
ADDON.CreateSyncFrame = CreateSyncFrame