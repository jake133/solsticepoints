| # | Timestamp| Function | Player | Change | Description | New Balance | 
|---|---|---|---|---|---|---| 
| 1 | 2023-04-11 19:08:58 | StoreDKP  | Meyhém | -913 | Correction | Balance 250 | 
| 2 | 2023-04-11 19:20:48 | StoreDKP  | Meyhém | 913 | Correction | Balance 1163 | 
| 3 | 2023-04-11 19:29:14 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 1 | Pull 744/Flame Leviathan | Balance 494 | 
| 4 | 2023-04-11 19:29:14 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 1 | Pull 744/Flame Leviathan | Balance 377 | 
| 5 | 2023-04-11 19:29:14 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 1 | Pull 744/Flame Leviathan | Balance 545 | 
| 6 | 2023-04-11 19:29:14 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 1 | Pull 744/Flame Leviathan | Balance 361 | 
| 7 | 2023-04-11 19:29:14 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 1 | Pull 744/Flame Leviathan | Balance 236 | 
| 8 | 2023-04-11 19:29:14 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 1 | Pull 744/Flame Leviathan | Balance 241 | 
| 9 | 2023-04-11 19:29:14 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 1 | Pull 744/Flame Leviathan | Balance 271 | 
| 10 | 2023-04-11 19:29:14 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 1 | Pull 744/Flame Leviathan | Balance 436 | 
| 11 | 2023-04-11 19:29:14 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 1 | Pull 744/Flame Leviathan | Balance 360 | 
| 12 | 2023-04-11 19:29:14 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 1 | Pull 744/Flame Leviathan | Balance 274 | 
| 13 | 2023-04-11 19:29:14 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 1 | Pull 744/Flame Leviathan | Balance 615 | 
| 14 | 2023-04-11 19:29:14 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 1 | Pull 744/Flame Leviathan | Balance 315 | 
| 15 | 2023-04-11 19:29:14 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 1 | Pull 744/Flame Leviathan | Balance 411 | 
| 16 | 2023-04-11 19:29:14 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 1 | Pull 744/Flame Leviathan | Balance 1164 | 
| 17 | 2023-04-11 19:29:14 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 1 | Pull 744/Flame Leviathan | Balance 284 | 
| 18 | 2023-04-11 19:29:14 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 1 | Pull 744/Flame Leviathan | Balance 364 | 
| 19 | 2023-04-11 19:29:14 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 1 | Pull 744/Flame Leviathan | Balance 629 | 
| 20 | 2023-04-11 19:29:14 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 1 | Pull 744/Flame Leviathan | Balance 532 | 
| 21 | 2023-04-11 19:29:14 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 1 | Pull 744/Flame Leviathan | Balance 748 | 
| 22 | 2023-04-11 19:29:14 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 1 | Pull 744/Flame Leviathan | Balance 319 | 
| 23 | 2023-04-11 19:29:14 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 1 | Pull 744/Flame Leviathan | Balance 542 | 
| 24 | 2023-04-11 19:29:14 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 1 | Pull 744/Flame Leviathan | Balance 336 | 
| 25 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 10 | Attendance | Balance 504 | 
| 26 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 10 | Attendance | Balance 387 | 
| 27 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 10 | Attendance | Balance 555 | 
| 28 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 10 | Attendance | Balance 371 | 
| 29 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 10 | Attendance | Balance 246 | 
| 30 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 10 | Attendance | Balance 251 | 
| 31 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 10 | Attendance | Balance 281 | 
| 32 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 10 | Attendance | Balance 446 | 
| 33 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 10 | Attendance | Balance 370 | 
| 34 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 10 | Attendance | Balance 284 | 
| 35 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 10 | Attendance | Balance 564 | 
| 36 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 10 | Attendance | Balance 625 | 
| 37 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 10 | Attendance | Balance 350 | 
| 38 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 10 | Attendance | Balance 325 | 
| 39 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 10 | Attendance | Balance 421 | 
| 40 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 10 | Attendance | Balance 1174 | 
| 41 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 10 | Attendance | Balance 294 | 
| 42 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 10 | Attendance | Balance 374 | 
| 43 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 10 | Attendance | Balance 639 | 
| 44 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 10 | Attendance | Balance 542 | 
| 45 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 10 | Attendance | Balance 758 | 
| 46 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 10 | Attendance | Balance 329 | 
| 47 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 10 | Attendance | Balance 1084 | 
| 48 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 10 | Attendance | Balance 552 | 
| 49 | 2023-04-11 19:46:45 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 10 | Attendance | Balance 346 | 
| 50 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 1 | Pull 744/Flame Leviathan | Balance 505 | 
| 51 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 1 | Pull 744/Flame Leviathan | Balance 388 | 
| 52 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 1 | Pull 744/Flame Leviathan | Balance 556 | 
| 53 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 1 | Pull 744/Flame Leviathan | Balance 372 | 
| 54 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 1 | Pull 744/Flame Leviathan | Balance 247 | 
| 55 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 1 | Pull 744/Flame Leviathan | Balance 252 | 
| 56 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 1 | Pull 744/Flame Leviathan | Balance 282 | 
| 57 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 1 | Pull 744/Flame Leviathan | Balance 447 | 
| 58 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 1 | Pull 744/Flame Leviathan | Balance 371 | 
| 59 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 1 | Pull 744/Flame Leviathan | Balance 285 | 
| 60 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 1 | Pull 744/Flame Leviathan | Balance 565 | 
| 61 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 1 | Pull 744/Flame Leviathan | Balance 626 | 
| 62 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 1 | Pull 744/Flame Leviathan | Balance 351 | 
| 63 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 1 | Pull 744/Flame Leviathan | Balance 326 | 
| 64 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 1 | Pull 744/Flame Leviathan | Balance 422 | 
| 65 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 1 | Pull 744/Flame Leviathan | Balance 1175 | 
| 66 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 1 | Pull 744/Flame Leviathan | Balance 295 | 
| 67 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 1 | Pull 744/Flame Leviathan | Balance 375 | 
| 68 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 1 | Pull 744/Flame Leviathan | Balance 640 | 
| 69 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 1 | Pull 744/Flame Leviathan | Balance 543 | 
| 70 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 1 | Pull 744/Flame Leviathan | Balance 759 | 
| 71 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 1 | Pull 744/Flame Leviathan | Balance 330 | 
| 72 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 1 | Pull 744/Flame Leviathan | Balance 1085 | 
| 73 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 1 | Pull 744/Flame Leviathan | Balance 553 | 
| 74 | 2023-04-11 19:47:45 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 1 | Pull 744/Flame Leviathan | Balance 347 | 
| 75 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 2 | Kill 744/Flame Leviathan | Balance 507 | 
| 76 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 2 | Kill 744/Flame Leviathan | Balance 390 | 
| 77 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 2 | Kill 744/Flame Leviathan | Balance 558 | 
| 78 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 2 | Kill 744/Flame Leviathan | Balance 374 | 
| 79 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 2 | Kill 744/Flame Leviathan | Balance 249 | 
| 80 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 2 | Kill 744/Flame Leviathan | Balance 254 | 
| 81 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 2 | Kill 744/Flame Leviathan | Balance 284 | 
| 82 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 2 | Kill 744/Flame Leviathan | Balance 449 | 
| 83 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 2 | Kill 744/Flame Leviathan | Balance 373 | 
| 84 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 2 | Kill 744/Flame Leviathan | Balance 287 | 
| 85 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 2 | Kill 744/Flame Leviathan | Balance 567 | 
| 86 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 2 | Kill 744/Flame Leviathan | Balance 628 | 
| 87 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 2 | Kill 744/Flame Leviathan | Balance 353 | 
| 88 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 2 | Kill 744/Flame Leviathan | Balance 328 | 
| 89 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 2 | Kill 744/Flame Leviathan | Balance 424 | 
| 90 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 2 | Kill 744/Flame Leviathan | Balance 1177 | 
| 91 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 2 | Kill 744/Flame Leviathan | Balance 297 | 
| 92 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 2 | Kill 744/Flame Leviathan | Balance 377 | 
| 93 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 2 | Kill 744/Flame Leviathan | Balance 642 | 
| 94 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 2 | Kill 744/Flame Leviathan | Balance 545 | 
| 95 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 2 | Kill 744/Flame Leviathan | Balance 761 | 
| 96 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 2 | Kill 744/Flame Leviathan | Balance 332 | 
| 97 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 2 | Kill 744/Flame Leviathan | Balance 1087 | 
| 98 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 2 | Kill 744/Flame Leviathan | Balance 555 | 
| 99 | 2023-04-11 19:48:58 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 2 | Kill 744/Flame Leviathan | Balance 349 | 
| 100 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 1 | Pull 747/XT-002 Deconstructor | Balance 508 | 
| 101 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 1 | Pull 747/XT-002 Deconstructor | Balance 391 | 
| 102 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 1 | Pull 747/XT-002 Deconstructor | Balance 559 | 
| 103 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 1 | Pull 747/XT-002 Deconstructor | Balance 375 | 
| 104 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 1 | Pull 747/XT-002 Deconstructor | Balance 250 | 
| 105 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 1 | Pull 747/XT-002 Deconstructor | Balance 255 | 
| 106 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 1 | Pull 747/XT-002 Deconstructor | Balance 285 | 
| 107 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 1 | Pull 747/XT-002 Deconstructor | Balance 450 | 
| 108 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 1 | Pull 747/XT-002 Deconstructor | Balance 374 | 
| 109 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 1 | Pull 747/XT-002 Deconstructor | Balance 288 | 
| 110 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 1 | Pull 747/XT-002 Deconstructor | Balance 568 | 
| 111 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 1 | Pull 747/XT-002 Deconstructor | Balance 629 | 
| 112 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 1 | Pull 747/XT-002 Deconstructor | Balance 354 | 
| 113 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 1 | Pull 747/XT-002 Deconstructor | Balance 329 | 
| 114 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 1 | Pull 747/XT-002 Deconstructor | Balance 425 | 
| 115 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 1 | Pull 747/XT-002 Deconstructor | Balance 1178 | 
| 116 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 1 | Pull 747/XT-002 Deconstructor | Balance 298 | 
| 117 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 1 | Pull 747/XT-002 Deconstructor | Balance 378 | 
| 118 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 1 | Pull 747/XT-002 Deconstructor | Balance 643 | 
| 119 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 1 | Pull 747/XT-002 Deconstructor | Balance 546 | 
| 120 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 1 | Pull 747/XT-002 Deconstructor | Balance 762 | 
| 121 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 1 | Pull 747/XT-002 Deconstructor | Balance 333 | 
| 122 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 1 | Pull 747/XT-002 Deconstructor | Balance 1088 | 
| 123 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 1 | Pull 747/XT-002 Deconstructor | Balance 556 | 
| 124 | 2023-04-11 19:54:12 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 1 | Pull 747/XT-002 Deconstructor | Balance 350 | 
| 125 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 2 | Kill 747/XT-002 Deconstructor | Balance 510 | 
| 126 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 2 | Kill 747/XT-002 Deconstructor | Balance 393 | 
| 127 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 2 | Kill 747/XT-002 Deconstructor | Balance 561 | 
| 128 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 2 | Kill 747/XT-002 Deconstructor | Balance 377 | 
| 129 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 2 | Kill 747/XT-002 Deconstructor | Balance 252 | 
| 130 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 2 | Kill 747/XT-002 Deconstructor | Balance 257 | 
| 131 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 2 | Kill 747/XT-002 Deconstructor | Balance 287 | 
| 132 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 2 | Kill 747/XT-002 Deconstructor | Balance 452 | 
| 133 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 2 | Kill 747/XT-002 Deconstructor | Balance 376 | 
| 134 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 2 | Kill 747/XT-002 Deconstructor | Balance 290 | 
| 135 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 2 | Kill 747/XT-002 Deconstructor | Balance 570 | 
| 136 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 2 | Kill 747/XT-002 Deconstructor | Balance 631 | 
| 137 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 2 | Kill 747/XT-002 Deconstructor | Balance 356 | 
| 138 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 2 | Kill 747/XT-002 Deconstructor | Balance 331 | 
| 139 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 2 | Kill 747/XT-002 Deconstructor | Balance 427 | 
| 140 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 2 | Kill 747/XT-002 Deconstructor | Balance 1180 | 
| 141 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 2 | Kill 747/XT-002 Deconstructor | Balance 300 | 
| 142 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 2 | Kill 747/XT-002 Deconstructor | Balance 380 | 
| 143 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 2 | Kill 747/XT-002 Deconstructor | Balance 645 | 
| 144 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 2 | Kill 747/XT-002 Deconstructor | Balance 548 | 
| 145 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 2 | Kill 747/XT-002 Deconstructor | Balance 764 | 
| 146 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 2 | Kill 747/XT-002 Deconstructor | Balance 335 | 
| 147 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 2 | Kill 747/XT-002 Deconstructor | Balance 1090 | 
| 148 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 2 | Kill 747/XT-002 Deconstructor | Balance 558 | 
| 149 | 2023-04-11 19:56:54 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 2 | Kill 747/XT-002 Deconstructor | Balance 352 | 
| 150 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 1 | Pull 749/Kologarn | Balance 511 | 
| 151 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 1 | Pull 749/Kologarn | Balance 394 | 
| 152 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 1 | Pull 749/Kologarn | Balance 562 | 
| 153 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 1 | Pull 749/Kologarn | Balance 378 | 
| 154 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 1 | Pull 749/Kologarn | Balance 253 | 
| 155 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 1 | Pull 749/Kologarn | Balance 258 | 
| 156 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 1 | Pull 749/Kologarn | Balance 288 | 
| 157 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 1 | Pull 749/Kologarn | Balance 453 | 
| 158 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 1 | Pull 749/Kologarn | Balance 377 | 
| 159 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 1 | Pull 749/Kologarn | Balance 291 | 
| 160 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 1 | Pull 749/Kologarn | Balance 571 | 
| 161 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 1 | Pull 749/Kologarn | Balance 632 | 
| 162 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 1 | Pull 749/Kologarn | Balance 357 | 
| 163 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 1 | Pull 749/Kologarn | Balance 332 | 
| 164 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 1 | Pull 749/Kologarn | Balance 428 | 
| 165 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 1 | Pull 749/Kologarn | Balance 1181 | 
| 166 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 1 | Pull 749/Kologarn | Balance 301 | 
| 167 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 1 | Pull 749/Kologarn | Balance 381 | 
| 168 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 1 | Pull 749/Kologarn | Balance 646 | 
| 169 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 1 | Pull 749/Kologarn | Balance 549 | 
| 170 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 1 | Pull 749/Kologarn | Balance 765 | 
| 171 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 1 | Pull 749/Kologarn | Balance 336 | 
| 172 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 1 | Pull 749/Kologarn | Balance 1091 | 
| 173 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 1 | Pull 749/Kologarn | Balance 559 | 
| 174 | 2023-04-11 20:05:17 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 1 | Pull 749/Kologarn | Balance 353 | 
| 175 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 2 | Kill 749/Kologarn | Balance 513 | 
| 176 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 2 | Kill 749/Kologarn | Balance 396 | 
| 177 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 2 | Kill 749/Kologarn | Balance 564 | 
| 178 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 2 | Kill 749/Kologarn | Balance 380 | 
| 179 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 2 | Kill 749/Kologarn | Balance 255 | 
| 180 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 2 | Kill 749/Kologarn | Balance 260 | 
| 181 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 2 | Kill 749/Kologarn | Balance 290 | 
| 182 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 2 | Kill 749/Kologarn | Balance 455 | 
| 183 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 2 | Kill 749/Kologarn | Balance 379 | 
| 184 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 2 | Kill 749/Kologarn | Balance 293 | 
| 185 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 2 | Kill 749/Kologarn | Balance 573 | 
| 186 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 2 | Kill 749/Kologarn | Balance 634 | 
| 187 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 2 | Kill 749/Kologarn | Balance 359 | 
| 188 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 2 | Kill 749/Kologarn | Balance 334 | 
| 189 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 2 | Kill 749/Kologarn | Balance 430 | 
| 190 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 2 | Kill 749/Kologarn | Balance 1183 | 
| 191 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 2 | Kill 749/Kologarn | Balance 303 | 
| 192 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 2 | Kill 749/Kologarn | Balance 383 | 
| 193 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 2 | Kill 749/Kologarn | Balance 648 | 
| 194 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 2 | Kill 749/Kologarn | Balance 551 | 
| 195 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 2 | Kill 749/Kologarn | Balance 767 | 
| 196 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 2 | Kill 749/Kologarn | Balance 338 | 
| 197 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 2 | Kill 749/Kologarn | Balance 1093 | 
| 198 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 2 | Kill 749/Kologarn | Balance 561 | 
| 199 | 2023-04-11 20:07:28 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 2 | Kill 749/Kologarn | Balance 355 | 
| 200 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 1 | Pull 750/Auriaya | Balance 514 | 
| 201 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 1 | Pull 750/Auriaya | Balance 397 | 
| 202 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 1 | Pull 750/Auriaya | Balance 565 | 
| 203 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 1 | Pull 750/Auriaya | Balance 381 | 
| 204 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 1 | Pull 750/Auriaya | Balance 256 | 
| 205 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 1 | Pull 750/Auriaya | Balance 261 | 
| 206 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 1 | Pull 750/Auriaya | Balance 291 | 
| 207 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 1 | Pull 750/Auriaya | Balance 456 | 
| 208 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 1 | Pull 750/Auriaya | Balance 380 | 
| 209 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 1 | Pull 750/Auriaya | Balance 294 | 
| 210 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 1 | Pull 750/Auriaya | Balance 574 | 
| 211 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 1 | Pull 750/Auriaya | Balance 635 | 
| 212 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 1 | Pull 750/Auriaya | Balance 360 | 
| 213 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 1 | Pull 750/Auriaya | Balance 335 | 
| 214 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 1 | Pull 750/Auriaya | Balance 431 | 
| 215 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 1 | Pull 750/Auriaya | Balance 1184 | 
| 216 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 1 | Pull 750/Auriaya | Balance 304 | 
| 217 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 1 | Pull 750/Auriaya | Balance 384 | 
| 218 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 1 | Pull 750/Auriaya | Balance 649 | 
| 219 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 1 | Pull 750/Auriaya | Balance 552 | 
| 220 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 1 | Pull 750/Auriaya | Balance 768 | 
| 221 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 1 | Pull 750/Auriaya | Balance 339 | 
| 222 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 1 | Pull 750/Auriaya | Balance 1094 | 
| 223 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 1 | Pull 750/Auriaya | Balance 562 | 
| 224 | 2023-04-11 20:10:56 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 1 | Pull 750/Auriaya | Balance 356 | 
| 225 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 1 | Pull 750/Auriaya | Balance 515 | 
| 226 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 1 | Pull 750/Auriaya | Balance 398 | 
| 227 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 1 | Pull 750/Auriaya | Balance 566 | 
| 228 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 1 | Pull 750/Auriaya | Balance 382 | 
| 229 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 1 | Pull 750/Auriaya | Balance 257 | 
| 230 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 1 | Pull 750/Auriaya | Balance 262 | 
| 231 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 1 | Pull 750/Auriaya | Balance 292 | 
| 232 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 1 | Pull 750/Auriaya | Balance 457 | 
| 233 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 1 | Pull 750/Auriaya | Balance 381 | 
| 234 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 1 | Pull 750/Auriaya | Balance 295 | 
| 235 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 1 | Pull 750/Auriaya | Balance 575 | 
| 236 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 1 | Pull 750/Auriaya | Balance 636 | 
| 237 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 1 | Pull 750/Auriaya | Balance 361 | 
| 238 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 1 | Pull 750/Auriaya | Balance 336 | 
| 239 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 1 | Pull 750/Auriaya | Balance 432 | 
| 240 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 1 | Pull 750/Auriaya | Balance 1185 | 
| 241 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 1 | Pull 750/Auriaya | Balance 305 | 
| 242 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 1 | Pull 750/Auriaya | Balance 385 | 
| 243 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 1 | Pull 750/Auriaya | Balance 650 | 
| 244 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 1 | Pull 750/Auriaya | Balance 553 | 
| 245 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 1 | Pull 750/Auriaya | Balance 769 | 
| 246 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 1 | Pull 750/Auriaya | Balance 340 | 
| 247 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 1 | Pull 750/Auriaya | Balance 1095 | 
| 248 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 1 | Pull 750/Auriaya | Balance 563 | 
| 249 | 2023-04-11 20:16:49 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 1 | Pull 750/Auriaya | Balance 357 | 
| 250 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 2 | Kill 750/Auriaya | Balance 517 | 
| 251 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 2 | Kill 750/Auriaya | Balance 400 | 
| 252 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 2 | Kill 750/Auriaya | Balance 568 | 
| 253 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 2 | Kill 750/Auriaya | Balance 384 | 
| 254 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 2 | Kill 750/Auriaya | Balance 259 | 
| 255 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 2 | Kill 750/Auriaya | Balance 264 | 
| 256 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 2 | Kill 750/Auriaya | Balance 294 | 
| 257 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 2 | Kill 750/Auriaya | Balance 459 | 
| 258 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 2 | Kill 750/Auriaya | Balance 383 | 
| 259 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 2 | Kill 750/Auriaya | Balance 297 | 
| 260 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 2 | Kill 750/Auriaya | Balance 577 | 
| 261 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 2 | Kill 750/Auriaya | Balance 638 | 
| 262 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 2 | Kill 750/Auriaya | Balance 363 | 
| 263 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 2 | Kill 750/Auriaya | Balance 338 | 
| 264 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 2 | Kill 750/Auriaya | Balance 434 | 
| 265 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 2 | Kill 750/Auriaya | Balance 1187 | 
| 266 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 2 | Kill 750/Auriaya | Balance 307 | 
| 267 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 2 | Kill 750/Auriaya | Balance 387 | 
| 268 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 2 | Kill 750/Auriaya | Balance 652 | 
| 269 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 2 | Kill 750/Auriaya | Balance 555 | 
| 270 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 2 | Kill 750/Auriaya | Balance 771 | 
| 271 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 2 | Kill 750/Auriaya | Balance 342 | 
| 272 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 2 | Kill 750/Auriaya | Balance 1097 | 
| 273 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 2 | Kill 750/Auriaya | Balance 565 | 
| 274 | 2023-04-11 20:20:08 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 2 | Kill 750/Auriaya | Balance 359 | 
| 275 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 1 | Pull 751/Hodir | Balance 518 | 
| 276 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 1 | Pull 751/Hodir | Balance 401 | 
| 277 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 1 | Pull 751/Hodir | Balance 569 | 
| 278 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 1 | Pull 751/Hodir | Balance 385 | 
| 279 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 1 | Pull 751/Hodir | Balance 260 | 
| 280 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 1 | Pull 751/Hodir | Balance 265 | 
| 281 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 1 | Pull 751/Hodir | Balance 295 | 
| 282 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 1 | Pull 751/Hodir | Balance 460 | 
| 283 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 1 | Pull 751/Hodir | Balance 384 | 
| 284 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 1 | Pull 751/Hodir | Balance 298 | 
| 285 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 1 | Pull 751/Hodir | Balance 578 | 
| 286 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 1 | Pull 751/Hodir | Balance 639 | 
| 287 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 1 | Pull 751/Hodir | Balance 364 | 
| 288 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 1 | Pull 751/Hodir | Balance 339 | 
| 289 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 1 | Pull 751/Hodir | Balance 435 | 
| 290 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 1 | Pull 751/Hodir | Balance 1188 | 
| 291 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 1 | Pull 751/Hodir | Balance 308 | 
| 292 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 1 | Pull 751/Hodir | Balance 388 | 
| 293 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 1 | Pull 751/Hodir | Balance 653 | 
| 294 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 1 | Pull 751/Hodir | Balance 556 | 
| 295 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 1 | Pull 751/Hodir | Balance 772 | 
| 296 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 1 | Pull 751/Hodir | Balance 343 | 
| 297 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 1 | Pull 751/Hodir | Balance 1098 | 
| 298 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 1 | Pull 751/Hodir | Balance 566 | 
| 299 | 2023-04-11 20:27:21 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 1 | Pull 751/Hodir | Balance 360 | 
| 300 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 2 | Kill 751/Hodir | Balance 520 | 
| 301 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 2 | Kill 751/Hodir | Balance 403 | 
| 302 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 2 | Kill 751/Hodir | Balance 571 | 
| 303 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 2 | Kill 751/Hodir | Balance 387 | 
| 304 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 2 | Kill 751/Hodir | Balance 262 | 
| 305 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 2 | Kill 751/Hodir | Balance 267 | 
| 306 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 2 | Kill 751/Hodir | Balance 297 | 
| 307 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 2 | Kill 751/Hodir | Balance 462 | 
| 308 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 2 | Kill 751/Hodir | Balance 386 | 
| 309 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 2 | Kill 751/Hodir | Balance 300 | 
| 310 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 2 | Kill 751/Hodir | Balance 580 | 
| 311 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 2 | Kill 751/Hodir | Balance 641 | 
| 312 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 2 | Kill 751/Hodir | Balance 366 | 
| 313 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 2 | Kill 751/Hodir | Balance 341 | 
| 314 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 2 | Kill 751/Hodir | Balance 437 | 
| 315 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 2 | Kill 751/Hodir | Balance 1190 | 
| 316 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 2 | Kill 751/Hodir | Balance 310 | 
| 317 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 2 | Kill 751/Hodir | Balance 390 | 
| 318 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 2 | Kill 751/Hodir | Balance 655 | 
| 319 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 2 | Kill 751/Hodir | Balance 558 | 
| 320 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 2 | Kill 751/Hodir | Balance 774 | 
| 321 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 2 | Kill 751/Hodir | Balance 345 | 
| 322 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 2 | Kill 751/Hodir | Balance 1100 | 
| 323 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 2 | Kill 751/Hodir | Balance 568 | 
| 324 | 2023-04-11 20:31:22 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 2 | Kill 751/Hodir | Balance 362 | 
| 325 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 1 | Pull 752/Thorim | Balance 521 | 
| 326 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 1 | Pull 752/Thorim | Balance 404 | 
| 327 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 1 | Pull 752/Thorim | Balance 572 | 
| 328 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 1 | Pull 752/Thorim | Balance 388 | 
| 329 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 1 | Pull 752/Thorim | Balance 263 | 
| 330 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 1 | Pull 752/Thorim | Balance 268 | 
| 331 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 1 | Pull 752/Thorim | Balance 298 | 
| 332 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 1 | Pull 752/Thorim | Balance 463 | 
| 333 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 1 | Pull 752/Thorim | Balance 387 | 
| 334 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 1 | Pull 752/Thorim | Balance 301 | 
| 335 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 1 | Pull 752/Thorim | Balance 581 | 
| 336 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 1 | Pull 752/Thorim | Balance 642 | 
| 337 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 1 | Pull 752/Thorim | Balance 367 | 
| 338 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 1 | Pull 752/Thorim | Balance 342 | 
| 339 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 1 | Pull 752/Thorim | Balance 438 | 
| 340 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 1 | Pull 752/Thorim | Balance 1191 | 
| 341 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 1 | Pull 752/Thorim | Balance 311 | 
| 342 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 1 | Pull 752/Thorim | Balance 391 | 
| 343 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 1 | Pull 752/Thorim | Balance 656 | 
| 344 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 1 | Pull 752/Thorim | Balance 559 | 
| 345 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 1 | Pull 752/Thorim | Balance 775 | 
| 346 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 1 | Pull 752/Thorim | Balance 346 | 
| 347 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 1 | Pull 752/Thorim | Balance 1101 | 
| 348 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 1 | Pull 752/Thorim | Balance 569 | 
| 349 | 2023-04-11 20:36:50 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 1 | Pull 752/Thorim | Balance 363 | 
| 350 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 2 | Kill 752/Thorim | Balance 523 | 
| 351 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 2 | Kill 752/Thorim | Balance 406 | 
| 352 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 2 | Kill 752/Thorim | Balance 574 | 
| 353 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 2 | Kill 752/Thorim | Balance 390 | 
| 354 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 2 | Kill 752/Thorim | Balance 265 | 
| 355 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 2 | Kill 752/Thorim | Balance 270 | 
| 356 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 2 | Kill 752/Thorim | Balance 300 | 
| 357 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 2 | Kill 752/Thorim | Balance 465 | 
| 358 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 2 | Kill 752/Thorim | Balance 389 | 
| 359 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 2 | Kill 752/Thorim | Balance 303 | 
| 360 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 2 | Kill 752/Thorim | Balance 583 | 
| 361 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 2 | Kill 752/Thorim | Balance 644 | 
| 362 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 2 | Kill 752/Thorim | Balance 369 | 
| 363 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 2 | Kill 752/Thorim | Balance 344 | 
| 364 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 2 | Kill 752/Thorim | Balance 440 | 
| 365 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 2 | Kill 752/Thorim | Balance 1193 | 
| 366 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 2 | Kill 752/Thorim | Balance 313 | 
| 367 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 2 | Kill 752/Thorim | Balance 393 | 
| 368 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 2 | Kill 752/Thorim | Balance 658 | 
| 369 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 2 | Kill 752/Thorim | Balance 561 | 
| 370 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 2 | Kill 752/Thorim | Balance 777 | 
| 371 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 2 | Kill 752/Thorim | Balance 348 | 
| 372 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 2 | Kill 752/Thorim | Balance 1103 | 
| 373 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 2 | Kill 752/Thorim | Balance 571 | 
| 374 | 2023-04-11 20:42:18 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 2 | Kill 752/Thorim | Balance 365 | 
| 375 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | -527 | Loot purchased Scale of Fates | Balance 250 | 
| 376 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 4 | Distributed points from Loot | Balance 527 | 
| 377 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 4 | Distributed points from Loot | Balance 410 | 
| 378 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 4 | Distributed points from Loot | Balance 578 | 
| 379 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 4 | Distributed points from Loot | Balance 394 | 
| 380 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 4 | Distributed points from Loot | Balance 269 | 
| 381 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 4 | Distributed points from Loot | Balance 274 | 
| 382 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 4 | Distributed points from Loot | Balance 304 | 
| 383 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 4 | Distributed points from Loot | Balance 469 | 
| 384 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 4 | Distributed points from Loot | Balance 393 | 
| 385 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 4 | Distributed points from Loot | Balance 307 | 
| 386 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 4 | Distributed points from Loot | Balance 587 | 
| 387 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 4 | Distributed points from Loot | Balance 648 | 
| 388 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 4 | Distributed points from Loot | Balance 373 | 
| 389 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 4 | Distributed points from Loot | Balance 348 | 
| 390 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 4 | Distributed points from Loot | Balance 444 | 
| 391 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 4 | Distributed points from Loot | Balance 1197 | 
| 392 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 4 | Distributed points from Loot | Balance 317 | 
| 393 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 4 | Distributed points from Loot | Balance 397 | 
| 394 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 4 | Distributed points from Loot | Balance 662 | 
| 395 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 4 | Distributed points from Loot | Balance 565 | 
| 396 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 4 | Distributed points from Loot | Balance 254 | 
| 397 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 4 | Distributed points from Loot | Balance 352 | 
| 398 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 4 | Distributed points from Loot | Balance 1107 | 
| 399 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 4 | Distributed points from Loot | Balance 575 | 
| 400 | 2023-04-11 20:44:02 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 4 | Distributed points from Loot | Balance 369 | 
| 401 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 1 | Pull 753/Freya | Balance 528 | 
| 402 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 1 | Pull 753/Freya | Balance 411 | 
| 403 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 1 | Pull 753/Freya | Balance 579 | 
| 404 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 1 | Pull 753/Freya | Balance 395 | 
| 405 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 1 | Pull 753/Freya | Balance 270 | 
| 406 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 1 | Pull 753/Freya | Balance 275 | 
| 407 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 1 | Pull 753/Freya | Balance 305 | 
| 408 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 1 | Pull 753/Freya | Balance 470 | 
| 409 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 1 | Pull 753/Freya | Balance 394 | 
| 410 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 1 | Pull 753/Freya | Balance 308 | 
| 411 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 1 | Pull 753/Freya | Balance 588 | 
| 412 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 1 | Pull 753/Freya | Balance 649 | 
| 413 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 1 | Pull 753/Freya | Balance 374 | 
| 414 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 1 | Pull 753/Freya | Balance 349 | 
| 415 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 1 | Pull 753/Freya | Balance 445 | 
| 416 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 1 | Pull 753/Freya | Balance 1198 | 
| 417 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 1 | Pull 753/Freya | Balance 318 | 
| 418 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 1 | Pull 753/Freya | Balance 398 | 
| 419 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 1 | Pull 753/Freya | Balance 663 | 
| 420 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 1 | Pull 753/Freya | Balance 566 | 
| 421 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 1 | Pull 753/Freya | Balance 255 | 
| 422 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 1 | Pull 753/Freya | Balance 353 | 
| 423 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 1 | Pull 753/Freya | Balance 1108 | 
| 424 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 1 | Pull 753/Freya | Balance 576 | 
| 425 | 2023-04-11 20:55:19 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 1 | Pull 753/Freya | Balance 370 | 
| 426 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 2 | Kill 753/Freya | Balance 530 | 
| 427 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 2 | Kill 753/Freya | Balance 413 | 
| 428 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 2 | Kill 753/Freya | Balance 581 | 
| 429 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 2 | Kill 753/Freya | Balance 397 | 
| 430 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 2 | Kill 753/Freya | Balance 272 | 
| 431 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 2 | Kill 753/Freya | Balance 277 | 
| 432 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 2 | Kill 753/Freya | Balance 307 | 
| 433 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 2 | Kill 753/Freya | Balance 472 | 
| 434 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 2 | Kill 753/Freya | Balance 396 | 
| 435 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 2 | Kill 753/Freya | Balance 310 | 
| 436 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 2 | Kill 753/Freya | Balance 590 | 
| 437 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 2 | Kill 753/Freya | Balance 651 | 
| 438 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 2 | Kill 753/Freya | Balance 376 | 
| 439 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 2 | Kill 753/Freya | Balance 351 | 
| 440 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 2 | Kill 753/Freya | Balance 447 | 
| 441 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 2 | Kill 753/Freya | Balance 1200 | 
| 442 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 2 | Kill 753/Freya | Balance 320 | 
| 443 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 2 | Kill 753/Freya | Balance 400 | 
| 444 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 2 | Kill 753/Freya | Balance 665 | 
| 445 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 2 | Kill 753/Freya | Balance 568 | 
| 446 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 2 | Kill 753/Freya | Balance 257 | 
| 447 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 2 | Kill 753/Freya | Balance 355 | 
| 448 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 2 | Kill 753/Freya | Balance 1110 | 
| 449 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 2 | Kill 753/Freya | Balance 578 | 
| 450 | 2023-04-11 21:00:04 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 2 | Kill 753/Freya | Balance 372 | 
| 451 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | -242 | Loot purchased Legplates of the Wayward Vanquisher | Balance 230 | 
| 452 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 5 | Distributed points from Loot | Balance 535 | 
| 453 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 5 | Distributed points from Loot | Balance 418 | 
| 454 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 5 | Distributed points from Loot | Balance 586 | 
| 455 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 5 | Distributed points from Loot | Balance 402 | 
| 456 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 5 | Distributed points from Loot | Balance 277 | 
| 457 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 5 | Distributed points from Loot | Balance 282 | 
| 458 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 5 | Distributed points from Loot | Balance 312 | 
| 459 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 5 | Distributed points from Loot | Balance 235 | 
| 460 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 5 | Distributed points from Loot | Balance 401 | 
| 461 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 5 | Distributed points from Loot | Balance 315 | 
| 462 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 5 | Distributed points from Loot | Balance 595 | 
| 463 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 5 | Distributed points from Loot | Balance 656 | 
| 464 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 5 | Distributed points from Loot | Balance 381 | 
| 465 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 5 | Distributed points from Loot | Balance 356 | 
| 466 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 5 | Distributed points from Loot | Balance 452 | 
| 467 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 5 | Distributed points from Loot | Balance 1205 | 
| 468 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 5 | Distributed points from Loot | Balance 325 | 
| 469 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 5 | Distributed points from Loot | Balance 405 | 
| 470 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 5 | Distributed points from Loot | Balance 670 | 
| 471 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 5 | Distributed points from Loot | Balance 573 | 
| 472 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 5 | Distributed points from Loot | Balance 262 | 
| 473 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 5 | Distributed points from Loot | Balance 360 | 
| 474 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 5 | Distributed points from Loot | Balance 1115 | 
| 475 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 5 | Distributed points from Loot | Balance 583 | 
| 476 | 2023-04-11 21:01:33 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 5 | Distributed points from Loot | Balance 377 | 
| 477 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | -120 | Loot purchased Legplates of the Wayward Conqueror | Balance 192 | 
| 478 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 5 | Distributed points from Loot | Balance 540 | 
| 479 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 5 | Distributed points from Loot | Balance 423 | 
| 480 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 5 | Distributed points from Loot | Balance 591 | 
| 481 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 5 | Distributed points from Loot | Balance 407 | 
| 482 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 5 | Distributed points from Loot | Balance 282 | 
| 483 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 5 | Distributed points from Loot | Balance 287 | 
| 484 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 5 | Distributed points from Loot | Balance 197 | 
| 485 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 5 | Distributed points from Loot | Balance 240 | 
| 486 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 5 | Distributed points from Loot | Balance 406 | 
| 487 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 5 | Distributed points from Loot | Balance 320 | 
| 488 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 5 | Distributed points from Loot | Balance 600 | 
| 489 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 5 | Distributed points from Loot | Balance 661 | 
| 490 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 5 | Distributed points from Loot | Balance 386 | 
| 491 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 5 | Distributed points from Loot | Balance 361 | 
| 492 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 5 | Distributed points from Loot | Balance 457 | 
| 493 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 5 | Distributed points from Loot | Balance 1210 | 
| 494 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 5 | Distributed points from Loot | Balance 330 | 
| 495 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 5 | Distributed points from Loot | Balance 410 | 
| 496 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 5 | Distributed points from Loot | Balance 675 | 
| 497 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 5 | Distributed points from Loot | Balance 578 | 
| 498 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 5 | Distributed points from Loot | Balance 267 | 
| 499 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 5 | Distributed points from Loot | Balance 365 | 
| 500 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 5 | Distributed points from Loot | Balance 1120 | 
| 501 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 5 | Distributed points from Loot | Balance 588 | 
| 502 | 2023-04-11 21:02:03 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 5 | Distributed points from Loot | Balance 382 | 
| 503 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 1 | Pull 754/Mimiron | Balance 541 | 
| 504 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 1 | Pull 754/Mimiron | Balance 424 | 
| 505 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 1 | Pull 754/Mimiron | Balance 592 | 
| 506 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 1 | Pull 754/Mimiron | Balance 408 | 
| 507 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 1 | Pull 754/Mimiron | Balance 283 | 
| 508 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 1 | Pull 754/Mimiron | Balance 288 | 
| 509 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 1 | Pull 754/Mimiron | Balance 198 | 
| 510 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 1 | Pull 754/Mimiron | Balance 241 | 
| 511 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 1 | Pull 754/Mimiron | Balance 407 | 
| 512 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 1 | Pull 754/Mimiron | Balance 321 | 
| 513 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 1 | Pull 754/Mimiron | Balance 601 | 
| 514 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 1 | Pull 754/Mimiron | Balance 662 | 
| 515 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 1 | Pull 754/Mimiron | Balance 387 | 
| 516 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 1 | Pull 754/Mimiron | Balance 362 | 
| 517 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 1 | Pull 754/Mimiron | Balance 458 | 
| 518 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 1 | Pull 754/Mimiron | Balance 1211 | 
| 519 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 1 | Pull 754/Mimiron | Balance 331 | 
| 520 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 1 | Pull 754/Mimiron | Balance 411 | 
| 521 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 1 | Pull 754/Mimiron | Balance 676 | 
| 522 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 1 | Pull 754/Mimiron | Balance 579 | 
| 523 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 1 | Pull 754/Mimiron | Balance 268 | 
| 524 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 1 | Pull 754/Mimiron | Balance 366 | 
| 525 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 1 | Pull 754/Mimiron | Balance 1121 | 
| 526 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 1 | Pull 754/Mimiron | Balance 589 | 
| 527 | 2023-04-11 21:16:22 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 1 | Pull 754/Mimiron | Balance 383 | 
| 528 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 2 | Kill 754/Mimiron | Balance 543 | 
| 529 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 2 | Kill 754/Mimiron | Balance 426 | 
| 530 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 2 | Kill 754/Mimiron | Balance 594 | 
| 531 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 2 | Kill 754/Mimiron | Balance 410 | 
| 532 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 2 | Kill 754/Mimiron | Balance 285 | 
| 533 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 2 | Kill 754/Mimiron | Balance 290 | 
| 534 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 2 | Kill 754/Mimiron | Balance 200 | 
| 535 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 2 | Kill 754/Mimiron | Balance 243 | 
| 536 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 2 | Kill 754/Mimiron | Balance 409 | 
| 537 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 2 | Kill 754/Mimiron | Balance 323 | 
| 538 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 2 | Kill 754/Mimiron | Balance 603 | 
| 539 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 2 | Kill 754/Mimiron | Balance 664 | 
| 540 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 2 | Kill 754/Mimiron | Balance 389 | 
| 541 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 2 | Kill 754/Mimiron | Balance 364 | 
| 542 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 2 | Kill 754/Mimiron | Balance 460 | 
| 543 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 2 | Kill 754/Mimiron | Balance 1213 | 
| 544 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 2 | Kill 754/Mimiron | Balance 333 | 
| 545 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 2 | Kill 754/Mimiron | Balance 413 | 
| 546 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 2 | Kill 754/Mimiron | Balance 678 | 
| 547 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 2 | Kill 754/Mimiron | Balance 581 | 
| 548 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 2 | Kill 754/Mimiron | Balance 270 | 
| 549 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 2 | Kill 754/Mimiron | Balance 368 | 
| 550 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 2 | Kill 754/Mimiron | Balance 1123 | 
| 551 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 2 | Kill 754/Mimiron | Balance 591 | 
| 552 | 2023-04-11 21:23:44 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 2 | Kill 754/Mimiron | Balance 385 | 
| 553 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | -120 | Loot purchased Gauntlets of the Wayward Vanquisher | Balance 123 | 
| 554 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 5 | Distributed points from Loot | Balance 548 | 
| 555 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 5 | Distributed points from Loot | Balance 431 | 
| 556 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 5 | Distributed points from Loot | Balance 599 | 
| 557 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 5 | Distributed points from Loot | Balance 415 | 
| 558 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 5 | Distributed points from Loot | Balance 290 | 
| 559 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 5 | Distributed points from Loot | Balance 295 | 
| 560 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 5 | Distributed points from Loot | Balance 205 | 
| 561 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 5 | Distributed points from Loot | Balance 128 | 
| 562 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 5 | Distributed points from Loot | Balance 414 | 
| 563 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 5 | Distributed points from Loot | Balance 328 | 
| 564 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 5 | Distributed points from Loot | Balance 608 | 
| 565 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 5 | Distributed points from Loot | Balance 669 | 
| 566 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 5 | Distributed points from Loot | Balance 394 | 
| 567 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 5 | Distributed points from Loot | Balance 369 | 
| 568 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 5 | Distributed points from Loot | Balance 465 | 
| 569 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 5 | Distributed points from Loot | Balance 1218 | 
| 570 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 5 | Distributed points from Loot | Balance 338 | 
| 571 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 5 | Distributed points from Loot | Balance 418 | 
| 572 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 5 | Distributed points from Loot | Balance 683 | 
| 573 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 5 | Distributed points from Loot | Balance 586 | 
| 574 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 5 | Distributed points from Loot | Balance 275 | 
| 575 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 5 | Distributed points from Loot | Balance 373 | 
| 576 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 5 | Distributed points from Loot | Balance 1128 | 
| 577 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 5 | Distributed points from Loot | Balance 596 | 
| 578 | 2023-04-11 21:25:11 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 5 | Distributed points from Loot | Balance 390 | 
| 579 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | -184 | Loot purchased Gauntlets of the Wayward Conqueror | Balance 230 | 
| 580 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 5 | Distributed points from Loot | Balance 553 | 
| 581 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 5 | Distributed points from Loot | Balance 436 | 
| 582 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 5 | Distributed points from Loot | Balance 604 | 
| 583 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 5 | Distributed points from Loot | Balance 420 | 
| 584 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 5 | Distributed points from Loot | Balance 295 | 
| 585 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 5 | Distributed points from Loot | Balance 300 | 
| 586 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 5 | Distributed points from Loot | Balance 210 | 
| 587 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 5 | Distributed points from Loot | Balance 133 | 
| 588 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 5 | Distributed points from Loot | Balance 235 | 
| 589 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 5 | Distributed points from Loot | Balance 333 | 
| 590 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 5 | Distributed points from Loot | Balance 613 | 
| 591 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 5 | Distributed points from Loot | Balance 674 | 
| 592 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 5 | Distributed points from Loot | Balance 399 | 
| 593 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 5 | Distributed points from Loot | Balance 374 | 
| 594 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 5 | Distributed points from Loot | Balance 470 | 
| 595 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 5 | Distributed points from Loot | Balance 1223 | 
| 596 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 5 | Distributed points from Loot | Balance 343 | 
| 597 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 5 | Distributed points from Loot | Balance 423 | 
| 598 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 5 | Distributed points from Loot | Balance 688 | 
| 599 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 5 | Distributed points from Loot | Balance 591 | 
| 600 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 5 | Distributed points from Loot | Balance 280 | 
| 601 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 5 | Distributed points from Loot | Balance 378 | 
| 602 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 5 | Distributed points from Loot | Balance 1133 | 
| 603 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 5 | Distributed points from Loot | Balance 601 | 
| 604 | 2023-04-11 21:25:41 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 5 | Distributed points from Loot | Balance 395 | 
| 605 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 1 | Pull 755/General Vezax | Balance 554 | 
| 606 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 1 | Pull 755/General Vezax | Balance 437 | 
| 607 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 1 | Pull 755/General Vezax | Balance 605 | 
| 608 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 1 | Pull 755/General Vezax | Balance 421 | 
| 609 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 1 | Pull 755/General Vezax | Balance 296 | 
| 610 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 1 | Pull 755/General Vezax | Balance 301 | 
| 611 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 1 | Pull 755/General Vezax | Balance 211 | 
| 612 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 1 | Pull 755/General Vezax | Balance 134 | 
| 613 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 1 | Pull 755/General Vezax | Balance 236 | 
| 614 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 1 | Pull 755/General Vezax | Balance 334 | 
| 615 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 1 | Pull 755/General Vezax | Balance 614 | 
| 616 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 1 | Pull 755/General Vezax | Balance 675 | 
| 617 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 1 | Pull 755/General Vezax | Balance 400 | 
| 618 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 1 | Pull 755/General Vezax | Balance 375 | 
| 619 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 1 | Pull 755/General Vezax | Balance 471 | 
| 620 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 1 | Pull 755/General Vezax | Balance 1224 | 
| 621 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 1 | Pull 755/General Vezax | Balance 344 | 
| 622 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 1 | Pull 755/General Vezax | Balance 424 | 
| 623 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 1 | Pull 755/General Vezax | Balance 689 | 
| 624 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 1 | Pull 755/General Vezax | Balance 592 | 
| 625 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 1 | Pull 755/General Vezax | Balance 281 | 
| 626 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 1 | Pull 755/General Vezax | Balance 379 | 
| 627 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 1 | Pull 755/General Vezax | Balance 1134 | 
| 628 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 1 | Pull 755/General Vezax | Balance 602 | 
| 629 | 2023-04-11 21:42:20 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 1 | Pull 755/General Vezax | Balance 396 | 
| 630 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 2 | Kill 755/General Vezax | Balance 556 | 
| 631 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 2 | Kill 755/General Vezax | Balance 439 | 
| 632 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 2 | Kill 755/General Vezax | Balance 607 | 
| 633 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 2 | Kill 755/General Vezax | Balance 423 | 
| 634 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 2 | Kill 755/General Vezax | Balance 298 | 
| 635 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 2 | Kill 755/General Vezax | Balance 303 | 
| 636 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 2 | Kill 755/General Vezax | Balance 213 | 
| 637 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 2 | Kill 755/General Vezax | Balance 136 | 
| 638 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 2 | Kill 755/General Vezax | Balance 238 | 
| 639 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 2 | Kill 755/General Vezax | Balance 336 | 
| 640 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 2 | Kill 755/General Vezax | Balance 616 | 
| 641 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 2 | Kill 755/General Vezax | Balance 677 | 
| 642 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 2 | Kill 755/General Vezax | Balance 402 | 
| 643 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 2 | Kill 755/General Vezax | Balance 377 | 
| 644 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 2 | Kill 755/General Vezax | Balance 473 | 
| 645 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 2 | Kill 755/General Vezax | Balance 1226 | 
| 646 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 2 | Kill 755/General Vezax | Balance 346 | 
| 647 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 2 | Kill 755/General Vezax | Balance 426 | 
| 648 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 2 | Kill 755/General Vezax | Balance 691 | 
| 649 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 2 | Kill 755/General Vezax | Balance 594 | 
| 650 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 2 | Kill 755/General Vezax | Balance 283 | 
| 651 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 2 | Kill 755/General Vezax | Balance 381 | 
| 652 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 2 | Kill 755/General Vezax | Balance 1136 | 
| 653 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 2 | Kill 755/General Vezax | Balance 604 | 
| 654 | 2023-04-11 21:47:41 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 2 | Kill 755/General Vezax | Balance 398 | 
| 655 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | -100 | Loot purchased Ring of the Vacant Eye | Balance 138 | 
| 656 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 4 | Distributed points from Loot | Balance 560 | 
| 657 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 4 | Distributed points from Loot | Balance 443 | 
| 658 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 4 | Distributed points from Loot | Balance 611 | 
| 659 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 4 | Distributed points from Loot | Balance 427 | 
| 660 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 4 | Distributed points from Loot | Balance 302 | 
| 661 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 4 | Distributed points from Loot | Balance 307 | 
| 662 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 4 | Distributed points from Loot | Balance 217 | 
| 663 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 4 | Distributed points from Loot | Balance 140 | 
| 664 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 4 | Distributed points from Loot | Balance 142 | 
| 665 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 4 | Distributed points from Loot | Balance 340 | 
| 666 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 4 | Distributed points from Loot | Balance 620 | 
| 667 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 4 | Distributed points from Loot | Balance 681 | 
| 668 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 4 | Distributed points from Loot | Balance 406 | 
| 669 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 4 | Distributed points from Loot | Balance 381 | 
| 670 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 4 | Distributed points from Loot | Balance 477 | 
| 671 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 4 | Distributed points from Loot | Balance 1230 | 
| 672 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 4 | Distributed points from Loot | Balance 350 | 
| 673 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 4 | Distributed points from Loot | Balance 430 | 
| 674 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 4 | Distributed points from Loot | Balance 695 | 
| 675 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 4 | Distributed points from Loot | Balance 598 | 
| 676 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 4 | Distributed points from Loot | Balance 287 | 
| 677 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 4 | Distributed points from Loot | Balance 385 | 
| 678 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 4 | Distributed points from Loot | Balance 1140 | 
| 679 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 4 | Distributed points from Loot | Balance 608 | 
| 680 | 2023-04-11 21:48:41 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 4 | Distributed points from Loot | Balance 402 | 
| 681 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 1 | Pull 756/Yogg-Saron | Balance 561 | 
| 682 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 1 | Pull 756/Yogg-Saron | Balance 444 | 
| 683 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 1 | Pull 756/Yogg-Saron | Balance 612 | 
| 684 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 1 | Pull 756/Yogg-Saron | Balance 428 | 
| 685 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 1 | Pull 756/Yogg-Saron | Balance 303 | 
| 686 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 1 | Pull 756/Yogg-Saron | Balance 308 | 
| 687 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 1 | Pull 756/Yogg-Saron | Balance 218 | 
| 688 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 1 | Pull 756/Yogg-Saron | Balance 141 | 
| 689 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 1 | Pull 756/Yogg-Saron | Balance 143 | 
| 690 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 1 | Pull 756/Yogg-Saron | Balance 341 | 
| 691 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 1 | Pull 756/Yogg-Saron | Balance 621 | 
| 692 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 1 | Pull 756/Yogg-Saron | Balance 682 | 
| 693 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 1 | Pull 756/Yogg-Saron | Balance 407 | 
| 694 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 1 | Pull 756/Yogg-Saron | Balance 382 | 
| 695 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 1 | Pull 756/Yogg-Saron | Balance 478 | 
| 696 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 1 | Pull 756/Yogg-Saron | Balance 1231 | 
| 697 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 1 | Pull 756/Yogg-Saron | Balance 351 | 
| 698 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 1 | Pull 756/Yogg-Saron | Balance 431 | 
| 699 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 1 | Pull 756/Yogg-Saron | Balance 696 | 
| 700 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 1 | Pull 756/Yogg-Saron | Balance 599 | 
| 701 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 1 | Pull 756/Yogg-Saron | Balance 288 | 
| 702 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 1 | Pull 756/Yogg-Saron | Balance 386 | 
| 703 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 1 | Pull 756/Yogg-Saron | Balance 1141 | 
| 704 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 1 | Pull 756/Yogg-Saron | Balance 609 | 
| 705 | 2023-04-11 21:51:17 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 1 | Pull 756/Yogg-Saron | Balance 403 | 
| 706 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 2 | Kill 756/Yogg-Saron | Balance 563 | 
| 707 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 2 | Kill 756/Yogg-Saron | Balance 446 | 
| 708 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 2 | Kill 756/Yogg-Saron | Balance 614 | 
| 709 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 2 | Kill 756/Yogg-Saron | Balance 430 | 
| 710 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 2 | Kill 756/Yogg-Saron | Balance 305 | 
| 711 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 2 | Kill 756/Yogg-Saron | Balance 310 | 
| 712 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 2 | Kill 756/Yogg-Saron | Balance 220 | 
| 713 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 2 | Kill 756/Yogg-Saron | Balance 143 | 
| 714 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 2 | Kill 756/Yogg-Saron | Balance 145 | 
| 715 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 2 | Kill 756/Yogg-Saron | Balance 343 | 
| 716 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 2 | Kill 756/Yogg-Saron | Balance 623 | 
| 717 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 2 | Kill 756/Yogg-Saron | Balance 684 | 
| 718 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 2 | Kill 756/Yogg-Saron | Balance 409 | 
| 719 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 2 | Kill 756/Yogg-Saron | Balance 384 | 
| 720 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 2 | Kill 756/Yogg-Saron | Balance 480 | 
| 721 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 2 | Kill 756/Yogg-Saron | Balance 1233 | 
| 722 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 2 | Kill 756/Yogg-Saron | Balance 353 | 
| 723 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 2 | Kill 756/Yogg-Saron | Balance 433 | 
| 724 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 2 | Kill 756/Yogg-Saron | Balance 698 | 
| 725 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 2 | Kill 756/Yogg-Saron | Balance 601 | 
| 726 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 2 | Kill 756/Yogg-Saron | Balance 290 | 
| 727 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 2 | Kill 756/Yogg-Saron | Balance 388 | 
| 728 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 2 | Kill 756/Yogg-Saron | Balance 1143 | 
| 729 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 2 | Kill 756/Yogg-Saron | Balance 611 | 
| 730 | 2023-04-11 22:04:31 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 2 | Kill 756/Yogg-Saron | Balance 405 | 
| 731 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | -183 | Loot purchased Blood of the Old God | Balance 250 | 
| 732 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 4 | Distributed points from Loot | Balance 567 | 
| 733 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 4 | Distributed points from Loot | Balance 450 | 
| 734 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 4 | Distributed points from Loot | Balance 618 | 
| 735 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 4 | Distributed points from Loot | Balance 434 | 
| 736 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 4 | Distributed points from Loot | Balance 309 | 
| 737 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 4 | Distributed points from Loot | Balance 314 | 
| 738 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 4 | Distributed points from Loot | Balance 224 | 
| 739 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 4 | Distributed points from Loot | Balance 147 | 
| 740 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 4 | Distributed points from Loot | Balance 149 | 
| 741 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 4 | Distributed points from Loot | Balance 347 | 
| 742 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 4 | Distributed points from Loot | Balance 627 | 
| 743 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 4 | Distributed points from Loot | Balance 688 | 
| 744 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 4 | Distributed points from Loot | Balance 413 | 
| 745 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 4 | Distributed points from Loot | Balance 388 | 
| 746 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 4 | Distributed points from Loot | Balance 484 | 
| 747 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 4 | Distributed points from Loot | Balance 1237 | 
| 748 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 4 | Distributed points from Loot | Balance 357 | 
| 749 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 4 | Distributed points from Loot | Balance 254 | 
| 750 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 4 | Distributed points from Loot | Balance 702 | 
| 751 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 4 | Distributed points from Loot | Balance 605 | 
| 752 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 4 | Distributed points from Loot | Balance 294 | 
| 753 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 4 | Distributed points from Loot | Balance 392 | 
| 754 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 4 | Distributed points from Loot | Balance 1147 | 
| 755 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 4 | Distributed points from Loot | Balance 615 | 
| 756 | 2023-04-11 22:05:24 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 4 | Distributed points from Loot | Balance 409 | 
| 757 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | -375 | Loot purchased Mantle of the Wayward Conqueror | Balance 230 | 
| 758 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 5 | Distributed points from Loot | Balance 572 | 
| 759 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 5 | Distributed points from Loot | Balance 455 | 
| 760 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 5 | Distributed points from Loot | Balance 623 | 
| 761 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 5 | Distributed points from Loot | Balance 439 | 
| 762 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 5 | Distributed points from Loot | Balance 314 | 
| 763 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 5 | Distributed points from Loot | Balance 319 | 
| 764 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 5 | Distributed points from Loot | Balance 229 | 
| 765 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 5 | Distributed points from Loot | Balance 152 | 
| 766 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 5 | Distributed points from Loot | Balance 154 | 
| 767 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 5 | Distributed points from Loot | Balance 352 | 
| 768 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 5 | Distributed points from Loot | Balance 632 | 
| 769 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 5 | Distributed points from Loot | Balance 693 | 
| 770 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 5 | Distributed points from Loot | Balance 418 | 
| 771 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 5 | Distributed points from Loot | Balance 393 | 
| 772 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 5 | Distributed points from Loot | Balance 489 | 
| 773 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 5 | Distributed points from Loot | Balance 1242 | 
| 774 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 5 | Distributed points from Loot | Balance 362 | 
| 775 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 5 | Distributed points from Loot | Balance 259 | 
| 776 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 5 | Distributed points from Loot | Balance 707 | 
| 777 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 5 | Distributed points from Loot | Balance 235 | 
| 778 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 5 | Distributed points from Loot | Balance 299 | 
| 779 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 5 | Distributed points from Loot | Balance 397 | 
| 780 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 5 | Distributed points from Loot | Balance 1152 | 
| 781 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 5 | Distributed points from Loot | Balance 620 | 
| 782 | 2023-04-11 22:05:53 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 5 | Distributed points from Loot | Balance 414 | 
| 783 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | -132 | Loot purchased Mantle of the Wayward Conqueror | Balance 230 | 
| 784 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 5 | Distributed points from Loot | Balance 577 | 
| 785 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 5 | Distributed points from Loot | Balance 460 | 
| 786 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 5 | Distributed points from Loot | Balance 628 | 
| 787 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 5 | Distributed points from Loot | Balance 444 | 
| 788 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 5 | Distributed points from Loot | Balance 319 | 
| 789 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 5 | Distributed points from Loot | Balance 324 | 
| 790 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 5 | Distributed points from Loot | Balance 234 | 
| 791 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 5 | Distributed points from Loot | Balance 157 | 
| 792 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 5 | Distributed points from Loot | Balance 159 | 
| 793 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 5 | Distributed points from Loot | Balance 357 | 
| 794 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 5 | Distributed points from Loot | Balance 637 | 
| 795 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 5 | Distributed points from Loot | Balance 698 | 
| 796 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 5 | Distributed points from Loot | Balance 423 | 
| 797 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 5 | Distributed points from Loot | Balance 398 | 
| 798 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 5 | Distributed points from Loot | Balance 494 | 
| 799 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 5 | Distributed points from Loot | Balance 1247 | 
| 800 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 5 | Distributed points from Loot | Balance 235 | 
| 801 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 5 | Distributed points from Loot | Balance 264 | 
| 802 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 5 | Distributed points from Loot | Balance 712 | 
| 803 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 5 | Distributed points from Loot | Balance 240 | 
| 804 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 5 | Distributed points from Loot | Balance 304 | 
| 805 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 5 | Distributed points from Loot | Balance 402 | 
| 806 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 5 | Distributed points from Loot | Balance 1157 | 
| 807 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 5 | Distributed points from Loot | Balance 625 | 
| 808 | 2023-04-11 22:06:22 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 5 | Distributed points from Loot | Balance 419 | 
| 809 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | -462 | Loot purchased Sanity's Bond | Balance 250 | 
| 810 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 4 | Distributed points from Loot | Balance 581 | 
| 811 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 4 | Distributed points from Loot | Balance 464 | 
| 812 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 4 | Distributed points from Loot | Balance 632 | 
| 813 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 4 | Distributed points from Loot | Balance 448 | 
| 814 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 4 | Distributed points from Loot | Balance 323 | 
| 815 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 4 | Distributed points from Loot | Balance 328 | 
| 816 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 4 | Distributed points from Loot | Balance 238 | 
| 817 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 4 | Distributed points from Loot | Balance 161 | 
| 818 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 4 | Distributed points from Loot | Balance 163 | 
| 819 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 4 | Distributed points from Loot | Balance 361 | 
| 820 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 4 | Distributed points from Loot | Balance 641 | 
| 821 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 4 | Distributed points from Loot | Balance 702 | 
| 822 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 4 | Distributed points from Loot | Balance 427 | 
| 823 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 4 | Distributed points from Loot | Balance 402 | 
| 824 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 4 | Distributed points from Loot | Balance 498 | 
| 825 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 4 | Distributed points from Loot | Balance 1251 | 
| 826 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 4 | Distributed points from Loot | Balance 239 | 
| 827 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 4 | Distributed points from Loot | Balance 268 | 
| 828 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 4 | Distributed points from Loot | Balance 254 | 
| 829 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 4 | Distributed points from Loot | Balance 244 | 
| 830 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 4 | Distributed points from Loot | Balance 308 | 
| 831 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 4 | Distributed points from Loot | Balance 406 | 
| 832 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 4 | Distributed points from Loot | Balance 1161 | 
| 833 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 4 | Distributed points from Loot | Balance 629 | 
| 834 | 2023-04-11 22:06:53 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 4 | Distributed points from Loot | Balance 423 | 
| 835 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 1 | Pull 748/Assembly of Iron | Balance 582 | 
| 836 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 1 | Pull 748/Assembly of Iron | Balance 465 | 
| 837 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 1 | Pull 748/Assembly of Iron | Balance 633 | 
| 838 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 1 | Pull 748/Assembly of Iron | Balance 449 | 
| 839 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 1 | Pull 748/Assembly of Iron | Balance 324 | 
| 840 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 1 | Pull 748/Assembly of Iron | Balance 329 | 
| 841 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 1 | Pull 748/Assembly of Iron | Balance 239 | 
| 842 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 1 | Pull 748/Assembly of Iron | Balance 162 | 
| 843 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 1 | Pull 748/Assembly of Iron | Balance 164 | 
| 844 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 1 | Pull 748/Assembly of Iron | Balance 362 | 
| 845 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 1 | Pull 748/Assembly of Iron | Balance 642 | 
| 846 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 1 | Pull 748/Assembly of Iron | Balance 703 | 
| 847 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 1 | Pull 748/Assembly of Iron | Balance 428 | 
| 848 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 1 | Pull 748/Assembly of Iron | Balance 403 | 
| 849 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 1 | Pull 748/Assembly of Iron | Balance 499 | 
| 850 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 1 | Pull 748/Assembly of Iron | Balance 1252 | 
| 851 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 1 | Pull 748/Assembly of Iron | Balance 240 | 
| 852 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 1 | Pull 748/Assembly of Iron | Balance 269 | 
| 853 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 1 | Pull 748/Assembly of Iron | Balance 255 | 
| 854 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 1 | Pull 748/Assembly of Iron | Balance 245 | 
| 855 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 1 | Pull 748/Assembly of Iron | Balance 309 | 
| 856 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 1 | Pull 748/Assembly of Iron | Balance 407 | 
| 857 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 1 | Pull 748/Assembly of Iron | Balance 1162 | 
| 858 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 1 | Pull 748/Assembly of Iron | Balance 630 | 
| 859 | 2023-04-11 22:08:27 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 1 | Pull 748/Assembly of Iron | Balance 424 | 
| 860 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 2 | Kill 748/Assembly of Iron | Balance 584 | 
| 861 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 2 | Kill 748/Assembly of Iron | Balance 467 | 
| 862 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 2 | Kill 748/Assembly of Iron | Balance 635 | 
| 863 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 2 | Kill 748/Assembly of Iron | Balance 451 | 
| 864 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 2 | Kill 748/Assembly of Iron | Balance 326 | 
| 865 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 2 | Kill 748/Assembly of Iron | Balance 331 | 
| 866 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 2 | Kill 748/Assembly of Iron | Balance 241 | 
| 867 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 2 | Kill 748/Assembly of Iron | Balance 164 | 
| 868 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 2 | Kill 748/Assembly of Iron | Balance 166 | 
| 869 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 2 | Kill 748/Assembly of Iron | Balance 364 | 
| 870 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 2 | Kill 748/Assembly of Iron | Balance 644 | 
| 871 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 2 | Kill 748/Assembly of Iron | Balance 705 | 
| 872 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 2 | Kill 748/Assembly of Iron | Balance 430 | 
| 873 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 2 | Kill 748/Assembly of Iron | Balance 405 | 
| 874 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 2 | Kill 748/Assembly of Iron | Balance 501 | 
| 875 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 2 | Kill 748/Assembly of Iron | Balance 1254 | 
| 876 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 2 | Kill 748/Assembly of Iron | Balance 242 | 
| 877 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 2 | Kill 748/Assembly of Iron | Balance 271 | 
| 878 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 2 | Kill 748/Assembly of Iron | Balance 257 | 
| 879 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 2 | Kill 748/Assembly of Iron | Balance 247 | 
| 880 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 2 | Kill 748/Assembly of Iron | Balance 311 | 
| 881 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 2 | Kill 748/Assembly of Iron | Balance 409 | 
| 882 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 2 | Kill 748/Assembly of Iron | Balance 1164 | 
| 883 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 2 | Kill 748/Assembly of Iron | Balance 632 | 
| 884 | 2023-04-11 22:13:24 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 2 | Kill 748/Assembly of Iron | Balance 426 | 
| 885 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | -159 | Loot purchased Insurmountable Fervor | Balance 250 | 
| 886 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 4 | Distributed points from Loot | Balance 588 | 
| 887 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 4 | Distributed points from Loot | Balance 471 | 
| 888 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 4 | Distributed points from Loot | Balance 639 | 
| 889 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 4 | Distributed points from Loot | Balance 455 | 
| 890 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 4 | Distributed points from Loot | Balance 330 | 
| 891 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 4 | Distributed points from Loot | Balance 335 | 
| 892 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 4 | Distributed points from Loot | Balance 245 | 
| 893 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 4 | Distributed points from Loot | Balance 168 | 
| 894 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 4 | Distributed points from Loot | Balance 170 | 
| 895 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 4 | Distributed points from Loot | Balance 368 | 
| 896 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 4 | Distributed points from Loot | Balance 648 | 
| 897 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 4 | Distributed points from Loot | Balance 709 | 
| 898 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 4 | Distributed points from Loot | Balance 434 | 
| 899 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 4 | Distributed points from Loot | Balance 409 | 
| 900 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 4 | Distributed points from Loot | Balance 505 | 
| 901 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 4 | Distributed points from Loot | Balance 1258 | 
| 902 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 4 | Distributed points from Loot | Balance 246 | 
| 903 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 4 | Distributed points from Loot | Balance 275 | 
| 904 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 4 | Distributed points from Loot | Balance 261 | 
| 905 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 4 | Distributed points from Loot | Balance 251 | 
| 906 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 4 | Distributed points from Loot | Balance 315 | 
| 907 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 4 | Distributed points from Loot | Balance 254 | 
| 908 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 4 | Distributed points from Loot | Balance 1168 | 
| 909 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 4 | Distributed points from Loot | Balance 636 | 
| 910 | 2023-04-11 22:14:17 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 4 | Distributed points from Loot | Balance 430 | 
| 911 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 1 | Pull 745/Ignis the Furnace Master | Balance 589 | 
| 912 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 1 | Pull 745/Ignis the Furnace Master | Balance 472 | 
| 913 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 1 | Pull 745/Ignis the Furnace Master | Balance 640 | 
| 914 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 1 | Pull 745/Ignis the Furnace Master | Balance 456 | 
| 915 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 1 | Pull 745/Ignis the Furnace Master | Balance 331 | 
| 916 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 1 | Pull 745/Ignis the Furnace Master | Balance 336 | 
| 917 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 1 | Pull 745/Ignis the Furnace Master | Balance 246 | 
| 918 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 1 | Pull 745/Ignis the Furnace Master | Balance 169 | 
| 919 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 1 | Pull 745/Ignis the Furnace Master | Balance 171 | 
| 920 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 1 | Pull 745/Ignis the Furnace Master | Balance 369 | 
| 921 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 1 | Pull 745/Ignis the Furnace Master | Balance 649 | 
| 922 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 1 | Pull 745/Ignis the Furnace Master | Balance 710 | 
| 923 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 1 | Pull 745/Ignis the Furnace Master | Balance 435 | 
| 924 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 1 | Pull 745/Ignis the Furnace Master | Balance 410 | 
| 925 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 1 | Pull 745/Ignis the Furnace Master | Balance 506 | 
| 926 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 1 | Pull 745/Ignis the Furnace Master | Balance 1259 | 
| 927 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 1 | Pull 745/Ignis the Furnace Master | Balance 247 | 
| 928 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 1 | Pull 745/Ignis the Furnace Master | Balance 276 | 
| 929 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 1 | Pull 745/Ignis the Furnace Master | Balance 262 | 
| 930 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 1 | Pull 745/Ignis the Furnace Master | Balance 252 | 
| 931 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 1 | Pull 745/Ignis the Furnace Master | Balance 316 | 
| 932 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 1 | Pull 745/Ignis the Furnace Master | Balance 255 | 
| 933 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 1 | Pull 745/Ignis the Furnace Master | Balance 1169 | 
| 934 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 1 | Pull 745/Ignis the Furnace Master | Balance 637 | 
| 935 | 2023-04-11 22:22:10 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 1 | Pull 745/Ignis the Furnace Master | Balance 431 | 
| 936 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 2 | Kill 745/Ignis the Furnace Master | Balance 591 | 
| 937 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 2 | Kill 745/Ignis the Furnace Master | Balance 474 | 
| 938 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 2 | Kill 745/Ignis the Furnace Master | Balance 642 | 
| 939 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 2 | Kill 745/Ignis the Furnace Master | Balance 458 | 
| 940 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 2 | Kill 745/Ignis the Furnace Master | Balance 333 | 
| 941 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 2 | Kill 745/Ignis the Furnace Master | Balance 338 | 
| 942 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 2 | Kill 745/Ignis the Furnace Master | Balance 248 | 
| 943 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 2 | Kill 745/Ignis the Furnace Master | Balance 171 | 
| 944 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 2 | Kill 745/Ignis the Furnace Master | Balance 173 | 
| 945 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 2 | Kill 745/Ignis the Furnace Master | Balance 371 | 
| 946 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 2 | Kill 745/Ignis the Furnace Master | Balance 651 | 
| 947 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 2 | Kill 745/Ignis the Furnace Master | Balance 712 | 
| 948 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 2 | Kill 745/Ignis the Furnace Master | Balance 437 | 
| 949 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 2 | Kill 745/Ignis the Furnace Master | Balance 412 | 
| 950 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 2 | Kill 745/Ignis the Furnace Master | Balance 508 | 
| 951 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 2 | Kill 745/Ignis the Furnace Master | Balance 1261 | 
| 952 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 2 | Kill 745/Ignis the Furnace Master | Balance 249 | 
| 953 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 2 | Kill 745/Ignis the Furnace Master | Balance 278 | 
| 954 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 2 | Kill 745/Ignis the Furnace Master | Balance 264 | 
| 955 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 2 | Kill 745/Ignis the Furnace Master | Balance 254 | 
| 956 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 2 | Kill 745/Ignis the Furnace Master | Balance 318 | 
| 957 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 2 | Kill 745/Ignis the Furnace Master | Balance 257 | 
| 958 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 2 | Kill 745/Ignis the Furnace Master | Balance 1171 | 
| 959 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 2 | Kill 745/Ignis the Furnace Master | Balance 639 | 
| 960 | 2023-04-11 22:25:39 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 2 | Kill 745/Ignis the Furnace Master | Balance 433 | 
| 961 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 1 | Pull 746/Razorscale | Balance 592 | 
| 962 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 1 | Pull 746/Razorscale | Balance 475 | 
| 963 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 1 | Pull 746/Razorscale | Balance 643 | 
| 964 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 1 | Pull 746/Razorscale | Balance 459 | 
| 965 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 1 | Pull 746/Razorscale | Balance 334 | 
| 966 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 1 | Pull 746/Razorscale | Balance 339 | 
| 967 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 1 | Pull 746/Razorscale | Balance 249 | 
| 968 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 1 | Pull 746/Razorscale | Balance 172 | 
| 969 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 1 | Pull 746/Razorscale | Balance 174 | 
| 970 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 1 | Pull 746/Razorscale | Balance 372 | 
| 971 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 1 | Pull 746/Razorscale | Balance 652 | 
| 972 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 1 | Pull 746/Razorscale | Balance 713 | 
| 973 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 1 | Pull 746/Razorscale | Balance 438 | 
| 974 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 1 | Pull 746/Razorscale | Balance 413 | 
| 975 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 1 | Pull 746/Razorscale | Balance 509 | 
| 976 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 1 | Pull 746/Razorscale | Balance 1262 | 
| 977 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 1 | Pull 746/Razorscale | Balance 250 | 
| 978 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 1 | Pull 746/Razorscale | Balance 279 | 
| 979 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 1 | Pull 746/Razorscale | Balance 265 | 
| 980 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 1 | Pull 746/Razorscale | Balance 255 | 
| 981 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 1 | Pull 746/Razorscale | Balance 319 | 
| 982 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 1 | Pull 746/Razorscale | Balance 258 | 
| 983 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 1 | Pull 746/Razorscale | Balance 1172 | 
| 984 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 1 | Pull 746/Razorscale | Balance 640 | 
| 985 | 2023-04-11 22:26:54 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 1 | Pull 746/Razorscale | Balance 434 | 
| 986 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 2 | Kill 746/Razorscale | Balance 594 | 
| 987 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 2 | Kill 746/Razorscale | Balance 477 | 
| 988 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 2 | Kill 746/Razorscale | Balance 645 | 
| 989 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 2 | Kill 746/Razorscale | Balance 461 | 
| 990 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 2 | Kill 746/Razorscale | Balance 336 | 
| 991 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 2 | Kill 746/Razorscale | Balance 341 | 
| 992 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 2 | Kill 746/Razorscale | Balance 251 | 
| 993 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 2 | Kill 746/Razorscale | Balance 174 | 
| 994 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 2 | Kill 746/Razorscale | Balance 176 | 
| 995 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 2 | Kill 746/Razorscale | Balance 374 | 
| 996 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 2 | Kill 746/Razorscale | Balance 654 | 
| 997 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 2 | Kill 746/Razorscale | Balance 715 | 
| 998 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 2 | Kill 746/Razorscale | Balance 440 | 
| 999 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 2 | Kill 746/Razorscale | Balance 415 | 
| 1000 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 2 | Kill 746/Razorscale | Balance 511 | 
| 1001 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 2 | Kill 746/Razorscale | Balance 1264 | 
| 1002 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 2 | Kill 746/Razorscale | Balance 252 | 
| 1003 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 2 | Kill 746/Razorscale | Balance 281 | 
| 1004 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 2 | Kill 746/Razorscale | Balance 267 | 
| 1005 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 2 | Kill 746/Razorscale | Balance 257 | 
| 1006 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 2 | Kill 746/Razorscale | Balance 321 | 
| 1007 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 2 | Kill 746/Razorscale | Balance 260 | 
| 1008 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 2 | Kill 746/Razorscale | Balance 1174 | 
| 1009 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 2 | Kill 746/Razorscale | Balance 642 | 
| 1010 | 2023-04-11 22:30:37 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 2 | Kill 746/Razorscale | Balance 436 | 
| 1011 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | -90 | Loot purchased Bracers of the Broodmother | Balance 177 | 
| 1012 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 4 | Distributed points from Loot | Balance 598 | 
| 1013 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 4 | Distributed points from Loot | Balance 481 | 
| 1014 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 4 | Distributed points from Loot | Balance 649 | 
| 1015 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 4 | Distributed points from Loot | Balance 465 | 
| 1016 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 4 | Distributed points from Loot | Balance 340 | 
| 1017 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 4 | Distributed points from Loot | Balance 345 | 
| 1018 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 4 | Distributed points from Loot | Balance 255 | 
| 1019 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 4 | Distributed points from Loot | Balance 178 | 
| 1020 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 4 | Distributed points from Loot | Balance 180 | 
| 1021 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 4 | Distributed points from Loot | Balance 378 | 
| 1022 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 4 | Distributed points from Loot | Balance 658 | 
| 1023 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 4 | Distributed points from Loot | Balance 719 | 
| 1024 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 4 | Distributed points from Loot | Balance 444 | 
| 1025 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 4 | Distributed points from Loot | Balance 419 | 
| 1026 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 4 | Distributed points from Loot | Balance 515 | 
| 1027 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 4 | Distributed points from Loot | Balance 1268 | 
| 1028 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 4 | Distributed points from Loot | Balance 256 | 
| 1029 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 4 | Distributed points from Loot | Balance 285 | 
| 1030 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 4 | Distributed points from Loot | Balance 181 | 
| 1031 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 4 | Distributed points from Loot | Balance 261 | 
| 1032 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 4 | Distributed points from Loot | Balance 325 | 
| 1033 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 4 | Distributed points from Loot | Balance 264 | 
| 1034 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 4 | Distributed points from Loot | Balance 1178 | 
| 1035 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 4 | Distributed points from Loot | Balance 646 | 
| 1036 | 2023-04-11 22:31:25 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 4 | Distributed points from Loot | Balance 440 | 
| 1037 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | -265 | Loot purchased Pyrelight Circle | Balance 250 | 
| 1038 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Aythya | 4 | Distributed points from Loot | Balance 602 | 
| 1039 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Bravend | 4 | Distributed points from Loot | Balance 485 | 
| 1040 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Darkhourz | 4 | Distributed points from Loot | Balance 653 | 
| 1041 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Derios | 4 | Distributed points from Loot | Balance 469 | 
| 1042 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Dunsmar | 4 | Distributed points from Loot | Balance 344 | 
| 1043 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Dvoom | 4 | Distributed points from Loot | Balance 349 | 
| 1044 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Eillowee | 4 | Distributed points from Loot | Balance 259 | 
| 1045 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Gaylestrum | 4 | Distributed points from Loot | Balance 182 | 
| 1046 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Hempoil | 4 | Distributed points from Loot | Balance 184 | 
| 1047 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Ironflurry | 4 | Distributed points from Loot | Balance 382 | 
| 1048 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Jazzmene | 4 | Distributed points from Loot | Balance 662 | 
| 1049 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Jáxshow | 4 | Distributed points from Loot | Balance 723 | 
| 1050 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Kekett | 4 | Distributed points from Loot | Balance 448 | 
| 1051 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Lachý | 4 | Distributed points from Loot | Balance 423 | 
| 1052 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Mayaell | 4 | Distributed points from Loot | Balance 254 | 
| 1053 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | 4 | Distributed points from Loot | Balance 1272 | 
| 1054 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Montecrié | 4 | Distributed points from Loot | Balance 260 | 
| 1055 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Mykos | 4 | Distributed points from Loot | Balance 289 | 
| 1056 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Ogdru | 4 | Distributed points from Loot | Balance 185 | 
| 1057 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Phal | 4 | Distributed points from Loot | Balance 265 | 
| 1058 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Rosemondon | 4 | Distributed points from Loot | Balance 329 | 
| 1059 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Ryzus | 4 | Distributed points from Loot | Balance 268 | 
| 1060 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Slayingfreak | 4 | Distributed points from Loot | Balance 1182 | 
| 1061 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Snoweyes | 4 | Distributed points from Loot | Balance 650 | 
| 1062 | 2023-04-11 22:32:50 | StoreDKP 2023-04-11_Ulduar_Tues | Soullina | 4 | Distributed points from Loot | Balance 444 | 
| 1063 | 2023-04-11 22:35:56 | StoreDKP 2023-04-11_Ulduar_Tues | Meyhém | -913 | Correction | Balance 359 | 
