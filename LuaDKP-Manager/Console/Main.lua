ADDON = {}

------------------------------------------------------------

local function CheckLocation()
  if ADDON.FileExists("../../../Interface/AddOns/nul") then
    print("Error: Manager seems to be installed in WoW's Interface/AddOns folder!")
    print()
    print("Please install Manager outside of the Interface/AddOns folder,")
    print("otherwise it may accidentally get overwritten by an updater.")
    print("Good locations are your Desktop or Documents folders.")
    print()
    print("Read the LuaDKP documentation if you want to know more...")
    os.exit(1)
  end
end

------------------------------------------------------------

local function CheckConfig()

  -- add trailing slash
  local basedir = ADDON.Config.basedir
  if string.sub(basedir, -1) ~= "/" then
    basedir = basedir .. "/"
    ADDON.Config.basedir = basedir
  end

  -- check basedir folder
  if not ADDON.FileExists(basedir .. "WTF/Account/nul") or not ADDON.FileExists(basedir .. "Interface/AddOns/nul") then
    print("Error: WoW basedir not found or incorrect!")
    print("Please set the correct value in Config.lua.")
    os.exit(1)
  end

  -- check Output folder
  if not ADDON.FileExists("Output/nul") then
    print("Error: Output folder missing! Please create it in LuaDKP's root directory.")
    os.exit(1)
  end
end

------------------------------------------------------------

local function CheckWoW()

  -- detect WoW
  local cnt = 0
  for line in io.popen('tasklist /FI "WINDOWTITLE eq WORLD OF WARCRAFT" 2> nul'):lines() do
    cnt = cnt + 1
  end

  -- if uncertain, ask user!
  if cnt == 0 then
    print("World of Warcraft must not be running while you import/export.")
    print("Please make sure the game is closed before you proceed.")
    print()
    if ADDON.Choice("Continue?", "yn") ~= "y" then
      return false
    end
  end

  -- WoW seems to be running!
  if cnt > 2 then
    print("World of Warcraft must not be running while you import/export.")
    print("Please close the game client first and then try again.")
    print()
    ADDON.Pause()
    return false
  end

  return true
end

------------------------------------------------------------

local function ShowAdvanced()

  repeat
    ADDON.Clear()
    print("================")
    print("=== Advanced ===")
    print("================")
    print()
    print("1. Translate Items")
    print("0. Back")
    print()
    local d = ADDON.Choice("Answer", "10")
    print()

    if d == "1" then
      ADDON.LoadGroups()
      ADDON.TranslateItems()
      ADDON.Groups = nil
    end

  until d == "0"
end

------------------------------------------------------------

local function ShowMain()
  CheckLocation()
  CheckConfig()

  repeat
    ADDON.Clear()
    print("============")
    print("=== Main ===")
    print("============")
    print()
    print("1. Import points/raids from WTF")
    print("2. Create HTML")
	print("3. Push to Gitlab")
    print()
    print("a. Push to AWS")
    print("b. Export Addon")
	print("c. Advanced")
    print()
	print("0. Exit")
    print()
    local d = ADDON.Choice("Answer", "123abc0")
    print()

    if d == "1" then
      if CheckWoW() then
        ADDON.ImportWTF()
      end
    end

	if d == "2" then
      ADDON.LoadGroups()
      ADDON.ExportHTML()
      ADDON.Groups = nil
    end

	if d == "3" then
	  os.execute("git add . && git commit -m \"weekly raid\" && git push")
	  ADDON.Pause()
    end


	if d == "a" then
	  local bucket_suffix = os.date("%Y-%m-%d_%H-%M-%S.md")
	  --os.execute("git status")
	  --os.execute("aws --version")
	  print("setting Access Key...")
	  --os.execute("set AWS_ACCESS_KEY_ID=xxxx")

	  print("setting Access Secret...")
	  --os.execute("set AWS_SECRET_ACCESS_KEY=xxxx")

	  print("setting AWS region...")
	  --os.execute("set AWS_DEFAULT_REGION=us-east-1")

	  print("uploading s3://solsticepoints/Solstice.html...")
	  os.execute("aws s3 cp Output/Solstice.html s3://solsticepoints/Solstice.html")

	  print("uploading s3://solsticepoints/dkp-backup-" .. bucket_suffix .. "...")
	  os.execute("aws s3 cp tempDKPLog/directToAWS.md s3://solsticepoints/dkp-backup-" .. bucket_suffix)

	  print("setting Solstice.html acl public-read...")
	  os.execute("aws s3api put-object-acl --bucket solsticepoints --key Solstice.html --acl public-read")
	  os.execute("cd tempDKPLog && del directToAWS.md")
	  print()
	  print("AWS successfully updated!")
	  ADDON.Pause()
    end

    if d == "b" then
      if CheckWoW() then
        ADDON.ExportAddon()
      end
    end

    if d == "c" then
      ShowAdvanced()
    end

  until d == "0"
end

------------------------------------------------------------

-- load tools
assert(loadfile("Console/Tools.lua"))("LuaDKP", ADDON)

-- load console
ADDON.LoadFile("Console/Evaluate.lua")
ADDON.LoadFile("Console/ImportWTF.lua")
ADDON.LoadFile("Console/ExportAddon.lua")
ADDON.LoadFile("Console/ExportHTML.lua")
ADDON.LoadFile("Console/TranslateItems.lua")
ADDON.LoadFile("Console/Menu.lua")

ADDON.LoadFile("Console/MasterDKP.lua")


-- load configuarion
ADDON.LoadFile("Config.lua")

-- load misc
ADDON.LoadFile("Misc/Definitions.lua")
ADDON.LoadFile("Misc/ItemTranslation-" .. ADDON.Config.expansion .. ".lua")

------------------------------------------------------------

-- show main menu
ShowMain()
