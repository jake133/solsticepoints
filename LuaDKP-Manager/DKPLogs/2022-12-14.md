| Timestamp | Player | Change | Description | New Balance |
|---|---|---|---|---|
|1 2022-12-14 19:17:39 StoreDKP | Aythya | 10 | Attendance | Balance 314|
|2 2022-12-14 19:17:39 StoreDKP | Bravend | 10 | Attendance | Balance 404|
|3 2022-12-14 19:17:39 StoreDKP | Eillowee | 10 | Attendance | Balance 215|
|4 2022-12-14 19:17:39 StoreDKP | Ironflurry | 10 | Attendance | Balance 565|
|5 2022-12-14 19:17:39 StoreDKP | Jazzmene | 10 | Attendance | Balance 288|
|6 2022-12-14 19:17:39 StoreDKP | Lachý | 10 | Attendance | Balance 372|
|7 2022-12-14 19:17:39 StoreDKP | Mayaell | 10 | Attendance | Balance 312|
|8 2022-12-14 19:17:39 StoreDKP | Meyhém | 10 | Attendance | Balance 291|
|9 2022-12-14 19:17:39 StoreDKP | Orcbane | 10 | Attendance | Balance 109|
|10 2022-12-14 19:17:39 StoreDKP | Phal | 10 | Attendance | Balance 257|
|11 2022-12-14 19:17:39 StoreDKP | Ryzus | 10 | Attendance | Balance 815|
|12 2022-12-14 19:17:39 StoreDKP | Snoweyes | 10 | Attendance | Balance 510|
|13 2022-12-14 19:17:39 StoreDKP | Wreckel | 10 | Attendance | Balance 469|
|14 2022-12-14 19:18:21 StoreDKP | Slayingfreak | 10 | Correction | Balance 233|
|15 2022-12-14 19:44:11 StoreDKP | Aythya | 1 | Pull 1118/Patchwerk | Balance 315|
|16 2022-12-14 19:44:11 StoreDKP | Bravend | 1 | Pull 1118/Patchwerk | Balance 405|
|17 2022-12-14 19:44:11 StoreDKP | Eillowee | 1 | Pull 1118/Patchwerk | Balance 216|
|18 2022-12-14 19:44:11 StoreDKP | Ironflurry | 1 | Pull 1118/Patchwerk | Balance 566|
|19 2022-12-14 19:44:11 StoreDKP | Lachý | 1 | Pull 1118/Patchwerk | Balance 373|
|20 2022-12-14 19:44:11 StoreDKP | Meyhém | 1 | Pull 1118/Patchwerk | Balance 292|
|21 2022-12-14 19:44:11 StoreDKP | Orcbane | 1 | Pull 1118/Patchwerk | Balance 110|
|22 2022-12-14 19:44:11 StoreDKP | Phal | 1 | Pull 1118/Patchwerk | Balance 258|
|23 2022-12-14 19:44:11 StoreDKP | Ryzus | 1 | Pull 1118/Patchwerk | Balance 816|
|24 2022-12-14 19:44:11 StoreDKP | Wreckel | 1 | Pull 1118/Patchwerk | Balance 470|
|25 2022-12-14 19:47:07 StoreDKP | Aythya | 2 | Kill 1118/Patchwerk | Balance 317|
|26 2022-12-14 19:47:07 StoreDKP | Bravend | 2 | Kill 1118/Patchwerk | Balance 407|
|27 2022-12-14 19:47:07 StoreDKP | Eillowee | 2 | Kill 1118/Patchwerk | Balance 218|
|28 2022-12-14 19:47:07 StoreDKP | Ironflurry | 2 | Kill 1118/Patchwerk | Balance 568|
|29 2022-12-14 19:47:07 StoreDKP | Lachý | 2 | Kill 1118/Patchwerk | Balance 375|
|30 2022-12-14 19:47:07 StoreDKP | Meyhém | 2 | Kill 1118/Patchwerk | Balance 294|
|31 2022-12-14 19:47:07 StoreDKP | Orcbane | 2 | Kill 1118/Patchwerk | Balance 112|
|32 2022-12-14 19:47:07 StoreDKP | Phal | 2 | Kill 1118/Patchwerk | Balance 260|
|33 2022-12-14 19:47:07 StoreDKP | Ryzus | 2 | Kill 1118/Patchwerk | Balance 818|
|34 2022-12-14 19:47:07 StoreDKP | Wreckel | 2 | Kill 1118/Patchwerk | Balance 472|
|35 2022-12-14 19:50:23 StoreDKP | Aythya | 1 | Pull 1111/Grobbulus | Balance 318|
|36 2022-12-14 19:50:23 StoreDKP | Bravend | 1 | Pull 1111/Grobbulus | Balance 408|
|37 2022-12-14 19:50:23 StoreDKP | Eillowee | 1 | Pull 1111/Grobbulus | Balance 219|
|38 2022-12-14 19:50:23 StoreDKP | Ironflurry | 1 | Pull 1111/Grobbulus | Balance 569|
|39 2022-12-14 19:50:23 StoreDKP | Lachý | 1 | Pull 1111/Grobbulus | Balance 376|
|40 2022-12-14 19:50:23 StoreDKP | Meyhém | 1 | Pull 1111/Grobbulus | Balance 295|
|41 2022-12-14 19:50:23 StoreDKP | Orcbane | 1 | Pull 1111/Grobbulus | Balance 113|
|42 2022-12-14 19:50:23 StoreDKP | Phal | 1 | Pull 1111/Grobbulus | Balance 261|
|43 2022-12-14 19:50:23 StoreDKP | Ryzus | 1 | Pull 1111/Grobbulus | Balance 819|
|44 2022-12-14 19:50:23 StoreDKP | Wreckel | 1 | Pull 1111/Grobbulus | Balance 473|
|45 2022-12-14 19:52:41 StoreDKP | Aythya | 2 | Kill 1111/Grobbulus | Balance 320|
|46 2022-12-14 19:52:41 StoreDKP | Bravend | 2 | Kill 1111/Grobbulus | Balance 410|
|47 2022-12-14 19:52:41 StoreDKP | Eillowee | 2 | Kill 1111/Grobbulus | Balance 221|
|48 2022-12-14 19:52:41 StoreDKP | Ironflurry | 2 | Kill 1111/Grobbulus | Balance 571|
|49 2022-12-14 19:52:41 StoreDKP | Lachý | 2 | Kill 1111/Grobbulus | Balance 378|
|50 2022-12-14 19:52:41 StoreDKP | Meyhém | 2 | Kill 1111/Grobbulus | Balance 297|
|51 2022-12-14 19:52:41 StoreDKP | Orcbane | 2 | Kill 1111/Grobbulus | Balance 115|
|52 2022-12-14 19:52:41 StoreDKP | Phal | 2 | Kill 1111/Grobbulus | Balance 263|
|53 2022-12-14 19:52:41 StoreDKP | Ryzus | 2 | Kill 1111/Grobbulus | Balance 821|
|54 2022-12-14 19:52:41 StoreDKP | Wreckel | 2 | Kill 1111/Grobbulus | Balance 475|
|55 2022-12-14 19:54:22 StoreDKP | Aythya | 1 | Pull 1108/Gluth | Balance 321|
|56 2022-12-14 19:54:22 StoreDKP | Bravend | 1 | Pull 1108/Gluth | Balance 411|
|57 2022-12-14 19:54:22 StoreDKP | Eillowee | 1 | Pull 1108/Gluth | Balance 222|
|58 2022-12-14 19:54:22 StoreDKP | Ironflurry | 1 | Pull 1108/Gluth | Balance 572|
|59 2022-12-14 19:54:22 StoreDKP | Lachý | 1 | Pull 1108/Gluth | Balance 379|
|60 2022-12-14 19:54:22 StoreDKP | Meyhém | 1 | Pull 1108/Gluth | Balance 298|
|61 2022-12-14 19:54:22 StoreDKP | Orcbane | 1 | Pull 1108/Gluth | Balance 116|
|62 2022-12-14 19:54:22 StoreDKP | Phal | 1 | Pull 1108/Gluth | Balance 264|
|63 2022-12-14 19:54:22 StoreDKP | Ryzus | 1 | Pull 1108/Gluth | Balance 822|
|64 2022-12-14 19:54:22 StoreDKP | Wreckel | 1 | Pull 1108/Gluth | Balance 476|
|65 2022-12-14 19:57:07 StoreDKP | Aythya | 2 | Kill 1108/Gluth | Balance 323|
|66 2022-12-14 19:57:07 StoreDKP | Bravend | 2 | Kill 1108/Gluth | Balance 413|
|67 2022-12-14 19:57:07 StoreDKP | Eillowee | 2 | Kill 1108/Gluth | Balance 224|
|68 2022-12-14 19:57:07 StoreDKP | Ironflurry | 2 | Kill 1108/Gluth | Balance 574|
|69 2022-12-14 19:57:07 StoreDKP | Lachý | 2 | Kill 1108/Gluth | Balance 381|
|70 2022-12-14 19:57:07 StoreDKP | Meyhém | 2 | Kill 1108/Gluth | Balance 300|
|71 2022-12-14 19:57:07 StoreDKP | Orcbane | 2 | Kill 1108/Gluth | Balance 118|
|72 2022-12-14 19:57:07 StoreDKP | Phal | 2 | Kill 1108/Gluth | Balance 266|
|73 2022-12-14 19:57:07 StoreDKP | Ryzus | 2 | Kill 1108/Gluth | Balance 824|
|74 2022-12-14 19:57:07 StoreDKP | Wreckel | 2 | Kill 1108/Gluth | Balance 478|
|75 2022-12-14 20:00:06 StoreDKP | Aythya | 1 | Pull 1120/Thaddius | Balance 324|
|76 2022-12-14 20:00:06 StoreDKP | Bravend | 1 | Pull 1120/Thaddius | Balance 414|
|77 2022-12-14 20:00:06 StoreDKP | Eillowee | 1 | Pull 1120/Thaddius | Balance 225|
|78 2022-12-14 20:00:06 StoreDKP | Ironflurry | 1 | Pull 1120/Thaddius | Balance 575|
|79 2022-12-14 20:00:06 StoreDKP | Lachý | 1 | Pull 1120/Thaddius | Balance 382|
|80 2022-12-14 20:00:06 StoreDKP | Meyhém | 1 | Pull 1120/Thaddius | Balance 301|
|81 2022-12-14 20:00:06 StoreDKP | Orcbane | 1 | Pull 1120/Thaddius | Balance 119|
|82 2022-12-14 20:00:06 StoreDKP | Phal | 1 | Pull 1120/Thaddius | Balance 267|
|83 2022-12-14 20:00:06 StoreDKP | Ryzus | 1 | Pull 1120/Thaddius | Balance 825|
|84 2022-12-14 20:00:06 StoreDKP | Wreckel | 1 | Pull 1120/Thaddius | Balance 479|
|85 2022-12-14 20:04:16 StoreDKP | Aythya | 2 | Kill 1120/Thaddius | Balance 326|
|86 2022-12-14 20:04:16 StoreDKP | Bravend | 2 | Kill 1120/Thaddius | Balance 416|
|87 2022-12-14 20:04:16 StoreDKP | Eillowee | 2 | Kill 1120/Thaddius | Balance 227|
|88 2022-12-14 20:04:16 StoreDKP | Ironflurry | 2 | Kill 1120/Thaddius | Balance 577|
|89 2022-12-14 20:04:16 StoreDKP | Lachý | 2 | Kill 1120/Thaddius | Balance 384|
|90 2022-12-14 20:04:16 StoreDKP | Meyhém | 2 | Kill 1120/Thaddius | Balance 303|
|91 2022-12-14 20:04:16 StoreDKP | Orcbane | 2 | Kill 1120/Thaddius | Balance 121|
|92 2022-12-14 20:04:16 StoreDKP | Phal | 2 | Kill 1120/Thaddius | Balance 269|
|93 2022-12-14 20:04:16 StoreDKP | Ryzus | 2 | Kill 1120/Thaddius | Balance 827|
|94 2022-12-14 20:04:16 StoreDKP | Wreckel | 2 | Kill 1120/Thaddius | Balance 481|
|95 2022-12-14 20:09:46 StoreDKP | Aythya | 1 | Pull 1113/Instructor Razuvious | Balance 327|
|96 2022-12-14 20:09:46 StoreDKP | Bravend | 1 | Pull 1113/Instructor Razuvious | Balance 417|
|97 2022-12-14 20:09:46 StoreDKP | Eillowee | 1 | Pull 1113/Instructor Razuvious | Balance 228|
|98 2022-12-14 20:09:46 StoreDKP | Ironflurry | 1 | Pull 1113/Instructor Razuvious | Balance 578|
|99 2022-12-14 20:09:46 StoreDKP | Lachý | 1 | Pull 1113/Instructor Razuvious | Balance 385|
|100 2022-12-14 20:09:46 StoreDKP | Meyhém | 1 | Pull 1113/Instructor Razuvious | Balance 304|
|101 2022-12-14 20:09:46 StoreDKP | Orcbane | 1 | Pull 1113/Instructor Razuvious | Balance 122|
|102 2022-12-14 20:09:46 StoreDKP | Phal | 1 | Pull 1113/Instructor Razuvious | Balance 270|
|103 2022-12-14 20:09:46 StoreDKP | Ryzus | 1 | Pull 1113/Instructor Razuvious | Balance 828|
|104 2022-12-14 20:09:46 StoreDKP | Wreckel | 1 | Pull 1113/Instructor Razuvious | Balance 482|
|105 2022-12-14 20:12:55 StoreDKP | Aythya | 2 | Kill 1113/Instructor Razuvious | Balance 329|
|106 2022-12-14 20:12:55 StoreDKP | Bravend | 2 | Kill 1113/Instructor Razuvious | Balance 419|
|107 2022-12-14 20:12:55 StoreDKP | Eillowee | 2 | Kill 1113/Instructor Razuvious | Balance 230|
|108 2022-12-14 20:12:55 StoreDKP | Ironflurry | 2 | Kill 1113/Instructor Razuvious | Balance 580|
|109 2022-12-14 20:12:55 StoreDKP | Lachý | 2 | Kill 1113/Instructor Razuvious | Balance 387|
|110 2022-12-14 20:12:55 StoreDKP | Meyhém | 2 | Kill 1113/Instructor Razuvious | Balance 306|
|111 2022-12-14 20:12:55 StoreDKP | Orcbane | 2 | Kill 1113/Instructor Razuvious | Balance 124|
|112 2022-12-14 20:12:55 StoreDKP | Phal | 2 | Kill 1113/Instructor Razuvious | Balance 272|
|113 2022-12-14 20:12:55 StoreDKP | Ryzus | 2 | Kill 1113/Instructor Razuvious | Balance 830|
|114 2022-12-14 20:12:55 StoreDKP | Wreckel | 2 | Kill 1113/Instructor Razuvious | Balance 484|
|115 2022-12-14 20:17:47 StoreDKP | Aythya | 1 | Pull 1109/Gothik the Harvester | Balance 330|
|116 2022-12-14 20:17:47 StoreDKP | Bravend | 1 | Pull 1109/Gothik the Harvester | Balance 420|
|117 2022-12-14 20:17:47 StoreDKP | Eillowee | 1 | Pull 1109/Gothik the Harvester | Balance 231|
|118 2022-12-14 20:17:47 StoreDKP | Ironflurry | 1 | Pull 1109/Gothik the Harvester | Balance 581|
|119 2022-12-14 20:17:47 StoreDKP | Lachý | 1 | Pull 1109/Gothik the Harvester | Balance 388|
|120 2022-12-14 20:17:47 StoreDKP | Meyhém | 1 | Pull 1109/Gothik the Harvester | Balance 307|
|121 2022-12-14 20:17:47 StoreDKP | Orcbane | 1 | Pull 1109/Gothik the Harvester | Balance 125|
|122 2022-12-14 20:17:47 StoreDKP | Phal | 1 | Pull 1109/Gothik the Harvester | Balance 273|
|123 2022-12-14 20:17:47 StoreDKP | Ryzus | 1 | Pull 1109/Gothik the Harvester | Balance 831|
|124 2022-12-14 20:17:47 StoreDKP | Wreckel | 1 | Pull 1109/Gothik the Harvester | Balance 485|
|125 2022-12-14 20:22:59 StoreDKP | Aythya | 2 | Kill 1109/Gothik the Harvester | Balance 332|
|126 2022-12-14 20:22:59 StoreDKP | Bravend | 2 | Kill 1109/Gothik the Harvester | Balance 422|
|127 2022-12-14 20:22:59 StoreDKP | Eillowee | 2 | Kill 1109/Gothik the Harvester | Balance 233|
|128 2022-12-14 20:22:59 StoreDKP | Ironflurry | 2 | Kill 1109/Gothik the Harvester | Balance 583|
|129 2022-12-14 20:22:59 StoreDKP | Lachý | 2 | Kill 1109/Gothik the Harvester | Balance 390|
|130 2022-12-14 20:22:59 StoreDKP | Meyhém | 2 | Kill 1109/Gothik the Harvester | Balance 309|
|131 2022-12-14 20:22:59 StoreDKP | Orcbane | 2 | Kill 1109/Gothik the Harvester | Balance 127|
|132 2022-12-14 20:22:59 StoreDKP | Phal | 2 | Kill 1109/Gothik the Harvester | Balance 275|
|133 2022-12-14 20:22:59 StoreDKP | Ryzus | 2 | Kill 1109/Gothik the Harvester | Balance 833|
|134 2022-12-14 20:22:59 StoreDKP | Wreckel | 2 | Kill 1109/Gothik the Harvester | Balance 487|
|135 2022-12-14 20:30:07 StoreDKP | Aythya | 1 | Pull 1121/The Four Horsemen | Balance 333|
|136 2022-12-14 20:30:07 StoreDKP | Bravend | 1 | Pull 1121/The Four Horsemen | Balance 423|
|137 2022-12-14 20:30:07 StoreDKP | Eillowee | 1 | Pull 1121/The Four Horsemen | Balance 234|
|138 2022-12-14 20:30:07 StoreDKP | Ironflurry | 1 | Pull 1121/The Four Horsemen | Balance 584|
|139 2022-12-14 20:30:07 StoreDKP | Lachý | 1 | Pull 1121/The Four Horsemen | Balance 391|
|140 2022-12-14 20:30:07 StoreDKP | Meyhém | 1 | Pull 1121/The Four Horsemen | Balance 310|
|141 2022-12-14 20:30:07 StoreDKP | Orcbane | 1 | Pull 1121/The Four Horsemen | Balance 128|
|142 2022-12-14 20:30:07 StoreDKP | Phal | 1 | Pull 1121/The Four Horsemen | Balance 276|
|143 2022-12-14 20:30:07 StoreDKP | Ryzus | 1 | Pull 1121/The Four Horsemen | Balance 834|
|144 2022-12-14 20:30:07 StoreDKP | Wreckel | 1 | Pull 1121/The Four Horsemen | Balance 488|
|145 2022-12-14 20:32:49 StoreDKP | Aythya | 2 | Kill 1121/The Four Horsemen | Balance 335|
|146 2022-12-14 20:32:49 StoreDKP | Bravend | 2 | Kill 1121/The Four Horsemen | Balance 425|
|147 2022-12-14 20:32:49 StoreDKP | Eillowee | 2 | Kill 1121/The Four Horsemen | Balance 236|
|148 2022-12-14 20:32:49 StoreDKP | Ironflurry | 2 | Kill 1121/The Four Horsemen | Balance 586|
|149 2022-12-14 20:32:49 StoreDKP | Lachý | 2 | Kill 1121/The Four Horsemen | Balance 393|
|150 2022-12-14 20:32:49 StoreDKP | Meyhém | 2 | Kill 1121/The Four Horsemen | Balance 312|
|151 2022-12-14 20:32:49 StoreDKP | Orcbane | 2 | Kill 1121/The Four Horsemen | Balance 130|
|152 2022-12-14 20:32:49 StoreDKP | Phal | 2 | Kill 1121/The Four Horsemen | Balance 278|
|153 2022-12-14 20:32:49 StoreDKP | Ryzus | 2 | Kill 1121/The Four Horsemen | Balance 836|
|154 2022-12-14 20:32:49 StoreDKP | Wreckel | 2 | Kill 1121/The Four Horsemen | Balance 490|
|155 2022-12-14 20:34:54 StoreDKP | Aythya | 1 | Pull 1107/Anub'Rekhan | Balance 336|
|156 2022-12-14 20:34:54 StoreDKP | Bravend | 1 | Pull 1107/Anub'Rekhan | Balance 426|
|157 2022-12-14 20:34:54 StoreDKP | Eillowee | 1 | Pull 1107/Anub'Rekhan | Balance 237|
|158 2022-12-14 20:34:54 StoreDKP | Ironflurry | 1 | Pull 1107/Anub'Rekhan | Balance 587|
|159 2022-12-14 20:34:54 StoreDKP | Lachý | 1 | Pull 1107/Anub'Rekhan | Balance 394|
|160 2022-12-14 20:34:54 StoreDKP | Meyhém | 1 | Pull 1107/Anub'Rekhan | Balance 313|
|161 2022-12-14 20:34:54 StoreDKP | Orcbane | 1 | Pull 1107/Anub'Rekhan | Balance 131|
|162 2022-12-14 20:34:54 StoreDKP | Phal | 1 | Pull 1107/Anub'Rekhan | Balance 279|
|163 2022-12-14 20:34:54 StoreDKP | Ryzus | 1 | Pull 1107/Anub'Rekhan | Balance 837|
|164 2022-12-14 20:34:54 StoreDKP | Wreckel | 1 | Pull 1107/Anub'Rekhan | Balance 491|
|165 2022-12-14 20:36:30 StoreDKP | Aythya | 2 | Kill 1107/Anub'Rekhan | Balance 338|
|166 2022-12-14 20:36:30 StoreDKP | Bravend | 2 | Kill 1107/Anub'Rekhan | Balance 428|
|167 2022-12-14 20:36:30 StoreDKP | Eillowee | 2 | Kill 1107/Anub'Rekhan | Balance 239|
|168 2022-12-14 20:36:30 StoreDKP | Ironflurry | 2 | Kill 1107/Anub'Rekhan | Balance 589|
|169 2022-12-14 20:36:30 StoreDKP | Lachý | 2 | Kill 1107/Anub'Rekhan | Balance 396|
|170 2022-12-14 20:36:30 StoreDKP | Meyhém | 2 | Kill 1107/Anub'Rekhan | Balance 315|
|171 2022-12-14 20:36:30 StoreDKP | Orcbane | 2 | Kill 1107/Anub'Rekhan | Balance 133|
|172 2022-12-14 20:36:30 StoreDKP | Phal | 2 | Kill 1107/Anub'Rekhan | Balance 281|
|173 2022-12-14 20:36:30 StoreDKP | Ryzus | 2 | Kill 1107/Anub'Rekhan | Balance 839|
|174 2022-12-14 20:36:30 StoreDKP | Wreckel | 2 | Kill 1107/Anub'Rekhan | Balance 493|
|175 2022-12-14 20:40:46 StoreDKP | Aythya | 1 | Pull 1110/Grand Widow Faerlina | Balance 339|
|176 2022-12-14 20:40:46 StoreDKP | Bravend | 1 | Pull 1110/Grand Widow Faerlina | Balance 429|
|177 2022-12-14 20:40:46 StoreDKP | Eillowee | 1 | Pull 1110/Grand Widow Faerlina | Balance 240|
|178 2022-12-14 20:40:46 StoreDKP | Ironflurry | 1 | Pull 1110/Grand Widow Faerlina | Balance 590|
|179 2022-12-14 20:40:46 StoreDKP | Lachý | 1 | Pull 1110/Grand Widow Faerlina | Balance 397|
|180 2022-12-14 20:40:46 StoreDKP | Meyhém | 1 | Pull 1110/Grand Widow Faerlina | Balance 316|
|181 2022-12-14 20:40:46 StoreDKP | Orcbane | 1 | Pull 1110/Grand Widow Faerlina | Balance 134|
|182 2022-12-14 20:40:46 StoreDKP | Phal | 1 | Pull 1110/Grand Widow Faerlina | Balance 282|
|183 2022-12-14 20:40:46 StoreDKP | Ryzus | 1 | Pull 1110/Grand Widow Faerlina | Balance 840|
|184 2022-12-14 20:40:46 StoreDKP | Wreckel | 1 | Pull 1110/Grand Widow Faerlina | Balance 494|
|185 2022-12-14 20:42:11 StoreDKP | Aythya | 2 | Kill 1110/Grand Widow Faerlina | Balance 341|
|186 2022-12-14 20:42:11 StoreDKP | Bravend | 2 | Kill 1110/Grand Widow Faerlina | Balance 431|
|187 2022-12-14 20:42:11 StoreDKP | Eillowee | 2 | Kill 1110/Grand Widow Faerlina | Balance 242|
|188 2022-12-14 20:42:11 StoreDKP | Ironflurry | 2 | Kill 1110/Grand Widow Faerlina | Balance 592|
|189 2022-12-14 20:42:11 StoreDKP | Lachý | 2 | Kill 1110/Grand Widow Faerlina | Balance 399|
|190 2022-12-14 20:42:11 StoreDKP | Meyhém | 2 | Kill 1110/Grand Widow Faerlina | Balance 318|
|191 2022-12-14 20:42:11 StoreDKP | Orcbane | 2 | Kill 1110/Grand Widow Faerlina | Balance 136|
|192 2022-12-14 20:42:11 StoreDKP | Phal | 2 | Kill 1110/Grand Widow Faerlina | Balance 284|
|193 2022-12-14 20:42:11 StoreDKP | Ryzus | 2 | Kill 1110/Grand Widow Faerlina | Balance 842|
|194 2022-12-14 20:42:11 StoreDKP | Wreckel | 2 | Kill 1110/Grand Widow Faerlina | Balance 496|
|195 2022-12-14 20:45:45 StoreDKP | Aythya | 1 | Pull 1116/Maexxna | Balance 342|
|196 2022-12-14 20:45:45 StoreDKP | Bravend | 1 | Pull 1116/Maexxna | Balance 432|
|197 2022-12-14 20:45:45 StoreDKP | Eillowee | 1 | Pull 1116/Maexxna | Balance 243|
|198 2022-12-14 20:45:45 StoreDKP | Ironflurry | 1 | Pull 1116/Maexxna | Balance 593|
|199 2022-12-14 20:45:45 StoreDKP | Lachý | 1 | Pull 1116/Maexxna | Balance 400|
|200 2022-12-14 20:45:45 StoreDKP | Meyhém | 1 | Pull 1116/Maexxna | Balance 319|
|201 2022-12-14 20:45:45 StoreDKP | Orcbane | 1 | Pull 1116/Maexxna | Balance 137|
|202 2022-12-14 20:45:45 StoreDKP | Phal | 1 | Pull 1116/Maexxna | Balance 285|
|203 2022-12-14 20:45:45 StoreDKP | Ryzus | 1 | Pull 1116/Maexxna | Balance 843|
|204 2022-12-14 20:45:45 StoreDKP | Wreckel | 1 | Pull 1116/Maexxna | Balance 497|
|205 2022-12-14 20:47:41 StoreDKP | Aythya | 2 | Kill 1116/Maexxna | Balance 344|
|206 2022-12-14 20:47:41 StoreDKP | Bravend | 2 | Kill 1116/Maexxna | Balance 434|
|207 2022-12-14 20:47:41 StoreDKP | Eillowee | 2 | Kill 1116/Maexxna | Balance 245|
|208 2022-12-14 20:47:41 StoreDKP | Ironflurry | 2 | Kill 1116/Maexxna | Balance 595|
|209 2022-12-14 20:47:41 StoreDKP | Lachý | 2 | Kill 1116/Maexxna | Balance 402|
|210 2022-12-14 20:47:41 StoreDKP | Meyhém | 2 | Kill 1116/Maexxna | Balance 321|
|211 2022-12-14 20:47:41 StoreDKP | Orcbane | 2 | Kill 1116/Maexxna | Balance 139|
|212 2022-12-14 20:47:41 StoreDKP | Phal | 2 | Kill 1116/Maexxna | Balance 287|
|213 2022-12-14 20:47:41 StoreDKP | Ryzus | 2 | Kill 1116/Maexxna | Balance 845|
|214 2022-12-14 20:47:41 StoreDKP | Wreckel | 2 | Kill 1116/Maexxna | Balance 499|
|215 2022-12-14 20:51:36 StoreDKP | Aythya | 1 | Pull 1117/Noth the Plaguebringer | Balance 345|
|216 2022-12-14 20:51:36 StoreDKP | Bravend | 1 | Pull 1117/Noth the Plaguebringer | Balance 435|
|217 2022-12-14 20:51:36 StoreDKP | Eillowee | 1 | Pull 1117/Noth the Plaguebringer | Balance 246|
|218 2022-12-14 20:51:36 StoreDKP | Ironflurry | 1 | Pull 1117/Noth the Plaguebringer | Balance 596|
|219 2022-12-14 20:51:36 StoreDKP | Lachý | 1 | Pull 1117/Noth the Plaguebringer | Balance 403|
|220 2022-12-14 20:51:36 StoreDKP | Meyhém | 1 | Pull 1117/Noth the Plaguebringer | Balance 322|
|221 2022-12-14 20:51:36 StoreDKP | Orcbane | 1 | Pull 1117/Noth the Plaguebringer | Balance 140|
|222 2022-12-14 20:51:36 StoreDKP | Phal | 1 | Pull 1117/Noth the Plaguebringer | Balance 288|
|223 2022-12-14 20:51:36 StoreDKP | Ryzus | 1 | Pull 1117/Noth the Plaguebringer | Balance 846|
|224 2022-12-14 20:51:36 StoreDKP | Wreckel | 1 | Pull 1117/Noth the Plaguebringer | Balance 500|
|225 2022-12-14 20:54:36 StoreDKP | Aythya | 2 | Kill 1117/Noth the Plaguebringer | Balance 347|
|226 2022-12-14 20:54:36 StoreDKP | Bravend | 2 | Kill 1117/Noth the Plaguebringer | Balance 437|
|227 2022-12-14 20:54:36 StoreDKP | Eillowee | 2 | Kill 1117/Noth the Plaguebringer | Balance 248|
|228 2022-12-14 20:54:36 StoreDKP | Ironflurry | 2 | Kill 1117/Noth the Plaguebringer | Balance 598|
|229 2022-12-14 20:54:36 StoreDKP | Lachý | 2 | Kill 1117/Noth the Plaguebringer | Balance 405|
|230 2022-12-14 20:54:36 StoreDKP | Meyhém | 2 | Kill 1117/Noth the Plaguebringer | Balance 324|
|231 2022-12-14 20:54:36 StoreDKP | Orcbane | 2 | Kill 1117/Noth the Plaguebringer | Balance 142|
|232 2022-12-14 20:54:36 StoreDKP | Phal | 2 | Kill 1117/Noth the Plaguebringer | Balance 290|
|233 2022-12-14 20:54:36 StoreDKP | Ryzus | 2 | Kill 1117/Noth the Plaguebringer | Balance 848|
|234 2022-12-14 20:54:36 StoreDKP | Wreckel | 2 | Kill 1117/Noth the Plaguebringer | Balance 502|
|235 2022-12-14 20:57:26 StoreDKP | Aythya | 1 | Pull 1112/Heigan the Unclean | Balance 348|
|236 2022-12-14 20:57:26 StoreDKP | Bravend | 1 | Pull 1112/Heigan the Unclean | Balance 438|
|237 2022-12-14 20:57:26 StoreDKP | Eillowee | 1 | Pull 1112/Heigan the Unclean | Balance 249|
|238 2022-12-14 20:57:26 StoreDKP | Ironflurry | 1 | Pull 1112/Heigan the Unclean | Balance 599|
|239 2022-12-14 20:57:26 StoreDKP | Lachý | 1 | Pull 1112/Heigan the Unclean | Balance 406|
|240 2022-12-14 20:57:26 StoreDKP | Meyhém | 1 | Pull 1112/Heigan the Unclean | Balance 325|
|241 2022-12-14 20:57:26 StoreDKP | Orcbane | 1 | Pull 1112/Heigan the Unclean | Balance 143|
|242 2022-12-14 20:57:26 StoreDKP | Phal | 1 | Pull 1112/Heigan the Unclean | Balance 291|
|243 2022-12-14 20:57:26 StoreDKP | Ryzus | 1 | Pull 1112/Heigan the Unclean | Balance 849|
|244 2022-12-14 20:57:26 StoreDKP | Wreckel | 1 | Pull 1112/Heigan the Unclean | Balance 503|
|245 2022-12-14 21:00:19 StoreDKP | Aythya | 2 | Kill 1112/Heigan the Unclean | Balance 350|
|246 2022-12-14 21:00:19 StoreDKP | Bravend | 2 | Kill 1112/Heigan the Unclean | Balance 440|
|247 2022-12-14 21:00:19 StoreDKP | Eillowee | 2 | Kill 1112/Heigan the Unclean | Balance 251|
|248 2022-12-14 21:00:19 StoreDKP | Ironflurry | 2 | Kill 1112/Heigan the Unclean | Balance 601|
|249 2022-12-14 21:00:19 StoreDKP | Lachý | 2 | Kill 1112/Heigan the Unclean | Balance 408|
|250 2022-12-14 21:00:19 StoreDKP | Meyhém | 2 | Kill 1112/Heigan the Unclean | Balance 327|
|251 2022-12-14 21:00:19 StoreDKP | Orcbane | 2 | Kill 1112/Heigan the Unclean | Balance 145|
|252 2022-12-14 21:00:19 StoreDKP | Phal | 2 | Kill 1112/Heigan the Unclean | Balance 293|
|253 2022-12-14 21:00:19 StoreDKP | Ryzus | 2 | Kill 1112/Heigan the Unclean | Balance 851|
|254 2022-12-14 21:00:19 StoreDKP | Wreckel | 2 | Kill 1112/Heigan the Unclean | Balance 505|
|255 2022-12-14 21:08:40 StoreDKP | Aythya | 1 | Pull 1115/Loatheb | Balance 351|
|256 2022-12-14 21:08:40 StoreDKP | Bravend | 1 | Pull 1115/Loatheb | Balance 441|
|257 2022-12-14 21:08:40 StoreDKP | Eillowee | 1 | Pull 1115/Loatheb | Balance 252|
|258 2022-12-14 21:08:40 StoreDKP | Ironflurry | 1 | Pull 1115/Loatheb | Balance 602|
|259 2022-12-14 21:08:40 StoreDKP | Lachý | 1 | Pull 1115/Loatheb | Balance 409|
|260 2022-12-14 21:08:40 StoreDKP | Meyhém | 1 | Pull 1115/Loatheb | Balance 328|
|261 2022-12-14 21:08:40 StoreDKP | Orcbane | 1 | Pull 1115/Loatheb | Balance 146|
|262 2022-12-14 21:08:40 StoreDKP | Phal | 1 | Pull 1115/Loatheb | Balance 294|
|263 2022-12-14 21:08:40 StoreDKP | Ryzus | 1 | Pull 1115/Loatheb | Balance 852|
|264 2022-12-14 21:08:40 StoreDKP | Wreckel | 1 | Pull 1115/Loatheb | Balance 506|
|265 2022-12-14 21:12:22 StoreDKP | Aythya | 2 | Kill 1115/Loatheb | Balance 353|
|266 2022-12-14 21:12:22 StoreDKP | Bravend | 2 | Kill 1115/Loatheb | Balance 443|
|267 2022-12-14 21:12:22 StoreDKP | Eillowee | 2 | Kill 1115/Loatheb | Balance 254|
|268 2022-12-14 21:12:22 StoreDKP | Ironflurry | 2 | Kill 1115/Loatheb | Balance 604|
|269 2022-12-14 21:12:22 StoreDKP | Lachý | 2 | Kill 1115/Loatheb | Balance 411|
|270 2022-12-14 21:12:22 StoreDKP | Meyhém | 2 | Kill 1115/Loatheb | Balance 330|
|271 2022-12-14 21:12:22 StoreDKP | Orcbane | 2 | Kill 1115/Loatheb | Balance 148|
|272 2022-12-14 21:12:22 StoreDKP | Phal | 2 | Kill 1115/Loatheb | Balance 296|
|273 2022-12-14 21:12:22 StoreDKP | Ryzus | 2 | Kill 1115/Loatheb | Balance 854|
|274 2022-12-14 21:12:22 StoreDKP | Wreckel | 2 | Kill 1115/Loatheb | Balance 508|
|275 2022-12-14 21:13:51 StoreDKP | Aythya | 1 | Pull 1119/Sapphiron | Balance 354|
|276 2022-12-14 21:13:51 StoreDKP | Bravend | 1 | Pull 1119/Sapphiron | Balance 444|
|277 2022-12-14 21:13:51 StoreDKP | Eillowee | 1 | Pull 1119/Sapphiron | Balance 255|
|278 2022-12-14 21:13:51 StoreDKP | Ironflurry | 1 | Pull 1119/Sapphiron | Balance 605|
|279 2022-12-14 21:13:51 StoreDKP | Lachý | 1 | Pull 1119/Sapphiron | Balance 412|
|280 2022-12-14 21:13:51 StoreDKP | Meyhém | 1 | Pull 1119/Sapphiron | Balance 331|
|281 2022-12-14 21:13:51 StoreDKP | Orcbane | 1 | Pull 1119/Sapphiron | Balance 149|
|282 2022-12-14 21:13:51 StoreDKP | Phal | 1 | Pull 1119/Sapphiron | Balance 297|
|283 2022-12-14 21:13:51 StoreDKP | Ryzus | 1 | Pull 1119/Sapphiron | Balance 855|
|284 2022-12-14 21:13:51 StoreDKP | Wreckel | 1 | Pull 1119/Sapphiron | Balance 509|
|285 2022-12-14 21:17:35 StoreDKP | Aythya | 2 | Kill 1119/Sapphiron | Balance 356|
|286 2022-12-14 21:17:35 StoreDKP | Bravend | 2 | Kill 1119/Sapphiron | Balance 446|
|287 2022-12-14 21:17:35 StoreDKP | Eillowee | 2 | Kill 1119/Sapphiron | Balance 257|
|288 2022-12-14 21:17:35 StoreDKP | Ironflurry | 2 | Kill 1119/Sapphiron | Balance 607|
|289 2022-12-14 21:17:35 StoreDKP | Lachý | 2 | Kill 1119/Sapphiron | Balance 414|
|290 2022-12-14 21:17:35 StoreDKP | Meyhém | 2 | Kill 1119/Sapphiron | Balance 333|
|291 2022-12-14 21:17:35 StoreDKP | Orcbane | 2 | Kill 1119/Sapphiron | Balance 151|
|292 2022-12-14 21:17:35 StoreDKP | Phal | 2 | Kill 1119/Sapphiron | Balance 299|
|293 2022-12-14 21:17:35 StoreDKP | Ryzus | 2 | Kill 1119/Sapphiron | Balance 857|
|294 2022-12-14 21:17:35 StoreDKP | Wreckel | 2 | Kill 1119/Sapphiron | Balance 511|
|295 2022-12-14 21:19:18 StoreDKP | Aythya | 1 | Pull 1114/Kel'Thuzad | Balance 357|
|296 2022-12-14 21:19:18 StoreDKP | Bravend | 1 | Pull 1114/Kel'Thuzad | Balance 447|
|297 2022-12-14 21:19:18 StoreDKP | Eillowee | 1 | Pull 1114/Kel'Thuzad | Balance 258|
|298 2022-12-14 21:19:18 StoreDKP | Ironflurry | 1 | Pull 1114/Kel'Thuzad | Balance 608|
|299 2022-12-14 21:19:18 StoreDKP | Lachý | 1 | Pull 1114/Kel'Thuzad | Balance 415|
|300 2022-12-14 21:19:18 StoreDKP | Meyhém | 1 | Pull 1114/Kel'Thuzad | Balance 334|
|301 2022-12-14 21:19:18 StoreDKP | Orcbane | 1 | Pull 1114/Kel'Thuzad | Balance 152|
|302 2022-12-14 21:19:18 StoreDKP | Phal | 1 | Pull 1114/Kel'Thuzad | Balance 300|
|303 2022-12-14 21:19:18 StoreDKP | Ryzus | 1 | Pull 1114/Kel'Thuzad | Balance 858|
|304 2022-12-14 21:19:18 StoreDKP | Wreckel | 1 | Pull 1114/Kel'Thuzad | Balance 512|
|305 2022-12-14 21:27:24 StoreDKP | Aythya | 1 | Pull 1114/Kel'Thuzad | Balance 358|
|306 2022-12-14 21:27:24 StoreDKP | Bravend | 1 | Pull 1114/Kel'Thuzad | Balance 448|
|307 2022-12-14 21:27:24 StoreDKP | Eillowee | 1 | Pull 1114/Kel'Thuzad | Balance 259|
|308 2022-12-14 21:27:24 StoreDKP | Ironflurry | 1 | Pull 1114/Kel'Thuzad | Balance 609|
|309 2022-12-14 21:27:24 StoreDKP | Lachý | 1 | Pull 1114/Kel'Thuzad | Balance 416|
|310 2022-12-14 21:27:24 StoreDKP | Meyhém | 1 | Pull 1114/Kel'Thuzad | Balance 335|
|311 2022-12-14 21:27:24 StoreDKP | Orcbane | 1 | Pull 1114/Kel'Thuzad | Balance 153|
|312 2022-12-14 21:27:24 StoreDKP | Phal | 1 | Pull 1114/Kel'Thuzad | Balance 301|
|313 2022-12-14 21:27:24 StoreDKP | Ryzus | 1 | Pull 1114/Kel'Thuzad | Balance 859|
|314 2022-12-14 21:27:24 StoreDKP | Wreckel | 1 | Pull 1114/Kel'Thuzad | Balance 513|
|315 2022-12-14 21:36:11 StoreDKP | Aythya | 2 | Kill 1114/Kel'Thuzad | Balance 360|
|316 2022-12-14 21:36:11 StoreDKP | Bravend | 2 | Kill 1114/Kel'Thuzad | Balance 450|
|317 2022-12-14 21:36:11 StoreDKP | Eillowee | 2 | Kill 1114/Kel'Thuzad | Balance 261|
|318 2022-12-14 21:36:11 StoreDKP | Ironflurry | 2 | Kill 1114/Kel'Thuzad | Balance 611|
|319 2022-12-14 21:36:11 StoreDKP | Lachý | 2 | Kill 1114/Kel'Thuzad | Balance 418|
|320 2022-12-14 21:36:11 StoreDKP | Meyhém | 2 | Kill 1114/Kel'Thuzad | Balance 337|
|321 2022-12-14 21:36:11 StoreDKP | Orcbane | 2 | Kill 1114/Kel'Thuzad | Balance 155|
|322 2022-12-14 21:36:11 StoreDKP | Phal | 2 | Kill 1114/Kel'Thuzad | Balance 303|
|323 2022-12-14 21:36:11 StoreDKP | Ryzus | 2 | Kill 1114/Kel'Thuzad | Balance 861|
|324 2022-12-14 21:36:11 StoreDKP | Wreckel | 2 | Kill 1114/Kel'Thuzad | Balance 515|
|325 2022-12-14 21:51:16 StoreDKP | Aythya | 1 | Pull 734/Malygos | Balance 361|
|326 2022-12-14 21:51:16 StoreDKP | Bravend | 1 | Pull 734/Malygos | Balance 451|
|327 2022-12-14 21:51:16 StoreDKP | Eillowee | 1 | Pull 734/Malygos | Balance 262|
|328 2022-12-14 21:51:16 StoreDKP | Ironflurry | 1 | Pull 734/Malygos | Balance 612|
|329 2022-12-14 21:51:16 StoreDKP | Lachý | 1 | Pull 734/Malygos | Balance 419|
|330 2022-12-14 21:51:16 StoreDKP | Meyhém | 1 | Pull 734/Malygos | Balance 338|
|331 2022-12-14 21:51:16 StoreDKP | Orcbane | 1 | Pull 734/Malygos | Balance 156|
|332 2022-12-14 21:51:16 StoreDKP | Phal | 1 | Pull 734/Malygos | Balance 304|
|333 2022-12-14 21:51:16 StoreDKP | Ryzus | 1 | Pull 734/Malygos | Balance 862|
|334 2022-12-14 21:51:16 StoreDKP | Wreckel | 1 | Pull 734/Malygos | Balance 516|
|335 2022-12-14 21:58:02 StoreDKP | Aythya | 2 | Kill 734/Malygos | Balance 363|
|336 2022-12-14 21:58:02 StoreDKP | Bravend | 2 | Kill 734/Malygos | Balance 453|
|337 2022-12-14 21:58:02 StoreDKP | Eillowee | 2 | Kill 734/Malygos | Balance 264|
|338 2022-12-14 21:58:02 StoreDKP | Ironflurry | 2 | Kill 734/Malygos | Balance 614|
|339 2022-12-14 21:58:02 StoreDKP | Lachý | 2 | Kill 734/Malygos | Balance 421|
|340 2022-12-14 21:58:02 StoreDKP | Meyhém | 2 | Kill 734/Malygos | Balance 340|
|341 2022-12-14 21:58:02 StoreDKP | Orcbane | 2 | Kill 734/Malygos | Balance 158|
|342 2022-12-14 21:58:02 StoreDKP | Phal | 2 | Kill 734/Malygos | Balance 306|
|343 2022-12-14 21:58:02 StoreDKP | Ryzus | 2 | Kill 734/Malygos | Balance 864|
|344 2022-12-14 21:58:02 StoreDKP | Wreckel | 2 | Kill 734/Malygos | Balance 518|
|345 2022-12-14 22:01:02 StoreDKP | Aythya | -49 | Group Correction | Balance 314|
|346 2022-12-14 22:01:02 StoreDKP | Bravend | -49 | Group Correction | Balance 404|
|347 2022-12-14 22:01:02 StoreDKP | Eillowee | -49 | Group Correction | Balance 215|
|348 2022-12-14 22:01:02 StoreDKP | Ironflurry | -49 | Group Correction | Balance 565|
|349 2022-12-14 22:01:02 StoreDKP | Lachý | -49 | Group Correction | Balance 372|
|350 2022-12-14 22:01:02 StoreDKP | Meyhém | -49 | Group Correction | Balance 291|
|351 2022-12-14 22:01:02 StoreDKP | Orcbane | -49 | Group Correction | Balance 109|
|352 2022-12-14 22:01:02 StoreDKP | Phal | -49 | Group Correction | Balance 257|
|353 2022-12-14 22:01:02 StoreDKP | Ryzus | -49 | Group Correction | Balance 815|
|354 2022-12-14 22:01:02 StoreDKP | Wreckel | -49 | Group Correction | Balance 469|