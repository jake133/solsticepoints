local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Ironflurry",
    [ 2] = "Lachý",
  },
  kills = {
    {boss = 742, timestamp = "2023-01-17 17:23", players = {2}},
  },
  description = "2023-01-12_OS",
  drops = {
    {player =  2, timestamp = "2023-01-17 17:42", item = 40250, cost = 90, costType = "Solstice Points", name = "Aged Winter Cloak"}, -- Lachý
  },
  pulls = {
    },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
