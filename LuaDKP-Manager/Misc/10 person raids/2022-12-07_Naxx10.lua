local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Bravend",
    [ 2] = "Darkhourz",
    [ 3] = "Ironflurry",
    [ 4] = "Lachý",
    [ 5] = "Mayaell",
    [ 6] = "Meyhém",
    [ 7] = "Rudigar",
    [ 8] = "Ryzus",
    [ 9] = "Snoweyes",
  },
  kills = {
    {boss = 1107, timestamp = "2022-12-07 21:26", players = {1,2,3,4,5,6,7,8,9}},
    {boss = 1110, timestamp = "2022-12-07 21:33", players = {1,2,3,4,5,6,7,8,9}},
    {boss = 1116, timestamp = "2022-12-07 21:41", players = {1,2,3,4,5,6,7,8,9}},
    {boss = 1113, timestamp = "2022-12-07 21:51", players = {1,2,3,4,5,6,7,8,9}},
    {boss = 1109, timestamp = "2022-12-07 22:10", players = {1,2,3,4,5,6,7,8,9}},
    {boss = 1117, timestamp = "2022-12-07 22:33", players = {1,2,3,4,5,6,7,8,9}},
  },
  description = "2022-12-07_Naxx10",
  drops = {
    {player =  8, timestamp = "2022-12-07 21:27", item = 39146, cost = "", costType = "MS", name = "Collar of Dissolution"}, -- Ryzus
    {player =  5, timestamp = "2022-12-07 21:27", item = 39190, cost = "", costType = "MS", name = "Agonal Sash"}, -- Mayaell
    {player =  6, timestamp = "2022-12-07 21:35", item = 39216, cost = "", costType = "OS", name = "Sash of Mortal Desire"}, -- Meyhém
    {player =  5, timestamp = "2022-12-07 21:42", item = 39232, cost = "", costType = "MS", name = "Pendant of Lost Vocations"}, -- Mayaell
    {player =  7, timestamp = "2022-12-07 22:33", item = 39239, cost = "", costType = "MS", name = "Chestplate of the Risen Soldier"}, -- Rudigar
  },
  pulls = {
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
      },
      boss = 1107,
      timestamp = "2022-12-07 21:23",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
      },
      boss = 1110,
      timestamp = "2022-12-07 21:31",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
      },
      boss = 1116,
      timestamp = "2022-12-07 21:38",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
      },
      boss = 1113,
      timestamp = "2022-12-07 21:49",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
      },
      boss = 1109,
      timestamp = "2022-12-07 21:56",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
      },
      boss = 1109,
      timestamp = "2022-12-07 22:04",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
      },
      boss = 1117,
      timestamp = "2022-12-07 22:29",
    },
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
