local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Batriisyaa",
    [ 2] = "Bravend",
    [ 3] = "Darkhourz",
    [ 4] = "Eillowee",
    [ 5] = "Fellador",
    [ 6] = "Ironflurry",
    [ 7] = "Lachý",
    [ 8] = "Phal",
    [ 9] = "Rosemondon",
    [10] = "Snoweyes",
    [11] = "Xuduku",
  },
  kills = {
    {boss = 1118, timestamp = "2022-11-30 21:27", players = {1,2,3,4,5,6,7,8,9,10,11}},
    {boss = 1111, timestamp = "2022-11-30 21:35", players = {1,2,3,4,5,6,7,8,9,10,11}},
    {boss = 1107, timestamp = "2022-11-30 21:42", players = {1,2,3,4,5,6,7,8,9,10,11}},
    {boss = 1110, timestamp = "2022-11-30 21:49", players = {1,2,3,4,5,6,7,8,9,10,11}},
    {boss = 1116, timestamp = "2022-11-30 21:57", players = {1,2,3,4,5,6,7,8,9,10,11}},
  },
  description = "2022-11-30_Naxx10",
  drops = {
    {player =  5, timestamp = "2022-11-30 21:42", item = 39193, cost = "", costType = "MS", name = "Band of Neglected Pleas"}, -- Fellador
    {player = 11, timestamp = "2022-11-30 21:50", item = 39199, cost = "", costType = "OS", name = "Watchful Eye"}, -- Xuduku
    {player = 11, timestamp = "2022-11-30 21:59", item = 39231, cost = "", costType = "OS", name = "Timeworn Silken Band"}, -- Xuduku
  },
  pulls = {
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
      },
      boss = 1118,
      timestamp = "2022-11-30 21:14",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
      },
      boss = 1118,
      timestamp = "2022-11-30 21:23",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
      },
      boss = 1111,
      timestamp = "2022-11-30 21:31",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
      },
      boss = 1107,
      timestamp = "2022-11-30 21:39",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
      },
      boss = 1110,
      timestamp = "2022-11-30 21:46",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
      },
      boss = 1116,
      timestamp = "2022-11-30 21:54",
    },
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
