local _, ADDON = ...

------------------------------------------------------------

local accounts = {
  [0] = "?", -- do not remove!
  [1] = {basegp = 100, name = "WotLK"},
  --[2] = {basegp = 100, name = "Phase 2"},
  --[3] = {basegp = 100, name = "Phase 3"},
  --[4] = {basegp = 100, name = "Phase 4"},
  }

------------------------------------------------------------

-- export tables
ADDON.InitGroup.Accounts = accounts
