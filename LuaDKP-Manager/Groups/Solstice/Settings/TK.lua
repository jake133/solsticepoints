local _, ADDON = ...

------------------------------------------------------------

local items = {
  -- Tempest Keep BOSS LOOT

	-- Trash
	[30026] = {boss = 1, slot = 6, xtype  = 3, account = 2, cost = 30}, --Bands of the Celestial Archer
	[30029] = {boss = 1, slot = 7, xtype  = 2, account = 2, cost = 30}, --Bark-Gloves of Ancient Wisdom
	[30020] = {boss = 1, slot = 8, xtype  = 1, account = 2, cost = 30}, --Fire-Cord of the Magus
	[30030] = {boss = 1, slot = 8, xtype  = 3, account = 2, cost = 30}, --Girdle of Fallen Stars
	[30024] = {boss = 1, slot = 3, xtype  = 1, account = 2, cost = 30}, --Mantle of the Elven Kings
	[30183] = {boss = 1, cost = 10, account = 2}, --Nether Vortex
	[30028] = {boss = 1, slot = 11, account = 2, cost = 35}, --Seventh Ring of the Tirisfalen
	
	-- Recipes
	[30280] = {boss = 2, account = 2, cost = 10}, --Pattern: Belt of Blasting
	[30302] = {boss = 2, account = 2, cost = 10}, --Pattern: Belt of Deep Shadow
	[30301] = {boss = 2, account = 2, cost = 10}, --Pattern: Belt of Natural Power
	[30303] = {boss = 2, account = 2, cost = 10}, --Pattern: Belt of the Black Eagle
	[30281] = {boss = 2, account = 2, cost = 10}, --Pattern: Belt of the Long Road
	[30282] = {boss = 2, account = 2, cost = 10}, --Pattern: Boots of Blasting
	[30305] = {boss = 2, account = 2, cost = 10}, --Pattern: Boots of Natural Grace
	[30307] = {boss = 2, account = 2, cost = 10}, --Pattern: Boots of the Crimson Hawk
	[30283] = {boss = 2, account = 2, cost = 10}, --Pattern: Boots of the Long Road
	[30306] = {boss = 2, account = 2, cost = 10}, --Pattern: Boots of Utter Darkness
	[30308] = {boss = 2, account = 2, cost = 10}, --Pattern: Hurricane Boots
	[30304] = {boss = 2, account = 2, cost = 10}, --Pattern: Monsoon Belt
	[30321] = {boss = 2, account = 2, cost = 10}, --Plans: Belt of the Guardian
	[30323] = {boss = 2, account = 2, cost = 10}, --Plans: Boots of the Protector
	[30322] = {boss = 2, account = 2, cost = 10}, --Plans: Red Belt of Battle
	[30324] = {boss = 2, account = 2, cost = 10}, --Plans: Red Havoc Boots
	
	-- Al'ar
	[29949] = {boss = 730, slot = 17, xtype  = 14, account = 2, cost = 50}, --Arcanite Steam-Pistol
	[29922] = {boss = 730, slot = 11, account = 2, cost = 35}, --Band of Al'ar
	[29948] = {boss = 730, slot = 15, xtype  = 11, account = 2, cost = 35}, --Claw of the Phoenix
	[29921] = {boss = 730, slot = 5, xtype  = 3, account = 2, cost = 30}, --Fire Crest Breastplate
	[29947] = {boss = 730, slot = 7, xtype  = 2, account = 2, cost = 30}, --Gloves of the Searing Grip
	[29918] = {boss = 730, slot = 6, xtype  = 1, account = 2, cost = 30}, --Mindstorm Wristbands
	[29924] = {boss = 730, slot = 13, xtype  = 7, account = 2, cost = 35}, --Netherbane
	[29920] = {boss = 730, slot = 11, account = 2, cost = 35}, --Phoenix-Ring of Rebirth
	[29925] = {boss = 730, slot = 4, account = 2, cost = 30}, --Phoenix-Wing Cloak
	[29923] = {boss = 730, slot = 18, account = 2, cost = 35}, --Talisman of the Sun King
	[30448] = {boss = 730, slot = 12, account = 2, cost = 35}, --Talon of Al'ar
	[32944] = {boss = 730, slot = 14, xtype  = 11, account = 2, cost = 35}, --Talon of the Phoenix
	[30447] = {boss = 730, slot = 12, account = 2, cost = 35}, --Tome of Fiery Redemption
	
	-- Void Reaver
	[29986] = {boss = 731, slot = 1, xtype  = 1, account = 2, cost = 30}, --Cowl of the Grand Engineer
	[30619] = {boss = 731, slot = 12, account = 2, cost = 35}, --Fel Reaver's Piston
	[29983] = {boss = 731, slot = 1, xtype  = 4, account = 2, cost = 30}, --Fel-Steel Warhelm
	[29984] = {boss = 731, slot = 8, xtype  = 2, account = 2, cost = 30}, --Girdle of Zaetar
	[30248] = {boss = 731, slot = 3, xtype  = 18, account = 2, cost = 40}, --Pauldrons of the Vanquished Champion
	[30249] = {boss = 731, slot = 3, xtype  = 18, account = 2, cost = 40}, --Pauldrons of the Vanquished Defender
	[30250] = {boss = 731, slot = 3, xtype  = 18, account = 2, cost = 40}, --Pauldrons of the Vanquished Hero
	[29985] = {boss = 731, slot = 9, xtype  = 3, account = 2, cost = 30}, --Void Reaver Greaves
	[30450] = {boss = 731, slot = 12, account = 2, cost = 35}, --Warp-Spring Coil
	[32515] = {boss = 731, slot = 6, xtype  = 4, account = 2, cost = 30}, --Wristguards of Determination
	
	-- Solarian
	[32267] = {boss = 732, slot = 10, xtype  = 4, account = 2, cost = 30}, --Boots of the Resilient
	[29981] = {boss = 732, slot = 16, xtype  = 10, account = 2, cost = 50}, --Ethereum Life-Staff
	[29965] = {boss = 732, slot = 8, xtype  = 4, account = 2, cost = 30}, --Girdle of the Righteous Path
	[29950] = {boss = 732, slot = 9, xtype  = 4, account = 2, cost = 30}, --Greaves of the Bloodwarder
	[29962] = {boss = 732, slot = 13, xtype  = 5, account = 2, cost = 35}, --Heartrazor
	[30446] = {boss = 732, slot = 12, account = 2, cost = 35}, --Solarian's Sapphire
	[29977] = {boss = 732, slot = 9, xtype  = 1, account = 2, cost = 30}, --Star-Soul Breeches
	[29951] = {boss = 732, slot = 10, xtype  = 3, account = 2, cost = 30}, --Star-Strider Boots
	[29972] = {boss = 732, slot = 9, xtype  = 1, account = 2, cost = 30}, --Trousers of the Astromancer
	[29966] = {boss = 732, slot = 6, xtype  = 2, account = 2, cost = 30}, --Vambraces of Ending
	[30449] = {boss = 732, slot = 12, account = 2, cost = 35}, --Void Star Talisman
	[29982] = {boss = 732, slot = 17, xtype  = 15, account = 2, cost = 35}, --Wand of the Forgotten Star
	[29976] = {boss = 732, slot = 7, xtype  = 3, account = 2, cost = 30}, --Worldstorm Gauntlets
	
	-- KT
	[32458] = {boss = 733, account = 2, cost = 30}, --Ashes of Al'ar
	[29997] = {boss = 733, slot = 11, account = 2, cost = 35}, --Band of the Ranger-General
	[30236] = {boss = 733, slot = 5, xtype  = 18, account = 2, cost = 40}, --Chestguard of the Vanquished Champion
	[30237] = {boss = 733, slot = 5, xtype  = 18, account = 2, cost = 40}, --Chestguard of the Vanquished Defender
	[30238] = {boss = 733, slot = 5, xtype  = 18, account = 2, cost = 40}, --Chestguard of the Vanquished Hero
	[29990] = {boss = 733, slot = 1, xtype  = 1, account = 2, cost = 30}, --Crown of the Sun
	[29987] = {boss = 733, slot = 7, xtype  = 1, account = 2, cost = 30}, --Gauntlets of the Sun King
	[29995] = {boss = 733, slot = 9, xtype  = 2, account = 2, cost = 30}, --Leggings of Murderous Intent
	[29996] = {boss = 733, slot = 13, xtype  = 8, account = 2, cost = 35}, --Rod of the Sun King
	[29992] = {boss = 733, slot = 4, account = 2, cost = 30}, --Royal Cloak of the Sunstriders
	[29998] = {boss = 733, slot = 7, xtype  = 4, account = 2, cost = 30}, --Royal Gauntlets of Silvermoon
	[29991] = {boss = 733, slot = 9, xtype  = 3, account = 2, cost = 30}, --Sunhawk Leggings
	[29989] = {boss = 733, slot = 4, account = 2, cost = 30}, --Sunshower Light Cloak
	[29994] = {boss = 733, slot = 4, account = 2, cost = 30}, --Thalassian Wildercloak
	[29988] = {boss = 733, slot = 16, xtype  = 10, account = 2, cost = 50}, --The Nexus Key
	[29993] = {boss = 733, slot = 16, xtype  = 6, account = 2, cost = 50}, --Twinblade of the Phoenix
	[32405] = {boss = 733, account = 2, cost = 35}, --Verdant Sphere
}
--"Slots" : 
		-- "0" : "?"
		-- "1" : Head 
		-- "2" : Neck
		-- "3" : Shoulder
		-- "4" : Back
		-- "5" : Chest
		-- "6" : Wrist
		-- "7" : Hands
		-- "8" : Waist
		-- "9" : Legs
		-- "10" : Feet
		-- "11" : Finger
		-- "12" : Trinket

		-- "13" : One-Hand
		-- "14" : Main Hand
		-- "15" : Off Hand
		-- "16" : Two-Hand
		-- "17" : Ranged
		-- "18" : Held in Off-hand

-- "Types" : 
		-- "0" : "?"
		-- "1" : Cloth
		-- "2" : Leather
		-- "3" : Mail
		-- "4" : Plate

		-- "5" : Dagger
		-- "6" : Sword
		-- "7" : Axe
		-- "8" : Mace
		-- "9" : Polearm
		-- "10" : Staff
		-- "11" : Fist Weapon
		-- "12" : Bow
		-- "13" : Crossbow
		-- "14" : Gun
		-- "15" : Wand
		-- "16" : Thrown

		-- "17" : Shield
		-- "18" : Token
		
-- "Class"
		-- 1 = Druid
		-- 2 = Hunter
		-- 3 = Mage
		-- 4 = Paladin
		-- 5 = Priest
		-- 6 = Rogue
		-- 7 = Shaman
		-- 8 = Warlock
		-- 9 = Warrior
------------------------------------------------------------

-- export items
ADDON.InitGroup.Items = ADDON.InitGroup.Items or {}
for k,v in pairs(items) do
  ADDON.InitGroup.Items[k] = v
end
items = nil
