local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Annarinn",
    [ 3] = "Capecrusader",
    [ 4] = "Daddygodsu",
    [ 5] = "Darkhourz",
    [ 6] = "Escs",
    [ 7] = "Gaylestrum",
    [ 8] = "Ironflurry",
    [ 9] = "Jazzmene",
    [10] = "Lachý",
    [11] = "Lightspire",
    [12] = "Maevice",
    [13] = "Mayaell",
    [14] = "Melindara",
    [15] = "Morarin",
    [16] = "Randala",
    [17] = "Rosemondon",
    [18] = "Shazam",
    [19] = "Slayingfreak",
    [20] = "Snoweyes",
    [21] = "Soullina",
    [22] = "Talasame",
    [23] = "Television",
    [24] = "Thesoap",
    [25] = "Undercookd",
  },
  kills = {
    {boss = 1107, timestamp = "2022-10-18 19:53", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 1110, timestamp = "2022-10-18 20:01", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 1116, timestamp = "2022-10-18 20:12", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 1118, timestamp = "2022-10-18 20:23", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 1111, timestamp = "2022-10-18 20:32", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 1108, timestamp = "2022-10-18 20:38", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 1120, timestamp = "2022-10-18 20:50", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 1117, timestamp = "2022-10-18 20:59", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 1112, timestamp = "2022-10-18 21:17", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 1115, timestamp = "2022-10-18 21:27", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 1113, timestamp = "2022-10-18 21:38", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 1109, timestamp = "2022-10-18 21:49", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 1121, timestamp = "2022-10-18 22:00", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 1119, timestamp = "2022-10-18 22:21", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 1114, timestamp = "2022-10-18 22:33", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-10-18_Naxx",
  drops = {
    {player = 21, timestamp = "2022-10-18 19:49", item = 40412, cost = "", costType = "MS", name = "Ousted Bead Necklace"}, -- Soullina
    {player =  5, timestamp = "2022-10-18 19:54", item = 39706, cost = "", costType = "MS", name = "Sabatons of Sudden Reprisal"}, -- Darkhourz
    {player =  7, timestamp = "2022-10-18 19:54", item = 39712, cost = "", costType = "MS", name = "Gemmed Wand of the Nerubians"}, -- Gaylestrum
    {player = 16, timestamp = "2022-10-18 19:55", item = 39722, cost = "", costType = "MS", name = "Swarm Bindings"}, -- Randala
    {player = 13, timestamp = "2022-10-18 19:56", item = 40064, cost = "", costType = "MS", name = "Thunderstorm Amulet"}, -- Mayaell
    {player = 19, timestamp = "2022-10-18 20:03", item = 39723, cost = "", costType = "MS", name = "Fire-Scorched Greathelm"}, -- Slayingfreak
    {player = 16, timestamp = "2022-10-18 20:04", item = 39756, cost = "", costType = "MS", name = "Tunic of Prejudice"}, -- Randala
    {player = 11, timestamp = "2022-10-18 20:05", item = 40108, cost = "", costType = "MS", name = "Seized Beauty"}, -- Lightspire
    {player = 15, timestamp = "2022-10-18 20:13", item = 40254, cost = "", costType = "MS", name = "Cloak of Averted Crisis"}, -- Morarin
    {player = 14, timestamp = "2022-10-18 20:14", item = 39759, cost = "", costType = "MS", name = "Ablative Chitin Girdle"}, -- Melindara
    {player = 25, timestamp = "2022-10-18 20:15", item = 39767, cost = "", costType = "MS", name = "Undiminished Battleplate"}, -- Undercookd
    {player = 12, timestamp = "2022-10-18 20:17", item = 40408, cost = "", costType = "MS", name = "Haunting Call"}, -- Maevice
    {player = 23, timestamp = "2022-10-18 20:25", item = 40107, cost = "", costType = "MS", name = "Sand-Worn Band"}, -- Television
    {player =  6, timestamp = "2022-10-18 20:26", item = 40259, cost = "", costType = "MS", name = "Waistguard of Divine Grace"}, -- Escs
    {player = 18, timestamp = "2022-10-18 20:28", item = 40272, cost = "", costType = "OS", name = "Girdle of the Gambit"}, -- Shazam
    {player = 16, timestamp = "2022-10-18 20:33", item = 40254, cost = "", costType = "MS", name = "Cloak of Averted Crisis"}, -- Randala
    {player =  6, timestamp = "2022-10-18 20:34", item = 40274, cost = "", costType = "MS", name = "Bracers of Liberation"}, -- Escs
    {player = 17, timestamp = "2022-10-18 20:34", item = 40289, cost = "", costType = "OS", name = "Sympathetic Amice"}, -- Rosemondon
    {player = 24, timestamp = "2022-10-18 20:35", item = 40280, cost = "", costType = "MS", name = "Origin of Nightmares"}, -- Thesoap
    {player =  1, timestamp = "2022-10-18 20:40", item = 40627, cost = "", costType = "MS", name = "Breastplate of the Lost Vanquisher"}, -- Anatyr
    {player =  7, timestamp = "2022-10-18 20:41", item = 40273, cost = "", costType = "MS", name = "Surplus Limb"}, -- Gaylestrum
    {player = 10, timestamp = "2022-10-18 20:42", item = 40639, cost = 120, costType = "Solstice Points", name = "Mantle of the Lost Vanquisher"}, -- Lachý
    {player =  3, timestamp = "2022-10-18 20:51", item = 40636, cost = "", costType = "MS", name = "Legplates of the Lost Vanquisher"}, -- Capecrusader
    {player = 11, timestamp = "2022-10-18 20:52", item = 40634, cost = "", costType = "MS", name = "Legplates of the Lost Conqueror"}, -- Lightspire
    {player =  6, timestamp = "2022-10-18 20:53", item = 40298, cost = "", costType = "MS", name = "Faceguard of the Succumbed"}, -- Escs
    {player = 21, timestamp = "2022-10-18 20:53", item = 40304, cost = "", costType = "MS", name = "Headpiece of Fungal Bloom"}, -- Soullina
    {player =  8, timestamp = "2022-10-18 21:00", item = 40065, cost = "", costType = "MS", name = "Fool's Trial"}, -- Ironflurry
    {player = 16, timestamp = "2022-10-18 21:01", item = 40200, cost = "", costType = "MS", name = "Belt of Potent Chanting"}, -- Randala
    {player =  5, timestamp = "2022-10-18 21:01", item = 40189, cost = "", costType = "MS", name = "Angry Dread"}, -- Darkhourz
    {player =  8, timestamp = "2022-10-18 21:02", item = 40186, cost = "", costType = "MS", name = "Thrusting Bands"}, -- Ironflurry
    {player =  9, timestamp = "2022-10-18 21:19", item = 40236, cost = "", costType = "MS", name = "Serene Echoes"}, -- Jazzmene
    {player =  2, timestamp = "2022-10-18 21:19", item = 40250, cost = "", costType = "MS", name = "Aged Winter Cloak"}, -- Annarinn
    {player =  1, timestamp = "2022-10-18 21:20", item = 40206, cost = "", costType = "MS", name = "Iron-Spring Jumpers"}, -- Anatyr
    {player = 21, timestamp = "2022-10-18 21:28", item = 40639, cost = 120, costType = "Solstice Points", name = "Mantle of the Lost Vanquisher"}, -- Soullina
    {player = 23, timestamp = "2022-10-18 21:28", item = 40637, cost = "", costType = "MS", name = "Mantle of the Lost Conqueror"}, -- Television
    {player = 10, timestamp = "2022-10-18 21:31", item = 40246, cost = "", costType = "MS", name = "Boots of Impetuous Ideals"}, -- Lachý
    {player = 21, timestamp = "2022-10-18 21:33", item = 40409, cost = "", costType = "MS", name = "Boots of the Escaped Captive"}, -- Soullina
    {player = 21, timestamp = "2022-10-18 21:40", item = 40071, cost = "", costType = "MS", name = "Chains of Adoration"}, -- Soullina
    {player = 12, timestamp = "2022-10-18 21:41", item = 40326, cost = "", costType = "MS", name = "Boots of Forlorn Wishes"}, -- Maevice
    {player = 19, timestamp = "2022-10-18 21:41", item = 40318, cost = "", costType = "MS", name = "Legplates of Double Strikes"}, -- Slayingfreak
    {player = 14, timestamp = "2022-10-18 21:43", item = 40306, cost = "", costType = "MS", name = "Bracers of the Unholy Knight"}, -- Melindara
    {player =  2, timestamp = "2022-10-18 21:49", item = 40250, cost = "", costType = "MS", name = "Aged Winter Cloak"}, -- Annarinn
    {player = 15, timestamp = "2022-10-18 21:50", item = 40335, cost = "", costType = "MS", name = "Touch of Horror"}, -- Morarin
    {player =  6, timestamp = "2022-10-18 21:51", item = 40332, cost = "", costType = "MS", name = "Abetment Bracers"}, -- Escs
    {player = 17, timestamp = "2022-10-18 22:01", item = 40627, cost = "", costType = "MS", name = "Breastplate of the Lost Vanquisher"}, -- Rosemondon
    {player = 18, timestamp = "2022-10-18 22:02", item = 40626, cost = "", costType = "MS", name = "Breastplate of the Lost Protector"}, -- Shazam
    {player =  1, timestamp = "2022-10-18 22:03", item = 40343, cost = "", costType = "MS", name = "Armageddon"}, -- Anatyr
    {player =  9, timestamp = "2022-10-18 22:03", item = 40286, cost = "", costType = "OS", name = "Mantle of the Corrupted"}, -- Jazzmene
    {player =  9, timestamp = "2022-10-18 22:34", item = 40375, cost = "", costType = "MS", name = "Ring of Decaying Beauty"}, -- Jazzmene
    {player =  8, timestamp = "2022-10-18 22:34", item = 40362, cost = "", costType = "MS", name = "Gloves of Fast Reactions"}, -- Ironflurry
    {player =  5, timestamp = "2022-10-18 22:35", item = 40371, cost = "", costType = "MS", name = "Bandit's Insignia"}, -- Darkhourz
    {player = 11, timestamp = "2022-10-18 22:35", item = 40382, cost = "", costType = "MS", name = "Soul of the Dead"}, -- Lightspire
    {player =  4, timestamp = "2022-10-18 22:36", item = 40385, cost = "", costType = "MS", name = "Envoy of Mortality"}, -- Daddygodsu
    {player = 17, timestamp = "2022-10-18 22:37", item = 40399, cost = "", costType = "MS", name = "Signet of Manifested Pain"}, -- Rosemondon
    {player = 22, timestamp = "2022-10-18 22:38", item = 40402, cost = "", costType = "MS", name = "Last Laugh"}, -- Talasame
    {player = 18, timestamp = "2022-10-18 22:39", item = 40632, cost = "", costType = "MS", name = "Crown of the Lost Protector"}, -- Shazam
  },
  pulls = {
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 1107,
      timestamp = "2022-10-18 19:50",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 1110,
      timestamp = "2022-10-18 19:58",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 1116,
      timestamp = "2022-10-18 20:10",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 1118,
      timestamp = "2022-10-18 20:20",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 1111,
      timestamp = "2022-10-18 20:29",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 1108,
      timestamp = "2022-10-18 20:36",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 1120,
      timestamp = "2022-10-18 20:44",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 1117,
      timestamp = "2022-10-18 20:56",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 1112,
      timestamp = "2022-10-18 21:14",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 1115,
      timestamp = "2022-10-18 21:23",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 1113,
      timestamp = "2022-10-18 21:35",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 1109,
      timestamp = "2022-10-18 21:43",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 1121,
      timestamp = "2022-10-18 21:56",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 1119,
      timestamp = "2022-10-18 22:16",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 1114,
      timestamp = "2022-10-18 22:24",
    },
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
