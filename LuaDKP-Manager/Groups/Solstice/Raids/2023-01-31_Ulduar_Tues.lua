local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Aythya",
    [ 2] = "Batriisyaa",
    [ 3] = "Bravend",
    [ 4] = "Darkhourz",
    [ 5] = "Darknightxx",
    [ 6] = "Dvoom",
    [ 7] = "Eillowee",
    [ 8] = "Gaylestrum",
    [ 9] = "Hempoil",
    [10] = "Ironflurry",
    [11] = "Jazzmene",
    [12] = "Jáxshow",
    [13] = "Lachý",
    [14] = "Mayaell",
    [15] = "Meyhém",
    [16] = "Mykos",
    [17] = "Perceliath",
    [18] = "Phal",
    [19] = "Rosemondon",
    [20] = "Ryzus",
    [21] = "Slayingfreak",
    [22] = "Snoweyes",
    [23] = "Soullina",
    [24] = "Television",
    [25] = "Wreckel",
  },
  kills = {
    {boss = 744, timestamp = "2023-01-31 19:48", players = {1,2,3,4,5,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 747, timestamp = "2023-01-31 20:01", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 746, timestamp = "2023-01-31 20:09", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 745, timestamp = "2023-01-31 20:31", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 749, timestamp = "2023-01-31 20:55", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 748, timestamp = "2023-01-31 21:06", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 750, timestamp = "2023-01-31 21:34", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 751, timestamp = "2023-01-31 21:48", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 752, timestamp = "2023-01-31 22:32", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2023-01-31_Ulduar_Tues",
  drops = {
    {player =  5, timestamp = "2023-01-31 19:50", item = 45110, cost = "", costType = "MS", name = "Titanguard"}, -- Darknightxx
    {player = 20, timestamp = "2023-01-31 19:50", item = 45107, cost = "", costType = "MS", name = "Iron Riveted War Helm"}, -- Ryzus
    {player = 14, timestamp = "2023-01-31 19:53", item = 45541, cost = "", costType = "MS", name = "Shroud of Alteration"}, -- Mayaell
    {player =  2, timestamp = "2023-01-31 20:01", item = 45250, cost = "", costType = "MS", name = "Crazed Construct Ring"}, -- Batriisyaa
    {player = 19, timestamp = "2023-01-31 20:02", item = 45260, cost = "", costType = "OS", name = "Boots of Hasty Revival"}, -- Rosemondon
    {player =  9, timestamp = "2023-01-31 20:02", item = 45253, cost = "", costType = "MS", name = "Mantle of Wavering Calm"}, -- Hempoil
    {player = 21, timestamp = "2023-01-31 20:10", item = 45137, cost = "", costType = "OS", name = "Veranus' Bane"}, -- Slayingfreak
    {player = 11, timestamp = "2023-01-31 20:11", item = 45147, cost = "", costType = "MS", name = "Guiding Star"}, -- Jazzmene
    {player =  6, timestamp = "2023-01-31 20:11", item = 45510, cost = "", costType = "MS", name = "Libram of Discord"}, -- Dvoom
    {player = 15, timestamp = "2023-01-31 20:32", item = 45157, cost = "", costType = "MS", name = "Cindershard Ring"}, -- Meyhém
    {player =  7, timestamp = "2023-01-31 20:32", item = 45168, cost = 100, costType = "Solstice Points", name = "Pyrelight Circle"}, -- Eillowee
    {player = 24, timestamp = "2023-01-31 20:56", item = 45268, cost = "", costType = "OS", name = "Gloves of the Pythonic Guardian"}, -- Television
    {player = 10, timestamp = "2023-01-31 20:56", item = 45265, cost = "", costType = "MS", name = "Shoulderpads of the Monolith"}, -- Ironflurry
    {player =  3, timestamp = "2023-01-31 20:57", item = 45273, cost = "", costType = "MS", name = "Handwraps of Plentiful Recovery"}, -- Bravend
    {player =  4, timestamp = "2023-01-31 21:07", item = 45224, cost = 90, costType = "Solstice Points", name = "Drape of the Lithe"}, -- Darkhourz
    {player =  3, timestamp = "2023-01-31 21:08", item = 45234, cost = "", costType = "MS", name = "Rapture"}, -- Bravend
    {player =  7, timestamp = "2023-01-31 21:09", item = 45240, cost = "", costType = "MS", name = "Raiments of the Iron Council"}, -- Eillowee
    {player =  5, timestamp = "2023-01-31 21:35", item = 45319, cost = "", costType = "MS", name = "Cloak of the Makers"}, -- Darknightxx
    {player =  6, timestamp = "2023-01-31 21:35", item = 45436, cost = "", costType = "OS", name = "Libram of the Resolute"}, -- Dvoom
    {player =  8, timestamp = "2023-01-31 21:35", item = 45437, cost = "", costType = "MS", name = "Runescribed Blade"}, -- Gaylestrum
    {player =  9, timestamp = "2023-01-31 21:49", item = 45632, cost = 120, costType = "Solstice Points", name = "Breastplate of the Wayward Conqueror"}, -- Hempoil
    {player = 22, timestamp = "2023-01-31 21:49", item = 45633, cost = 120, costType = "Solstice Points", name = "Breastplate of the Wayward Protector"}, -- Snoweyes
    {player = 16, timestamp = "2023-01-31 21:50", item = 45453, cost = "", costType = "MS", name = "Winter's Icy Embrace"}, -- Mykos
    {player = 18, timestamp = "2023-01-31 22:34", item = 45638, cost = 120, costType = "Solstice Points", name = "Crown of the Wayward Conqueror"}, -- Phal
    {player =  1, timestamp = "2023-01-31 22:35", item = 45639, cost = "", costType = "MS", name = "Crown of the Wayward Protector"}, -- Aythya
  },
  pulls = {
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 744,
      timestamp = "2023-01-31 19:40",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 744,
      timestamp = "2023-01-31 19:46",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 747,
      timestamp = "2023-01-31 19:57",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 746,
      timestamp = "2023-01-31 20:03",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 745,
      timestamp = "2023-01-31 20:19",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 745,
      timestamp = "2023-01-31 20:26",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 749,
      timestamp = "2023-01-31 20:51",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 748,
      timestamp = "2023-01-31 21:01",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 750,
      timestamp = "2023-01-31 21:14",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 750,
      timestamp = "2023-01-31 21:19",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 750,
      timestamp = "2023-01-31 21:24",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 750,
      timestamp = "2023-01-31 21:29",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 751,
      timestamp = "2023-01-31 21:42",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 752,
      timestamp = "2023-01-31 22:01",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 752,
      timestamp = "2023-01-31 22:08",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 752,
      timestamp = "2023-01-31 22:13",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
      },
      boss = 752,
      timestamp = "2023-01-31 22:26",
    },
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
