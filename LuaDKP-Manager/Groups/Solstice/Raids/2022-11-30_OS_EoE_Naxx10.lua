local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Batriisyaa",
    [ 2] = "Bravend",
    [ 3] = "Darkhourz",
    [ 4] = "Eillowee",
    [ 5] = "Fellador",
    [ 6] = "Floordaddy",
    [ 7] = "Gaylestrum",
    [ 8] = "Hempoil",
    [ 9] = "Ironflurry",
    [10] = "Jazzmene",
    [11] = "Jormandir",
    [12] = "Lachý",
    [13] = "Lolhaha",
    [14] = "Mayaell",
    [15] = "Melindara",
    [16] = "Meyhém",
    [17] = "Notsneakie",
    [18] = "Phal",
    [19] = "Rosemondon",
    [20] = "Rudigar",
    [21] = "Ryzus",
    [22] = "Snoweyes",
    [23] = "Soullina",
    [24] = "Talasame",
    [25] = "Television",
    [26] = "Twiche",
    [27] = "Xuduku",
  },
  kills = {
    {boss = 742, timestamp = "2022-11-30 20:25", players = {1,2,3,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26}},
    {boss = 734, timestamp = "2022-11-30 20:48", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27}},
  },
  description = "2022-11-30_OS_EoE_Naxx10",
  drops = {
    {player =  3, timestamp = "2022-11-30 20:28", item = 44006, cost = 90, costType = "Solstice Points", name = "Obsidian Greathelm"}, -- Darkhourz
    {player =  9, timestamp = "2022-11-30 20:28", item = 40630, cost = "", costType = "MS", name = "Gauntlets of the Lost Vanquisher"}, -- Ironflurry
    {player = 23, timestamp = "2022-11-30 20:28", item = 40433, cost = "", costType = "MS", name = "Wyrmrest Band"}, -- Soullina
    {player = 17, timestamp = "2022-11-30 20:29", item = 40431, cost = "", costType = "MS", name = "Fury of the Five Flights"}, -- Notsneakie
    {player = 16, timestamp = "2022-11-30 20:29", item = 40629, cost = "", costType = "OS", name = "Gauntlets of the Lost Protector"}, -- Meyhém
    {player =  7, timestamp = "2022-11-30 20:30", item = 44002, cost = 90, costType = "Solstice Points", name = "The Sanctum's Flowing Vestments"}, -- Gaylestrum
    {player =  2, timestamp = "2022-11-30 20:49", item = 40194, cost = 90, costType = "Solstice Points", name = "Blanketing Robes of Snow"}, -- Bravend
    {player = 22, timestamp = "2022-11-30 20:49", item = 40543, cost = "", costType = "MS", name = "Blue Aspect Helm"}, -- Snoweyes
    {player =  2, timestamp = "2022-11-30 20:50", item = 40532, cost = "", costType = "MS", name = "Living Ice Crystals"}, -- Bravend
  },
  pulls = {
    {
      players = {
        1,
        2,
        3,
        4,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
      },
      boss = 742,
      timestamp = "2022-11-30 19:40",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
      },
      boss = 742,
      timestamp = "2022-11-30 19:46",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
      },
      boss = 742,
      timestamp = "2022-11-30 19:51",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
      },
      boss = 742,
      timestamp = "2022-11-30 19:57",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
      },
      boss = 742,
      timestamp = "2022-11-30 20:02",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
      },
      boss = 742,
      timestamp = "2022-11-30 20:08",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
      },
      boss = 742,
      timestamp = "2022-11-30 20:14",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
      },
      boss = 742,
      timestamp = "2022-11-30 20:21",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
        27,
      },
      boss = 734,
      timestamp = "2022-11-30 20:40",
    },
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
