local _, ADDON = ...

ADDON.InitGroup = {}
ADDON.Groups = ADDON.Groups or {}
ADDON.Groups["Solstice"] = ADDON.InitGroup

ADDON.InitGroup.Files = {
  "2022-10-12_Naxx2",
  "2022-10-18_Naxx",
}
