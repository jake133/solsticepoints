local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Anatyr",
    [ 2] = "Annarinn",
    [ 3] = "Capecrusader",
    [ 4] = "Daddygodsu",
    [ 5] = "Darkhourz",
    [ 6] = "Floreina",
    [ 7] = "Gaylestrum",
    [ 8] = "Ironflurry",
    [ 9] = "Jazzmene",
    [10] = "Jixzo",
    [11] = "Lachý",
    [12] = "Lightspire",
    [13] = "Maevice",
    [14] = "Mayaell",
    [15] = "Melindara",
    [16] = "Pallylove",
    [17] = "Rosemondon",
    [18] = "Selstorm",
    [19] = "Slayingfreak",
    [20] = "Snoweyes",
    [21] = "Talasame",
    [22] = "Television",
    [23] = "Wreckel",
    [24] = "Wyen",
    [25] = "Xandies",
  },
  kills = {
    {boss = 1113, timestamp = "2022-10-12 20:00", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,20,21,22,23,24,25}},
    {boss = 1109, timestamp = "2022-10-12 20:13", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,20,21,22,23,24,25}},
    {boss = 1121, timestamp = "2022-10-12 20:30", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,20,21,22,23,24,25}},
    {boss = 1119, timestamp = "2022-10-12 20:45", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
    {boss = 1114, timestamp = "2022-10-12 21:44", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}},
  },
  description = "2022-10-12_Naxx2",
  drops = {
    {player =  1, timestamp = "2022-10-12 20:01", item = 40074, cost = "", costType = "MS", name = "Strong-Handed Ring"}, -- Anatyr
    {player = 25, timestamp = "2022-10-12 20:01", item = 40327, cost = "", costType = "MS", name = "Girdle of Recuperation"}, -- Xandies
    {player =  8, timestamp = "2022-10-12 20:02", item = 40319, cost = "", costType = "MS", name = "Chestpiece of Suspicion"}, -- Ironflurry
    {player = 16, timestamp = "2022-10-12 20:02", item = 40316, cost = "", costType = "MS", name = "Gauntlets of Guiding Touch"}, -- Pallylove
    {player = 14, timestamp = "2022-10-12 20:14", item = 40341, cost = "", costType = "MS", name = "Shackled Cinch"}, -- Mayaell
    {player = 21, timestamp = "2022-10-12 20:15", item = 40334, cost = "", costType = "MS", name = "Burdened Shoulderplates"}, -- Talasame
    {player = 16, timestamp = "2022-10-12 20:16", item = 40332, cost = "", costType = "MS", name = "Abetment Bracers"}, -- Pallylove
    {player = 24, timestamp = "2022-10-12 20:17", item = 40258, cost = "", costType = "MS", name = "Forethought Talisman"}, -- Wyen
    {player = 24, timestamp = "2022-10-12 20:20", item = 40410, cost = "", costType = "OS", name = "Shadow of the Ghoul"}, -- Wyen
    {player = 24, timestamp = "2022-10-12 20:33", item = 40625, cost = "", costType = "MS", name = "Breastplate of the Lost Conqueror"}, -- Wyen
    {player =  5, timestamp = "2022-10-12 20:33", item = 40627, cost = "", costType = "MS", name = "Breastplate of the Lost Vanquisher"}, -- Darkhourz
    {player =  6, timestamp = "2022-10-12 20:35", item = 40350, cost = "", costType = "MS", name = "Urn of Lost Memories"}, -- Floreina
    {player = 22, timestamp = "2022-10-12 20:35", item = 40345, cost = "", costType = "MS", name = "Broken Promise"}, -- Television
    {player =  8, timestamp = "2022-10-12 20:48", item = 40368, cost = "", costType = "MS", name = "Murder"}, -- Ironflurry
    {player = 25, timestamp = "2022-10-12 20:49", item = 40376, cost = "", costType = "MS", name = "Legwraps of the Defeated Dragon"}, -- Xandies
    {player =  6, timestamp = "2022-10-12 20:51", item = 40380, cost = "", costType = "MS", name = "Gloves of Grandeur"}, -- Floreina
    {player = 17, timestamp = "2022-10-12 21:46", item = 40633, cost = "", costType = "MS", name = "Crown of the Lost Vanquisher"}, -- Rosemondon
    {player = 10, timestamp = "2022-10-12 21:47", item = 40633, cost = "", costType = "MS", name = "Crown of the Lost Vanquisher"}, -- Jixzo
    {player =  9, timestamp = "2022-10-12 21:48", item = 40398, cost = "", costType = "MS", name = "Leggings of Mortal Arrogance"}, -- Jazzmene
    {player = 21, timestamp = "2022-10-12 21:49", item = 40400, cost = "", costType = "MS", name = "Wall of Terror"}, -- Talasame
    {player = 15, timestamp = "2022-10-12 21:49", item = 40384, cost = "", costType = "MS", name = "Betrayer of Humanity"}, -- Melindara
    {player = 13, timestamp = "2022-10-12 22:24", item = 40432, cost = "", costType = "MS", name = "Illustration of the Dragon Soul"}, -- Maevice
    {player = 18, timestamp = "2022-10-12 22:24", item = 44006, cost = "", costType = "MS", name = "Obsidian Greathelm"}, -- Selstorm
    {player =  7, timestamp = "2022-10-12 22:25", item = 40630, cost = "", costType = "MS", name = "Gauntlets of the Lost Vanquisher"}, -- Gaylestrum
    {player = 20, timestamp = "2022-10-12 22:26", item = 40629, cost = "", costType = "MS", name = "Gauntlets of the Lost Protector"}, -- Snoweyes
    {player = 15, timestamp = "2022-10-12 22:26", item = 40446, cost = "", costType = "MS", name = "Dragon Brood Legguards"}, -- Melindara
    {player = 17, timestamp = "2022-10-12 22:27", item = 44002, cost = "", costType = "MS", name = "The Sanctum's Flowing Vestments"}, -- Rosemondon
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
