local _, ADDON = ...

------------------------------------------------------------

local items = {
  -- SSC BOSS LOOT

  -- Trash
  [30027] = {boss =    1, slot =10, xtype =4, account = 2, cost = 30, note = ""}, -- Boots of Courage Unending 
  [30183] = {boss =    1, account = 2, cost = 10, note = ""}, -- Nether Vortex 
  [30022] = {boss =    1, slot =2, account = 2, cost = 35, note = ""}, -- Pendant of the Perilous 
  [30025] = {boss =    1, slot =17, xtype =16, account = 2, cost = 35, note = ""}, -- Serpentshrine Shuriken 
  [30620] = {boss =    1, slot =12, account = 2, cost = 35, note = ""}, -- Spyglass of the Hidden Fleet 
  [30023] = {boss =    1, account = 2, cost = 35, note = ""}, -- Totem of the Maelstrom 
  [30021] = {boss =    1, slot =16, xtype =10, account = 2, cost = 50, note = ""}, -- Wildfury Greatstaff 

  -- Recipes
  [30280] = {boss =    2, account = 2, cost = 10, note = ""}, -- Pattern: Belt of Blasting 
  [30302] = {boss =    2, account = 2, cost = 10, note = ""}, -- Pattern: Belt of Deep Shadow 
  [30301] = {boss =    2, account = 2, cost = 10, note = ""}, -- Pattern: Belt of Natural Power 
  [30303] = {boss =    2, account = 2, cost = 10, note = ""}, -- Pattern: Belt of the Black Eagle 
  [30281] = {boss =    2, account = 2, cost = 10, note = ""}, -- Pattern: Belt of the Long Road 
  [30282] = {boss =    2, account = 2, cost = 10, note = ""}, -- Pattern: Boots of Blasting 
  [30305] = {boss =    2, account = 2, cost = 10, note = ""}, -- Pattern: Boots of Natural Grace 
  [30307] = {boss =    2, account = 2, cost = 10, note = ""}, -- Pattern: Boots of the Crimson Hawk 
  [30283] = {boss =    2, account = 2, cost = 10, note = ""}, -- Pattern: Boots of the Long Road 
  [30306] = {boss =    2, account = 2, cost = 10, note = ""}, -- Pattern: Boots of Utter Darkness 
  [30308] = {boss =    2, account = 2, cost = 10, note = ""}, -- Pattern: Hurricane Boots 
  [30304] = {boss =    2, account = 2, cost = 10, note = ""}, -- Pattern: Monsoon Belt 
  [30321] = {boss =    2, account = 2, cost = 10, note = ""}, -- Plans: Belt of the Guardian 
  [30323] = {boss =    2, account = 2, cost = 10, note = ""}, -- Plans: Boots of the Protector 
  [30322] = {boss =    2, account = 2, cost = 10, note = ""}, -- Plans: Red Belt of Battle 
  [30324] = {boss =    2, account = 2, cost = 10, note = ""}, -- Plans: Red Havoc Boots 

  -- Hydross
  [33055] = {boss = 623, slot =11, account = 2, cost = 35, note = ""}, -- Band of Vile Aggression
  [30047] = {boss = 623, slot =6, xtype =3, account = 2, cost = 30, note = ""}, -- Blackfathom Warbands
  [30050] = {boss = 623, slot =10, xtype =1, account = 2, cost = 30, note = ""}, -- Boots of the Shifting Nightmare
  [30048] = {boss = 623, slot =1, xtype =4, account = 2, cost = 30, note = ""}, -- Brighthelm of Justice
  [30049] = {boss = 623, slot =18, account = 2, cost = 35, note = ""}, -- Fathomstone
  [30051] = {boss = 623, account = 2, cost = 35, note = ""}, -- Idol of the Crescent Goddess
  [30664] = {boss = 623, slot =12, account = 2, cost = 35, class =  1, note = ""}, -- Living Root of the Wildheart
  [30053] = {boss = 623, slot =3, xtype =4, account = 2, cost = 30, note = ""}, -- Pauldrons of the Wardancer
  [30054] = {boss = 623, slot =5, xtype =3, account = 2, cost = 30, note = ""}, -- Ranger-General's Chestguard
  [30052] = {boss = 623, slot =11, account = 2, cost = 35, note = ""}, -- Ring of Lethality
  [30056] = {boss = 623, slot =5, xtype =1, account = 2, cost = 30, note = ""}, -- Robe of Hateful Echoes
  [30629] = {boss = 623, slot =12, account = 2, cost = 35, note = ""}, -- Scarab of Displacement
  [30055] = {boss = 623, slot =3, xtype =2, account = 2, cost = 30, note = ""}, -- Shoulderpads of the Stranger
  [32516] = {boss = 623, slot =6, xtype =1, account = 2, cost = 30, note = ""}, -- Wraps of Purification

  -- Lurker Below
  [30061] = {boss = 624, slot =11, account = 2, cost = 35, note = ""}, -- Ancestral Ring of Conquest
  [30060] = {boss = 624, slot =2, xtype =10, account = 2, cost = 30, note = ""}, -- Boots of Effortless Striking
  [30057] = {boss = 624, slot =6, xtype =4, account = 2, cost = 30, note = ""}, -- Bracers of Eradication
  [30059] = {boss = 624, slot =2, account = 2, cost = 35, note = ""}, -- Choker of Animalistic Fury
  [30064] = {boss = 624, slot =8, xtype =1, account = 2, cost = 30, note = ""}, -- Cord of Screaming Terrors
  [30665] = {boss = 624, slot =12, account = 2, cost = 35, note = ""}, -- Earring of Soulful Meditation
  [30065] = {boss = 624, slot =5, xtype =140, account = 2, cost = 30, note = ""}, -- Glowing Breastplate of Truth
  [30062] = {boss = 624, slot =6, xtype =2, account = 2, cost = 30, note = ""}, -- Grove-Bands of Remulos
  [30063] = {boss = 624, account = 2, cost = 35, note = ""}, -- Libram of Absolute Truth
  [30058] = {boss = 624, slot =14, xtype =8, account = 2, cost = 35, note = ""}, -- Mallet of the Tides
  [30066] = {boss = 624, slot =10, xtype =3, account = 2, cost = 30, note = ""}, -- Tempest-Strider Boots
  [33054] = {boss = 624, slot =11, account = 2, cost = 35, note = ""}, -- The Seal of Danzalar
  [30067] = {boss = 624, slot =10, xtype =1, account = 2, cost = 30, note = ""}, -- Velvet Boots of the Guardian

  -- Leotheras
  [30097] = {boss = 625, slot =3, xtype =3, account = 2, cost = 30, note = ""}, -- Coral-Barbed Shoulderpads
  [30095] = {boss = 625, slot =14, xtype =6, account = 2, cost = 35, note = ""}, -- Fang of the Leviathan
  [30096] = {boss = 625, slot =8, xtype =4, account = 2, cost = 30, note = ""}, -- Girdle of the Invulnerable
  [30239] = {boss = 625, slot =7, xtype =18, account = 2, cost = 40, note = ""}, -- Gloves of the Vanquished Champion
  [30240] = {boss = 625, slot =7, xtype =18, account = 2, cost = 40, note = ""}, -- Gloves of the Vanquished Defender
  [30241] = {boss = 625, slot =7, xtype =18, account = 2, cost = 40, note = ""}, -- Gloves of the Vanquished Hero
  [30092] = {boss = 625, slot =10, xtype =2, account = 2, cost = 30, note = ""}, -- Orca-Hide Boots
  [30091] = {boss = 625, slot = 6, xtype =3, account = 2, cost = 30, note = ""}, -- True-Aim Stalker Bands
  [30627] = {boss = 625, slot =12, account = 2, cost = 35, note = ""}, -- Tsunami Talisman

  -- Karathress
  [30101] = {boss = 626, slot =5, xtype =2, account = 2, cost = 30, note = ""}, -- Bloodsea Brigand's Vest
  [30663] = {boss = 626, slot =12, account = 2, cost = 35, class = 7, note = ""}, -- Fathom-Brooch of the Tidewalker
  [30099] = {boss = 626, slot =2, account = 2, cost = 35, note = ""}, -- Frayed Tether of the Drowned
  [30245] = {boss = 626, slot =9, xtype =18, account = 2, cost = 40, note = ""}, -- Leggings of the Vanquished Champion
  [30246] = {boss = 626, slot =9, xtype =18, account = 2, cost = 40, note = ""}, -- Leggings of the Vanquished Defender
  [30247] = {boss = 626, slot =9, xtype =18, account = 2, cost = 40, note = ""}, -- Leggings of the Vanquished Hero
  [30626] = {boss = 626, slot =12, account = 2, cost = 35, note = ""}, -- Sextant of Unstable Currents
  [30100] = {boss = 626, slot =10, xtype =1, account = 2, cost = 30, note = ""}, -- Soul-Strider Boots
  [30090] = {boss = 626, slot =16, xtype =8, account = 2, cost = 50, note = ""}, -- World Breaker

  -- Morogrim
  [33058] = {boss = 627, slot =11, account = 2, cost = 35, note = ""}, -- Band of the Vigilant
  [30068] = {boss = 627, slot =8, xtype =3, account = 2, cost = 30, note = ""}, -- Girdle of the Tidal Call
  [30075] = {boss = 627, slot =5, xtype =2, account = 2, cost = 30, note = ""}, -- Gnarled Chestpiece of the Ancients
  [30079] = {boss = 627, slot =3, xtype =3, account = 2, cost = 30, note = ""}, -- Illidari Shoulderpads
  [30080] = {boss = 627, slot =17, xtype =15, account = 2, cost = 35, note = ""}, -- Luminescent Rod of the Naaru
  [30085] = {boss = 627, slot =3, xtype =3, account = 2, cost = 30, note = ""}, -- Mantle of the Tireless Tracker
  [30084] = {boss = 627, slot =3, xtype =4, account = 2, cost = 30, note = ""}, -- Pauldrons of the Argent Sentinel
  [30008] = {boss = 627, slot =2, account = 2, cost = 35, note = ""}, -- Pendant of the Lost Ages
  [30098] = {boss = 627, slot =4, xtype =1, account = 2, cost = 30, note = ""}, -- Razor-Scale Battlecloak
  [30083] = {boss = 627, slot =11, account = 2, cost = 35, note = ""}, -- Ring of Sundered Souls
  [30720] = {boss = 627, slot =12, account = 2, class = 3, cost = 35, note = ""}, -- Serpent-Coil Braid
  [30082] = {boss = 627, slot =13, xtype =6, account = 2, cost = 35, note = ""}, -- Talon of Azshara
  [30081] = {boss = 627, slot =10, xtype =4, account = 2, cost = 30, note = ""}, -- Warboots of Obliteration

  -- Lady Vashj
  [30106] = {boss = 628, slot =8, xtype =2, account = 2, cost = 30, note = ""}, -- Belt of One-Hundred Deaths
  [30104] = {boss = 628, slot =10, xtype =3, account = 2, cost = 30, note = ""}, -- Cobra-Lash Boots
  [30110] = {boss = 628, slot =11, account = 2, cost = 35, note = ""}, -- Coral Band of the Revived
  [30103] = {boss = 628, slot =13, xtype =5, account = 2, cost = 35, note = ""}, -- Fang of Vashj
  [30112] = {boss = 628, slot =7, xtype =4, account = 2, cost = 30, note = ""}, -- Glorious Gauntlets of Crestfall
  [30242] = {boss = 628, slot =1, xtype =18, account = 2, cost = 40, note = ""}, -- Helm of the Vanquished Champion
  [30243] = {boss = 628, slot =1, xtype =18, account = 2, cost = 40, note = ""}, -- Helm of the Vanquished Defender
  [30244] = {boss = 628, slot =1, xtype =18, account = 2, cost = 40, note = ""}, -- Helm of the Vanquished Hero
  [30102] = {boss = 628, slot =5, xtype =4, account = 2, cost = 30, note = ""}, -- Krakken-Heart Breastplate
  [30108] = {boss = 628, slot =14, xtype =8, account = 2, cost = 35, note = ""}, -- Lightfathom Scepter
  [30621] = {boss = 628, slot =12, account = 2, cost = 35, note = ""}, -- Prism of Inner Calm
  [30109] = {boss = 628, slot =1, account = 2, cost = 35, note = ""}, -- Ring of Endless Coils
  [30111] = {boss = 628, slot =3, xtype =2, account = 2, cost = 30, note = ""}, -- Runetotem's Mantle
  [30105] = {boss = 628, slot =17, xtype =12, account = 2, cost = 50, note = ""}, -- Serpent Spine Longbow
  [30107] = {boss = 628, slot =15, xtype =1, account = 2, cost = 30, note = ""}, -- Vestments of the Sea-Witch
}
--"Slots" : 
		-- "0" : "?"
		-- "1" : Head 
		-- "2" : Neck
		-- "3" : Shoulder
		-- "4" : Back
		-- "5" : Chest
		-- "6" : Wrist
		-- "7" : Hands
		-- "8" : Waist
		-- "9" : Legs
		-- "10" : Feet
		-- "11" : Finger
		-- "12" : Trinket

		-- "13" : One-Hand
		-- "14" : Main Hand
		-- "15" : Off Hand
		-- "16" : Two-Hand
		-- "17" : Ranged
		-- "18" : Held in Off-hand

-- "Types" : 
		-- "0" : "?"
		-- "1" : Cloth
		-- "2" : Leather
		-- "3" : Mail
		-- "4" : Plate

		-- "5" : Dagger
		-- "6" : Sword
		-- "7" : Axe
		-- "8" : Mace
		-- "9" : Polearm
		-- "10" : Staff
		-- "11" : Fist Weapon
		-- "12" : Bow
		-- "13" : Crossbow
		-- "14" : Gun
		-- "15" : Wand
		-- "16" : Thrown

		-- "17" : Shield
		-- "18" : Token
		
-- "Class"
		-- 1 = Druid
		-- 2 = Hunter
		-- 3 = Mage
		-- 4 = Paladin
		-- 5 = Priest
		-- 6 = Rogue
		-- 7 = Shaman
		-- 8 = Warlock
		-- 9 = Warrior

-- not certain where this is used, but keeping incase it ends up useful
-- 1 = Tank, 2 = Melee, 3 = Ranged, 4 = Heal


------------------------------------------------------------

-- export items
ADDON.InitGroup.Items = ADDON.InitGroup.Items or {}
for k,v in pairs(items) do
  ADDON.InitGroup.Items[k] = v
end
items = nil
