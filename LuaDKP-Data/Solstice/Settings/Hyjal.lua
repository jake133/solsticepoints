local _, ADDON = ...

------------------------------------------------------------

local items = {
  -- MOUNT HYJAL LOOT

  -- trash
	[32285] = {boss = 1, account = 3,cost = 10 }, --Design: Flashing Crimson Spinel
	[32295] = {boss = 1, account = 3,cost = 10 }, --Design: Mystic Lionseye
	[32296] = {boss = 1, account = 3,cost = 10 }, --Design: Great Lionseye
	[32307] = {boss = 1, account = 3,cost = 10 }, --Design: Veiled Pyrestone
	[32297] = {boss = 1, account = 3,cost = 10 }, --Design: Sovereign Shadowsong Amethyst
	[32298] = {boss = 1, account = 3,cost = 10 }, --Design: Shifting Shadowsong Amethyst
	[32303] = {boss = 1, account = 3,cost = 10 }, --Design: Inscribed Pyrestone
	[32289] = {boss = 1, account = 3,cost = 10 }, --Design: Stormy Empyrean Sapphire

	[32590] = {boss = 1, slot = 4, account = 3, cost = 60 }, --Nethervoid Cloak
	[34010] = {boss = 1, slot = 4, account = 3,cost = 60 }, --Pepe's Shroud of Pacification
	[32609] = {boss = 1, slot = 10, xtype = 1, account = 3, cost = 60 }, --Boots of the Divine Light
	[32592] = {boss = 1, slot = 5, xtype = 3, account = 3, cost = 60 }, --Chestguard of Relentless Storms
	[32591] = {boss = 1, slot = 2, account = 3, cost = 70 }, --Choker of Serrated Blades
	[32589] = {boss = 1, slot = 2, account = 3, cost = 70 }, --Hellfire-Encased Pendant
	[34009] = {boss = 1, slot = 14, xtype = 8, account = 3,cost = 70 }, --Hammer of judgement
	[32946] = {boss = 1, slot = 14, xtype = 11, account = 3,cost = 70 }, --Claw of Molten Fury
	[32945] = {boss = 1, slot = 15, xtype = 11, account = 3,cost = 70 }, --Fist of Molten Fury
		
	-- Rage Winterchill
	[30870] = {boss = 618, slot = 6, xtype = 1, account = 3,cost = 60 }, --Cuffs of Devastation
	[30871] = {boss = 618, slot = 6, xtype = 1, account = 3,cost = 60 }, --Bracers of Martyrdom
	[30865] = {boss = 618, slot = 13, xtype = 6, account = 3,cost = 70 }, --Tracker's Blade
	[30863] = {boss = 618, slot = 6, xtype = 2, account = 3,cost = 60 }, --Deadly Cuffs
	[30868] = {boss = 618, slot = 6, xtype = 2, account = 3,cost = 60 }, --Rejuvenating Bracers
	[30873] = {boss = 618, slot = 10, xtype = 3, account = 3,cost = 60 }, --Stillwater Boots
	[30864] = {boss = 618, slot = 6, xtype = 3, account = 3,cost = 60 }, --Bracers of the Pathfinder
	[30869] = {boss = 618, slot = 6, xtype = 3, account = 3,cost = 60 }, --Howling Wind Bracers
	[30872] = {boss = 618, slot = 18, account = 3,cost = 70 }, --Chronicle of Dark Secrets
	[30866] = {boss = 618, slot = 3, xtype = 4, account = 3,cost = 60 }, --Blood-stained Pauldrons
	[30861] = {boss = 618, slot = 6, xtype = 4, account = 3,cost = 60 }, --Furious Shackles
	[30862] = {boss = 618, slot = 6, xtype = 4, account = 3,cost = 60 }, --Blessed Adamantite Bracers
	
	-- Anetheron
	[30885] = {boss = 619, slot = 10, xtype = 1, account = 3,cost = 60 }, --Archbishop's Slippers
	[30884] = {boss = 619, slot = 3, xtype = 1, account = 3,cost = 60 }, --Hatefury Mantle
	[30888] = {boss = 619, slot = 8, xtype = 1, account = 3,cost = 60 }, --Anetheron's Noose
	[30886] = {boss = 619, slot = 10, xtype = 2, account = 3,cost = 60 }, --Enchanted Leather Sandals
	[30879] = {boss = 619, slot = 8, xtype = 2, account = 3,cost = 60 }, --Don Alejandro's Money Belt
	[30887] = {boss = 619, slot = 5, xtype = 3, account = 3,cost = 60 }, --Golden Links of Restoration
	[30880] = {boss = 619, slot = 10, xtype = 3, account = 3,cost = 60 }, --Quickstrider Moccasins
	[30874] = {boss = 619, slot = 13, xtype = 6, account = 3,cost = 70 }, --The Unbreakable Will
	[30881] = {boss = 619, slot = 13, xtype = 6, account = 3,cost = 70 }, --Blade of Infamy
	[30878] = {boss = 619, slot = 3, xtype = 4, account = 3,cost = 60 }, --Glimmering Steel Mantle
	[30882] = {boss = 619, slot = 15, xtype = 17, account = 3,cost = 70 }, --Bastion of Light
	[30883] = {boss = 619, slot = 16, xtype = 10, account = 3,cost = 100 }, --Pillar of Ferocity
	
	-- Kaz'rogal'
	[30894] = {boss = 620, slot = 10, xtype = 1, account = 3,cost = 60 }, --Blue Suede Shoes
	[30916] = {boss = 620, slot = 9, xtype = 1, account = 3,cost = 60 }, --Leggings of Channeled Elements
	[30895] = {boss = 620, slot = 8, xtype = 1, account = 3,cost = 60 }, --Angelista's Sash
	[30891] = {boss = 620, slot = 10, xtype = 2, account = 3,cost = 60 }, --Black Featherlight Boots
	[30917] = {boss = 620, slot = 3, xtype = 2, account = 3,cost = 60 }, --Razorfury Mantle
	[30914] = {boss = 620, slot = 8, xtype = 2, account = 3,cost = 60 }, --Belt of the Crescent Moon
	[30893] = {boss = 620, slot = 9, xtype = 3, account = 3,cost = 60 }, --Sun-touched Chain Leggings
	[30892] = {boss = 620, slot = 3, xtype = 3, account = 3,cost = 60 }, --Beast-tamer's Shoulders
	[30919] = {boss = 620, slot = 8, xtype = 3, account = 3,cost = 60 }, --Valestalker Girdle
	[30918] = {boss = 620, slot = 14, xtype = 8, account = 3,cost = 70 }, --Hammer of Atonement
	[30915] = {boss = 620, slot = 8, xtype = 4, account = 3,cost = 60 }, --Belt of Seething Fury
	[30889] = {boss = 620, slot = 15, xtype = 17, account = 3,cost = 70 }, --Kaz'rogal's Hardened Heart
	
	-- Azgalor
	[31092] = {boss = 621, slot = 7, xtype = 18, account = 3,cost = 80 }, --Gloves of the Forgotten Conqueror
	[31093] = {boss = 621, slot = 7, xtype = 18, account = 3,cost = 80 }, --Gloves of the Forgotten Vanquisher
	[31094] = {boss = 621, slot = 7, xtype = 18, account = 3,cost = 80 }, --Gloves of the Forgotten Protector
	[30901] = {boss = 621, slot = 13, xtype = 6, account = 3,cost = 70 }, --Boundless Agony
	[30899] = {boss = 621, slot = 5, xtype = 2, account = 3,cost = 60 }, --Don Rodrigo's Poncho
	[30898] = {boss = 621, slot = 9, xtype = 2, account = 3,cost = 60 }, --Shady Dealer's Pantaloons
	[30900] = {boss = 621, slot = 9, xtype = 3, account = 3,cost = 60 }, --Bow-stitched Leggings
	[30896] = {boss = 621, slot = 5, xtype = 4, account = 3,cost = 60 }, --Glory of the Defender
	[30897] = {boss = 621, slot = 8, xtype = 4, account = 3,cost = 60 }, --Girdle of Hope
	
	-- Archimonde
	[31095] = {boss = 622, slot = 1, xtype = 18, account = 3,cost = 80 }, --Helm of the Forgotten Protector
	[31096] = {boss = 622, slot = 1, xtype = 18, account = 3,cost = 80 }, --Helm of the Forgotten Vanquisher
	[31097] = {boss = 622, slot = 1, xtype = 18, account = 3,cost = 80 }, --Helm of the Forgotten Conqueror
	[30906] = {boss = 622, slot = 17, xtype = 12, account = 3,cost = 100 }, --Bristleblitz Striker
	[30913] = {boss = 622, slot = 5, xtype = 1, account = 3,cost = 60 }, --Robes of Rhonin
	[30912] = {boss = 622, slot = 9, xtype = 1, account = 3,cost = 60 }, --Leggings of Eternity
	[30905] = {boss = 622, slot = 5, xtype = 2, account = 3,cost = 60 }, --Midnight Chestguard
	[30907] = {boss = 622, slot = 5, xtype = 3, account = 3,cost = 60 }, --Mail of Fevered Pursuit
	[30911] = {boss = 622, slot = 18, account = 3,cost = 70 }, --Scepter of Purification
	[30910] = {boss = 622, slot = 14, xtype = 6, account = 3,cost = 70 }, --Tempest of Chaos
	[30904] = {boss = 622, slot = 5, xtype = 4, account = 3,cost = 60 }, --Savior's Grasp
	[30903] = {boss = 622, slot = 9, xtype = 4, account = 3,cost = 60 }, --Legguards of Endless Rage
	[30909] = {boss = 622, slot = 15, xtype = 17, account = 3,cost = 70 }, --Antonidas's Aegis of Rapt Concentration
	[30908] = {boss = 622, slot = 16, xtype = 10, account = 3,cost = 100 }, --Apostle of Argus
	[30902] = {boss = 622, slot = 16, xtype = 6, account = 3,cost = 100 }, --Cataclysm's Edge


}
--"Slots" : 
		-- "0" : "?"
		-- "1" : Head 
		-- "2" : Neck
		-- "3" : Shoulder
		-- "4" : Back
		-- "5" : Chest
		-- "6" : Wrist
		-- "7" : Hands
		-- "8" : Waist
		-- "9" : Legs
		-- "10" : Feet
		-- "11" : Finger
		-- "12" : Trinket

		-- "13" : One-Hand
		-- "14" : Main Hand
		-- "15" : Off Hand
		-- "16" : Two-Hand
		-- "17" : Ranged
		-- "18" : Held in Off-hand

-- "Types" : 
		-- "0" : "?"
		-- "1" : Cloth
		-- "2" : Leather
		-- "3" : Mail
		-- "4" : Plate

		-- "5" : Dagger
		-- "6" : Sword
		-- "7" : Axe
		-- "8" : Mace
		-- "9" : Polearm
		-- "10" : Staff
		-- "11" : Fist Weapon
		-- "12" : Bow
		-- "13" : Crossbow
		-- "14" : Gun
		-- "15" : Wand
		-- "16" : Thrown

		-- "17" : Shield
		-- "18" : Token
------------------------------------------------------------

-- export items
ADDON.InitGroup.Items = ADDON.InitGroup.Items or {}
for k,v in pairs(items) do
  ADDON.InitGroup.Items[k] = v
end
items = nil
