local _, ADDON = ...

------------------------------------------------------------

-- Console/Definitions.lua:
-- X CLASS
-- 1 = Druid, 2 = Hunter, 3 = Mage, 4 = Paladin, 5 = Priest, 6 = Rogue, 7 = Shaman, 8 = Warlock, 9 = Warrior

-- ROLE
-- 1 = Tank, 2 = Melee, 3 = Ranged, 4 = Heal

------------------------------------------------------------

local roster = {
--["PlayerName"] = {xclass = 1, role = 1, nickname = "NameWithoutSpecialCharacters"},
	["Airfoil"] = {xclass = 1, role = 4},
	["Aythya"] = {xclass = 2, role = 3},
	["Badreams"] = {xclass = 3, role = 3},
	["Blackclaws"] = {xclass = 9, role = 2},
	["Boneappletea"] = {xclass = 8, role = 3},
	["Brickk"] = {xclass = 9, role = 1},
	["Cailyse"] = {xclass = 6, role = 2},
	["Capecrusader"] = {xclass = 6, role = 2},
	["Ceruwolfe"] = {xclass = 4, role = 4},
	["Chalula"] = {xclass = 1, role = 2},
	["Dagarle"] = {xclass = 4, role = 2},
	["Damelion"] = {xclass = 4, role = 4},
	["Eillowee"] = {xclass = 8, role = 3},
	["Elorn"] = {xclass = 2, role = 3},
	["Estrellita"] = {xclass = 5, role = 4},
	["Frodes"] = {xclass = 1, role = 4},
	["Furiozah"] = {xclass = 9, role = 2},
	["Gaylestrum"] = {xclass = 3, role = 3},
	["Ghostbeard"] = {xclass = 7, role = 3},
	["Grayskyy"] = {xclass = 6, role = 2},
	["Grimdoom"] = {xclass = 2, role = 3},
	["Heidie"] = {xclass = 7, role = 4},
	["Inxi"] = {xclass = 1, role = 2},
	["Iohe"] = {xclass = 1, role = 2},
	["Noritotes"] = {xclass = 7, role = 2},
	["Irontitann"] = {xclass = 4, role = 1},
	["Jazzmene"] = {xclass = 5, role = 4},
	["J�stic�"] = {xclass = 7, role = 4},
	["Kalisae"] = {xclass = 1, role = 1},
	["Kharlamagne"] = {xclass = 4, role = 4},
	["Kyrika"] = {xclass = 8, role = 3},
	["Lach�"] = {xclass = 3, role = 3},
	["Lilybet"] = {xclass = 4, role = 4},
	["Luethien"] = {xclass = 5, role = 4},
	["Pencilvestor"] = {xclass = 6, role = 2},
	["Robotchickin"] = {xclass = 3, role = 3},
	["Roragorn"] = {xclass = 1, role = 3},
	["Sadistia"] = {xclass = 5, role = 3},
	["Sinniaa"] = {xclass = 1, role = 4},
	["Sinwave"] = {xclass = 7, role = 4},
	["Skeeta"] = {xclass = 2, role = 3},
	["Slayingfreak"] = {xclass = 9, role = 1},
	["Soulbane"] = {xclass = 8, role = 3},
	["Superboots"] = {xclass = 5, role = 4},
	["Sylaramynd"] = {xclass = 1, role = 4},
	["Glancen"] = {xclass = 5, role = 4},
	["Velladonna"] = {xclass = 8, role = 3},
	["Snoweyes"] = {xclass = 2, role = 3},
	["Wootzz"] = {xclass = 8, role = 3},
	["Xandies"] = {xclass = 7, role = 3},
	["Xremi"] = {xclass = 4, role = 4},
	["Ma�rlyn"] = {xclass = 8, role = 3},
	["Hateshift"] = {xclass = 8, role = 3},
}

------------------------------------------------------------

-- export tables
ADDON.InitGroup.Roster = roster
