local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Boozzé",
    [ 2] = "Hallowedsoul",
    [ 3] = "Jazzmene",
    [ 4] = "Lachý",
    [ 5] = "Noritotes",
    [ 6] = "Phal",
    [ 7] = "Sagger",
    [ 8] = "Snoweyes",
    [ 9] = "Soullina",
    [10] = "Television",
  },
  kills = {
    {boss = 1107, timestamp = "2022-12-20 20:00", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1110, timestamp = "2022-12-20 20:06", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1116, timestamp = "2022-12-20 20:12", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1118, timestamp = "2022-12-20 20:21", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1111, timestamp = "2022-12-20 20:26", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1108, timestamp = "2022-12-20 20:30", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1120, timestamp = "2022-12-20 20:38", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1113, timestamp = "2022-12-20 20:46", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1109, timestamp = "2022-12-20 20:57", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1121, timestamp = "2022-12-20 21:05", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1117, timestamp = "2022-12-20 21:18", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1112, timestamp = "2022-12-20 21:25", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1115, timestamp = "2022-12-20 21:32", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1119, timestamp = "2022-12-20 21:38", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1114, timestamp = "2022-12-20 21:52", players = {1,2,3,4,5,6,7,8,9,10}},
  },
  description = "2022-12-20_NAXX10",
  drops = {
    {player =  7, timestamp = "2022-12-20 20:02", item = 39146, cost = "", costType = "MS", name = "Collar of Dissolution"}, -- Sagger
    {player =  4, timestamp = "2022-12-20 20:13", item = 39229, cost = "", costType = "MS", name = "Embrace of the Spider"}, -- Lachý
    {player =  5, timestamp = "2022-12-20 20:17", item = 39468, cost = "", costType = "OS", name = "The Stray"}, -- Noritotes
    {player =  5, timestamp = "2022-12-20 20:21", item = 39275, cost = "", costType = "OS", name = "Contagion Gloves"}, -- Noritotes
    {player =  5, timestamp = "2022-12-20 20:27", item = 39283, cost = "", costType = "OS", name = "Putrescent Bands"}, -- Noritotes
    {player =  5, timestamp = "2022-12-20 20:31", item = 40623, cost = "", costType = "OS", name = "Spaulders of the Lost Protector"}, -- Noritotes
    {player =  1, timestamp = "2022-12-20 20:38", item = 39293, cost = "", costType = "OS", name = "Blackened Legplates of Feugen"}, -- Boozzé
    {player =  5, timestamp = "2022-12-20 20:39", item = 40620, cost = "", costType = "OS", name = "Leggings of the Lost Protector"}, -- Noritotes
    {player =  5, timestamp = "2022-12-20 20:47", item = 39307, cost = "", costType = "OS", name = "Iron Rings of Endurance"}, -- Noritotes
    {player =  1, timestamp = "2022-12-20 20:49", item = 39297, cost = "", costType = "OS", name = "Cloak of Darkening"}, -- Boozzé
    {player =  1, timestamp = "2022-12-20 20:57", item = 39369, cost = "", costType = "OS", name = "Sabatons of Deathlike Gloom"}, -- Boozzé
    {player =  1, timestamp = "2022-12-20 20:58", item = 39390, cost = "", costType = "OS", name = "Resurgent Phantom Bindings"}, -- Boozzé
    {player =  7, timestamp = "2022-12-20 21:06", item = 40612, cost = "", costType = "OS", name = "Chestguard of the Lost Vanquisher"}, -- Sagger
    {player =  5, timestamp = "2022-12-20 21:25", item = 39240, cost = "", costType = "OS", name = "Noth's Curse"}, -- Noritotes
    {player =  7, timestamp = "2022-12-20 21:26", item = 39234, cost = "", costType = "OS", name = "Plague-Impervious Boots"}, -- Sagger
    {player =  7, timestamp = "2022-12-20 21:33", item = 39245, cost = "", costType = "OS", name = "Demise"}, -- Sagger
    {player =  5, timestamp = "2022-12-20 21:33", item = 39254, cost = "", costType = "OS", name = "Saltarello Shoes"}, -- Noritotes
    {player =  2, timestamp = "2022-12-20 21:53", item = 39426, cost = "", costType = "MS", name = "Wand of the Archlich"}, -- Hallowedsoul
    {player =  3, timestamp = "2022-12-20 21:53", item = 40616, cost = "", costType = "OS", name = "Helm of the Lost Conqueror"}, -- Jazzmene
    {player =  1, timestamp = "2022-12-20 21:53", item = 40622, cost = "", costType = "OS", name = "Spaulders of the Lost Conqueror"}, -- Boozzé
    {player =  7, timestamp = "2022-12-20 21:55", item = 44569, cost = "", costType = "MS", name = "Key to the Focusing Iris"}, -- Sagger
    {player =  5, timestamp = "2022-12-20 21:55", item = 39399, cost = "", costType = "OS", name = "Helm of the Vast Legions"}, -- Noritotes
  },
  pulls = {
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1107,
      timestamp = "2022-12-20 19:58",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1110,
      timestamp = "2022-12-20 20:04",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1116,
      timestamp = "2022-12-20 20:10",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1118,
      timestamp = "2022-12-20 20:18",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1111,
      timestamp = "2022-12-20 20:24",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1108,
      timestamp = "2022-12-20 20:28",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1120,
      timestamp = "2022-12-20 20:34",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1113,
      timestamp = "2022-12-20 20:44",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1109,
      timestamp = "2022-12-20 20:51",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1121,
      timestamp = "2022-12-20 21:02",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1117,
      timestamp = "2022-12-20 21:15",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1112,
      timestamp = "2022-12-20 21:22",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1115,
      timestamp = "2022-12-20 21:28",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1119,
      timestamp = "2022-12-20 21:34",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1114,
      timestamp = "2022-12-20 21:43",
    },
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
