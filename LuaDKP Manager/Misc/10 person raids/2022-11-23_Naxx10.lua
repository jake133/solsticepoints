local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Bellamira",
    [ 2] = "Darkhourz",
    [ 3] = "Gaylestrum",
    [ 4] = "Ironflurry",
    [ 5] = "Lachý",
    [ 6] = "Phal",
    [ 7] = "Rosemondon",
    [ 8] = "Shämröck",
    [ 9] = "Snoweyes",
    [10] = "Soullina",
  },
  kills = {
    {boss = 1107, timestamp = "2022-11-23 19:52", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1110, timestamp = "2022-11-23 20:00", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1116, timestamp = "2022-11-23 20:09", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1118, timestamp = "2022-11-23 20:24", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1111, timestamp = "2022-11-23 20:33", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1108, timestamp = "2022-11-23 20:40", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1120, timestamp = "2022-11-23 20:51", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1117, timestamp = "2022-11-23 21:03", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1112, timestamp = "2022-11-23 21:12", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1115, timestamp = "2022-11-23 21:21", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1113, timestamp = "2022-11-23 21:42", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1109, timestamp = "2022-11-23 21:55", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1121, timestamp = "2022-11-23 22:20", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1119, timestamp = "2022-11-23 22:27", players = {1,2,3,4,5,6,7,8,9,10}},
    {boss = 1114, timestamp = "2022-11-23 22:39", players = {1,2,3,4,5,6,7,8,9,10}},
  },
  description = "2022-11-23_Naxx10",
  drops = {
    {player =  6, timestamp = "2022-11-23 19:53", item = 39139, cost = "", costType = "OS", name = "Ravaging Sabatons"}, -- Phal
    {player =  2, timestamp = "2022-11-23 20:00", item = 39195, cost = "", costType = "MS", name = "Bracers of Lost Sentiments"}, -- Darkhourz
    {player =  8, timestamp = "2022-11-23 20:01", item = 39215, cost = "", costType = "OS", name = "Boots of the Follower"}, -- Shämröck
    {player =  2, timestamp = "2022-11-23 20:10", item = 39225, cost = "", costType = "MS", name = "Cloak of Armed Strife"}, -- Darkhourz
    {player =  3, timestamp = "2022-11-23 20:11", item = 39229, cost = "", costType = "MS", name = "Embrace of the Spider"}, -- Gaylestrum
    {player =  8, timestamp = "2022-11-23 20:13", item = 39472, cost = "", costType = "OS", name = "Chain of Latent Energies"}, -- Shämröck
    {player =  3, timestamp = "2022-11-23 20:17", item = 39472, cost = "", costType = "MS", name = "Chain of Latent Energies"}, -- Gaylestrum
    {player =  8, timestamp = "2022-11-23 20:25", item = 39274, cost = "", costType = "OS", name = "Retcher's Shoulderpads"}, -- Shämröck
    {player =  7, timestamp = "2022-11-23 20:41", item = 40621, cost = "", costType = "OS", name = "Leggings of the Lost Vanquisher"}, -- Rosemondon
    {player =  2, timestamp = "2022-11-23 20:41", item = 39344, cost = "", costType = "MS", name = "Slayer of the Lifeless"}, -- Darkhourz
    {player =  2, timestamp = "2022-11-23 20:52", item = 39291, cost = "", costType = "MS", name = "Torment of the Banished"}, -- Darkhourz
    {player =  8, timestamp = "2022-11-23 20:52", item = 40620, cost = "", costType = "MS", name = "Leggings of the Lost Protector"}, -- Shämröck
    {player = 10, timestamp = "2022-11-23 21:04", item = 39237, cost = "", costType = "OS", name = "Spaulders of Resumed Battle"}, -- Soullina
    {player =  8, timestamp = "2022-11-23 21:04", item = 39242, cost = "", costType = "OS", name = "Robes of Hoarse Breaths"}, -- Shämröck
    {player =  7, timestamp = "2022-11-23 21:13", item = 39255, cost = "", costType = "MS", name = "Staff of the Plague Beast"}, -- Rosemondon
    {player =  8, timestamp = "2022-11-23 21:22", item = 40623, cost = "", costType = "MS", name = "Spaulders of the Lost Protector"}, -- Shämröck
    {player = 10, timestamp = "2022-11-23 21:43", item = 39299, cost = "", costType = "OS", name = "Rapid Attack Gloves"}, -- Soullina
    {player =  8, timestamp = "2022-11-23 21:43", item = 39308, cost = "", costType = "OS", name = "Girdle of Lenience"}, -- Shämröck
    {player =  6, timestamp = "2022-11-23 21:56", item = 39345, cost = "", costType = "OS", name = "Girdle of the Ascended Phantom"}, -- Phal
    {player =  8, timestamp = "2022-11-23 21:57", item = 39392, cost = "", costType = "OS", name = "Veiled Amulet of Life"}, -- Shämröck
    {player =  6, timestamp = "2022-11-23 22:40", item = 39393, cost = "", costType = "OS", name = "Claymore of Ancient Power"}, -- Phal
    {player =  8, timestamp = "2022-11-23 22:40", item = 40611, cost = "", costType = "MS", name = "Chestguard of the Lost Protector"}, -- Shämröck
    {player =  5, timestamp = "2022-11-23 22:41", item = 44569, cost = "", costType = "MS", name = "Key to the Focusing Iris"}, -- Lachý
    {player =  6, timestamp = "2022-11-23 22:41", item = 39403, cost = "", costType = "OS", name = "Helm of the Unsubmissive"}, -- Phal
    {player =  8, timestamp = "2022-11-23 22:42", item = 39421, cost = "", costType = "MS", name = "Gem of Imprisoned Vassals"}, -- Shämröck
    {player =  7, timestamp = "2022-11-23 22:43", item = 39423, cost = "", costType = "OS", name = "Hammer of the Astral Plane"}, -- Rosemondon
    {player =  8, timestamp = "2022-11-23 22:43", item = 40617, cost = "", costType = "MS", name = "Helm of the Lost Protector"}, -- Shämröck
  },
  pulls = {
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1107,
      timestamp = "2022-11-23 19:49",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1110,
      timestamp = "2022-11-23 19:58",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1116,
      timestamp = "2022-11-23 20:07",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1118,
      timestamp = "2022-11-23 20:19",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1111,
      timestamp = "2022-11-23 20:30",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1108,
      timestamp = "2022-11-23 20:36",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1120,
      timestamp = "2022-11-23 20:46",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1117,
      timestamp = "2022-11-23 20:59",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1112,
      timestamp = "2022-11-23 21:09",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1115,
      timestamp = "2022-11-23 21:17",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1113,
      timestamp = "2022-11-23 21:39",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1109,
      timestamp = "2022-11-23 21:48",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1121,
      timestamp = "2022-11-23 22:06",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1121,
      timestamp = "2022-11-23 22:17",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1119,
      timestamp = "2022-11-23 22:23",
    },
    {
      players = {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
      },
      boss = 1114,
      timestamp = "2022-11-23 22:29",
    },
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
