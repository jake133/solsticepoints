local _, ADDON = ...

------------------------------------------------------------

local masterDKP = {
  Maevice = 457,
  Ryzus = 318,
  Deverin = 62,
  Snoweyes = 541,
  ["Dëmonbeta"] = 84,
  Dillster = 31,
  Sagger = 92,
  Phal = 531,
  Scumbags = 16,
  Darkhourz = 544,
  Slayingfreak = 1074,
  Jormandir = 101,
  Anatyr = 123,
  Dunsmar = 235,
  Ogdru = 628,
  Jazzmene = 554,
  Lorri = 47,
  Thesoap = 65,
  Lightspire = 100,
  Kekett = 340,
  Bravend = 376,
  Birdlove = 0,
  Xandies = 98,
  Ironflurry = 273,
  Rudigar = 702,
  Handofmight = 25,
  ["Lachý"] = 314,
  Escs = 65,
  Amylnitrate = 18,
  Mayaell = 410,
  Television = 410,
  ["Jáxshow"] = 614,
  Pinn = 31,
  Wreckel = 235,
  Orcbane = 391,
  Twiche = 35,
  Damelion = 105,
  Hydraxt = 33,
  Pallipuckey = 314,
  Javafanatic = 144,
  Varvo = 159,
  Tarati = 10,
  Rebirthstory = 17,
  Derios = 360,
  Mykos = 363,
  Gaylestrum = 435,
  Hempoil = 359,
  Tocko = 21,
  Morarin = -55,
  Missmancy = 39,
  Darkwillard = 95,
  Eillowee = 270,
  Bellamira = 67,
  ["Meyhém"] = 1163,
  Undercookd = 65,
  Celepyro = 22,
  Xuduku = 135,
  Stormysky = 30,
  Fellador = 52,
  Varv = 0,
  ["Põlaris"] = 59,
  Batriisyaa = 586,
  Tyni = 35,
  Furiozah = 19,
  Chestfly = 78,
  Talasame = 559,
  Strainaar = 71,
  Perceliath = 104,
  Dayme = 402,
  ["Montecrié"] = 283,
  Darknightxx = 142,
  Dozzy = 241,
  Toatsie = 23,
  Aythya = 493,
  Melindara = 402,
  Lolhaha = 35,
  Soullina = 335,
  Bobojin = 404,
  Rosemondon = 747,
  Dvoom = 240,
}

------------------------------------------------------------

-- export masterDKP
ADDON.MasterDKP = masterDKP or {}
