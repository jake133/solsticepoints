local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Aythya",
    [ 2] = "Batriisyaa",
    [ 3] = "Bravend",
    [ 4] = "Dvoom",
    [ 5] = "Eillowee",
    [ 6] = "Gaylestrum",
    [ 7] = "Hempoil",
    [ 8] = "Jazzmene",
    [ 9] = "Kattianna",
    [10] = "Kekett",
    [11] = "Lachý",
    [12] = "Mayaell",
    [13] = "Meyhém",
    [14] = "Orcbane",
    [15] = "Phal",
    [16] = "Rosemondon",
    [17] = "Rudigar",
    [18] = "Ryzus",
    [19] = "Slayingfreak",
    [20] = "Snoweyes",
    [21] = "Soullina",
    [22] = "Television",
    [23] = "Xuduku",
  },
  kills = {
    {boss = 1117, timestamp = "2023-01-10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}},
    {boss = 1112, timestamp = "2023-01-10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}},
    {boss = 1115, timestamp = "2023-01-10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}},
    {boss = 1107, timestamp = "2023-01-10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}},
    {boss = 1110, timestamp = "2023-01-10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}},
    {boss = 1116, timestamp = "2023-01-10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}},
    {boss = 1118, timestamp = "2023-01-10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}},
    {boss = 1111, timestamp = "2023-01-10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}},
    {boss = 1108, timestamp = "2023-01-10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}},
    {boss = 1120, timestamp = "2023-01-10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}},
    {boss = 1113, timestamp = "2023-01-10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}},
    {boss = 1109, timestamp = "2023-01-10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}},
    {boss = 1121, timestamp = "2023-01-10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}},
    {boss = 1119, timestamp = "2023-01-10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}},
    {boss = 1114, timestamp = "2023-01-10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}},
    {boss = 734, timestamp = "2023-01-10", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23}},
  },
  description = "2023-01-11_Tues",
  drops = {
    {player =  2, timestamp = "2023-01-11 16:35", item = 40403, cost = 90, costType = "Solstice Points", name = "Drape of the Deadly Foe"}, -- Batriisyaa
    {player =  3, timestamp = "2023-01-11 16:36", item = 40255, cost = 100, costType = "Solstice Points", name = "Dying Curse"}, -- Bravend
    {player =  4, timestamp = "2023-01-11 16:37", item = 40256, cost = 100, costType = "Solstice Points", name = "Grim Toll"}, -- Dvoom
    {player =  5, timestamp = "2023-01-11 16:37", item = 44577, cost = 120, costType = "Solstice Points", name = "Heroic Key to the Focusing Iris"}, -- Eillowee
  },
  pulls = {
    },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
