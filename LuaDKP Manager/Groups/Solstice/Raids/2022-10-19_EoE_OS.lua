local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
	[1] = "Acribus",
	[2] = "Anatyr",
	[3] = "Capecrusader",
	[4] = "Celepyro",
	[5] = "Daddygodsu",
	[6] = "Darkhourz",
	[7] = "Gaylestrum",
	[8] = "Ironflurry",
	[9] = "Jazzmene",
	[10] = "Lachý",
	[11] = "Maevice",
	[12] = "Mayaell",
	[13] = "Melindara",
	[14] = "Randala",
	[15] = "Rosemondon",
	[16] = "Snoweyes",
	[17] = "Television",
	[18] = "Wreckel",
  },
  kills = {
    {boss = 742, timestamp = "2022-10-26 20:52", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18}},
    {boss = 734, timestamp = "2022-10-26 21:38", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18}},
  },
  description = "2022-10-19_EoE_OS",
  drops = {
  },
  pulls = {
  },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
