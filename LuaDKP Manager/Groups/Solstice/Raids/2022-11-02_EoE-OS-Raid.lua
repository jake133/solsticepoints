local _, ADDON = ...

------------------------------------------------------------

local raid = {
  players = {
    [ 0] = "?",
    [ 1] = "Amylnitrate",
    [ 2] = "Annarinn",
    [ 3] = "Batriisyaa",
    [ 4] = "Buva",
    [ 5] = "Capecrusader",
    [ 6] = "Darkhourz",
    [ 7] = "Dillster",
    [ 8] = "Floordaddy",
    [ 9] = "Gaylestrum",
    [10] = "Holycakes",
    [11] = "Jazzmene",
    [12] = "Jormandir",
    [13] = "Lachý",
    [14] = "Maevice",
    [15] = "Mayaell",
    [16] = "Melindara",
    [17] = "Meyhém",
    [18] = "Phal",
    [19] = "Pinn",
    [20] = "Randala",
    [21] = "Rosemondon",
    [22] = "Ryzus",
    [23] = "Slayingfreak",
    [24] = "Snoweyes",
    [25] = "Talasame",
    [26] = "Television",
    [27] = "Varcy",
    [28] = "Wreckel",
  },
  kills = {
    {boss = 734, timestamp = "2022-11-02 18:58", players = {2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,24,25,26,28}},
    {boss = 742, timestamp = "2022-11-02 20:20", players = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28}},
  },
  description = "2022-11-02_EoE-OS-Raid",
  drops = {
    {player =  3, timestamp = "2022-11-02 19:01", item = 40531, costType = "MS", name = "Mark of Norgannon"}, -- Batriisyaa
    {player = 13, timestamp = "2022-11-02 19:03", item = 40560, costType = "MS", name = "Leggings of the Wanton Spellcaster"}, -- Lachý
    {player = 11, timestamp = "2022-11-02 19:04", item = 40562, costType = "MS", name = "Hood of Rationality"}, -- Jazzmene
    {player = 25, timestamp = "2022-11-02 19:05", item = 40589, costType = "MS", name = "Legplates of Sovereignty"}, -- Talasame
    {player = 26, timestamp = "2022-11-02 20:23", item = 44000, costType = "MS", name = "Dragonstorm Breastplate"}, -- Television
    {player = 15, timestamp = "2022-11-02 20:24", item = 40630, cost = 120, costType = "Solstice Points", name = "Gauntlets of the Lost Vanquisher"}, -- Mayaell
    {player =  2, timestamp = "2022-11-02 20:24", item = 40431, cost = 100, costType = "Solstice Points", name = "Fury of the Five Flights"}, -- Annarinn
    {player = 23, timestamp = "2022-11-02 20:26", item = 40629, costType = "MS", name = "Gauntlets of the Lost Protector"}, -- Slayingfreak
    {player = 27, timestamp = "2022-11-02 20:26", item = 44007, costType = "MS", name = "Headpiece of Reconciliation"}, -- Varcy
    {player = 20, timestamp = "2022-11-02 20:27", item = 40455, costType = "MS", name = "Staff of Restraint"}, -- Randala
  },
  pulls = {
    },
}

------------------------------------------------------------

-- export raid
ADDON.InitGroup.Raids = ADDON.InitGroup.Raids or {}
table.insert(ADDON.InitGroup.Raids, raid)
