local _, ADDON = ...

------------------------------------------------------------

local items = {

  -- High King Maulgar
  [28799] = {boss = 649, slot = 8, xtype = 1, account = 8, cost = 300, note = ""}, -- Belt of Divine Inspiration
  [28795] = {boss = 649, slot = 6, xtype = 4, account = 8, cost = 300, note = ""}, -- Bladespire Warbands
  [28797] = {boss = 649, slot = 4, xtype = 1, account = 8, cost = 300, note = ""}, -- Brute Cloak of the Ogre-Magi
  [28800] = {boss = 649, slot = 16, xtype = 8, account = 8, cost = 300, note = ""}, -- Hammer of the Naaru
  [28796] = {boss = 649, slot = 1, xtype = 2, account = 8, cost = 300, note = ""}, -- Malefic Mask of the Shadows
  [28801] = {boss = 649, slot = 1, xtype = 3, account = 8, cost = 300, note = ""}, -- Maulgar's Warhelm
  [29763] = {boss = 649, slot = 3, xtype = 18, account = 8, cost = 300, note = ""}, -- Pauldrons of the Fallen Champion
  [29764] = {boss = 649, slot = 3, xtype = 18, account = 8, cost = 300, note = ""}, -- Pauldrons of the Fallen Defender
  [29762] = {boss = 649, slot = 3, xtype = 18, account = 8, cost = 300, note = ""}, -- Pauldrons of the Fallen Hero

  -- Gruul the DragonKiller
  [28825] = {boss = 650, xtype = 17, account = 8, cost = 300, note = ""}, -- Aldori Legacy Defender
  [28794] = {boss = 650, slot = 16, xtype = 7, account = 8, cost = 300, note = ""}, -- Axe of the Gronn Lords
  [28802] = {boss = 650, slot = 14, xtype = 6, account = 8, cost = 300, note = ""}, -- Bloodmaw Magus-Blade
  [28804] = {boss = 650, slot = 1, xtype = 1, account = 8, cost = 300, note = ""}, -- Collar of Cho'gall
  [28803] = {boss = 650, slot = 1, xtype = 2, account = 8, cost = 300, note = ""}, -- Cowl of Nature's Breath
  [28830] = {boss = 650, slot = 12, account = 8, cost = 300, note = ""}, -- Dragonspine Trophy
  [28823] = {boss = 650, slot = 12, account = 8, cost = 300, note = ""}, -- Eye of Gruul
  [28824] = {boss = 650, slot = 7, xtype = 4, account = 8, cost = 300, note = ""}, -- Gauntlets of Martial Perfection
  [28827] = {boss = 650, slot = 7, xtype = 3, account = 8, cost = 300, note = ""}, -- Gauntlets of the Dragonslayer
  [28828] = {boss = 650, slot = 8, xtype = 2, account = 8, cost = 300, note = ""}, -- Gronn-Stitched Girdle
  [29766] = {boss = 650, slot = 9, xtype = 18, account = 8, cost = 300, note = ""}, -- Leggings of the Fallen Champion
  [29767] = {boss = 650, slot = 9, xtype = 18, account = 8, cost = 300, note = ""}, -- Leggings of the Fallen Defender
  [29765] = {boss = 650, slot = 9, xtype = 18, account = 8, cost = 300, note = ""}, -- Leggings of the Fallen Hero
  [28826] = {boss = 650, slot = 17, xtype = 16, account = 8, cost = 300, note = ""}, -- Shuriken of Negation
  [28822] = {boss = 650, slot = 2, account = 8, cost = 300, note = ""}, -- Teeth of Gruul
  [28810] = {boss = 650, slot = 10, xtype = 3, account = 8, cost = 300, note = ""}, -- Windshear Boots

}
--"Slots" : 
		-- "0" : "?"
		-- "1" : Head 
		-- "2" : Neck
		-- "3" : Shoulder
		-- "4" : Back
		-- "5" : Chest
		-- "6" : Wrist
		-- "7" : Hands
		-- "8" : Waist
		-- "9" : Legs
		-- "10" : Feet
		-- "11" : Finger
		-- "12" : Trinket

		-- "13" : One-Hand
		-- "14" : Main Hand
		-- "15" : Off Hand
		-- "16" : Two-Hand
		-- "17" : Ranged
		-- "18" : Held in Off-hand

-- "Types" : 
		-- "0" : "?"
		-- "1" : Cloth
		-- "2" : Leather
		-- "3" : Mail
		-- "4" : Plate

		-- "5" : Dagger
		-- "6" : Sword
		-- "7" : Axe
		-- "8" : Mace
		-- "9" : Polearm
		-- "10" : Staff
		-- "11" : Fist Weapon
		-- "12" : Bow
		-- "13" : Crossbow
		-- "14" : Gun
		-- "15" : Wand
		-- "16" : Thrown

		-- "17" : Shield
		-- "18" : Token
		
-- "Class"
		-- 1 = Druid
		-- 2 = Hunter
		-- 3 = Mage
		-- 4 = Paladin
		-- 5 = Priest
		-- 6 = Rogue
		-- 7 = Shaman
		-- 8 = Warlock
		-- 9 = Warrior

-- not certain where this is used, but keeping incase it ends up useful
-- 1 = Tank, 2 = Melee, 3 = Ranged, 4 = Heal

------------------------------------------------------------

-- export items
ADDON.InitGroup.Items = ADDON.InitGroup.Items or {}
for k,v in pairs(items) do
  ADDON.InitGroup.Items[k] = v
end
items = nil
