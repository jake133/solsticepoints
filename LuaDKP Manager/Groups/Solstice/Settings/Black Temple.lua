local _, ADDON = ...

------------------------------------------------------------

local items = {
  -- BLACK TEMPLE LOOT
	-- Trash
	[32590] = {boss = 1, slot = 4, account = 3, cost = 60 }, --Nethervoid Cloak
	[34012] = {boss = 1, slot = 4, account = 3, cost = 60 }, --Shroud of the Final Stand
	[32609] = {boss = 1, slot = 10, xtype = 1, account = 3, cost = 60 }, --Boots of the Divine Light
	[32593] = {boss = 1, slot = 10, xtype = 2, account = 3, cost = 60 }, --Treads of the Den Mother
	[32592] = {boss = 1, slot = 5, xtype = 3, account = 3, cost = 60 }, --Chestguard of Relentless Storms
	[32608] = {boss = 1, slot = 7, xtype = 4, account = 3, cost = 60 }, --Pillager's Gauntlets
	[32606] = {boss = 1, slot = 8, xtype = 4, account = 3, cost = 60 }, --Girdle of the Lightbearer
	[32591] = {boss = 1, slot = 2, account = 3, cost = 70 }, --Choker of Serrated Blades
	[32589] = {boss = 1, slot = 2, account = 3, cost = 70 }, --Hellfire-Encased Pendant

	[32526] = {boss = 1, slot = 11, account = 3, cost = 70 }, --Band of Devastation
	[32528] = {boss = 1, slot = 11, account = 3, cost = 70 }, --Blessed Band of Karabor
	[32527] = {boss = 1, slot = 11, account = 3, cost = 70 }, --Ring of Ancient Knowledge
	[34009] = {boss = 1, slot = 14, xtype = 8, account = 3,cost = 70 }, --Hammer of judgement
	[32943] = {boss = 1, slot = 13, xtype = 8, account = 3, cost = 70 }, --Swiftsteel Bludgeon
	[34011] = {boss = 1, slot = 15, xtype = 17, account = 3, cost = 70 }, --Illidari RuneShield

	-- Recipes
	[32737] = {boss = 2, account = 3, cost = 10 }, --Plans: Swiftsteel Shoulders
	[32739] = {boss = 2, account = 3, cost = 10 }, --Plans: Dawnsteel Shoulders
	[32745] = {boss = 2, account = 3, cost = 10 }, --Pattern: Shoulderpads of Renewed Life
	[32747] = {boss = 2, account = 3, cost = 10 }, --Pattern: Swiftstrike Shoulderss
	[32749] = {boss = 2, account = 3, cost = 10 }, --Pattern: Shoulders of Lightning Reflexes
	[32751] = {boss = 2, account = 3, cost = 10 }, --Pattern: Living Earth Shoulders
	[32753] = {boss = 2, account = 3, cost = 10 }, --Pattern: Swiftheal Mantle
	[32755] = {boss = 2, account = 3, cost = 10 }, --Pattern: Mantle of Nimble Thought
	[32736] = {boss = 2, account = 3, cost = 10 }, --Plans: Swiftsteel Bracers
	[32738] = {boss = 2, account = 3, cost = 10 }, --Plans: Dawnsteel Bracers
	[32744] = {boss = 2, account = 3, cost = 10 }, --Pattern: Bracers of Renewed Life
	[32746] = {boss = 2, account = 3, cost = 10 }, --Pattern: Swiftstrike Bracers
	[32748] = {boss = 2, account = 3, cost = 10 }, --Pattern: Bindings of Lightning Reflexes
	[32750] = {boss = 2, account = 3, cost = 10 }, --Pattern: Living Earth Bindings
	[32752] = {boss = 2, account = 3, cost = 10 }, --Pattern: Swiftheal Wraps
	[32754] = {boss = 2, account = 3, cost = 10 }, --Pattern: Bracers of Nimble Thought

  	-- Naj'entus
	[32232] = {boss = 601, slot = 6, xtype = 4, account = 3, cost = 60 }, --Eternium Shell Bracers
	[32234] = {boss = 601, slot = 7, xtype = 3, account = 3, cost = 60 }, --Fists of Mukoa
	[32236] = {boss = 601, slot = 13, xtype = 7, account = 3, cost = 70 }, --Rising Tide
	[32237] = {boss = 601, slot = 14, xtype = 5, account = 3, cost = 70 }, --The Maelstrom's Fury
	[32238] = {boss = 601, slot = 11, account = 3, cost = 70 }, --Ring of Calming Waves
	[32239] = {boss = 601, slot = 10, xtype = 1, account = 3, cost = 60 }, --Slippers of the Seacaller
	[32240] = {boss = 601, slot = 1, xtype = 2, account = 3, cost = 60 }, --Guise of the Tidal Lurker
	[32241] = {boss = 601, slot = 1, xtype = 3, account = 3, cost = 60 }, --Helm of Soothing Currents
	[32242] = {boss = 601, slot = 10, xtype = 3, account = 3, cost = 60 }, --Boots of Oceanic Fury
	[32243] = {boss = 601, slot = 10, xtype = 4, account = 3, cost = 60 }, --Pearl Inlaid Boots
	[32245] = {boss = 601, slot = 10, xtype = 4, account = 3, cost = 60 }, --Tide-stomper's Greaves
	[32247] = {boss = 601, slot = 11, account = 3, cost = 70 }, --Ring of Captured Storms
	[32248] = {boss = 601, slot = 16, xtype = 9, account = 3, cost = 100 }, --Halberd of Desolation
	[32377] = {boss = 601, slot = 3, xtype = 2, account = 3, cost = 60 }, --Mantle of Darkness

	-- Supremus
	[32250] = {boss = 602, slot = 3, xtype = 4, account = 3, cost = 60 }, --Pauldrons of Abyssal Fury
	[32251] = {boss = 602, slot = 6, xtype = 3, account = 3, cost = 60 }, --Wraps of Precise Flight
	[32252] = {boss = 602, slot = 5, xtype = 2, account = 3, cost = 60 }, --Nether Shadow Tunic
	[32253] = {boss = 602, slot = 17, xtype = 13, account = 3, cost = 100 }, --Legionkiller
	[32254] = {boss = 602, slot = 13, xtype = 7, account = 3, cost = 70 }, --The Brutalizer
	[32255] = {boss = 602, slot = 15, xtype = 17, account = 3, cost = 70 }, --Felstone Bulwark
	[32256] = {boss = 602, slot = 8, xtype = 1, account = 3, cost = 60 }, --Waistwrap of Infinity
	[32257] = {boss = 602, account = 3, cost = 70 }, --Idol of the White Stag
	[32258] = {boss = 602, slot = 8, xtype = 3, account = 3, cost = 60 }, --Naturalist's Preserving Cinch
	[32259] = {boss = 602, slot = 6, xtype = 3, account = 3, cost = 60 }, --Bands of the Coming Storm
	[32260] = {boss = 602, slot = 2, account = 3, cost = 70 }, --Choker of Endless Nightmares
	[32261] = {boss = 602, slot = 11, account = 3, cost = 70 }, --Band of the Abyssal Lord
	[32262] = {boss = 602, slot = 13, xtype = 8, account = 3, cost = 70 }, --Syphon of the Nathrezim

	-- Shade of Akama
	[32263] = {boss = 603, slot = 9, xtype = 4, account = 3, cost = 60 }, --Praetorian's Legguards
	[32264] = {boss = 603, slot = 3, xtype = 3, account = 3, cost = 60 }, --3s of the Hidden Predator
	[32265] = {boss = 603, slot = 8, xtype = 2, account = 3, cost = 60 }, --Shadow-walker's Cord
	[32266] = {boss = 603, slot = 11, account = 3, cost = 70 }, --11 of Deceitful Intent
	[32268] = {boss = 603, slot = 10, xtype = 4, account = 3, cost = 60 }, --Myrmidon's Treads
	[32270] = {boss = 603, slot = 6, xtype = 1, account = 3, cost = 60 }, --Focused Mana Bindings
	[32271] = {boss = 603, slot = 9, xtype = 2, account = 3, cost = 60 }, --Kilt of Immortal Nature
	[32273] = {boss = 603, slot = 3, xtype = 1, account = 3, cost = 60 }, --Amice of Brilliant Light
	[32275] = {boss = 603, slot = 7, xtype = 3, account = 3, cost = 60 }, --Spiritwalker Gauntlets
	[32276] = {boss = 603, slot = 8, xtype = 3, account = 3, cost = 60 }, --Flashfire Girdle
	[32278] = {boss = 603, slot = 7, xtype = 4, account = 3, cost = 60 }, --Grips of Silent Justice
	[32279] = {boss = 603, slot = 6, xtype = 4, account = 3, cost = 60 }, --The Seeker's 6guards
	[32361] = {boss = 603, slot = 18, account = 3, cost = 70 }, --Blind-Seers Icon
	[32513] = {boss = 603, slot = 6, xtype = 1, account = 3, cost = 60 }, --6bands of Divine Influence

	-- Teron Gorefiend
	[32280] = {boss = 604, slot = 7, xtype = 4, account = 3, cost = 60 }, --Gauntlets of Enforcement
	[32323] = {boss = 604, slot = 4, account = 3, cost = 60 }, --Shadowmoon Destroyer's Drape
	[32324] = {boss = 604, slot = 6, xtype = 2, account = 3, cost = 60 }, --Insidious Bands
	[32325] = {boss = 604, slot = 17, xtype = 14, account = 3, cost = 100 }, --Rifle of the Stoic Guardian
	[32326] = {boss = 604, slot = 17, xtype = 16, account = 3, cost = 70 }, --Twisted Blades of Zarak
	[32327] = {boss = 604, slot = 5, xtype = 1, account = 3, cost = 60 }, --Robe of the Shadow Council
	[32328] = {boss = 604, slot = 7, xtype = 2, account = 3, cost = 60 }, --Botanist's Gloves of Growth
	[32329] = {boss = 604, slot = 1, xtype = 1, account = 3, cost = 60 }, --Cowl of Benevolence
	[32330] = {boss = 604, account = 3, cost = 70 }, --Totem of Ancestral Guidance
	[32348] = {boss = 604, slot = 16, xtype = 7, account = 3, cost = 100 }, --Soul Cleaver
	[32510] = {boss = 604, slot = 10, xtype = 3, account = 3, cost = 60 }, --Softstep Boots of Tracking
	[32512] = {boss = 604, slot = 8, xtype = 4, account = 3, cost = 60 }, --Girdle of Lordaeron's Fallen

	-- Gurtogg Bloodboil
	[32269] = {boss = 605, slot = 13, xtype = 5, account = 3, cost = 70 }, --Messenger of Fate
	[32333] = {boss = 605, slot = 8, xtype = 4, account = 3, cost = 60 }, --Girdle of Stability
	[32334] = {boss = 605, slot = 5, xtype = 3, account = 3, cost = 60 }, --Vest of Mounting Assault
	[32335] = {boss = 605, slot = 11, xtype = 11, account = 3, cost = 70 }, --Unstoppable Aggressor's Ring
	[32337] = {boss = 605, slot = 4, xtype = 4, account = 3, cost = 60 }, --Shroud of Forgiveness
	[32338] = {boss = 605, slot = 3, xtype = 1, account = 3, cost = 60 }, --Blood-cursed Shoulderpads
	[32339] = {boss = 605, slot = 8, xtype = 2, account = 3, cost = 60 }, --Belt of Primal Majesty
	[32340] = {boss = 605, slot = 5, xtype = 1, account = 3, cost = 60 }, --Garments of Temperance
	[32341] = {boss = 605, slot = 9, xtype = 4, account = 3, cost = 60 }, --Leggings of Divine Retribution
	[32342] = {boss = 605, slot = 8, xtype = 4, account = 3, cost = 60 }, --Girdle of Mighty Resolve
	[32343] = {boss = 605, slot = 17, xtype = 15, account = 3, cost = 70 }, --Wand of Prismatic Focus
	[32344] = {boss = 605, slot = 16, xtype = 10, account = 3, cost = 100 }, --Staff of Immaculate Recovery
	[32501] = {boss = 605, slot = 12, account = 3, cost = 70 }, --Shadowmoon Insignia
	
	-- Reliquary of Souls
	[32332] = {boss = 606, slot = 16, xtype = 8, account = 3, cost = 100 }, --Torch of the Damned
	[32345] = {boss = 606, slot = 10, xtype = 4, account = 3, cost = 60 }, --Dreadboots of the Legion
	[32346] = {boss = 606, slot = 8, xtype = 3, account = 3, cost = 60 }, --Boneweave Girdle
	[32347] = {boss = 606, slot = 7, xtype = 2, account = 3, cost = 60 }, --Grips of Damnation
	[32349] = {boss = 606, slot = 2, account = 3, cost = 70 }, --Translucent Spellthread 2lace
	[32350] = {boss = 606, slot = 18, account = 3, cost = 70 }, --Touch of Inspiration
	[32351] = {boss = 606, slot = 6, xtype = 2, account = 3, cost = 60 }, --Elunite Empowered Bracers
	[32352] = {boss = 606, slot = 10, xtype = 2, account = 3, cost = 60 }, --Naturewarden's Treads
	[32353] = {boss = 606, slot = 7, xtype = 1, account = 3, cost = 60 }, --Gloves of Unfailing Faith
	[32354] = {boss = 606, slot = 1, xtype = 4, account = 3, cost = 60 }, --Crown of Empowered Fate
	[32362] = {boss = 606, slot = 2, account = 3, cost = 70 }, --Pendant of Titans
	[32363] = {boss = 606, slot = 17, xtype = 15, account = 3, cost = 70 }, --Naaru-Blessed Life Rod
	[32517] = {boss = 606, slot = 3, xtype = 3, account = 3, cost = 60 }, --The Wavemender's Mantle

	-- Mother Shahraz
	[31101] = {boss = 607, xtype = 18, account = 3, cost = 80 }, --Pauldrons of the Forgotten Conqueror
	[31102] = {boss = 607, xtype = 18, account = 3, cost = 80 }, --Pauldrons of the Forgotten Vanquisher
	[31103] = {boss = 607, xtype = 18, account = 3, cost = 80 }, --Pauldrons of the Forgotten Protector
	[32365] = {boss = 607, slot = 5, xtype = 4, account = 3, cost = 60 }, --Heartshatter Breastplate
	[32366] = {boss = 607, slot = 10, xtype = 2, account = 3, cost = 60 }, --Shadowmaster's Boots
	[32367] = {boss = 607, slot = 9, xtype = 1, account = 3, cost = 60 }, --Leggings of Devastation
	[32368] = {boss = 607, account = 3, cost = 70 }, --Tome of the Lightb11er
	[32369] = {boss = 607, slot = 13, xtype = 6, account = 3, cost = 70 }, --Blade of Savagery
	[32370] = {boss = 607, slot = 2, account = 3, cost = 70 }, --Nadina's Pendant of Purity

	-- Illidari Council
	[31098] = {boss = 608, xtype = 18, account = 3, cost = 80 }, --Leggings of the Forgotten Conqueror
	[31099] = {boss = 608, xtype = 18, account = 3, cost = 80 }, --Leggings of the Forgotten Vanquisher
	[31100] = {boss = 608, xtype = 18, account = 3, cost = 80 }, --Leggings of the Forgotten Protector
	[32331] = {boss = 608, slot = 4, account = 3, cost = 60 }, --Back of the Illidari Council
	[32373] = {boss = 608, slot = 1, xtype = 4, account = 3, cost = 60 }, --Helm of the Illidari Shatterer
	[32376] = {boss = 608, slot = 1, xtype = 3, account = 3, cost = 60 }, --Forest Prowler's Helm
	[32505] = {boss = 608, slot = 12, account = 3, cost = 70 }, --Madness of the Betrayer
	[32518] = {boss = 608, slot = 3, xtype = 2, account = 3, cost = 60 }, --Veil of Turning Leaves
	[32519] = {boss = 608, slot = 8, xtype = 1, account = 3, cost = 60 }, --Belt of Divine Guidance

	-- Illidan Stormrage
	[31089] = {boss = 609, xtype = 18, account = 3, cost = 80 }, --Chestguard of the Forgotten Conqueror
	[31090] = {boss = 609, xtype = 18, account = 3, cost = 80 }, --Chestguard of the Forgotten Vanquisher
	[31091] = {boss = 609, xtype = 18, account = 3, cost = 80 }, --Chestguard of the Forgotten Protector
	[32235] = {boss = 609, slot = 1, xtype = 2, account = 3, cost = 60 }, --Cursed Vision of Sargeras
	[32336] = {boss = 609, slot = 17, xtype = 12, account = 3, cost = 100 }, --Black Bow of the Betrayer
	[32374] = {boss = 609, slot = 16, xtype = 10, account = 3, cost = 100 }, --Zhar'doom, Greatstaff of the Devourer
	[32375] = {boss = 609, slot = 15, xtype = 17, account = 3, cost = 70 }, --Bulwark of Azzinoth
	[32471] = {boss = 609, slot = 13, xtype = 5, account = 3, cost = 70 }, --Shard of Azzinoth
	[32483] = {boss = 609, slot = 12, account = 3, cost = 70 }, --The Skull of Gul'dan
	[32496] = {boss = 609, slot = 12, account = 3, cost = 70 }, --Memento of Tyrande
	[32497] = {boss = 609, slot = 11, account = 3, cost = 70 }, --Stormrage Signet Ring
	[32500] = {boss = 609, slot = 14, xtype = 8, account = 3, cost = 70 }, --Crystal Spire of Karabor
	[32521] = {boss = 609, slot = 1, xtype = 4, account = 3, cost = 60 }, --Faceplate of the Impenetrable
	[32524] = {boss = 609, slot = 4, account = 3, cost = 60 }, --Shroud of the Highborne
	[32525] = {boss = 609, slot = 1, xtype = 1, account = 3, cost = 60 }, --Cowl of the Illidari High Lord
	[32837] = {boss = 609, slot = 14, xtype = 6, account = 3, cost = 100 }, --Warglaive of Azzinoth
	[32838] = {boss = 609, slot = 15, xtype = 6, account = 3, cost = 100 }, --Warglaive of Azzinoth
}
--"Slots" : 
		-- "0" : "?"
		-- "1" : Head 
		-- "2" : Neck
		-- "3" : Shoulder
		-- "4" : Back
		-- "5" : Chest
		-- "6" : Wrist
		-- "7" : Hands
		-- "8" : Waist
		-- "9" : Legs
		-- "10" : Feet
		-- "11" : Finger
		-- "12" : Trinket

		-- "13" : One-Hand
		-- "14" : Main Hand
		-- "15" : Off Hand
		-- "16" : Two-Hand
		-- "17" : Ranged
		-- "18" : Held in Off-hand

-- "Types" : 
		-- "0" : "?"
		-- "1" : Cloth
		-- "2" : Leather
		-- "3" : Mail
		-- "4" : Plate

		-- "5" : Dagger
		-- "6" : Sword
		-- "7" : Axe
		-- "8" : Mace
		-- "9" : Polearm
		-- "10" : Staff
		-- "11" : Fist Weapon
		-- "12" : Bow
		-- "13" : Crossbow
		-- "14" : Gun
		-- "15" : Wand
		-- "16" : Thrown

		-- "17" : Shield
		-- "18" : Token
		
-- "Class"
		-- 1 = Druid
		-- 2 = Hunter
		-- 3 = Mage
		-- 4 = Paladin
		-- 5 = Priest
		-- 6 = Rogue
		-- 7 = Shaman
		-- 8 = Warlock
		-- 9 = Warrior
------------------------------------------------------------

-- export items
ADDON.InitGroup.Items = ADDON.InitGroup.Items or {}
for k,v in pairs(items) do
  ADDON.InitGroup.Items[k] = v
end
items = nil
