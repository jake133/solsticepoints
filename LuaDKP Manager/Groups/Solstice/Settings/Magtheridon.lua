local _, ADDON = ...

------------------------------------------------------------

local items = {

  -- Magtheridon's Lair'
  [29458] = {boss = 651, slot =15, xtype =17, account = 9, cost = 300, note = ""}, -- Aegis of the Vindicator
  [29754] = {boss = 651, slot =5, xtype =18, account = 9, cost = 300, note = ""}, -- Chestguard of the Fallen Champion
  [29753] = {boss = 651, slot =5, xtype =18, account = 9, cost = 300, note = ""}, -- Chestguard of the Fallen Defender
  [29755] = {boss = 651, slot =5, xtype =18, account = 9, cost = 300, note = ""}, -- Chestguard of the Fallen Hero
  [28777] = {boss = 651, slot =4, xtype =1, account = 9, cost = 300, note = ""}, -- Cloak of the Pit Stalker
  [28782] = {boss = 651, slot =16, xtype =10, account = 9, cost = 300, note = ""}, -- Crystalheart Pulse-Staff
  [28783] = {boss = 651, slot =17, xtype =15, account = 9, cost = 300, note = ""}, -- Eredar Wand of Obliteration
  [28789] = {boss = 651, slot =12, account = 9, cost = 300, note = "Magic DPS"}, -- Eye of Magtheridon
  [28779] = {boss = 651, slot =8, xtype =4, account = 9, cost = 300, note = ""}, -- Girdle of the Endless Pit
  [28774] = {boss = 651, slot =16, xtype =9, account = 9, cost = 300, note = ""}, -- Glaive of the Pit
  [28781] = {boss = 651, slot =18, account = 9, cost = 300, note = ""}, -- Karaborian Talisman
  [28776] = {boss = 651, slot =7, xtype =2, account = 9, cost = 300, note = ""}, -- Liar's Tongue Gloves
  [32385] = {boss = 651, slot =11, xtype =18, account = 9, cost = 300, note = ""}, -- Magtheridon's Head
  [34845] = {boss = 651, account = 9, cost = 300, note = ""}, -- Pit Lord's Satchel
  [28780] = {boss = 651, slot =7, xtype =1, account = 9, cost = 300, note = ""}, -- Soul-Eater's Handwraps
  [28778] = {boss = 651, slot =8, xtype =3, account = 9, cost = 300, note = ""}, -- Terror Pit Girdle
  [28775] = {boss = 651, slot =1, xtype =4, account = 9, cost = 300, note = ""}, -- Thundering Greathelm

}

--"Slots" : 
		-- "0" : "?"
		-- "1" : Head 
		-- "2" : Neck
		-- "3" : Shoulder
		-- "4" : Back
		-- "5" : Chest
		-- "6" : Wrist
		-- "7" : Hands
		-- "8" : Waist
		-- "9" : Legs
		-- "10" : Feet
		-- "11" : Finger
		-- "12" : Trinket

		-- "13" : One-Hand
		-- "14" : Main Hand
		-- "15" : Off Hand
		-- "16" : Two-Hand
		-- "17" : Ranged
		-- "18" : Held in Off-hand

-- "Types" : 
		-- "0" : "?"
		-- "1" : Cloth
		-- "2" : Leather
		-- "3" : Mail
		-- "4" : Plate

		-- "5" : Dagger
		-- "6" : Sword
		-- "7" : Axe
		-- "8" : Mace
		-- "9" : Polearm
		-- "10" : Staff
		-- "11" : Fist Weapon
		-- "12" : Bow
		-- "13" : Crossbow
		-- "14" : Gun
		-- "15" : Wand
		-- "16" : Thrown

		-- "17" : Shield
		-- "18" : Token
		
-- "Class"
		-- 1 = Druid
		-- 2 = Hunter
		-- 3 = Mage
		-- 4 = Paladin
		-- 5 = Priest
		-- 6 = Rogue
		-- 7 = Shaman
		-- 8 = Warlock
		-- 9 = Warrior

-- not certain where this is used, but keeping incase it ends up useful
-- 1 = Tank, 2 = Melee, 3 = Ranged, 4 = Heal


------------------------------------------------------------

-- export items
ADDON.InitGroup.Items = ADDON.InitGroup.Items or {}
for k,v in pairs(items) do
  ADDON.InitGroup.Items[k] = v
end
items = nil
