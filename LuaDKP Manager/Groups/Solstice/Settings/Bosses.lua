local _, ADDON = ...

------------------------------------------------------------

local bosses = {
  [   0] = "?", -- do not remove!
  [   1] = {en = "Trash"},
  [   2] = {en = "Recipes"},
  [   3] = {en = "All Bosses"},

	[734] = {points = 1, accounts = {1}, en = "Malygos"},	-- The Eye of Eternity
  	[742] = {points = 1, accounts = {1}, en = "Sartharion"},	-- The Obsidian Sanctum

	[1107] = {points = 1, accounts = {1}, en = "Anub'Rekhan"},	-- Naxxramas
	[1108] = {points = 1, accounts = {1}, en = "Gluth"},	-- Naxxramas
	[1109] = {points = 1, accounts = {1}, en = "Gothik the Harvester"},	-- Naxxramas
	[1110] = {points = 1, accounts = {1}, en = "Grand Widow Faerlina"},	-- Naxxramas
	[1111] = {points = 1, accounts = {1}, en = "Grobbulus"},	-- Naxxramas
	[1112] = {points = 1, accounts = {1}, en = "Heigan the Unclean"},	-- Naxxramas
	[1113] = {points = 1, accounts = {1}, en = "Instructor Razuvious"},	-- Naxxramas
	[1114] = {points = 1, accounts = {1}, en = "Kel'Thuzad"},	-- Naxxramas
	[1115] = {points = 1, accounts = {1}, en = "Loatheb"},	-- Naxxramas
	[1116] = {points = 1, accounts = {1}, en = "Maexxna"},	-- Naxxramas
	[1117] = {points = 1, accounts = {1}, en = "Noth the Plaguebringer"},	-- Naxxramas
	[1118] = {points = 1, accounts = {1}, en = "Patchwerk"},	-- Naxxramas
	[1119] = {points = 1, accounts = {1}, en = "Sapphiron"},	-- Naxxramas
	[1120] = {points = 1, accounts = {1}, en = "Thaddius"},	-- Naxxramas
	[1121] = {points = 1, accounts = {1}, en = "The Four Horsemen"},	-- Naxxramas
		-- 
	[1126] = {points = 1, accounts = {1}, en = "Archavon the Stone Watcher"},	-- Vault of Archavon
	[1127] = {points = 1, accounts = {1}, en = "Emalon the Storm Watcher"},	-- Vault of Archavon
	[1128] = {points = 1, accounts = {1}, en = "Koralon the Flame Watcher"},	-- Vault of Archavon
	[1129] = {points = 1, accounts = {1}, en = "Toravon the Ice Watcher"},	-- Vault of Archavon

	-- Ulduar
	[744] = {points = 10, accounts = {1}, en = "Flame Leviathan"},
	[745] = {points = 10, accounts = {1}, en = "Ignis the Furnace Master"},
	[746] = {points = 10, accounts = {1}, en = "Razorscale"},
	[747] = {points = 10, accounts = {1}, en = "XT-002 Deconstructor"},
	[748] = {points = 10, accounts = {1}, en = "Assembly of Iron"},
	[749] = {points = 10, accounts = {1}, en = "Kologarn"},
	[750] = {points = 10, accounts = {1}, en = "Auriaya"},
	[753] = {points = 10, accounts = {1}, en = "Freya"},
	[751] = {points = 10, accounts = {1}, en = "Hodir"},
	[754] = {points = 10, accounts = {1}, en = "Mimiron"},
	[752] = {points = 10, accounts = {1}, en = "Thorim"},
	[755] = {points = 10, accounts = {1}, en = "General Vezax"},
	[756] = {points = 15, accounts = {1}, en = "Yogg-Saron"},
	[757] = {points = 20, accounts = {1}, en = "Algalon the Observer"},

	-- THESE VALUES ARE LIKELY WRONG AS THE OS AND EoE VALUES WERE INCORRECT
		-- ULDUAR
	[1130] = {points = 1, accounts = {1}, en = "Algalon the Observer"},	-- Ulduar
	[1131] = {points = 1, accounts = {1}, en = "Auriaya"},	-- Ulduar
	[1132] = {points = 1, accounts = {1}, en = "Flame Leviathan"},	-- Ulduar
	[1133] = {points = 1, accounts = {1}, en = "Freya"},	-- Ulduar
	[1134] = {points = 1, accounts = {1}, en = "General Vezax"},	-- Ulduar
	[1135] = {points = 1, accounts = {1}, en = "Hodir"},	-- Ulduar
	[1136] = {points = 1, accounts = {1}, en = "Ignis the Furnace Master"},	-- Ulduar
	[1137] = {points = 1, accounts = {1}, en = "Kologarn"},	-- Ulduar
	[1138] = {points = 1, accounts = {1}, en = "Mimiron"},	-- Ulduar
	[1139] = {points = 1, accounts = {1}, en = "Razorscale"},	-- Ulduar
	[1140] = {points = 1, accounts = {1}, en = "The Assembly of Iron"},	-- Ulduar
	[1141] = {points = 1, accounts = {1}, en = "Thorim"},	-- Ulduar
	[1142] = {points = 1, accounts = {1}, en = "XT-002 Deconstructor"},	-- Ulduar
	[1143] = {points = 1, accounts = {1}, en = "Yogg-Saron"},	-- Ulduar
		-- 
	[1144] = {points = 1, accounts = {1}, en = "Hogger"},	-- Stormwind Stockade
	[1145] = {points = 1, accounts = {1}, en = "Lord Overheat"},	-- Stormwind Stockade
	[1146] = {points = 1, accounts = {1}, en = "Randolph Moloch"},	-- Stormwind Stockade
		-- 
	[1147] = {points = 1, accounts = {1}, en = "Baltharus the Warborn"},	-- The Ruby Sanctum
	[1148] = {points = 1, accounts = {1}, en = "General Zarithrian"},	-- The Ruby Sanctum
	[1149] = {points = 1, accounts = {1}, en = "Saviana Ragefire"},	-- The Ruby Sanctum
	[1150] = {points = 1, accounts = {1}, en = "Halion"},	-- The Ruby Sanctum
		-- 
	[1164] = {points = 1, accounts = {1}, en = "Elder Brightleaf"},	-- Ulduar
	[1165] = {points = 1, accounts = {1}, en = "Elder Ironbranch"},	-- Ulduar
	[1166] = {points = 1, accounts = {1}, en = "Elder Stonebark"},	-- Ulduar
		-- 
	[1185] = {points = 1, accounts = {1}, en = "Majordomo Staghelm"},	-- Firelands
	[1197] = {points = 1, accounts = {1}, en = "Beth'tilac"},	-- Firelands
	[1200] = {points = 1, accounts = {1}, en = "Baleroc"},	-- Firelands
	[1203] = {points = 1, accounts = {1}, en = "Ragnaros"},	-- Firelands
	[1204] = {points = 1, accounts = {1}, en = "Lord Rhyolith"},	-- Firelands
	[1205] = {points = 1, accounts = {1}, en = "Shannox"},	-- Firelands
	[1206] = {points = 1, accounts = {1}, en = "Alysrazor"},	-- Firelands
	[1250] = {points = 1, accounts = {1}, en = "Occu'thar"},	-- Baradin Hold
		-- 
	[1271] = {points = 1, accounts = {1}, en = "Murozond"},	-- End Time
		-- 
	[1272] = {points = 1, accounts = {1}, en = "Peroth'arn"},	-- Well of Eternity
	[1273] = {points = 1, accounts = {1}, en = "Queen Azshara"},	-- Well of Eternity
	[1274] = {points = 1, accounts = {1}, en = "Mannoroth"},	-- Well of Eternity
		-- 
	[1291] = {points = 1, accounts = {1}, en = "Spine of Deathwing"},	-- Dragon Soul
	[1292] = {points = 1, accounts = {1}, en = "Morchok"},	-- Dragon Soul
	[1294] = {points = 1, accounts = {1}, en = "Warlord Zon'ozz"},	-- Dragon Soul
	[1295] = {points = 1, accounts = {1}, en = "Yor'sahj the Unsleeping"},	-- Dragon Soul
	[1296] = {points = 1, accounts = {1}, en = "Hagara"},	-- Dragon Soul
	[1297] = {points = 1, accounts = {1}, en = "Ultraxion"},	-- Dragon Soul
	[1298] = {points = 1, accounts = {1}, en = "Warmaster Blackhorn"},	-- Dragon Soul
	[1299] = {points = 1, accounts = {1}, en = "Madness of Deathwing"},	-- Dragon Soul
		-- 
	[1022] = {points = 1, accounts = {1}, en = "Atramedes"},	-- Blackwing Descent
	[1023] = {points = 1, accounts = {1}, en = "Chimaeron"},	-- Blackwing Descent
	[1024] = {points = 1, accounts = {1}, en = "Magmaw"},	-- Blackwing Descent
	[1025] = {points = 1, accounts = {1}, en = "Maloriak"},	-- Blackwing Descent
	[1026] = {points = 1, accounts = {1}, en = "Nefarian's End"},	-- Blackwing Descent
	[1027] = {points = 1, accounts = {1}, en = "Omnotron Defense System"},	-- Blackwing Descent
		-- 
	[1028] = {points = 1, accounts = {1}, en = "Ascendant Council"},	-- The Bastion of Twilight
	[1029] = {points = 1, accounts = {1}, en = "Cho'gall"},	-- The Bastion of Twilight
	[1030] = {points = 1, accounts = {1}, en = "Halfus Wyrmbreaker"},	-- The Bastion of Twilight
	[1032] = {points = 1, accounts = {1}, en = "Theralion and Valiona"},	-- The Bastion of Twilight
		-- 
	[1033] = {points = 1, accounts = {1}, en = "Argaloth"},	-- Baradin Hold
		-- 
	[1034] = {points = 1, accounts = {1}, en = "Al'Akir"},	-- Throne of the Four Winds
	[1035] = {points = 1, accounts = {1}, en = "Conclave of Wind"},	-- Throne of the Four Winds
		-- 
	[1041] = {points = 1, accounts = {1}, en = "Altairus"},	-- The Vortex Pinnacle
	[1042] = {points = 1, accounts = {1}, en = "Asaad"},	-- The Vortex Pinnacle
	[1043] = {points = 1, accounts = {1}, en = "Grand Vizier Ertan"},	-- The Vortex Pinnacle
		-- 
	[1044] = {points = 1, accounts = {1}, en = "Commander Ulthok"},	-- Throne of the Tides
	[1045] = {points = 1, accounts = {1}, en = "Lady Naz'jar"},	-- Throne of the Tides
	[1046] = {points = 1, accounts = {1}, en = "Mindbender Ghur'sha"},	-- Throne of the Tides
	[1047] = {points = 1, accounts = {1}, en = "Ozumat"},	-- Throne of the Tides
		-- 
	[1048] = {points = 1, accounts = {1}, en = "Drahga Shadowburner"},	-- Grim Batol
	[1049] = {points = 1, accounts = {1}, en = "Erudax"},	-- Grim Batol
	[1050] = {points = 1, accounts = {1}, en = "Forgemaster Throngus"},	-- Grim Batol
	[1051] = {points = 1, accounts = {1}, en = "General Umbriss"},	-- Grim Batol
		-- 
	[1052] = {points = 1, accounts = {1}, en = "General Husam"},	-- Lost City of the Tol'vir
	[1053] = {points = 1, accounts = {1}, en = "High Prophet Barim"},	-- Lost City of the Tol'vir
	[1054] = {points = 1, accounts = {1}, en = "Lockmaw"},	-- Lost City of the Tol'vir
	[1055] = {points = 1, accounts = {1}, en = "Siamat"},	-- Lost City of the Tol'vir
		-- 
	[1056] = {points = 1, accounts = {1}, en = "Corborus"},	-- The Stonecore
	[1057] = {points = 1, accounts = {1}, en = "High Priestess Azil"},	-- The Stonecore
	[1058] = {points = 1, accounts = {1}, en = "Ozruk"},	-- The Stonecore
	[1059] = {points = 1, accounts = {1}, en = "Slabhide"},	-- The Stonecore
		-- 
	[1074] = {points = 1, accounts = {1}, en = "Ammunae"},	-- Halls of Origination
	[1075] = {points = 1, accounts = {1}, en = "Anraphet"},	-- Halls of Origination
	[1076] = {points = 1, accounts = {1}, en = "Earthrager Ptah"},	-- Halls of Origination
	[1077] = {points = 1, accounts = {1}, en = "Isiset"},	-- Halls of Origination
	[1078] = {points = 1, accounts = {1}, en = "Rajh"},	-- Halls of Origination
	[1079] = {points = 1, accounts = {1}, en = "Setesh"},	-- Halls of Origination
	[1080] = {points = 1, accounts = {1}, en = "Temple Guardian Anhuur"},	-- Halls of Origination
		-- 
	[1082] = {points = 1, accounts = {1}, en = "Sinestra"},	-- The Bastion of Twilight
	[1083] = {points = 1, accounts = {1}, en = "Sinestra"},	-- The Bastion of Twilight
		-- 
	[1085] = {points = 1, accounts = {1}, en = "Anub'arak"},	-- Trial of the Crusader
	[1086] = {points = 1, accounts = {1}, en = "Faction Champions"},	-- Trial of the Crusader
	[1087] = {points = 1, accounts = {1}, en = "Lord Jaraxxus"},	-- Trial of the Crusader
	[1088] = {points = 1, accounts = {1}, en = "Northrend Beasts"},	-- Trial of the Crusader
	[1089] = {points = 1, accounts = {1}, en = "Val'kyr Twins"},	-- Trial of the Crusader
		-- 
		-- 
	[1095] = {points = 1, accounts = {1}, en = "Blood Council"},	-- Icecrown Citadel
	[1096] = {points = 1, accounts = {1}, en = "Deathbringer Saurfang"},	-- Icecrown Citadel
	[1097] = {points = 1, accounts = {1}, en = "Festergut"},	-- Icecrown Citadel
	[1098] = {points = 1, accounts = {1}, en = "Valithria Dreamwalker"},	-- Icecrown Citadel
	[1099] = {points = 1, accounts = {1}, en = "Icecrown Gunship Battle"},	-- Icecrown Citadel
	[1100] = {points = 1, accounts = {1}, en = "Lady Deathwhisper"},	-- Icecrown Citadel
	[1101] = {points = 1, accounts = {1}, en = "Lord Marrowgar"},	-- Icecrown Citadel
	[1102] = {points = 1, accounts = {1}, en = "Professor Putricide"},	-- Icecrown Citadel
	[1103] = {points = 1, accounts = {1}, en = "Queen Lana'thel"},	-- Icecrown Citadel
	[1104] = {points = 1, accounts = {1}, en = "Rotface"},	-- Icecrown Citadel
	[1105] = {points = 1, accounts = {1}, en = "Sindragosa"},	-- Icecrown Citadel
	[1106] = {points = 1, accounts = {1}, en = "The Lich King"},	-- Icecrown Citadel

	-- Gruul:
	--[649] = {points = 1, accounts = {8}, en = "High King Maulgar"},
	--[650] = {points = 1, accounts = {8}, en = "Gruul the Dragonkiller"},

	-- Magtheridon:
	--[651] = {points = 1, accounts = {9}, en = "Magtheridon"},

	-- SSC:
	[623] = {points = 1, accounts = {2}, en = "Hydross"},
	[624] = {points = 1, accounts = {2}, en = "Lurker"},
	[625] = {points = 1, accounts = {2}, en = "Leotheras"},
	[626] = {points = 1, accounts = {2}, en = "Fathom-Lord Karathress"},
	[627] = {points = 1, accounts = {2}, en = "Morogrim Tidewalker"},
	[628] = {points = 1, accounts = {2}, en = "Lady Vashj"},

	-- TK:
	[730] = {points = 1, accounts = {2}, en = "Al'ar"},
	[731] = {points = 1, accounts = {2}, en = "Void Reaver"},
	[732] = {points = 1, accounts = {2}, en = "High Astromancer Solarian"},
	[733] = {points = 1, accounts = {2}, en = "Kael'thas Sunstrider"},

  -- Black Temple
	[601] = {points = 1, accounts = {3}, en = "High Warlord Naj'entus"},
	[602] = {points = 1, accounts = {3}, en = "Supremus"},
	[603] = {points = 1, accounts = {3}, en = "Shade of Akama"},
	[604] = {points = 1, accounts = {3}, en = "Teron Gorefiend"},
	[605] = {points = 1, accounts = {3}, en = "Gurtogg Bloodboil"},
	[606] = {points = 1, accounts = {3}, en = "Reliquary of Souls"},
	[607] = {points = 1, accounts = {3}, en = "Mother Shahraz"},
	[608] = {points = 1, accounts = {3}, en = "The Illidari Council"},
	[609] = {points = 1, accounts = {3}, en = "Illidan Stormrage"},

  -- Mount Hyjal 
	[618] = {points = 1, accounts = {3}, en = "Rage Winterchill"},
	[619] = {points = 1, accounts = {3}, en = "Anetheron"},
	[620] = {points = 1, accounts = {3}, en = "Kaz'rogal"},
	[621] = {points = 1, accounts = {3}, en = "Azgalor"},
	[622] = {points = 1, accounts = {3}, en = "Archimonde"},

  -- Sunwell
	[724] = {points = 1, accounts = {4}, en = "Kalecgos"},
	[725] = {points = 1, accounts = {4}, en = "Brutallus"},
	[726] = {points = 1, accounts = {4}, en = "Felmyst"},
	[727] = {points = 1, accounts = {4}, en = "Eredar Twins"},
	[728] = {points = 1, accounts = {4}, en = "M'uru"},
	[729] = {points = 1, accounts = {4}, en = "Kil'Jaeden"},
}

------------------------------------------------------------

-- export tables
ADDON.InitGroup.Bosses = bosses
