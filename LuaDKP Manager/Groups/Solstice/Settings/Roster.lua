local _, ADDON = ...

------------------------------------------------------------

-- Console/Definitions.lua:
-- X CLASS
-- 1 = Druid, 2 = Hunter, 3 = Mage, 4 = Paladin, 5 = Priest, 6 = Rogue, 7 = Shaman, 8 = Warlock, 9 = Warrior, 10 = DeathKnight

-- ROLE
-- 1 = Tank, 2 = Melee, 3 = Ranged, 4 = Heal

------------------------------------------------------------

local roster = {
--["PlayerName"] = {xclass = 1, role = 1, nickname = "NameWithoutSpecialCharacters"},
	["Airfoil"]={xclass=1,role=4},
	["Annarinn"]={xclass=2,role=3},
	["Aythya"]={xclass=2,role=3},
	["Badreams"]={xclass=3,role=3},
	["Blackclaws"]={xclass=9,role=2},
	["Boneappletea"]={xclass=8,role=3},
	["Bravend"]={xclass=5,role=4},
	["Batriisyaa"]={xclass=10,role=2},
	["Cailyse"]={xclass=6,role=2},
	["Dayme"]={xclass=6,role=2},
	["Ceruwolfe"]={xclass=4,role=4},
	["Chalula"]={xclass=1,role=2},
	["Damelion"]={xclass=4,role=4},
	["Eillowee"]={xclass=8,role=3},
	["Elorn"]={xclass=2,role=3},
	["Frodes"]={xclass=1,role=4},
	["Furiozah"]={xclass=9,role=2},
	["Gaylestrum"]={xclass=3,role=3},
	["Grayskyy"]={xclass=6,role=2},
	["Grimdoom"]={xclass=2,role=3},
	["Heidie"]={xclass=7,role=4},
	["Inxi"]={xclass=1,role=2},
	["Iohe"]={xclass=1,role=2},
	["Jazzmene"]={xclass=5,role=4},
	["Lightspire"]={xclass=5,role=4},
	["Kalisae"]={xclass=1,role=1},
	["Kharlamagne"]={xclass=4,role=4},
	["Kyrika"]={xclass=8,role=3},
	["Lachy"]={xclass=3,role=3},
	["Lach�"]={xclass=3,role=3},
	["Lilybet"]={xclass=4,role=4},
	["Sadistia"]={xclass=5,role=3},
	["Sinniaa"]={xclass=1,role=4},
	["Sinwave"]={xclass=7,role=4},
	["Meyh�m"]={xclass=7,role=4},
	["Skeeta"]={xclass=2,role=3},
	["Slayingfreak"]={xclass=9,role=1},
	["Soulbane"]={xclass=8,role=3},
	["Sylaramynd"]={xclass=1,role=4},
	["Glancen"]={xclass=5,role=4},
	["Snoweyes"]={xclass=2,role=3},
	["Xandies"]={xclass=7,role=3},
	["Kekett"]={xclass=7,role=2},
	["Xremi"]={xclass=4,role=4},
	["Ma�rlyn"]={xclass=8,role=3},
	["Hateshift"]={xclass=8,role=3},
	["Anatyr"]={xclass=10,role=3},
	["Anatyr"]={xclass=10,role=3},
	["Buva"]={xclass=6,role=3},
	["Floordaddy"]={xclass=3,role=3},
	["Jormandir"]={xclass=3,role=3},
	["Darkhourz"]={xclass=10,role=3},
	["Ryzus"]={xclass=10,role=3},
	["Ironflurry"]={xclass=6,role=3},
	["Javafanatic"]={xclass=5,role=3},
	["Lizaveta"]={xclass=5,role=3},
	["Maevice"]={xclass=8,role=3},
	["Mayaell"]={xclass=1,role=3},
	["Melindara"]={xclass=10,role=3},
	["Phal"]={xclass=4,role=3},
	["Randala"]={xclass=1,role=3},
	["Rosemondon"]={xclass=1,role=3},
	["Soullina"]={xclass=1,role=3},
	["Talasame"]={xclass=9,role=3},
	["Television"]={xclass=4,role=3},
	["Wreckel"]={xclass=4,role=3},
	["Dvoom"]={xclass=4,role=3},
	["Hempoil"]={xclass=8,role=3},
	["Rudigar"]={xclass=4,role=3},
	["Sagger"]={xclass=10,role=2},
	["Orcbane"]={xclass=9,role=1},
	["Dunsmar"]={xclass=9,role=2},
	["Mykos"]={xclass=1,role=2},
	["Ogdru"]={xclass=1,role=4},
	["Derios"]={xclass=8,role=3},
}

------------------------------------------------------------

-- export tables
ADDON.InitGroup.Roster = roster
