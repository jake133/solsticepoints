local _, ADDON = ...

------------------------------------------------------------

local items = {
  -- Sunwell LOOT

	-- trash
	[34349] = {boss = 1, slot = 17, xtype = 16, account = 4, cost = 70 }, --Blade of Life's Inevitability
	[34350] = {boss = 1, slot = 7, xtype = 3, account = 4, cost = 60 }, --Gauntlets of the Ancient Shadowmoon
	[34409] = {boss = 1, slot = 7, xtype = 3, account = 4, cost = 60 }, --Gauntlets of the Ancient Frostwolf
	[34346] = {boss = 1, slot = 15, xtype = 11, account = 4, cost = 70 }, --Mounting Vengeance
	[35733] = {boss = 1, slot = 11, account = 4, cost = 70 }, --Ring of Harmonic Beauty
	[34183] = {boss = 1, slot = 16, xtype = 9, account = 4, cost = 100 }, --Shivering Felspine
	[34351] = {boss = 1, slot = 7, xtype = 2, account = 4, cost = 60 }, --Tranquil Majesty Wraps
	[34407] = {boss = 1, slot = 7, xtype = 2, account = 4, cost = 60 }, --Tranquil Moonlight Wraps
	[34348] = {boss = 1, slot = 17, xtype = 15, account = 4, cost = 70 }, --Wand of Cleansing Light
	[34347] = {boss = 1, slot = 17, xtype = 15, account = 4, cost = 70 }, --Wand of the Demonsoul

	-- recipes
	[35202] = {boss = 2,  account = 4, cost = 10 }, --Design: Amulet of Flowing Life
	[35200] = {boss = 2,  account = 4, cost = 10 }, --Design: Hard Khorium Band
	[35203] = {boss = 2,  account = 4, cost = 10 }, --Design: Hard Khorium Choker
	[35198] = {boss = 2,  account = 4, cost = 10 }, --Design: Loop of Forged Power
	[35201] = {boss = 2,  account = 4, cost = 10 }, --Design: Pendant of Sunfire
	[35199] = {boss = 2,  account = 4, cost = 10 }, --Design: Ring of Flowing Life
	[35218] = {boss = 2,  account = 4, cost = 10 }, --Pattern: Carapace of Sun and Shadow
	[35217] = {boss = 2,  account = 4, cost = 10 }, --Pattern: Embrace of the Phoenix
	[35213] = {boss = 2,  account = 4, cost = 10 }, --Pattern: Fletcher's Gloves of the Phoenix
	[35214] = {boss = 2,  account = 4, cost = 10 }, --Pattern: Gloves of Immortal Dusk
	[35205] = {boss = 2,  account = 4, cost = 10 }, --Pattern: Hands of Eternal Light
	[35216] = {boss = 2,  account = 4, cost = 10 }, --Pattern: Leather Chestguard of the Sun
	[35212] = {boss = 2,  account = 4, cost = 10 }, --Pattern: Leather Gauntlets of the Sun
	[35207] = {boss = 2,  account = 4, cost = 10 }, --Pattern: Robe of Eternal Light
	[35219] = {boss = 2,  account = 4, cost = 10 }, --Pattern: Sun-Drenched Scale Chestguard
	[35215] = {boss = 2,  account = 4, cost = 10 }, --Pattern: Sun-Drenched Scale Gloves
	[35204] = {boss = 2,  account = 4, cost = 10 }, --Pattern: Sunfire Handwraps
	[35206] = {boss = 2,  account = 4, cost = 10 }, --Pattern: Sunfire Robe
	[35209] = {boss = 2,  account = 4, cost = 10 }, --Plans: Hard Khorium Battlefists
	[35211] = {boss = 2,  account = 4, cost = 10 }, --Plans: Hard Khorium Battleplate
	[35210] = {boss = 2,  account = 4, cost = 10 }, --Plans: Sunblessed Breastplate
	[35208] = {boss = 2,  account = 4, cost = 10 }, --Plans: Sunblessed Gauntlets
	[35186] = {boss = 2,  account = 4, cost = 10 }, --Schematic: Annihilator Holo-Gogs
	[35196] = {boss = 2,  account = 4, cost = 10 }, --Schematic: Hard Khorium Goggles
	[35190] = {boss = 2,  account = 4, cost = 10 }, --Schematic: Hyper-Magnified Moon Specs
	[35193] = {boss = 2,  account = 4, cost = 10 }, --Schematic: Lightning Etched Specs
	[35195] = {boss = 2,  account = 4, cost = 10 }, --Schematic: Mayhem Projection Goggles
	[35189] = {boss = 2,  account = 4, cost = 10 }, --Schematic: Powerheal 9000 Lens
	[35192] = {boss = 2,  account = 4, cost = 10 }, --Schematic: Primal-Attuned Goggles
	[35194] = {boss = 2,  account = 4, cost = 10 }, --Schematic: Surestrike Goggles v3.0

	-- Kalecgos
	[34164] = {boss = 724, slot = 13, xtype = 6, account = 4, cost = 70 }, --Dragonscale-Encrusted Longblade
	[34165] = {boss = 724, slot = 13, xtype = 5, account = 4, cost = 70 }, --Fang of Kalecgos
	[34166] = {boss = 724, slot = 11, account = 4, cost = 70 }, --Band of Lucent Beams
	[34167] = {boss = 724, slot = 9, xtype = 4, account = 4, cost = 60 }, --Legplates of the Holy Juggernaut
	[34168] = {boss = 724, slot = 9, xtype = 3, account = 4, cost = 60 }, --Starstalker Legguards
	[34169] = {boss = 724, slot = 9, xtype = 2, account = 4, cost = 60 }, --Breeches of Natural Aggression
	[34170] = {boss = 724, slot = 9, xtype = 1, account = 4, cost = 60 }, --Pantaloons of Calming Strife
	[34848] = {boss = 724, slot = 6, xtype = 18, account = 4, cost = 80 }, --Bracers of the Forgotten Conqueror
	[34851] = {boss = 724, slot = 6, xtype = 18, account = 4, cost = 80 }, --Bracers of the Forgotten Protector
	[34852] = {boss = 724, slot = 6, xtype = 18, account = 4, cost = 80 }, --Bracers of the Forgotten Vanquisher
	[34386] = {boss = 724, slot = 9, xtype = 1, account = 4, cost = 60 }, --Pantaloons of Growing Strife
	[34384] = {boss = 724, slot = 9, xtype = 2, account = 4, cost = 60 }, --Breeches of Natural Splendor
	[34382] = {boss = 724, slot = 9, xtype = 4, account = 4, cost = 60 }, --Judicator's Legguards

	-- Brutallus
	[34176] = {boss = 725, slot = 14, xtype = 8, account = 4, cost = 70 }, --Reign of Misery
	[34177] = {boss = 725, slot = 2, account = 4, cost = 70 }, --Clutch of Demise
	[34178] = {boss = 725, slot = 2, account = 4, cost = 70 }, --Collar of the Pit Lord
	[34179] = {boss = 725, slot = 18, account = 4, cost = 70 }, --Heart of the Pit
	[34180] = {boss = 725, slot = 9, xtype = 4, account = 4, cost = 60 }, --Felfury Legplates
	[34181] = {boss = 725, slot = 9, xtype = 1, account = 4, cost = 60 }, --Leggings of Calamity
	[34381] = {boss = 725, slot = 9, xtype = 4, account = 4, cost = 60 }, --Felstrength Legplates
	[34853] = {boss = 725, slot = 8, xtype = 18, account = 4, cost = 80 }, --Belt of the Forgotten Conqueror
	[34854] = {boss = 725, slot = 8, xtype = 18, account = 4, cost = 80 }, --Belt of the Forgotten Protector
	[34855] = {boss = 725, slot = 8, xtype = 18, account = 4, cost = 80 }, --Belt of the Forgotten Vanquisher

	-- Felmyst
	[34182] = {boss = 726, slot = 16, xtype = 10, account = 4, cost = 100 }, --Grand Magister's Staff of Torrents
	[34184] = {boss = 726, slot = 2, account = 4, cost = 70 }, --Brooch of the Highborne
	[34185] = {boss = 726, slot = 15, xtype = 17, account = 4, cost = 70 }, --Sword Breaker's Bulwark
	[34186] = {boss = 726, slot = 9, xtype = 3, account = 4, cost = 60 }, --Chain Links of the Tumultuous Storm
	[34188] = {boss = 726, slot = 9, xtype = 2, account = 4, cost = 60 }, --Leggings of the Immortal Night
	[34352] = {boss = 726, slot = 7, xtype = 4, account = 4, cost = 60 }, --Borderland Fortress Grips
	[34385] = {boss = 726, slot = 9, xtype = 2, account = 4, cost = 60 }, --Leggings of the Immortal Beast
	[34383] = {boss = 726, slot = 9, xtype = 3, account = 4, cost = 60 }, --Kilt of Spiritual Reconstruction
	[34856] = {boss = 726, slot = 10, xtype = 18, account = 4, cost = 80 }, --Boots of the Forgotten Conqueror
	[34857] = {boss = 726, slot = 10, xtype = 18, account = 4, cost = 80 }, --Boots of the Forgotten Protector
	[34858] = {boss = 726, slot = 10, xtype = 18, account = 4, cost = 80 }, --Boots of the Forgotten Vanquisher

	-- Eredar Twins
	[34205] = {boss = 727, slot = 4, xtype = 1, account = 4, cost = 60 }, --Shroud of Redeemed Souls
	[34190] = {boss = 727, slot = 4, xtype = 1, account = 4, cost = 60 }, --Crimson Paragon's Cover
	[34210] = {boss = 727, slot = 3, xtype = 1, account = 4, cost = 60 }, --Amice of the Convoker
	[34202] = {boss = 727, slot = 3, xtype = 1, account = 4, cost = 60 }, --Shawl of Wonderment
	[34393] = {boss = 727, slot = 3, xtype = 1, account = 4, cost = 60 }, --Shoulderpads of Knowledge's Pursuit
	[34209] = {boss = 727, slot = 3, xtype = 2, account = 4, cost = 60 }, --Spaulders of Reclamation
	[34391] = {boss = 727, slot = 3, xtype = 2, account = 4, cost = 60 }, --Spaulders of Devastation
	[34195] = {boss = 727, slot = 3, xtype = 2, account = 4, cost = 60 }, --Shoulderpads of Vehemence
	[34392] = {boss = 727, slot = 3, xtype = 2, account = 4, cost = 60 }, --Demontooth Shoulderpads
	[34194] = {boss = 727, slot = 3, xtype = 3, account = 4, cost = 60 }, --Mantle of the Golden Forest
	[34208] = {boss = 727, slot = 3, xtype = 3, account = 4, cost = 60 }, --Equilibrium Epaulets
	[34390] = {boss = 727, slot = 3, xtype = 3, account = 4, cost = 60 }, --Erupting Epaulets
	[34192] = {boss = 727, slot = 3, xtype = 4, account = 4, cost = 60 }, --Pauldrons of Perseverance
	[34388] = {boss = 727, slot = 3, xtype = 4, account = 4, cost = 60 }, --Pauldrons of Berserking
	[34193] = {boss = 727, slot = 3, xtype = 4, account = 4, cost = 60 }, --Spaulders of the Thalassian Savior
	[34389] = {boss = 727, slot = 3, xtype = 4, account = 4, cost = 60 }, --Spaulders of the Thalassian Defender
	[35290] = {boss = 727, slot = 2, account = 4, cost = 70 }, --Sin'dorei Pendant of Conquest
	[35291] = {boss = 727, slot = 2, account = 4, cost = 70 }, --Sin'dorei Pendant of Salvation
	[35292] = {boss = 727, slot = 2, account = 4, cost = 70 }, --Sin'dorei Pendant of Triumph
	[34204] = {boss = 727, slot = 2, account = 4, cost = 70 }, --Amulet of Unfettered Magics
	[34189] = {boss = 727, slot = 11, account = 4, cost = 70 }, --Band of Ruinous Delight
	[34206] = {boss = 727, slot = 18, account = 4, cost = 70 }, --Book of Highborne Hymns
	[34197] = {boss = 727, slot = 13, xtype = 6, account = 4, cost = 70 }, --Shiv of Exsanguination
	[34199] = {boss = 727, slot = 14, xtype = 8, account = 4, cost = 70 }, --Archon's Gavel
	[34203] = {boss = 727, slot = 13, xtype = 11, account = 4, cost = 70 }, --Grip of Mannoroth
	[34198] = {boss = 727, slot = 16, xtype = 10, account = 4, cost = 100 }, --Stanchion of Primal Instinct
	[34196] = {boss = 727, slot = 17, xtype = 12, account = 4, cost = 100 }, --Golden Bow of Quel'Thalas

	-- M'uru
	[34232] = {boss = 728, slot = 5, xtype = 1, account = 4, cost = 60 }, --Fel Conquerer Raiments
	[34233] = {boss = 728, slot = 5, xtype = 1, account = 4, cost = 60 }, --Robes of Faltered Light
	[34399] = {boss = 728, slot = 5, xtype = 1, account = 4, cost = 60 }, --Robes of Ghostly Hatred
	[34212] = {boss = 728, slot = 5, xtype = 2, account = 4, cost = 60 }, --Sunglow Vest
	[34398] = {boss = 728, slot = 5, xtype = 2, account = 4, cost = 60 }, --Utopian Tunic of Elune
	[34211] = {boss = 728, slot = 5, xtype = 2, account = 4, cost = 60 }, --Harness of Carnal Instinct
	[34397] = {boss = 728, slot = 5, xtype = 2, account = 4, cost = 60 }, --Bladed Chaos Tunic
	[34234] = {boss = 728, slot = 7, xtype = 2, account = 4, cost = 60 }, --Shadowed Gauntlets of Paroxysm
	[34408] = {boss = 728, slot = 7, xtype = 2, account = 4, cost = 60 }, --Gloves of the Forest Drifter
	[34229] = {boss = 728, slot = 5, xtype = 3, account = 4, cost = 60 }, --Garments of Serene Shores
	[34396] = {boss = 728, slot = 5, xtype = 3, account = 4, cost = 60 }, --Garments of Crashing Shores
	[34228] = {boss = 728, slot = 5, xtype = 3, account = 4, cost = 60 }, --Vicious Hawkstrider Hauberk
	[34215] = {boss = 728, slot = 5, xtype = 4, account = 4, cost = 60 }, --Warharness of Reckless Fury
	[34394] = {boss = 728, slot = 5, xtype = 4, account = 4, cost = 60 }, --Breastplate of Agony's Aversion
	[34240] = {boss = 728, slot = 7, xtype = 4, account = 4, cost = 60 }, --Gauntlets of the Soothed Soul
	[34216] = {boss = 728, slot = 5, xtype = 4, account = 4, cost = 60 }, --Heroic Judicator's Chestguard
	[34395] = {boss = 728, slot = 5, xtype = 4, account = 4, cost = 60 }, --Noble Judicator's Chestguard
	[34213] = {boss = 728, slot = 11, account = 4, cost = 70 }, --Ring of Hardened Resolve
	[34230] = {boss = 728, slot = 11, account = 4, cost = 70 }, --Ring of Omnipotence
	[35282] = {boss = 728, slot = 11, account = 4, cost = 70 }, --Sin'dorei Band of Dominance
	[35283] = {boss = 728, slot = 11, account = 4, cost = 70 }, --Sin'dorei Band of Salvation
	[35284] = {boss = 728, slot = 11, account = 4, cost = 70 }, --Sin'dorei Band of Triumph
	[34427] = {boss = 728, slot = 12, account = 4, cost = 70 }, --Blackened Naaru Sliver
	[34430] = {boss = 728, slot = 12, account = 4, cost = 70 }, --Glimmering Naaru Sliver
	[34429] = {boss = 728, slot = 12, account = 4, cost = 70 }, --Shifting Naaru Sliver
	[34428] = {boss = 728, slot = 12, account = 4, cost = 70 }, --Steely Naaru Sliver
	[34214] = {boss = 728, slot = 13, xtype = 6, account = 4, cost = 70 }, --Muramasa
	[34231] = {boss = 728, slot = 15, xtype = 17, account = 4, cost = 70 }, --Aegis of Angelic Fortune

	-- Kil'jaeden
	[34241] = {boss = 729, slot = 4, xtype = 1, account = 4, cost = 60 }, --Cloak of Unforgivable Sin
	[34242] = {boss = 729, slot = 4, xtype = 1, account = 4, cost = 60 }, --Tattered Cape of Antonidas
	[34339] = {boss = 729, slot = 1, xtype = 1, account = 4, cost = 60 }, --Cowl of Light's Purity
	[34405] = {boss = 729, slot = 1, xtype = 1, account = 4, cost = 60 }, --Helm of Arcane Purity
	[34340] = {boss = 729, slot = 1, xtype = 1, account = 4, cost = 60 }, --Dark Conjuror's Collar
	[34342] = {boss = 729, slot = 7, xtype = 1, account = 4, cost = 60 }, --Handguards of the Dawn
	[34406] = {boss = 729, slot = 7, xtype = 1, account = 4, cost = 60 }, --Gloves of Tyri's Power
	[34344] = {boss = 729, slot = 7, xtype = 1, account = 4, cost = 60 }, --Handguards of Defiled Worlds
	[34244] = {boss = 729, slot = 1, xtype = 2, account = 4, cost = 60 }, --Duplicitous Guise
	[34404] = {boss = 729, slot = 1, xtype = 2, account = 4, cost = 60 }, --Mask of the Fury Hunter
	[34245] = {boss = 729, slot = 1, xtype = 2, account = 4, cost = 60 }, --Cover of Ursol the Wise
	[34403] = {boss = 729, slot = 1, xtype = 2, account = 4, cost = 60 }, --Cover of Ursoc the Mighty
	[34333] = {boss = 729, slot = 1, xtype = 3, account = 4, cost = 60 }, --Coif of Alleria
	[34332] = {boss = 729, slot = 1, xtype = 3, account = 4, cost = 60 }, --Cowl of Gul'dan
	[34402] = {boss = 729, slot = 1, xtype = 3, account = 4, cost = 60 }, --Shroud of Chieftain Ner'zhul
	[34343] = {boss = 729, slot = 7, xtype = 3, account = 4, cost = 60 }, --Thalassian Ranger Gauntlets
	[34243] = {boss = 729, slot = 1, xtype = 4, account = 4, cost = 60 }, --Helm of Burning Righteousness
	[34401] = {boss = 729, slot = 1, xtype = 4, account = 4, cost = 60 }, --Helm of Uther's Resolve
	[34345] = {boss = 729, slot = 1, xtype = 4, account = 4, cost = 60 }, --Crown of Anasterian
	[34400] = {boss = 729, slot = 1, xtype = 4, account = 4, cost = 60 }, --Crown of Dath'Remar
	[34341] = {boss = 729, slot = 7, xtype = 4, account = 4, cost = 60 }, --Borderland Paingrips
	[34334] = {boss = 729, slot = 17, xtype = 12, account = 4, cost = 100 }, --Thori'dal, the Stars' Fury
	[34329] = {boss = 729, slot = 13, xtype = 5, account = 4, cost = 70 }, --Crux of the Apocalypse
	[34247] = {boss = 729, slot = 16, xtype = 6, account = 4, cost = 100 }, --Apolyon, the Soul-Render
	[34335] = {boss = 729, slot = 14, xtype = 8, account = 4, cost = 70 }, --Hammer of Sanctification
	[34331] = {boss = 729, slot = 14, xtype = 11, account = 4, cost = 70 }, --Hand of the Deceiver
	[34336] = {boss = 729, slot = 14, xtype = 5, account = 4, cost = 70 }, --Sunflare
	[34337] = {boss = 729, slot = 16, xtype = 10, account = 4, cost = 100 }, --Golden Staff of the Sin'dorei

}
--"Slots" : 
		-- "0" : "?"
		-- "1" : Head 
		-- "2" : Neck
		-- "3" : Shoulder
		-- "4" : Back
		-- "5" : Chest
		-- "6" : Wrist
		-- "7" : Hands
		-- "8" : Waist
		-- "9" : Legs
		-- "10" : Feet
		-- "11" : Finger
		-- "12" : Trinket

		-- "13" : One-Hand
		-- "14" : Main Hand
		-- "15" : Off Hand
		-- "16" : Two-Hand
		-- "17" : Ranged
		-- "18" : Held in Off-hand

-- "Types" : 
		-- "0" : "?"
		-- "1" : Cloth
		-- "2" : Leather
		-- "3" : Mail
		-- "4" : Plate

		-- "5" : Dagger
		-- "6" : Sword
		-- "7" : Axe
		-- "8" : Mace
		-- "9" : Polearm
		-- "10" : Staff
		-- "11" : Fist Weapon
		-- "12" : Bow
		-- "13" : Crossbow
		-- "14" : Gun
		-- "15" : Wand
		-- "16" : Thrown

		-- "17" : Shield
		-- "18" : Token
------------------------------------------------------------

-- export items
ADDON.InitGroup.Items = ADDON.InitGroup.Items or {}
for k,v in pairs(items) do
  ADDON.InitGroup.Items[k] = v
end
items = nil
