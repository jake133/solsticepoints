local _, ADDON = ...

------------------------------------------------------------

local translation = {
  [32256] = {
    en = "Waistwrap of Infinity",
  },
  [32264] = {
    en = "Shoulders of the Hidden Predator",
  },
  [30236] = {
    en = "Chestguard of the Vanquished Champion",
  },
  [30244] = {
    en = "Helm of the Vanquished Hero",
  },
  [32296] = {
    en = "Design: Great Lionseye",
  },
  [29765] = {
    en = "Leggings of the Fallen Hero",
  },
  [32328] = {
    en = "Botanist's Gloves of Growth",
  },
  [32336] = {
    en = "Black Bow of the Betrayer",
  },
  [32344] = {
    en = "Staff of Immaculate Recovery",
  },
  [28775] = {
    en = "Thundering Greathelm",
  },
  [28783] = {
    en = "Eredar Wand of Obliteration",
  },
  [30324] = {
    en = "Plans: Red Havoc Boots",
  },
  [28799] = {
    en = "Belt of Divine Inspiration",
  },
  [28823] = {
    en = "Eye of Gruul",
  },
  [30883] = {
    en = "Pillar of Ferocity",
  },
  [30891] = {
    en = "Black Featherlight Boots",
  },
  [30899] = {
    en = "Don Rodrigo's Poncho",
  },
  [30907] = {
    en = "Mail of Fevered Pursuit",
  },
  [30915] = {
    en = "Belt of Seething Fury",
  },
  [29925] = {
    en = "Phoenix-Wing Cloak",
  },
  [32496] = {
    en = "Memento of Tyrande",
  },
  [29949] = {
    en = "Arcanite Steam-Pistol",
  },
  [32512] = {
    en = "Girdle of Lordaeron's Fallen",
  },
  [29965] = {
    en = "Girdle of the Righteous Path",
  },
  [32528] = {
    en = "Blessed Band of Karabor",
  },
  [29981] = {
    en = "Ethereum Life-Staff",
  },
  [29989] = {
    en = "Sunshower Light Cloak",
  },
  [29997] = {
    en = "Band of the Ranger-General",
  },
  [30021] = {
    en = "Wildfury Greatstaff",
  },
  [30029] = {
    en = "Bark-Gloves of Ancient Wisdom",
  },
  [32592] = {
    en = "Chestguard of Relentless Storms",
  },
  [32943] = {
    en = "Swiftsteel Bludgeon",
  },
  [30053] = {
    en = "Pauldrons of the Wardancer",
  },
  [30061] = {
    en = "Ancestral Ring of Conquest",
  },
  [31091] = {
    en = "Chestguard of the Forgotten Protector",
  },
  [31099] = {
    en = "Leggings of the Forgotten Vanquisher",
  },
  [30085] = {
    en = "Mantle of the Tireless Tracker",
  },
  [30101] = {
    en = "Bloodsea Brigand's Vest",
  },
  [30109] = {
    en = "Ring of Endless Coils",
  },
  [32736] = {
    en = "Plans: Swiftsteel Bracers",
  },
  [32744] = {
    en = "Pattern: Bracers of Renewed Life",
  },
  [32241] = {
    en = "Helm of Soothing Currents",
  },
  [32257] = {
    en = "Idol of the White Stag",
  },
  [32265] = {
    en = "Shadow-walker's Cord",
  },
  [32273] = {
    en = "Amice of Brilliant Light",
  },
  [30237] = {
    en = "Chestguard of the Vanquished Defender",
  },
  [30245] = {
    en = "Leggings of the Vanquished Champion",
  },
  [32297] = {
    en = "Design: Sovereign Shadowsong Amethyst",
  },
  [29766] = {
    en = "Leggings of the Fallen Champion",
  },
  [32329] = {
    en = "Cowl of Benevolence",
  },
  [32337] = {
    en = "Shroud of Forgiveness",
  },
  [30301] = {
    en = "Pattern: Belt of Natural Power",
  },
  [28776] = {
    en = "Liar's Tongue Gloves",
  },
  [32361] = {
    en = "Blind-Seers Icon",
  },
  [32369] = {
    en = "Blade of Savagery",
  },
  [28800] = {
    en = "Hammer of the Naaru",
  },
  [32385] = {
    en = "Magtheridon's Head",
  },
  [30868] = {
    en = "Rejuvenating Bracers",
  },
  [30884] = {
    en = "Hatefury Mantle",
  },
  [30892] = {
    en = "Beast-tamer's Shoulders",
  },
  [30900] = {
    en = "Bow-stitched Leggings",
  },
  [30908] = {
    en = "Apostle of Argus",
  },
  [30916] = {
    en = "Leggings of Channeled Elements",
  },
  [29918] = {
    en = "Mindstorm Wristbands",
  },
  [32497] = {
    en = "Stormrage Signet Ring",
  },
  [29950] = {
    en = "Greaves of the Bloodwarder",
  },
  [32513] = {
    en = "Wristbands of Divine Influence",
  },
  [29966] = {
    en = "Vambraces of Ending",
  },
  [34845] = {
    en = "Pit Lord's Satchel",
  },
  [29982] = {
    en = "Wand of the Forgotten Star",
  },
  [29990] = {
    en = "Crown of the Sun",
  },
  [29998] = {
    en = "Royal Gauntlets of Silvermoon",
  },
  [30022] = {
    en = "Pendant of the Perilous",
  },
  [30030] = {
    en = "Girdle of Fallen Stars",
  },
  [32593] = {
    en = "Treads of the Den Mother",
  },
  [30054] = {
    en = "Ranger-General's Chestguard",
  },
  [30062] = {
    en = "Grove-Bands of Remulos",
  },
  [31092] = {
    en = "Gloves of the Forgotten Conqueror",
  },
  [31100] = {
    en = "Leggings of the Forgotten Protector",
  },
  [30102] = {
    en = "Krakken-Heart Breastplate",
  },
  [30621] = {
    en = "Prism of Inner Calm",
  },
  [30629] = {
    en = "Scarab of Displacement",
  },
  [32737] = {
    en = "Plans: Swiftsteel Shoulders",
  },
  [32234] = {
    en = "Fists of Mukoa",
  },
  [32242] = {
    en = "Boots of Oceanic Fury",
  },
  [32250] = {
    en = "Pauldrons of Abyssal Fury",
  },
  [32258] = {
    en = "Naturalist's Preserving Cinch",
  },
  [32266] = {
    en = "Ring of Deceitful Intent",
  },
  [30238] = {
    en = "Chestguard of the Vanquished Hero",
  },
  [30246] = {
    en = "Leggings of the Vanquished Defender",
  },
  [32298] = {
    en = "Design: Shifting Shadowsong Amethyst",
  },
  [29767] = {
    en = "Leggings of the Fallen Defender",
  },
  [32330] = {
    en = "Totem of Ancestral Guidance",
  },
  [32338] = {
    en = "Blood-cursed Shoulderpads",
  },
  [30302] = {
    en = "Pattern: Belt of Deep Shadow",
  },
  [28777] = {
    en = "Cloak of the Pit Stalker",
  },
  [32362] = {
    en = "Pendant of Titans",
  },
  [32370] = {
    en = "Nadina's Pendant of Purity",
  },
  [28801] = {
    en = "Maulgar's Warhelm",
  },
  [30861] = {
    en = "Furious Shackles",
  },
  [28825] = {
    en = "Aldori Legacy Defender",
  },
  [30885] = {
    en = "Archbishop's Slippers",
  },
  [30893] = {
    en = "Sun-touched Chain Leggings",
  },
  [30901] = {
    en = "Boundless Agony",
  },
  [30909] = {
    en = "Antonidas's Aegis of Rapt Concentration",
  },
  [30917] = {
    en = "Razorfury Mantle",
  },
  [32458] = {
    en = "Ashes of Al'ar",
  },
  [30446] = {
    en = "Solarian's Sapphire",
  },
  [29951] = {
    en = "Star-Strider Boots",
  },
  [29983] = {
    en = "Fel-Steel Warhelm",
  },
  [29991] = {
    en = "Sunhawk Leggings",
  },
  [30023] = {
    en = "Totem of the Maelstrom",
  },
  [30047] = {
    en = "Blackfathom Warbands",
  },
  [30055] = {
    en = "Shoulderpads of the Stranger",
  },
  [30063] = {
    en = "Libram of Absolute Truth",
  },
  [31093] = {
    en = "Gloves of the Forgotten Vanquisher",
  },
  [30079] = {
    en = "Illidari Shoulderpads",
  },
  [30095] = {
    en = "Fang of the Leviathan",
  },
  [30103] = {
    en = "Fang of Vashj",
  },
  [30111] = {
    en = "Runetotem's Mantle",
  },
  [30183] = {
    en = "Nether Vortex",
  },
  [32235] = {
    en = "Cursed Vision of Sargeras",
  },
  [32243] = {
    en = "Pearl Inlaid Boots",
  },
  [32251] = {
    en = "Wraps of Precise Flight",
  },
  [32259] = {
    en = "Bands of the Coming Storm",
  },
  [32267] = {
    en = "Boots of the Resilient",
  },
  [32275] = {
    en = "Spiritwalker Gauntlets",
  },
  [30239] = {
    en = "Gloves of the Vanquished Champion",
  },
  [30247] = {
    en = "Leggings of the Vanquished Hero",
  },
  [32307] = {
    en = "Design: Veiled Pyrestone",
  },
  [32323] = {
    en = "Shadowmoon Destroyer's Drape",
  },
  [32331] = {
    en = "Cloak of the Illidari Council",
  },
  [32339] = {
    en = "Belt of Primal Majesty",
  },
  [30303] = {
    en = "Pattern: Belt of the Black Eagle",
  },
  [28778] = {
    en = "Terror Pit Girdle",
  },
  [32363] = {
    en = "Naaru-Blessed Life Rod",
  },
  [28794] = {
    en = "Axe of the Gronn Lords",
  },
  [28802] = {
    en = "Bloodmaw Magus-Blade",
  },
  [28810] = {
    en = "Windshear Boots",
  },
  [30862] = {
    en = "Blessed Adamantite Bracers",
  },
  [28826] = {
    en = "Shuriken of Negation",
  },
  [30878] = {
    en = "Glimmering Steel Mantle",
  },
  [30886] = {
    en = "Enchanted Leather Sandals",
  },
  [30894] = {
    en = "Blue Suede Shoes",
  },
  [30902] = {
    en = "Cataclysm's Edge",
  },
  [30910] = {
    en = "Tempest of Chaos",
  },
  [30918] = {
    en = "Hammer of Atonement",
  },
  [29920] = {
    en = "Phoenix-Ring of Rebirth",
  },
  [32483] = {
    en = "The Skull of Gul'dan",
  },
  [30447] = {
    en = "Tome of Fiery Redemption",
  },
  [32515] = {
    en = "Wristguards of Determination",
  },
  [29976] = {
    en = "Worldstorm Gauntlets",
  },
  [29984] = {
    en = "Girdle of Zaetar",
  },
  [29992] = {
    en = "Royal Cloak of the Sunstriders",
  },
  [30008] = {
    en = "Pendant of the Lost Ages",
  },
  [30024] = {
    en = "Mantle of the Elven Kings",
  },
  [30048] = {
    en = "Brighthelm of Justice",
  },
  [30056] = {
    en = "Robe of Hateful Echoes",
  },
  [30064] = {
    en = "Cord of Screaming Terrors",
  },
  [31094] = {
    en = "Gloves of the Forgotten Protector",
  },
  [30080] = {
    en = "Luminescent Rod of the Naaru",
  },
  [30096] = {
    en = "Girdle of the Invulnerable",
  },
  [30104] = {
    en = "Cobra-Lash Boots",
  },
  [30112] = {
    en = "Glorious Gauntlets of Crestfall",
  },
  [30663] = {
    en = "Fathom-Brooch of the Tidewalker",
  },
  [32739] = {
    en = "Plans: Dawnsteel Shoulders",
  },
  [32236] = {
    en = "Rising Tide",
  },
  [32755] = {
    en = "Pattern: Mantle of Nimble Thought",
  },
  [32252] = {
    en = "Nether Shadow Tunic",
  },
  [32260] = {
    en = "Choker of Endless Nightmares",
  },
  [32268] = {
    en = "Myrmidon's Treads",
  },
  [32276] = {
    en = "Flashfire Girdle",
  },
  [30240] = {
    en = "Gloves of the Vanquished Defender",
  },
  [30248] = {
    en = "Pauldrons of the Vanquished Champion",
  },
  [29753] = {
    en = "Chestguard of the Fallen Defender",
  },
  [30280] = {
    en = "Pattern: Belt of Blasting",
  },
  [32332] = {
    en = "Torch of the Damned",
  },
  [32340] = {
    en = "Garments of Temperance",
  },
  [30304] = {
    en = "Pattern: Monsoon Belt",
  },
  [28779] = {
    en = "Girdle of the Endless Pit",
  },
  [28795] = {
    en = "Bladespire Warbands",
  },
  [28803] = {
    en = "Cowl of Nature's Breath",
  },
  [30863] = {
    en = "Deadly Cuffs",
  },
  [30871] = {
    en = "Bracers of Martyrdom",
  },
  [30879] = {
    en = "Don Alejandro's Money Belt",
  },
  [30887] = {
    en = "Golden Links of Restoration",
  },
  [30895] = {
    en = "Angelista's Sash",
  },
  [30903] = {
    en = "Legguards of Endless Rage",
  },
  [30911] = {
    en = "Scepter of Purification",
  },
  [30919] = {
    en = "Valestalker Girdle",
  },
  [29921] = {
    en = "Fire Crest Breastplate",
  },
  [30448] = {
    en = "Talon of Al'ar",
  },
  [32500] = {
    en = "Crystal Spire of Karabor",
  },
  [32516] = {
    en = "Wraps of Purification",
  },
  [29458] = {
    en = "Aegis of the Vindicator",
  },
  [29977] = {
    en = "Star-Soul Breeches",
  },
  [29985] = {
    en = "Void Reaver Greaves",
  },
  [29993] = {
    en = "Twinblade of the Phoenix",
  },
  [30025] = {
    en = "Serpentshrine Shuriken",
  },
  [30049] = {
    en = "Fathomstone",
  },
  [30057] = {
    en = "Bracers of Eradication",
  },
  [30065] = {
    en = "Glowing Breastplate of Truth",
  },
  [31095] = {
    en = "Helm of the Forgotten Protector",
  },
  [30081] = {
    en = "Warboots of Obliteration",
  },
  [30097] = {
    en = "Coral-Barbed Shoulderpads",
  },
  [30105] = {
    en = "Serpent Spine Longbow",
  },
  [34012] = {
    en = "Shroud of the Final Stand",
  },
  [34011] = {
    en = "Illidari Runeshield",
  },
  [33058] = {
    en = "Band of the Vigilant",
  },
  [30664] = {
    en = "Living Root of the Wildheart",
  },
  [33055] = {
    en = "Band of Vile Aggression",
  },
  [33054] = {
    en = "The Seal of Danzalar",
  },
  [32944] = {
    en = "Talon of the Phoenix",
  },
  [32838] = {
    en = "Warglaive of Azzinoth",
  },
  [32237] = {
    en = "The Maelstrom's Fury",
  },
  [32245] = {
    en = "Tide-stomper's Greaves",
  },
  [30720] = {
    en = "Serpent-Coil Braid",
  },
  [32261] = {
    en = "Band of the Abyssal Lord",
  },
  [32269] = {
    en = "Messenger of Fate",
  },
  [32837] = {
    en = "Warglaive of Azzinoth",
  },
  [30241] = {
    en = "Gloves of the Vanquished Hero",
  },
  [30249] = {
    en = "Pauldrons of the Vanquished Defender",
  },
  [32754] = {
    en = "Pattern: Bracers of Nimble Thought",
  },
  [29754] = {
    en = "Chestguard of the Fallen Champion",
  },
  [29762] = {
    en = "Pauldrons of the Fallen Hero",
  },
  [30281] = {
    en = "Pattern: Belt of the Long Road",
  },
  [32333] = {
    en = "Girdle of Stability",
  },
  [32341] = {
    en = "Leggings of Divine Retribution",
  },
  [30305] = {
    en = "Pattern: Boots of Natural Grace",
  },
  [28780] = {
    en = "Soul-Eater's Handwraps",
  },
  [30321] = {
    en = "Plans: Belt of the Guardian",
  },
  [28796] = {
    en = "Malefic Mask of the Shadows",
  },
  [28804] = {
    en = "Collar of Cho'gall",
  },
  [32753] = {
    en = "Pattern: Swiftheal Mantle",
  },
  [30864] = {
    en = "Bracers of the Pathfinder",
  },
  [30872] = {
    en = "Chronicle of Dark Secrets",
  },
  [30880] = {
    en = "Quickstrider Moccasins",
  },
  [30888] = {
    en = "Anetheron's Noose",
  },
  [30896] = {
    en = "Glory of the Defender",
  },
  [30904] = {
    en = "Savior's Grasp",
  },
  [30912] = {
    en = "Leggings of Eternity",
  },
  [32752] = {
    en = "Pattern: Swiftheal Wraps",
  },
  [32751] = {
    en = "Pattern: Living Earth Shoulders",
  },
  [32750] = {
    en = "Pattern: Living Earth Bindings",
  },
  [29922] = {
    en = "Band of Al'ar",
  },
  [32749] = {
    en = "Pattern: Shoulders of Lightning Reflexes",
  },
  [30449] = {
    en = "Void Star Talisman",
  },
  [32501] = {
    en = "Shadowmoon Insignia",
  },
  [32748] = {
    en = "Pattern: Bindings of Lightning Reflexes",
  },
  [29962] = {
    en = "Heartrazor",
  },
  [32525] = {
    en = "Cowl of the Illidari High Lord",
  },
  [32747] = {
    en = "Pattern: Swiftstrike Shoulders",
  },
  [29986] = {
    en = "Cowl of the Grand Engineer",
  },
  [29994] = {
    en = "Thalassian Wildercloak",
  },
  [32746] = {
    en = "Pattern: Swiftstrike Bracers",
  },
  [32745] = {
    en = "Pattern: Shoulderpads of Renewed Life",
  },
  [32738] = {
    en = "Plans: Dawnsteel Bracers",
  },
  [30026] = {
    en = "Bands of the Celestial Archer",
  },
  [32589] = {
    en = "Hellfire-Encased Pendant",
  },
  [32609] = {
    en = "Boots of the Divine Light",
  },
  [30050] = {
    en = "Boots of the Shifting Nightmare",
  },
  [30058] = {
    en = "Mallet of the Tides",
  },
  [30066] = {
    en = "Tempest-Strider Boots",
  },
  [31096] = {
    en = "Helm of the Forgotten Vanquisher",
  },
  [30082] = {
    en = "Talon of Azshara",
  },
  [30090] = {
    en = "World Breaker",
  },
  [30098] = {
    en = "Razor-Scale Battlecloak",
  },
  [30106] = {
    en = "Belt of One-Hundred Deaths",
  },
  [32608] = {
    en = "Pillager's Gauntlets",
  },
  [32606] = {
    en = "Girdle of the Lightbearer",
  },
  [31090] = {
    en = "Chestguard of the Forgotten Vanquisher",
  },
  [32354] = {
    en = "Crown of Empowered Fate",
  },
  [32527] = {
    en = "Ring of Ancient Knowledge",
  },
  [30665] = {
    en = "Earring of Soulful Meditation",
  },
  [32373] = {
    en = "Helm of the Illidari Shatterer",
  },
  [32524] = {
    en = "Shroud of the Highborne",
  },
  [32521] = {
    en = "Faceplate of the Impenetrable",
  },
  [28827] = {
    en = "Gauntlets of the Dragonslayer",
  },
  [32238] = {
    en = "Ring of Calming Waves",
  },
  [32374] = {
    en = "Zhar'doom, Greatstaff of the Devourer",
  },
  [32254] = {
    en = "The Brutalizer",
  },
  [32262] = {
    en = "Syphon of the Nathrezim",
  },
  [32270] = {
    en = "Focused Mana Bindings",
  },
  [32278] = {
    en = "Grips of Silent Justice",
  },
  [30242] = {
    en = "Helm of the Vanquished Champion",
  },
  [30250] = {
    en = "Pauldrons of the Vanquished Hero",
  },
  [32517] = {
    en = "The Wavemender's Mantle",
  },
  [29755] = {
    en = "Chestguard of the Fallen Hero",
  },
  [29763] = {
    en = "Pauldrons of the Fallen Champion",
  },
  [30282] = {
    en = "Pattern: Boots of Blasting",
  },
  [32334] = {
    en = "Vest of Mounting Assault",
  },
  [32342] = {
    en = "Girdle of Mighty Resolve",
  },
  [30306] = {
    en = "Pattern: Boots of Utter Darkness",
  },
  [28781] = {
    en = "Karaborian Talisman",
  },
  [30322] = {
    en = "Plans: Red Belt of Battle",
  },
  [28797] = {
    en = "Brute Cloak of the Ogre-Magi",
  },
  [32325] = {
    en = "Rifle of the Stoic Guardian",
  },
  [32505] = {
    en = "Madness of the Betrayer",
  },
  [30865] = {
    en = "Tracker's Blade",
  },
  [30873] = {
    en = "Stillwater Boots",
  },
  [30881] = {
    en = "Blade of Infamy",
  },
  [30889] = {
    en = "Kaz'rogal's Hardened Heart",
  },
  [30897] = {
    en = "Girdle of Hope",
  },
  [30905] = {
    en = "Midnight Chestguard",
  },
  [30913] = {
    en = "Robes of Rhonin",
  },
  [30307] = {
    en = "Pattern: Boots of the Crimson Hawk",
  },
  [32405] = {
    en = "Verdant Sphere",
  },
  [32377] = {
    en = "Mantle of Darkness",
  },
  [29923] = {
    en = "Talisman of the Sun King",
  },
  [32376] = {
    en = "Forest Prowler's Helm",
  },
  [30450] = {
    en = "Warp-Spring Coil",
  },
  [29947] = {
    en = "Gloves of the Searing Grip",
  },
  [32510] = {
    en = "Softstep Boots of Tracking",
  },
  [32518] = {
    en = "Veil of Turning Leaves",
  },
  [32526] = {
    en = "Band of Devastation",
  },
  [32368] = {
    en = "Tome of the Lightbringer",
  },
  [29987] = {
    en = "Gauntlets of the Sun King",
  },
  [29995] = {
    en = "Leggings of Murderous Intent",
  },
  [32367] = {
    en = "Leggings of Devastation",
  },
  [32366] = {
    en = "Shadowmaster's Boots",
  },
  [32365] = {
    en = "Heartshatter Breastplate",
  },
  [30027] = {
    en = "Boots of Courage Unending",
  },
  [32590] = {
    en = "Nethervoid Cloak",
  },
  [32353] = {
    en = "Gloves of Unfailing Faith",
  },
  [30051] = {
    en = "Idol of the Crescent Goddess",
  },
  [30059] = {
    en = "Choker of Animalistic Fury",
  },
  [30067] = {
    en = "Velvet Boots of the Guardian",
  },
  [30075] = {
    en = "Gnarled Chestpiece of the Ancients",
  },
  [30083] = {
    en = "Ring of Sundered Souls",
  },
  [30091] = {
    en = "True-Aim Stalker Bands",
  },
  [30099] = {
    en = "Frayed Tether of the Drowned",
  },
  [30107] = {
    en = "Vestments of the Sea-Witch",
  },
  [30626] = {
    en = "Sextant of Unstable Currents",
  },
  [32352] = {
    en = "Naturewarden's Treads",
  },
  [32351] = {
    en = "Elunite Empowered Bracers",
  },
  [32350] = {
    en = "Touch of Inspiration",
  },
  [32349] = {
    en = "Translucent Spellthread Necklace",
  },
  [32348] = {
    en = "Soul Cleaver",
  },
  [32347] = {
    en = "Grips of Damnation",
  },
  [32346] = {
    en = "Boneweave Girdle",
  },
  [32345] = {
    en = "Dreadboots of the Legion",
  },
  [32326] = {
    en = "Twisted Blades of Zarak",
  },
  [32239] = {
    en = "Slippers of the Seacaller",
  },
  [32247] = {
    en = "Ring of Captured Storms",
  },
  [32255] = {
    en = "Felstone Bulwark",
  },
  [32263] = {
    en = "Praetorian's Legguards",
  },
  [32271] = {
    en = "Kilt of Immortal Nature",
  },
  [32279] = {
    en = "The Seeker's Wristguards",
  },
  [30243] = {
    en = "Helm of the Vanquished Defender",
  },
  [32295] = {
    en = "Design: Mystic Lionseye",
  },
  [32303] = {
    en = "Design: Inscribed Pyrestone",
  },
  [32327] = {
    en = "Robe of the Shadow Council",
  },
  [29764] = {
    en = "Pauldrons of the Fallen Defender",
  },
  [30283] = {
    en = "Pattern: Boots of the Long Road",
  },
  [32335] = {
    en = "Unstoppable Aggressor's Ring",
  },
  [32343] = {
    en = "Wand of Prismatic Focus",
  },
  [28774] = {
    en = "Glaive of the Pit",
  },
  [28782] = {
    en = "Crystalheart Pulse-Staff",
  },
  [30323] = {
    en = "Plans: Boots of the Protector",
  },
  [32375] = {
    en = "Bulwark of Azzinoth",
  },
  [32324] = {
    en = "Insidious Bands",
  },
  [32289] = {
    en = "Design: Stormy Empyrean Sapphire",
  },
  [28822] = {
    en = "Teeth of Gruul",
  },
  [28830] = {
    en = "Dragonspine Trophy",
  },
  [30882] = {
    en = "Bastion of Light",
  },
  [32285] = {
    en = "Design: Flashing Crimson Spinel",
  },
  [30898] = {
    en = "Shady Dealer's Pantaloons",
  },
  [30906] = {
    en = "Bristleblitz Striker",
  },
  [30914] = {
    en = "Belt of the Crescent Moon",
  },
  [32280] = {
    en = "Gauntlets of Enforcement",
  },
  [32253] = {
    en = "Legionkiller",
  },
  [32471] = {
    en = "Shard of Azzinoth",
  },
  [29924] = {
    en = "Netherbane",
  },
  [28789] = {
    en = "Eye of Magtheridon",
  },
  [30866] = {
    en = "Blood-stained Pauldrons",
  },
  [29948] = {
    en = "Claw of the Phoenix",
  },
  [31103] = {
    en = "Pauldrons of the Forgotten Protector",
  },
  [32519] = {
    en = "Belt of Divine Guidance",
  },
  [29972] = {
    en = "Trousers of the Astromancer",
  },
  [30110] = {
    en = "Coral Band of the Revived",
  },
  [29988] = {
    en = "The Nexus Key",
  },
  [29996] = {
    en = "Rod of the Sun King",
  },
  [31102] = {
    en = "Pauldrons of the Forgotten Vanquisher",
  },
  [31101] = {
    en = "Pauldrons of the Forgotten Conqueror",
  },
  [30020] = {
    en = "Fire-Cord of the Magus",
  },
  [30028] = {
    en = "Seventh Ring of the Tirisfalen",
  },
  [32591] = {
    en = "Choker of Serrated Blades",
  },
  [31097] = {
    en = "Helm of the Forgotten Conqueror",
  },
  [30052] = {
    en = "Ring of Lethality",
  },
  [30060] = {
    en = "Boots of Effortless Striking",
  },
  [30068] = {
    en = "Girdle of the Tidal Call",
  },
  [31098] = {
    en = "Leggings of the Forgotten Conqueror",
  },
  [30084] = {
    en = "Pauldrons of the Argent Sentinel",
  },
  [30092] = {
    en = "Orca-Hide Boots",
  },
  [30100] = {
    en = "Soul-Strider Boots",
  },
  [30108] = {
    en = "Lightfathom Scepter",
  },
  [30627] = {
    en = "Tsunami Talisman",
  },
  [31089] = {
    en = "Chestguard of the Forgotten Conqueror",
  },
  [30619] = {
    en = "Fel Reaver's Piston",
  },
  [30620] = {
    en = "Spyglass of the Hidden Fleet",
  },
  [28824] = {
    en = "Gauntlets of Martial Perfection",
  },
  [28828] = {
    en = "Gronn-Stitched Girdle",
  },
  [30874] = {
    en = "The Unbreakable Will",
  },
  [30870] = {
    en = "Cuffs of Devastation",
  },
  [30869] = {
    en = "Howling Wind Bracers",
  },
  [32232] = {
    en = "Eternium Shell Bracers",
  },
  [32240] = {
    en = "Guise of the Tidal Lurker",
  },
  [32248] = {
    en = "Halberd of Desolation",
  },
  [30308] = {
    en = "Pattern: Hurricane Boots",
  },
}

------------------------------------------------------------

-- export translation
ADDON.ItemTranslation = translation
