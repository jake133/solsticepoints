| # | Timestamp| Function | Player | Change | Description | New Balance | 
|---|---|---|---|---|---|---| 
| 1 | 2023-02-15 19:17:24 | StoreDKP 2023-02-15_Ulduar_Tues | Dayme | 168 | Correction | Balance 215 | 
| 2 | 2023-02-15 19:17:38 | StoreDKP 2023-02-15_Ulduar_Tues | Kattianna | -168 | Correction | Balance 0 | 
| 3 | 2023-02-15 19:52:35 | StoreDKP 2023-02-15_Ulduar_Tues | Aythya | 10 | Attendance | Balance 944 | 
| 4 | 2023-02-15 19:52:35 | StoreDKP 2023-02-15_Ulduar_Tues | Bravend | 10 | Attendance | Balance 266 | 
| 5 | 2023-02-15 19:52:35 | StoreDKP 2023-02-15_Ulduar_Tues | Dayme | 10 | Attendance | Balance 225 | 
| 6 | 2023-02-15 19:52:35 | StoreDKP 2023-02-15_Ulduar_Tues | Derios | 10 | Attendance | Balance 130 | 
| 7 | 2023-02-15 19:52:35 | StoreDKP 2023-02-15_Ulduar_Tues | Dozzy | 10 | Attendance | Balance 157 | 
| 8 | 2023-02-15 19:52:35 | StoreDKP 2023-02-15_Ulduar_Tues | Dvoom | 10 | Attendance | Balance 444 | 
| 9 | 2023-02-15 19:52:35 | StoreDKP 2023-02-15_Ulduar_Tues | Eillowee | 10 | Attendance | Balance 193 | 
| 10 | 2023-02-15 19:52:35 | StoreDKP 2023-02-15_Ulduar_Tues | Gaylestrum | 10 | Attendance | Balance 515 | 
| 11 | 2023-02-15 19:52:35 | StoreDKP 2023-02-15_Ulduar_Tues | Hempoil | 10 | Attendance | Balance 318 | 
| 12 | 2023-02-15 19:52:35 | StoreDKP 2023-02-15_Ulduar_Tues | Ironflurry | 10 | Attendance | Balance 328 | 
| 13 | 2023-02-15 19:52:35 | StoreDKP 2023-02-15_Ulduar_Tues | Jazzmene | 10 | Attendance | Balance 253 | 
| 14 | 2023-02-15 19:52:35 | StoreDKP 2023-02-15_Ulduar_Tues | Jáxshow | 10 | Attendance | Balance 12 | 
| 15 | 2023-02-15 19:52:35 | StoreDKP 2023-02-15_Ulduar_Tues | Lachý | 10 | Attendance | Balance 196 | 
| 16 | 2023-02-15 19:52:35 | StoreDKP 2023-02-15_Ulduar_Tues | Meyhém | 10 | Attendance | Balance 396 | 
| 17 | 2023-02-15 19:52:35 | StoreDKP 2023-02-15_Ulduar_Tues | Montecrié | 10 | Attendance | Balance 10 | 
| 18 | 2023-02-15 19:52:35 | StoreDKP 2023-02-15_Ulduar_Tues | Mykos | 10 | Attendance | Balance 440 | 
| 19 | 2023-02-15 19:52:35 | StoreDKP 2023-02-15_Ulduar_Tues | Phal | 10 | Attendance | Balance 274 | 
| 20 | 2023-02-15 19:52:35 | StoreDKP 2023-02-15_Ulduar_Tues | Ryzus | 10 | Attendance | Balance 1445 | 
| 21 | 2023-02-15 19:52:35 | StoreDKP 2023-02-15_Ulduar_Tues | Slayingfreak | 10 | Attendance | Balance 308 | 
| 22 | 2023-02-15 19:52:35 | StoreDKP 2023-02-15_Ulduar_Tues | Snoweyes | 10 | Attendance | Balance 293 | 
| 23 | 2023-02-15 19:52:35 | StoreDKP 2023-02-15_Ulduar_Tues | Tarati | 10 | Attendance | Balance 10 | 
| 24 | 2023-02-15 19:52:35 | StoreDKP 2023-02-15_Ulduar_Tues | Television | 10 | Attendance | Balance 410 | 
